<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::post('inventories/import-excel', 'Admin\InventoryController@importExcel')->name('ajax_import_excel_inventory');

Route::post('/upload-anonymous', 'Admin\FileController@uploadAnonymous')->name('ajax_upload_anonymous');
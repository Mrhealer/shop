<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

// Auth
Route::group(['prefix' => 'auth', 'namespace' => 'Auth', 'as' => 'auth.'], function() {
	Route::get('/user/login', 'UserController@showLoginForm')->name('show_user_login_form');
	Route::post('/user/login', 'UserController@login')->name('user_login');;
	Route::get('/user/logout', 'UserController@logout')->name('user_logout');
	Route::get('/user/facebook', 'UserController@facebookLogin')->name('facebook_login');
	Route::get('/user/facebook/callback', 'UserController@callbackFacebookLogin')->name('callback_facebook_login');
	Route::get('/user/register','UserController@showRegisterForm')->name('show_user_register_form');
	Route::post('/user/register', 'UserController@registerUser')->name('register_user');
	Route::get('/user/verify-email/{token}', 'UserController@verifyEmail')->name('verify_user_email');
	Route::get('/user/forget-password','UserController@showForgetPasswordForm')->name('show_user_forget_password_form');
	Route::post('/user/forget-password', 'UserController@createResetPasswordToken')->name('create_user_reset_password_token');
	Route::get('/user/reset-password/{token}', 'UserController@showResetPasswordForm')->name('show_user_reset_password_form');
	Route::post('/user/reset-password/{token}', 'UserController@resetPassword')->name('reset_user_password');
	Route::get('/user/profile', 'UserController@profile')->name('user_profile');
	Route::patch('/user/update-profile', 'UserController@updateProfile')->name('update_user_profile');
	Route::get('/user/password', 'UserController@password')->name('user_password');
	Route::patch('/user/change-password', 'UserController@changePassword')->name('change_user_password');

	Route::get('/admin/login', 'AdminController@showLoginForm')->name('show_admin_login_form');
	Route::post('/admin/login', 'AdminController@login')->name('admin_login');
	Route::get('/admin/logout', 'AdminController@logout')->name('admin_logout');
	Route::get('/admin/forget-password','AdminController@showForgetPasswordForm')->name('show_admin_forget_password_form');
	Route::post('/admin/forget-password', 'AdminController@createResetPasswordToken')->name('create_admin_reset_password_token');
	Route::get('/admin/reset-password/{token}', 'AdminController@showResetPasswordForm')->name('show_admin_reset_password_form');
	Route::post('/admin/reset-password/{token}', 'AdminController@resetPassword')->name('reset_admin_password');
});

//Shop
Route::group(['namespace' => 'Shop'], function() {
    Route::get('/', 'HomeController@index')->name('index');
	Route::get('/danh-muc-tin', 'HomeController@allBlog')->name('blogs');
	Route::get('/danh-muc-tin/{blogCategory}', 'HomeController@blogCategory')->name('blog_category');
	Route::get('/tin-tuc/{post}', 'HomeController@post')->name('post');
    Route::get('/danh-muc-san-pham/{category}', 'HomeController@category')->name('category');
    Route::get('/san-pham/{product}', 'HomeController@product')->name('product');
    Route::get('/tim-kiem', 'HomeController@search')->name('search');
    Route::get('/category/{category}/products', 'HomeController@getCategoryProducts')->name('ajax_category_products');
    Route::get('/category/{category}/attributes', 'HomeController@getCategoryAttributes')->name('ajax_category_attributes');
    Route::get('/gio-hang', 'HomeController@cart')->name('cart');
    Route::get('/thanh-toan', 'HomeController@checkout')->name('checkout');
    Route::post('/order', 'HomeController@order')->name('order');
    Route::post('vouchers', 'HomeController@checkVoucher')->name('check_voucher');
    Route::get('xay-dung-cau-hinh', 'HomeController@buildConfig')->name('build_config');
    Route::get('export/config', 'HomeController@exportConfig')->name('export_config');
    Route::get('export-image/config/view', 'HomeController@exportImageConfigView')->name('export_image_config_view');
});

//Shop
Route::group(['prefix' => 'user', 'namespace' => 'Shop',  'as' => 'user.', 'middleware' => ['auth:user']], function() {
	Route::get('/orders', 'HomeController@showUserOrder')->name('order');
	Route::get('/info', 'HomeController@showUserInfo')->name('info');
	Route::get('/warranty', 'HomeController@showWarranty')->name('warranty');
	Route::post('/warranty-check', 'HomeController@checkWarranty')->name('check_warranty');
	Route::post('/warranty-report', 'HomeController@storeWarrantyReport')->name('report_warranty');
	Route::post('/update_info', 'HomeController@updateUserInfo')->name('update.info');
	Route::post('/update_password', 'HomeController@updatePassword')->name('update.password');
    Route::get('/products-purchased', 'HomeController@productsPurchased')->name('purchased');

	Route::get('/tickets', 'TicketController@index')->name('ticket_index');
	Route::get('/tickets/{ticket}', 'TicketController@show')->name('ticket_detail');
	Route::put('/tickets', 'TicketController@store')->name('ticket_create');
	Route::patch('/tickets/{ticket}', 'TicketController@update')->name('ticket_update');
	Route::post('/ticket-reply/{ticket}', 'TicketController@createMessage')->name('ticket_reply');
	Route::post('/ticket-rate-message/{message}', 'TicketController@rateMessage')->name('ticket_rate_message');

});

// For staff/
Route::group(['prefix' => 'staff', 'namespace' => 'Staff',  'as' => 'staff.', 'middleware' => ['auth:admin']], function() {
    Route::get('/', function () {
        return redirect(route('staff.orders_index'));
    })->name('index');
    Route::get('/orders', 'OrderController@index')->name('orders_index');
    Route::patch('orders/{order}/shipping-info', 'OrderController@updateShippingInfo')->name('ajax_update_shipping_info_order');
});


// Admin area
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => ['auth:admin']], function () {

    Route::get('/', function () {
        return redirect(route('admin.admin_profile'));
    })->name('index');
    // Prod Categories
    Route::get('/prod-categories', 'ProdCategoryController@index')->name('prod_categories_index');
    Route::post('prod-categories', 'ProdCategoryController@store')->name('ajax_create_prod_category');
    Route::patch('prod-categories/{category}', 'ProdCategoryController@update')->name('ajax_update_prod_category');
    Route::delete('prod-categories/{category}', 'ProdCategoryController@delete')->name('ajax_delete_prod_category');
    Route::get('prod-categories/search', 'ProdCategoryController@search')->name('ajax_search_prod_category');

    // Brand
    Route::get('/brands', 'BrandController@index')->name('brands_index');
    Route::post('brands', 'BrandController@store')->name('ajax_create_brand');
    Route::patch('brands/{brand}', 'BrandController@update')->name('ajax_update_brand');
    Route::delete('brands/{brand}', 'BrandController@delete')->name('ajax_delete_brand');
    Route::get('brands/search', 'BrandController@search')->name('ajax_search_brand');

    // Products
    Route::get('/products', 'ProductController@index')->name('products_index');
    Route::post('products', 'ProductController@store')->name('ajax_create_product');
    Route::patch('products/{product}', 'ProductController@update')->name('ajax_update_product');
    Route::patch('products/{product}/attributes', 'ProductController@updateAttributes')->name('ajax_update_product_attributes');
    Route::delete('products/{product}', 'ProductController@delete')->name('ajax_delete_product');
    Route::get('products/search', 'ProductController@search')->name('ajax_search_product');
	Route::post('products/import-excel', 'ProductController@importExcel')->name('ajax_import_excel_product');
    Route::get('/products/export', 'ProductController@exportExcel')->name('products_export');

    // Attributes
    Route::get('/attributes', 'AttributeController@index')->name('attributes_index');
    Route::post('attributes', 'AttributeController@store')->name('ajax_create_attribute');
    Route::patch('attributes/{attribute}', 'AttributeController@update')->name('ajax_update_attribute');
    Route::delete('attributes/{attribute}', 'AttributeController@delete')->name('ajax_delete_attribute');
    Route::get('attributes/{attribute}/values', 'AttributeController@getValues')->name('ajax_get_attribute_values');
    Route::post('attributes/{attribute}/values', 'AttributeController@addValue')->name('ajax_add_attribute_value');
    Route::patch('attributes/{attribute}/values/{attributeValue}', 'AttributeController@updateValue')->name('ajax_update_attribute_value');
    Route::delete('attributes-values/{attributeValue}', 'AttributeController@deleteValue')->name('ajax_delete_attribute_value');
    Route::delete('attributes/{attribute}', 'AttributeController@delete')->name('ajax_delete_attribute');
    Route::get('attributes/search', 'AttributeController@search')->name('ajax_search_attribute');

    // Attribute value
    Route::get('attribute-values/search', 'AttributeValueController@search')->name('ajax_search_attribute_value');

    // Order
    Route::get('/orders', 'OrderController@index')->name('orders_index');
    Route::patch('orders/{order}', 'OrderController@update')->name('ajax_update_order');
    Route::patch('orders/{order}/shipping-info', 'OrderController@updateShippingInfo')->name('ajax_update_shipping_info_order');
    Route::patch('orders/{order}/sync', 'OrderController@syncOrderUser')->name('ajax_sync_order');
	Route::get('orders/search', 'OrderController@search')->name('ajax_search_order');
	Route::get('orders/search-all', 'OrderController@searchAll')->name('ajax_search_all_order');
    Route::patch('order-product/{orderProduct}', 'OrderController@updateProduct')->name('ajax_order_update_product');
    Route::post('orders/{order}/exported-inventory', 'OrderController@checkExportedInventory')->name('ajax_order_check_exported_inventory');
    Route::get('orders/{order}/serial-list', 'OrderController@getSerialList')->name('ajax_order_get_serial_list');

    // Vouchers
    Route::get('/vouchers', 'VoucherController@index')->name('vouchers_index');
    Route::post('vouchers', 'VoucherController@store')->name('ajax_create_voucher');
    Route::patch('vouchers/{voucher}', 'VoucherController@update')->name('ajax_update_voucher');
    Route::get('vouchers/search', 'VoucherController@search')->name('ajax_search_voucher');

    // Warehouse
    Route::get('/warehouses', 'WarehouseController@index')->name('warehouse_index');
    Route::post('warehouses', 'WarehouseController@store')->name('ajax_create_warehouse');
    Route::patch('warehouses/{warehouse}', 'WarehouseController@update')->name('ajax_update_warehouse');
    Route::delete('warehouses/{warehouse}', 'WarehouseController@delete')->name('ajax_delete_warehouse');

    // Supply units
    Route::get('/supply-units', 'SupplyUnitController@index')->name('supply_unit_index');
    Route::post('supply-units', 'SupplyUnitController@store')->name('ajax_create_supply_unit');
    Route::patch('supply-units/{unit}', 'SupplyUnitController@update')->name('ajax_update_supply_unit');
    Route::delete('supply-units/{unit}', 'SupplyUnitController@delete')->name('ajax_delete_supply_unit');
    Route::post('supply-units/import-excel', 'SupplyUnitController@importExcel')->name('ajax_import_excel_supply_unit');

    // Custom menus
    Route::get('/menus', 'MenuController@index')->name('menu_index');
    Route::post('/menus', 'MenuController@store')->name('ajax_create_menu');
    Route::patch('menus/{menu}', 'MenuController@update')->name('ajax_update_menu');
    Route::delete('menus/{menu}', 'MenuController@delete')->name('ajax_delete_menu');
    Route::get('menus/{menu}', 'MenuController@detail')->name('ajax_get_menu');

    // Inventories
    Route::get('/inventories', 'InventoryController@index')->name('inventories_index');
    Route::post('inventories', 'InventoryController@store')->name('ajax_create_inventory');
    Route::patch('inventories/{inventory}', 'InventoryController@update')->name('ajax_update_inventory');
    Route::post('inventories/export', 'InventoryController@export')->name('ajax_export_inventory');
    Route::get('inventories/search', 'InventoryController@search')->name('ajax_search_inventory');
	Route::post('inventories/import-excel', 'InventoryController@importExcel')->name('ajax_import_excel_inventory');
    Route::post('inventories/checkEnoughByOrder/{order}', 'InventoryController@checkInventoryIsEnoughByOrder')->name('ajax_check_inventory_is_enough_by_order');
    Route::get('/inventories/export-excel', 'InventoryController@exportExcel')->name('inventories_export_excel');

    // Import serial by excel file
    Route::post('serial/import-excel', 'InventoryController@importExcelSerial')->name('ajax_import_excel_serial');

    // Inventory Warranty
    Route::get('/warranty', 'WarrantyController@index')->name('warranty_index');
    Route::post('warranty', 'WarrantyController@store')->name('ajax_create_warranty');
    Route::patch('warranty/{warranty}', 'WarrantyController@update')->name('ajax_update_warranty');
    Route::get('warranty/search', 'WarrantyController@search')->name('ajax_search_warranty');

	Route::get('/warranty_prod', 'WarrantyController@productList')->name('warranty_prod_index');
	Route::post('/warranty_prod', 'WarrantyController@getProductList')->name('get_warranty_prod_ajax');
//	Route::patch('/warranty_prod/{product}', 'WarrantyController@productUpdate')->name('update_warranty_prod');


    // Revenue
    Route::get('/revenue', 'RevenueController@index')->name('revenue_index');

    // Files
	Route::get('/files', 'FileController@index')->name('files_index');
	Route::post('/files/browser', 'FileController@browser')->name('ajax_browser_files');
	Route::post('/files', 'FileController@upload')->name('ajax_upload_file')->middleware('file_permission');
	Route::post('/files/folder', 'FileController@makeDirectory')->name('ajax_make_directory');
	Route::patch('/files/resize', 'FileController@resize')->name('ajax_resize_files')->middleware('file_permission');
	Route::patch('/files/delete', 'FileController@delete')->name('ajax_delete_files');

	//User
    Route::get('/profile', 'UserController@profile')->name('user_profile');
    Route::get('/users', 'UserController@index')->name('users_index');
	Route::get('/users/export', 'UserController@export')->name('users_export');
	Route::post('users', 'UserController@store')->name('ajax_create_user');
    Route::patch('users/{user}', 'UserController@update')->name('ajax_update_user');
    Route::patch('users/{user}/password', 'UserController@ajaxUpdateUserPassword')->name('ajax_update_user_password');
    Route::delete('users/{user}', 'UserController@delete')->name('ajax_delete_user');
	Route::get('/users/search', 'UserController@search')->name('ajax_search_user');

    // Blog Categories
    Route::get('/blog-categories', 'BlogCategoryController@index')->name('blog_categories_index');
    Route::post('blog-categories', 'BlogCategoryController@store')->name('ajax_create_blog_category');
    Route::patch('blog-categories/{category}', 'BlogCategoryController@update')->name('ajax_update_blog_category');
    Route::delete('blog-categories/{category}', 'BlogCategoryController@delete')->name('ajax_delete_blog_category');
    Route::get('blog-categories/search', 'BlogCategoryController@search')->name('ajax_search_blog_category');

    // Post
    Route::get('/posts', 'PostController@index')->name('posts_index');
    Route::post('posts', 'PostController@store')->name('ajax_create_post');
    Route::patch('posts/{post}', 'PostController@update')->name('ajax_update_post');
    Route::delete('posts/{post}', 'PostController@delete')->name('ajax_delete_post');
    Route::get('posts/search', 'PostController@search')->name('ajax_search_post');

	Route::group(['prefix' => 'tickets', 'as' => 'ticket.'], function($router) {
		$router->get('/', 'TicketController@index')->name('index');
		$router->get('/{ticket}', 'TicketController@show')->name('details');
        $router->post('/', 'TicketController@store')->name('create');
        $router->post('/receive/{ticket}', 'TicketController@receiveTicket')->name('receive');
		$router->patch('/{ticket}', 'TicketController@update')->name('update');
		$router->delete('/{ticket}', 'TicketController@destroy')->name('delete');
		$router->get('/search', 'TicketController@search')->name('search');
		$router->post('reply/{ticket}', 'TicketController@createMessage')->name('reply');

	});

	Route::group(['prefix' => 'ticket-cat', 'as' => 'ticket_cat.'], function($router) {
		$router->get('/', 'TicketCategoryController@index')->name('index');
		$router->post('/', 'TicketCategoryController@store')->name('create');
		$router->patch('/{category}', 'TicketCategoryController@update')->name('update');
		$router->delete('/{category}', 'TicketCategoryController@destroy')->name('delete');
		$router->get('/search', 'TicketCategoryController@search')->name('search');

    });

	Route::get('/profile', 'AdminController@profile')->name('admin_profile');
	Route::patch('/change-password', 'AdminController@changePassword')->name('change_admin_password');
	Route::patch('/update-profile', 'AdminController@updateProfile')->name('update_admin_profile');
    // Only System admin
    Route::group(['middleware' => ['auth:admin','auth.system_admin']], function () {

        //System
        Route::get('/setting', 'SettingController@index')->name('setting_index');
        Route::patch('setting/{setting}', 'SettingController@update')->name('ajax_update_setting');

	    // Role
	    Route::get('/roles', 'RoleController@index')->name('roles_index');
	    Route::post('roles', 'RoleController@store')->name('ajax_create_role');
	    Route::patch('roles/{role}', 'RoleController@update')->name('ajax_update_role');
	    Route::delete('roles/{role}', 'RoleController@delete')->name('ajax_delete_role');

	    // Admin
	    Route::get('/admins', 'AdminController@index')->name('admins_index');
	    Route::post('admins', 'AdminController@store')->name('ajax_create_admin');
	    Route::patch('admins/{admin}', 'AdminController@update')->name('ajax_update_admin');
	    Route::delete('admins/{admin}', 'AdminController@delete')->name('ajax_delete_admin');
    });

});

Route::group(['prefix' => 'cms', 'namespace' => 'Cms', 'as' => 'cms.', 'middleware' => ['auth:admin','auth.system_admin']], function () {
    Route::get('/', function () {
        return redirect(route('cms_index'));
    })->name('index');
    Route::get('/cms_template', 'AdminController@index')->name('cms_index');
    Route::get('/ware_houses', 'WareHousesController@index')->name('ware_houses');
});


Route::get('/upload-anonymous', 'Admin\FileController@showUploadAnonymousForm')->name('upload_anonymous');


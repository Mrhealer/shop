<?php
return [
    'view_detail' => 'Xem chi tiết',
    'checkout_now' => 'Thanh toán ngay',
    'total_count_products' => 'Tổng số sản phẩm',
    'total_money' => 'Tổng tiền',
    'add_to_cart' => 'Thêm vào giỏ hàng',
    'add_to_cart_success' => 'Đã thêm sản phẩm vào giỏ',
    'remove_from_cart' => 'Xóa khỏi giỏ hàng',
    'remove_from_cart_success' => 'Đã xóa sản phẩm khỏi giỏ hàng.',
    'cart_empty' => 'Giỏ hàng trống',
    'something_wrong' => 'Xảy ra lỗi',
    'order_success' => 'Đặt hàng thành công',
	'login_success' => 'Đăng nhập thành công',
	'update_user_info_success' => 'Cập nhật thông tin thành công',

];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Không tìm thấy thông tin đăng nhập hợp lệ.',
    'throttle' => 'Đăng nhập không đúng quá nhiều lần. Vui lòng thử lại sau :seconds giây.',
	'login_success' => 'Đặng nhập thành công, chuyển hướng sau 1s.',
	'password_updated' => 'Đặt lại mật khẩu thành công, chuyển hướng sau 1s.',
	'create_reset_password_success' => 'Hệ thống đã gửi liên kết đặt lại mật khẩu tới email của bạn, vui lòng kiểm tra email.',
	'register_success' => 'Đăng ký thành công, chuyển hướng sau 1s.',
	'change_password_success' => 'Đổi mật khẩu thành công.',
	'update_profile_success' => 'Cập nhật thông tin thành công, tải lại trang sau 1s.',
];

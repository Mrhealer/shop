export const imageStore = {
    state: {
        image: '',
    },
    getters: {
        getImage: state => state.image
    },
    mutations: {
        setImage (state, image) {
            state.image = image;
        }
    }
};
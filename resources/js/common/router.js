import route from "ziggy";
import { Ziggy } from "../ziggy";

export const Router = {
    route: (name, params, absolute) => route(name, params, absolute, Ziggy),
};

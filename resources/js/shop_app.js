/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';
import Vuex from 'vuex';
import VueLodash from 'vue-lodash';
import lodash from 'lodash';
import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated.js';
import route from '../../vendor/tightenco/ziggy/src/js/route';
import { Ziggy } from './ziggy';
import { shopStore } from './stores/shopStore';
import { Helper } from './common/helper';
import { Request } from './common/request';
import { env } from './config/app';
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import VueSweetalert2 from 'vue-sweetalert2';
import VueCarousel from 'vue-carousel';
import ElementUI from "element-ui";
import locale from "element-ui/lib/locale/lang/en";
import VueLazyload from 'vue-lazyload';
import LazyImage from './components/plugins/LazyImage';

Vue.use(ElementUI, { locale });

Vue.use(VueLazyload, {
    preLoad: 1.3,
    error: '/images/no-img.jpg',
    loading: '/images/logo.png',
    attempt: 1
})
Vue.use(VueCarousel);
Vue.use(Vuex);
Vue.use(VueLodash, { lodash: lodash });
Vue.use(VueInternationalization);
Vue.use(VueSweetalert2);
Vue.use(Loading, {
    color: '#272E79',
    height: 50,
    width: 50,
});
Vue.mixin({
    methods: {
        route: (name, params, absolute) => route(name, params, absolute, Ziggy),
    }
});


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
Vue.component('lazy-image', LazyImage);

const files = require.context('./components/shop', true, /\.vue$/i); // eslint-disable-line no-undef
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const i18n = new VueInternationalization({
    locale: document.head.querySelector('meta[name="locale"]').content,
    messages: Locale
});

Vue.prototype.Helper  = Helper;
Vue.prototype.Request  = Request;
Vue.prototype.env  = env;
window._kowcyber = new Vue({
    store: shopStore,
    el: '#app',
    i18n,
});

window.fbAsyncInit = function() {
    FB.init({
        xfbml: true,
        version: 'v5.0'
    });
};
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

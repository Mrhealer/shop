@extends('admin.layouts.app')
@section('content')
    <attribute-manager
        :params="{{ json_encode($params) }}"
        :attributes="{{ json_encode($attributes) }}"
        :attribute-types="{{ json_encode($_attributeTypes) }}"
        :categories="{{ json_encode($_categories) }}"
    ></attribute-manager>
@endsection

@extends('admin.layouts.app')
@section('content')
    <prod-category-manager
        :categories="{{ $categories->toJson() }}"
        :user="{{ $_user->toJson()}}"
        :keyword="{{ json_encode($keyword) }}"
    ></prod-category-manager>

@endsection

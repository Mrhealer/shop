@extends('admin.layouts.app')
@section('content')
    <menu-manager
        :menus="{{ json_encode($menus) }}"
        :keyword="{{ json_encode($keyword) }}"
        :menu-obj-type="{{ json_encode($_menuObjType) }}"
        :menu-obj-type-enums="{{ json_encode($_menuObjTypeEnums) }}"
    ></menu-manager>
@endsection

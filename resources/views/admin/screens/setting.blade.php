@extends('admin.layouts.app')
@section('content')
    <setting-manager
        :setting="{{ json_encode($setting) }}"
        :build-categories="{{ json_encode($buildCategories) }}"
        :user="{{ json_encode($_user) }}"
    ></setting-manager>

@endsection

@extends('admin.layouts.app')
@section('content')

	<ticket-manager
		:tickets="{{ json_encode($tickets) }}"
		:keyword="{{ json_encode($keyword) }}"
		:categories="{{ json_encode($categories) }}"
		:ticket-priority="{{ json_encode($_ticketPriority) }}"
		:ticket-priority-enum="{{ json_encode($_ticketPriorityEnum) }}"
		:ticket-status="{{ json_encode($_ticketStatus) }}"
		:ticket-status-enum="{{ json_encode($_ticketStatusEnum) }}"
	>
	</ticket-manager>

@endsection
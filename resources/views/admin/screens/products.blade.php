@extends('admin.layouts.app')
@section('content')
    <product-manager
            :products="{{ json_encode($products) }}"
            :params="{{ json_encode($params) }}"
            :attributes="{{ json_encode($_attributes) }}"
            :brands="{{ json_encode($_brands) }}"
            :categories="{{ json_encode($_categories) }}"
    ></product-manager>
@endsection

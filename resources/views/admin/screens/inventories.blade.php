@extends('admin.layouts.app')
@section('content')
    <inventory-manager
        :inventories="{{ json_encode($inventories) }}"
        :warehouses="{{ json_encode($_warehouses) }}"
        :supplyunits="{{ json_encode($_supplyunits) }}"
        :exportinventorytypes="{{ json_encode($_exportinventorytypes) }}"
        :inventory-status="{{ json_encode($_inventoryStatus) }}"
        :params="{{ json_encode($params) }}"
    ></inventory-manager>

@endsection

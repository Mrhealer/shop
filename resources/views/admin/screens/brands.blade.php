@extends('admin.layouts.app')
@section('content')
    <brand-manager
        :brands="{{ $brands->toJson() }}"
        :user="{{ $_user->toJson()}}"
        :keyword="{{ json_encode($keyword) }}"
    ></brand-manager>

@endsection

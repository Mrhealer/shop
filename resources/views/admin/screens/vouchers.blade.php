@extends('admin.layouts.app')
@section('content')
    <voucher-manager
        :vouchers="{{ json_encode($vouchers) }}"
        :user="{{ json_encode($_user) }}"
        :keywords="{{ json_encode($keyword) }}"
        :voucher-types="{{ json_encode($_voucherTypes) }}"
        :voucher-type-enum="{{ json_encode($_voucherTypeEnum) }}"
    ></voucher-manager>

@endsection

@extends('admin.layouts.app')
@section('content')
	<warranty-product-list
			:products="{{ $products->toJson() }}"
			:technicians="{{ json_encode($technicians) }}"
			:product-warranty-status="{{ json_encode($productWarrantyStatus) }}"
			:product-warranty-status-enum="{{ json_encode($productWarrantyStatusEnum) }}"
			:inventory-status-enum="{{ json_encode($inventoryStatusEnum) }}"
			:user="{{ json_encode($_user) }}"
			:params="{{ json_encode($params) }}"
	></warranty-product-list>

@endsection

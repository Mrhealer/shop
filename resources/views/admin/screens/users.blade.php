@extends('admin.layouts.app')
@section('content')
   <user-manager
           :user="{{ json_encode($_user) }}"
           :user-roles="{{ json_encode($_userRoles) }}"
           :users="{{ json_encode($users) }}"
   ></user-manager>
@endsection

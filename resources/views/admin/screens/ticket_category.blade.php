@extends('admin.layouts.app')
@section('content')

	<ticket-category-manager
			:categories="{{ json_encode($categories) }}"
			:user="{{ json_encode($_user) }}"
			:keyword="{{ json_encode($keyword) }}"
	>
	</ticket-category-manager>

@endsection
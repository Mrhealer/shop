@extends('admin.layouts.app')
@section('content')
    <div class="col-sm-12" style="display: block">
        <h3 class="row col-sm-12">Đơn hàng chờ</h3>
        <div class="row col-sm-12">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="material-icons">store</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Chờ xử lý</p>
                        <h3 class="card-title">{{ $pendingOrders }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="material-icons">store</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Đang xử lý</p>
                        <h3 class="card-title">{{ $processingOrders }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="material-icons">store</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Đang vận chuyển</p>
                        <h3 class="card-title">{{ $shippingOrders }}</h3>
                    </div>
                </div>
            </div>
        </div>
        <h3 class="row col-sm-12">Đơn hàng thành công</h3>
        <div class="row col-sm-12">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="green">
                        <i class="material-icons">store</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Hôm nay</p>
                        <h3 class="card-title">{{ $todaySuccessOrders }}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="material-icons">date_range</i> Tính từ {{ $today }}
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="green">
                        <i class="material-icons">store</i>
                    </div>
                    <div class="card-content">
                        <p class="category">30 ngày</p>
                        <h3 class="card-title">{{ $monthSuccessOrders }}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="material-icons">date_range</i> Tính từ {{ $month }}
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="green">
                        <i class="material-icons">store</i>
                    </div>
                    <div class="card-content">
                        <p class="category">120 ngày</p>
                        <h3 class="card-title">{{ $quarterSuccessOrders }}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="material-icons">date_range</i> Tính từ {{ $quarter }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <h3 class="row col-sm-12">Doanh thu</h3>
        <div class="row col-sm-12">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="green">
                        <i class="material-icons">store</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Hôm nay</p>
                        <h3 class="card-title">{{ $revenueToday }}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="material-icons">date_range</i> Tính từ {{ $today }}
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="green">
                        <i class="material-icons">store</i>
                    </div>
                    <div class="card-content">
                        <p class="category">30 ngày</p>
                        <h3 class="card-title">{{ $revenueMonth }}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="material-icons">date_range</i> Tính từ {{ $month }}
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="green">
                        <i class="material-icons">store</i>
                    </div>
                    <div class="card-content">
                        <p class="category">120 ngày</p>
                        <h3 class="card-title">{{ $revenueQuarter }}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="material-icons">date_range</i> Tính từ {{ $quarter }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

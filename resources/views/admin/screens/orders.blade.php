@extends('admin.layouts.app')
@section('content')
    <order-manager
        :orders="{{ $orders->toJson() }}"
        :user="{{ $_user->toJson()}}"
        :params="{{ json_encode($params) }}"
        :order-status="{{ json_encode($_orderStatus) }}"
        :shipping-status="{{ json_encode($_shippingStatus) }}"
        :order-status-enum="{{ json_encode($_orderStatusEnum ) }}"
        :order-shipping-status-enum="{{ json_encode($_orderShippingStatusEnum) }}"
        :order-actions="{{ json_encode($_orderActions) }}"
        :payment-method="{{ json_encode($_paymentMethod) }}"
        :payment-status="{{ json_encode($_paymentStatus) }}"
        :received-method="{{ json_encode($_receivedMethod) }}"
        :shippers="{{ json_encode($_shippers) }}"
        :transport-companies="{{ json_encode($_transportCompanies) }}"
    ></order-manager>
@endsection

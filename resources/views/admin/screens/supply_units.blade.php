@extends('admin.layouts.app')
@section('content')
    <supply-unit-manager
        :units="{{ json_encode($units) }}"
        :keyword="{{ json_encode($keyword) }}"
    ></supply-unit-manager>
@endsection

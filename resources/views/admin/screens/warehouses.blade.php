@extends('admin.layouts.app')
@section('content')
    <warehouse-manager
        :warehouses="{{ json_encode($warehouses) }}"
        :keyword="{{ json_encode($keyword) }}"
    ></warehouse-manager>
@endsection

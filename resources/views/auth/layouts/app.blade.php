<!doctype html>
<html lang="vi">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/favicon.png') }}" />
    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png')  }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="{{ asset('css/vendor/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/vendor/material-dashboard.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/auth_app.css') }}" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
    <body>
        @yield('content')
        <script src="{{asset('js/vendor/jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('js/vendor/bootstrap.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('js/vendor/material-kit.min.js')}}" type="text/javascript"></script>
    </body>
</html>

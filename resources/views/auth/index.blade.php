@extends('shop.layouts.app', ['class_name' => 'page-regis'])
@section('header')
    @include('shop.layouts.header')
@endsection
@section('content')
    <login-form
            :guard="{{ json_encode($guard) }}"
            :screen="{{ json_encode($screen) }}"
            :token="{{ json_encode($token ?? '') }}"
    ></login-form>
@endsection

<!DOCTYPE html>
<html>

<head>
    @include("cms.layouts.head")
</head>

<body>
<div id="wrapper" id="app">
    @include("cms.screens.sidebar")

    <div id="page-wrapper" class="gray-bg">
        @include("cms.layouts.nav_bar")
        <div class="wrapper wrapper-content">
            @yield('content')
            @include('cms.layouts.footer')
        </div>

    </div>
</div>
@include("cms.layouts.scripts")
</body>
</html>

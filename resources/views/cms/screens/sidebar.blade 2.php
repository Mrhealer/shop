<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                        <span>
                            <img alt="image" width="170" height="70"
                                 src="https://kowcomputer.com/storage/files/zone1/2hnmmcyWm05bQPMVQg3cwb0a1HqbKp1xT6xMlvlU.png"/>
                        </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                        <span class="block m-t-xs"> <strong class="font-bold">CMS Kowcomputer</strong>
                        </span>
                          <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="https://kowcomputer.com/">Liên hệ</a></li>
                            <li><a href="https://kowcomputer.com/">Hướng dẫn sử dụng</a></li>

                          </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="active">
                <a href="{{URL::to('cms/cms_template')}}"><i class="fa fa-tachometer"></i> <span class="nav-label">Tổng Quan</span> </a>
            </li>
            <li>
                <a href="{{URL::to('cms/ware_houses')}}"><i class="fa fa-building"></i> <span class="nav-label">Kho Hàng</span><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{URL::to('cms/ware_houses')}}">Tất Cả Kho</a></li>
                    <li><a href="page.html">Tất Cả Sản Phẩm</a></li>
                    <li><a href="add_page.html">Thêm Mới Kho</a></li>
                    <li><a href="page.html">Thêm Mới Sản Phẩm</a></li>
                </ul>
            </li>

            <li>
                <a href="page.html"><i class="fa fa-cart-arrow-down"></i> <span class="nav-label">Đơn Hàng</span><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="page.html">Tất Cả Đơn Hàng</a></li>
                    <li><a href="add_page.html">Thêm Mới Sản Phẩm</a></li>
                </ul>
            </li>


            <li>
                <a href="page.html"><i class="fa fa-bookmark-o"></i> <span
                        class="nav-label">Danh Mục Sản Phẩm</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="page.html">Tất Cả Danh Mục</a></li>
                    <li><a href="page.html">Tất Cả Thương Hiệu</a></li>
                    <li><a href="add_page.html">Thêm Mới DM SP</a></li>
                    <li><a href="page.html">Tất Cả Sản Phẩm</a></li>
                    <li><a href="add_page.html">Thêm Mới Sản Phẩm</a></li>
                </ul>
            </li>

            <li>
                <a href="page.html"><i class="fa fa-twitch"></i> <span class="nav-label">Bảo Hành</span><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="page.html">Tất Cả Bảo Hành</a></li>
                    <li><a href="page.html">Bảo Hành Từ User</a></li>
                    <li><a href="add_page.html">Bảo Hành Từ Hãng</a></li>
                </ul>
            </li>

            <li>
                <a href="media.html"><i class="fa fa-picture-o"></i> <span class="nav-label">Danh Mục Tin Tức</span></a>
            </li>


            <li>
                <a href="media.html"><i class="fa fa-picture-o"></i> <span class="nav-label">Mã Khuyến Mại</span></a>
            </li>

            <li>
                <a href="page.html"><i class="fa fa-twitch"></i> <span class="nav-label">Người Dùng</span><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="page.html">Tất Cả Người Dùng</a></li>
                    <li><a href="page.html">Nhân Viên</a></li>
                    <li><a href="add_page.html">Phân Quyền</a></li>
                </ul>
            </li>

            <li>
                <a href="media.html"><i class="fa fa-picture-o"></i> <span class="nav-label">Thư viện</span></a>
            </li>
            <li>
                <a href="post.html"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Bài viết</span><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="post.html">Tất cả bài viết</a></li>
                    <li><a href="add_post.html">Thêm bài viết</a></li>
                    <li><a href="category.html">Danh mục</a></li>

                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Cấu hình</span><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="information.html">Thông tin</a></li>
                    <li><a href="account.html">Tài khoản</a></li>
                    <li><a href="setting.html">Cài đặt hiển thị</a></li>

                </ul>
            </li>
        </ul>

    </div>
</nav>

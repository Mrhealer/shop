@extends('cms.layouts.home')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-2 m-b-xs">
                            <select class="input-sm form-control input-s-sm inline">
                                <option value="0">Chọn tác vụ</option>
                                <option value="1">Ẩn bài</option>
                                <option value="1">Bỏ thùng rác</option>

                            </select>

                        </div>
                        <div class="col-sm-1 m-b-xs">
                            <button class="btn btn-primary-apdung " type="button"><i class="fa fa-check"></i>&nbsp;Áp dụng</button>

                        </div>

                        <div class="col-sm-3">
                            <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
                                        <button type="button" class="btn btn-sm btn-primary"> Tìm kiếm</button> </span></div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>

                                <th>Tên Kho</th>
                                <th>Tác giả</th>
                                <th>Ngày cập nhật</th>
                                <th>Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><a href="add_post.html"> <span class="tag-post">Sản phẩm dưỡng da chất lượng hàng đầu Thế giới có gì?</span></a></td>
                                <td><a href=""> <span class="tag-post-tacgia">admin</span></a></td>
                                <td>Lần cuối cập nhật <br>01/01/2019</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td><a href="add_post.html"> <span class="tag-post">Sản phẩm dưỡng da chất lượng hàng đầu Thế giới có gì?</span></a></td>
                                <td><a href="#"> <span class="tag-post-tacgia">admin</span></a></td>
                                <td>Lần cuối cập nhật <br>01/01/2019</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td><a href="add_post.html"> <span class="tag-post">Sản phẩm dưỡng da chất lượng hàng đầu Thế giới có gì?</span></a></td>
                                <td><a href="#"> <span class="tag-post-tacgia">admin</span></a></td>
                                <td>Lần cuối cập nhật <br>01/01/2019</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td><a href="add_post.html"> <span class="tag-post">Sản phẩm dưỡng da chất lượng hàng đầu Thế giới có gì?</span></a></td>
                                <td><a href="#"> <span class="tag-post-tacgia">admin</span></a></td>
                                <td>Lần cuối cập nhật <br>01/01/2019</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td><a href="add_post.html"> <span class="tag-post">Sản phẩm dưỡng da chất lượng hàng đầu Thế giới có gì?</span></a></td>
                                <td><a href="#"> <span class="tag-post-tacgia">admin</span></a></td>
                                <td>Lần cuối cập nhật <br>01/01/2019</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td><a href="add_post.html"> <span class="tag-post">Sản phẩm dưỡng da chất lượng hàng đầu Thế giới có gì?</span></a></td>
                                <td><a href="#"> <span class="tag-post-tacgia">admin</span></a></td>
                                <td>Lần cuối cập nhật <br>01/01/2019</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td><a href="add_post.html"> <span class="tag-post">Sản phẩm dưỡng da chất lượng hàng đầu Thế giới có gì?</span></a></td>
                                <td><a href="#"> <span class="tag-post-tacgia">admin</span></a></td>
                                <td>Lần cuối cập nhật <br>01/01/2019</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>
@stop

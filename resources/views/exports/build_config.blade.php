<table>
    <thead>
    <tr>
        <td colspan="6">BÁO GIÁ XÂY DỰNG CẤU HÌNH - KOWCOMPUTER</td>
    </tr>
    <tr>
        <th>STT</th>
        <th>SKU</th>
        <th>Tên sản phẩm</th>
        <th>Số lượng</th>
        <th>Đơn giá</th>
        <th>Thành tiền</th>
    </tr>
    </thead>
    <tbody>
    @php
        $totalMoney = 0;
    @endphp
    @foreach($products as $index => $item)
        @php
            $totalMoney += $item['price'] * $item['quantity'];
        @endphp
        <tr>
            <td>{{ $index+1 }}</td>
            <td>{{ $item['sku'] }}</td>
            <td>{{ $item['name'] }}</td>
            <td>{{ $item['quantity'] }}</td>
            <td>{{ $item['price'] }}</td>
            <td>{{ $item['price'] * $item['quantity'] }}</td>
        </tr>

    @endforeach
    <tr>
        <td colspan="3"></td>
        <td colspan="2">Giảm giá</td>
        <td colspan="1">0</td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td colspan="2">Phí vận chuyển</td>
        <td colspan="1">0</td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td colspan="2">Tổng tiền</td>
        <td colspan="1">{{ $totalMoney }}</td>
    </tr>
    <tr>
        <td colspan="3">Đơn vị tính: VNĐ</td>
    </tr>
    <tr>
        <td colspan="3">Lưu ý: Đã bao gồm 10% thuế VAT</td>
    </tr>
    <tr>
        <td colspan="6">Báo giá có hiệu lực trong một ngày, mọi chi tiết xin liên hệ KOW Computer để biết thêm chi tiết.</td>
    </tr>
    <tr>
        <td colspan="6">Website: https://kowcomputer.com</td>
    </tr>
    </tbody>
</table>

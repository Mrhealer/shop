@extends('errors.layouts.app')
@section('title') Not found @endsection
@section('content')
    <div class="bubble"></div>
    <div class="bubble"></div>
    <div class="bubble"></div>
    <div class="bubble"></div>
    <div class="bubble"></div>
    <div class="main">
    <h1>404</h1>
    <p>You cannot access this content, please check the link.</p>
    <a href="{{ url('') }}">Homepage</a>
    </div>
@endsection




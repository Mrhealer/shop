@extends('notifications.layouts.app')
@section('title')Verification @endsection
@section('content')
    <div class="bubble"></div>
    <div class="bubble"></div>
    <div class="bubble"></div>
    <div class="bubble"></div>
    <div class="bubble"></div>
    <div class="main">
        <h1>SORRY</h1>
        <p>Your email is not verified. Please check your email to verify it.</p>
        <a href="{{ route('login') }}">Login</a>

    </div>
@endsection




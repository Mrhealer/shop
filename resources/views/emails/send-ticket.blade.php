
@component('mail::message')
# Kính chào  {{ $ticket->user->name }}

{{ $message }}

@component('mail::button', ['url' => route('user.ticket_detail', $ticket)])
Chi tiết
@endcomponent

Nếu có bất cứ vấn đề gì cần trợ giúp, quý khách vui lòng gởi yêu cầu hỗ trợ bằng cách truy cập vào [{{route('user.ticket_index')}}]({{route('user.ticket_index')}}).

Thông tin các kênh hỗ trợ vui lòng tham khảo tại  [{{ route('index') }}]({{ route('index') }}).

Chân thành cảm ơn và kính chào.
<br>
{{-- Footer --}}
@component('mail::footer')
	Copyright © Công Ty Cổ Phần Đầu Tư Và Phát Triển Công Nghệ Kow Technology.

	Điện thoại: 0987653333 | GPĐKKD số 0108753143 do Sở KHĐT TP Hà Nội cấp
@endcomponent

@endcomponent

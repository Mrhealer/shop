<div style="
    width: 100%;
    height: 100%;
    background: #CCCCCC;
    margin:0;
    padding: 100px 0 0 0;
    align-items: center;
    font-family: Arial, serif;
">
    <div style="
        width: 500px;
        background: rgba(255, 255, 255, 1);
        box-shadow: 0 5px 20px -5px rgba(0, 0, 0, 0.1);
        margin: 0 auto;
        padding: 10px 30px;
        border-radius: 10px;
    ">
        <h3 style="
            color: green;
            font-family: Arial, serif;
            text-align: left;
        ">Cám ơn bạn đã đặt hàng</h3>
{{--        <h2 style="--}}
{{--            color: red;--}}
{{--            font-family: Arial, serif;--}}
{{--            text-align: center;--}}
{{--        ">#{{ $order->code }}</h2>--}}
        <h3 style="
            font-family: Arial, serif;
            text-align: left;
        ">Ghi Nhận Thông Tin:</h3>
        <p style="
            font-family: Arial, serif;
            text-align: left;
">
            Name: {{ $order->name }}
        </p>
        <p style="
            font-family: Arial, serif;
            text-align: left;
">
            Phone: {{ $order->phone }}
        </p>
        <p style="
            font-family: Arial, serif;
            text-align: left;
">
            Email: {{ $order->email }}
        </p>
        <p style="
            font-family: Arial, serif;
            text-align: left;
">
            Address: {{ $order->address }}, {{ $order->district_name }}, {{ $order->province_name }}
        </p>
        @if(!empty($order->note))
            <p style="
            font-family: Arial, serif;
            text-align: left;
">
                Note: {{ $order->note }}
            </p>
        @endif
        <h3 style="
            font-family: Arial, serif;
            text-align: left;
            margin-top: 50px;
        ">Chi Tiết các sản phẩm:</h3>
        <table>
            @foreach($order->orderProducts as $product)
                <tr style="vertical-align: middle;" >
                    <td>
                        <img width="100" height="auto" src="{{ asset($product->image) }}" alt="{{ $product->name }}" />
                    </td>
                    <td>
                        <p>{{ $product->name }}</p>
                        <p>{{ $product->quantity }}  x  {{ number_format($product->price) }} VND </p>
                    </td>
                </tr>
            @endforeach
        </table>
        <h3 style="
            font-family: Arial, serif;
            text-align: left;
            margin-top: 50px;
        ">Thông tin thanh toán:</h3>

        <h3 style="font-family: Arial, serif; font-weight: bold; color: red;">Tiền phải thanh toán: {{ number_format($order->pay_money) }} VND</h3>
        <hr/>
{{--        <p style="--}}
{{--            font-family: Arial, serif;--}}
{{--            text-align: left;--}}
{{--        ">--}}
{{--            Pre-money: {{ $order->pre_money }} VND--}}
{{--        </p>--}}
{{--        <p style="--}}
{{--            font-family: Arial, serif;--}}
{{--            text-align: left;--}}
{{--        ">--}}
{{--            Tiền giảm giá: {{ $order->discount_money }} VND--}}
{{--        </p>--}}
{{--        @if(!empty($order->voucher_code))--}}
{{--            <p style="--}}
{{--            font-family: Arial, serif;--}}
{{--            text-align: left;--}}
{{--        ">--}}
{{--                Voucher: {{ strtoupper($order->voucher_code) }}--}}
{{--            </p>--}}
{{--        @endif--}}

    </div>
</div>

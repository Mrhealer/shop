<div class="s-cat">
    <div class="ss-cat d-lg-block d-none d-md-none d-sm-none d-xs-none">
        <ul class="ul-cat" style="overflow: auto">
            @foreach($_menusTree as $menuLevel1)
                <li>
                    @if ($menuLevel1->object_type === \App\Enums\CustomMenuObjType::PRODUCT_CATEGORY)
                        @php
                            $url = route('category', ['category' => $menuLevel1->prodCategory->slug]);
                            $icon = $menuLevel1->prodCategory->icon;
                        @endphp
                    @else
                        @php
                            $url = $menuLevel1->url;
                            $icon = '/images/link.png';
                        @endphp
                    @endif

                    <a href="{{ $url }}">
                        <img src="{{ $icon }}" alt="{{ $menuLevel1->name }}">
                        <p>{{ $menuLevel1->name }}</p>
                    </a>

                    <div class="sub-menu-container">
                        <div class="row">
                            @foreach($menuLevel1->children as $menuLevel2)
                                <div class="col-sm-3">
                                    <div class="sub-item">
                                        <div>
                                            <i class="fa fa-bookmark" aria-hidden="true"></i>
                                            @if ($menuLevel2->object_type === \App\Enums\CustomMenuObjType::PRODUCT_CATEGORY)
                                                @php
                                                    $url = route('category', ['category' => $menuLevel2->prodCategory->slug]);
                                                    $icon = $menuLevel2->prodCategory->icon;
                                                @endphp
                                            @else
                                                @php
                                                    $url = $menuLevel2->url;
                                                    $icon = '/images/link.png';
                                                @endphp
                                            @endif

                                            <a href="{{ $url }}" class="sub-tit">
                                                &nbsp{{ $menuLevel2->name }}
                                            </a>

                                            <br/>

                                            @foreach($menuLevel2->children as $menuLevel3)
                                                @if ($menuLevel3->object_type === \App\Enums\CustomMenuObjType::PRODUCT_CATEGORY)
                                                    @php
                                                        $url = route('category', ['category' => $menuLevel3->prodCategory->slug]);
                                                        $icon = $menuLevel3->prodCategory->icon;
                                                    @endphp
                                                @else
                                                    @php
                                                        $url = $menuLevel3->url;
                                                        $icon = '/images/link.png';
                                                    @endphp
                                                @endif

                                                <a href="{{ $url }}"
                                                   class="sub-name"> {{ $menuLevel3->name }}</a>
                                                <br />
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </li>
            @endforeach




{{--            @foreach($_prodCategoriesTree as $category)--}}
{{--                <li>--}}
{{--                    <a href="{{ route('category', ['category' => $category->slug ?? $category->id]) }}">--}}
{{--	                    <img src="{{ $category->icon }}" alt="{{ $category->name }}">--}}
{{--                        <p>{{ $category->name }}</p>--}}
{{--                    </a>--}}
{{--                    @if(!$category->children->isEmpty() && sizeof($category->children) > 0)--}}
{{--                        <div class="sub-menu-container">--}}
{{--                            <div class="row">--}}
{{--                                @foreach($category->children as $subCategory)--}}
{{--                                    <div class="col-sm-4">--}}
{{--                                        <div class="sub-item">--}}
{{--                                            <div>--}}
{{--                                                <i class="fa fa-bookmark" aria-hidden="true"></i><a href="{{ route('category', ['category' => $subCategory->slug]) }}"--}}
{{--                                                   class="sub-tit">&nbsp--}}
{{--                                                    {{ $subCategory->name }}--}}
{{--                                                  </a>--}}
{{--                                                <br />--}}
{{--                                                @foreach($subCategory->children as $subSubCategory)--}}
{{--                                                    <a href="{{ route('category', ['category' => $subSubCategory->slug]) }}"--}}
{{--                                                       class="sub-name"> {{ $subSubCategory->name }}</a>--}}
{{--                                                       <br />--}}
{{--                                                @endforeach--}}

{{--                                                @foreach($subCategory->brands as $brand)--}}
{{--                                                    <a href="{{ route('category', ['category' => $subCategory->slug, 'brands[]' => $brand->id]) }}"--}}
{{--                                                       class="sub-name"> {{ $brand->name }}</a>--}}
{{--                                                       <br />--}}
{{--                                                @endforeach--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                   </div>--}}
{{--                                @endforeach--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    @elseif(!$category->brands->isEmpty())--}}
{{--                        <div class="sub-menu-container">--}}
{{--						     <div class="row">--}}
{{--								<div class="col-sm-4">--}}
{{--									<div class="sub-item">--}}
{{--										<div>--}}
{{--											<i class="fa fa-flag" aria-hidden="true"></i><a href="{{ route('category', ['category' => $category->slug]) }}"--}}
{{--											   class="sub-tit">&nbsp--}}
{{--											Thương hiệu--}}
{{--											</a>--}}
{{--											<div>--}}
{{--												@foreach($category->brands as $brand)--}}
{{--													<a href="{{ route('category', ['category' => $category->slug, 'brands[]' => $brand->id]) }}"--}}
{{--													   class="sub-name"> {{ $brand->name }}</a>--}}
{{--													   <br />--}}
{{--												@endforeach--}}
{{--											</div>--}}
{{--										</div>--}}
{{--									</div>--}}
{{--								</div>--}}
{{--							 </div>--}}
{{--                    </div>--}}
{{--                    @endif--}}
{{--                </li>--}}
{{--            @endforeach--}}
        </ul>
    </div>
    <div class="ss-best-seller d-lg-block d-none d-md-none d-sm-none d-xs-none">
        <div  class="b-title">
            <p><i class="fa fa-paper-plane-o" aria-hidden="true"></i>BÁN CHẠY NHẤT</p>
        </div>
        <ul>
            @foreach($_bestSellerProducts as $product)
                <short-view-product :product="{{ $product }}"></short-view-product>
            @endforeach
        </ul>
    </div>
</div>

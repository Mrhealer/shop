@if ($paginator->hasPages())
    <!-- Pagination -->
    <div class="page-pagination">
        <ul>
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled">
                    <button type="button" class="v-pagination">
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                    </button>
                </li>
            @else
                <li>
                    <a href="{{ $paginator->previousPageUrl() }}">
                        <button type="button" class="v-pagination">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </button>
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="v-pagination  active">
                                <button type="button" class="v-pagination active">
                                    {{ $page }}
                                </button>
                            </li>
                        @elseif (($page >= $paginator->currentPage() -3 && $page <= $paginator->currentPage() + 3))
                            <li><a href="{{ $url }}">
                                    <button type="button" class="v-pagination">
                                        {{ $page }}
                                    </button>
                                </a>
                            </li>
                        @endif
                        @if (!$paginator->onFirstPage() && $page == 1 && $paginator->currentPage() > 4)
                            <li class="disabled">
                                <a href="{{ $paginator->previousPageUrl() }}" type="button" class="v-pagination">
                                    <button type="button" class="v-pagination"><span><i class="fa fa-ellipsis-h"></i></span></button>
                                </a>
                            </li>
                        @endif
                        @if ($page == $paginator->lastPage() - 1 && $paginator->currentPage() < $paginator->lastPage() -3)
                            <li class="disabled">
                                <a href="{{ $paginator->nextPageUrl() }}" type="button" class="v-pagination">
                                    <button type="button" class="v-pagination"><span><i class="fa fa-ellipsis-h"></i></span></button>
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li>
                    <a href="{{ $paginator->nextPageUrl() }}" type="button" class="v-pagination">
                        <button type="button" class="v-pagination">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </button>
                    </a>
                </li>
            @else
                <li class="disabled">
                    <button type="button" class="v-pagination">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </button>
                </li>
            @endif
        </ul>
    </div>
    <!-- Pagination -->
@endif

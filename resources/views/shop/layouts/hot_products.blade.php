<div class="ss-count-down">
	<div class="count-wrap-tab">
		<div class="title">
			<p><i class="fa fa-bolt" aria-hidden="true"></i> SẢN PHẨM HOT</p>
		</div>
	</div>
	<div class="ss-product">
		<div class="product-content">
			<div class="product-carousel cms-carousel owl-carousel owl-theme" data-margin="10" data-loop="true"
			     data-nav="true" data-dots="false" data-autoplay="false" data-large-items="4" data-medium-items="4"
			     data-small-items="3" data-xsmall-items="2">
				@foreach($_hotProducts as $product)
					<product-item :product="{{ json_encode($product) }}"></product-item>
				@endforeach
			</div>
		</div>
	</div>
</div>

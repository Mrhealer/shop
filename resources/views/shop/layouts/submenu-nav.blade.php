<div class="nav_div_total">
    <nav class="navbar navbar-expand-sm">
        <!-- Links -->
        <div class="nav-item-top" id="nav-item-top">
            <p>
                <i class="fa fa-bars" style=""></i>DANH MỤC SẢN PHẨM
            </p>

        </div>
        <ul class="navbar-nav">
            <li class="nav-item-sub">
                <a class="nav-link" href="https://kowcomputer.com/"><i class="fa fa-cart-plus"></i>TỔNG HỢP KHUYẾN MÃI
                    T6</a>
            </li>
            <li class="nav-item-sub">
                <a class="nav-link" href="https://kowcomputer.com/tin-tuc/thanh-toan-1"><i
                        class="fa fa-credit-card"></i>HƯỚNG DẪN THANH TOÁN</a>
            </li>
            <li class="nav-item-sub">
                <a class="nav-link" href="https://kowcomputer.com/tin-tuc/chinh-sach-kinh-doanh-4"><i
                        class="fa fa-credit-card"></i>HƯỚNG DẪN TRẢ GÓP</a>
            </li>

            <li class="nav-item-sub">
                <a class="nav-link" href="https://kowcomputer.com/tin-tuc/chinh-sach-bao-hanh-3"><i
                        class="fa fa-medkit"></i>CHÍNH SÁCH BẢO HÀNH</a>
            </li>
            <li class="nav-item-sub">
                <a class="nav-link" href="https://kowcomputer.com/tin-tuc/van-chuyen-lap-dat-2"><i
                        class="fa fa-ambulance"></i>CHÍNH SÁCH VẬN CHUYỂN</a>
            </li>
        </ul>

    </nav>
</div>

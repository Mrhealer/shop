<!-- ---------- Brand ---------- -->
<div class="f-brand">
	<div class="container">
		<div class="owl-slider">
			<div class="carousel03 owl-carousel owl-theme">
				@foreach($_allBrand as $brand)
					<div class="item">
						<a href="{{ route('search', ['brands[]' => $brand->id ]) }}" target="_blank">
							<img src="{{ $brand->image }}" alt="{{ $brand->name }}">
						</a>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>

<!-- ---------- Brand ---------- -->

<!-- ------------Scroll-to-top------------- -->
<div class="f-btn-top-phone">
	{{--    <a href="#" class="message">--}}
	{{--        <img src="{{ asset('pv_tmp/images/mess.png')  }}" alt="">--}}
	{{--    </a>--}}
	<a href="#" class="to-top">
		<img src="{{ asset('pv_tmp/images/scroll-to-top.png') }}" alt="">
	</a>
</div>
{{--<!-- ------------Scroll-to-top------------- -->--}}
{{--<!-- Load Facebook SDK for JavaScript -->--}}
{{--<div id="fb-root"></div>--}}
{{--<!-- Your customer chat code -->--}}
{{--<div class="fb-customerchat"--}}
{{--     attribution=setup_tool--}}
{{--     page_id="103313324646276"--}}
{{--     theme_color="#CC0000"--}}
{{--     logged_in_greeting="Chào mừng bạn đến với KOW Computer."--}}
{{--     logged_out_greeting="Chào mừng bạn đến với KOW Computer.">--}}
{{--</div>--}}
{{--<!-- Facebook Pixel Code -->--}}
{{--<script>--}}
{{--    !function(f,b,e,v,n,t,s)--}}
{{--    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?--}}
{{--        n.callMethod.apply(n,arguments):n.queue.push(arguments)};--}}
{{--        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';--}}
{{--        n.queue=[];t=b.createElement(e);t.async=!0;--}}
{{--        t.src=v;s=b.getElementsByTagName(e)[0];--}}
{{--        s.parentNode.insertBefore(t,s)}(window, document,'script',--}}
{{--        'https://connect.facebook.net/en_US/fbevents.js');--}}
{{--    fbq('init', '250864029570595');--}}
{{--    fbq('track', 'PageView');--}}
{{--</script>--}}
{{--<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=250864029570595&ev=PageView&noscript=1"/></noscript>--}}
{{--<!-- End Facebook P<!-- Facebook Pixel Code -->--}}
{{--<script>--}}
{{--  !function(f,b,e,v,n,t,s)--}}
{{--  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?--}}
{{--  n.callMethod.apply(n,arguments):n.queue.push(arguments)};--}}
{{--  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';--}}
{{--  n.queue=[];t=b.createElement(e);t.async=!0;--}}
{{--  t.src=v;s=b.getElementsByTagName(e)[0];--}}
{{--  s.parentNode.insertBefore(t,s)}(window, document,'script',--}}
{{--  'https://connect.facebook.net/en_US/fbevents.js');--}}
{{--  fbq('init', '250864029570595');--}}
{{--  fbq('track', 'PageView');--}}
{{--</script>--}}
{{--<noscript><img height="1" width="1" style="display:none"--}}
{{--  src="https://www.facebook.com/tr?id=250864029570595&ev=PageView&noscript=1"--}}
{{--/></noscript>--}}
<footer style="background: #313131;">
	<div class="footer-middle">

		<div class="container">
			<div class="f-midder">
				<div class="f-midder-item">
					<h5>KOW COMPUTER</h5>
					<span style="color: #ffffff">Pc Gaming và working station số 1 thị trường</span>
					<ul>
						<li>
							<span><i class="fas fa-tools"></i></span>
							<a href="{{ route('build_config') }}">Xây dựng cấu hình</a>
						</li>
						<li>
							<span><i class="far fa-newspaper"></i></span>
							<a href="{{ route('blogs') }}">Tin tức</a>
						</li>
						<li>
							<span><i class="fas fa-sign-in-alt"></i></span>
							<a href="{{ route('auth.user_login') }}">Đăng nhập</a>
						</li>
						<li>
							<span><i class="fas fa-user-plus"></i></span>
							<a href="{{ route('auth.register_user') }}">Đăng ký</a>
						</li>
					</ul>

				</div>
				<div class="f-midder-item">
					<h5>LIÊN HỆ VỚI CHÚNG TÔI</h5>
					<ul>
						<li>
							<span><i class="fas fa-map-marker-alt"></i></span>
							<span>Địa chỉ: {{$_shopSettings->address}}</span>
						</li>
						<li>
							<span><i class="fas fa-phone-square-alt"></i></span>
							<span>SĐT: {{$_shopSettings->phone}}</span>
						</li>
						<li>
							<span><i class="fas fa-envelope"></i></span>
							<a href="mailto:{{$_shopSettings->email}}">Email: {{$_shopSettings->email}}</a>
						</li>
						<li>
							<span><i class="fab fa-facebook-square"></i></span>
							<a href="https://www.facebook.com/kowcomputer/">Facebook: KOW Computer</a>
						</li>
					</ul>
					<h5>Hình thức thanh toán</h5>
					<a href="#"><img
								src="https://kowcomputer.com/storage/files/anonymous/5MTLzkuCsIR70xeRd7CTbIiX2Le8TT3x9DCRHrbs.png"></a>
					<a href="#"><img
								src="https://kowcomputer.com/storage/files/anonymous/Df1TGIjDYi2mPWKlzPGyIViWuMIibETgbaFRe7H4.png"></a>
					<a href="#"><img
								src="https://kowcomputer.com/storage/files/anonymous/i128lSB9IP7WZLBuVYm0rvSNWcTRD9bNoTFqh2Ay.png"></a>

				</div>
				<div class="f-midder-item">
					<h5>QUY ĐỊNH CHÍNH SÁCH</h5>
					<ul>
						<li>
							<span><i class="fas fa-angle-right"></i></span>
							<a href="https://kowcomputer.com/tin-tuc/van-chuyen-lap-dat-2">Vận chuyển - Lắp đặt</a>
						</li>
						<li>
							<span> <i class="fas fa-angle-right"></i></span>
							<a href="https://kowcomputer.com/tin-tuc/thanh-toan-1">Hướng dẫn thanh toán</a>
						</li>
						<li>
							<span> <i class="fas fa-angle-right"></i></span>
							<a href="https://kowcomputer.com/tin-tuc/chinh-sach-bao-hanh-3">Chính sách bảo hành</a>
						</li>
						<li>
							<span> <i class="fas fa-angle-right"></i></span>
							<a href="https://kowcomputer.com/tin-tuc/chinh-sach-kinh-doanh-4">Chính sách trả góp</a>
						</li>
						<li>
							<span> <i class="fas fa-angle-right"></i></span>
							<a href="https://kowcomputer.com/tin-tuc/chinh-sach-kinh-doanh-6">Chính sách đổi trả và hoàn
								tiền</a>
						</li>
						<li>
							<span> <i class="fas fa-angle-right"></i></span>
							<a href="https://kowcomputer.com/tin-tuc/chinh-sach-kinh-doanh-5">Chính sách bảo mật thông
								tin</a>
						</li>
					</ul>
				</div>
				<iframe
						src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fkowcomputer&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=174855319597604"
						width="340" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
						allowTransparency="true" allow="encrypted-media"></iframe>
			</div>

		</div>
	</div>

	<div class="footer-bottom">
		<div class="container">
			<div class="text-center">
				<div class="text-center">
					<h6>Công Ty Cổ Phần Đầu Tư Và Phát Triển Công Nghệ Kow Technology</h6>
					<p>©2020 Công Ty Cổ Phần Đầu Tư Và Phát Triển Công Nghệ Kow Technology / GPĐKKD số 0108753143 do Sở
						KHĐT TP.HN cấp</p>
				</div>
			</div>
		</div>
	</div>
</footer>

<products-recently
    @if(isset($productRecently))
    :product="{{ json_encode($productRecently) }}"
    @endif>
</products-recently>
<popup-product>

</popup-product>

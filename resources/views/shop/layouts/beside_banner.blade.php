<div class="banner-sider banner-left">
    <a href="#">
        <lazy-image img-url="{{ asset('pv_tmp/images/left_banner.jpg') }}" img-alt="banner"/>
    </a>
</div>

<div class="banner-sider banner-right">
    <a href="#">
        <lazy-image img-url="{{ asset('pv_tmp/images/right_banner.jpg') }}" img-alt="banner"/>
    </a>
</div>

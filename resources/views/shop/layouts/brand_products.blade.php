<div class="ss-product">
    <div class="product-title">
        <div class="s-tab-ct">
            <ul class="nav nav-tabs small justify-content-end" role="tablist">
                <li>
                    <h2><i class="fa fa-tag" aria-hidden="true"></i> {{ $category['name'] }}</h2>
                </li>
                @foreach($category['tabs'] as $key => $childCategory)
                <li class="nav-item"><a class="nav-link @if($key === 0) active @endif" data-toggle="tab" href="#category-{{ $key }}" role="tab">{{ $childCategory['name'] }}</a></li>
                @endforeach
            </ul>
            <div class="tab-content py-4">
                @foreach($category['tabs'] as $key => $childCategory)
                <div class="tab-pane @if($key === 0) active @endif" id="category-{{ $key }}" role="tabpanel">
                    <div class="product-content">
                        <div class="tab-flex">
                            <div class="width-80">
                                <div class="product-carousel cms-carousel owl-carousel owl-theme" data-margin="10" data-loop="true" data-nav="true" data-dots="false" data-autoplay="false" data-large-items="5" data-medium-items="4" data-small-items="3" data-xsmall-items="2">
                                    @foreach($childCategory['products'] as $product)
                                        <product-item :product="{{ json_encode($product) }}"></product-item>
                                   @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

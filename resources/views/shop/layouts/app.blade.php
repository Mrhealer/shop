<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Locale -->
	<meta name="locale" content="{{ app()->getLocale() }}">

    @if (View::hasSection('title'))
        <title>@yield('title')</title>
    @else
        <title>{{ $_shopSettings->title }}</title>
    @endif

    {{--<title>{{ $_shopSettings->title }}</title>--}}

	<link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
	<meta name="generator" content="Fox and Papay">
	<meta name="keywords" content="{{ $_shopSettings->keyword }}"/>
	<meta http-equiv="content-language" content="vi"/>
	<meta name="copyright" content="{{ $_shopSettings->name }}">
	<meta name="robots" content="noodp,index,follow"/>
	<meta http-equiv="X-UA-Compatible" content="requiresActiveX=true"/>
	<meta http-equiv="content-language" content="vi"/>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
	<meta name="DC.title" content="{{ $_shopSettings->title }}"/>
	<meta property="og:locale" content="vi_VN"/>
	<meta property="og:type" content="website"/>
	<meta property="og:title" content="{{ $_shopSettings->title }}"/>
	<meta property="og:description" content="{{ $_shopSettings->description }}"/>
	<meta property="og:url" content="{{ config('app.url') }}"/>
	<meta property="og:site_name" content="{{ $_shopSettings->name }}"/>
	<meta property="og:image" content="{{ url($_shopSettings->thumbnail) }}"/>
	<meta property="og:image:secure_url" content="{{ url($_shopSettings->thumbnail) }}"/>
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:title" content="{{ $_shopSettings->title }}"/>
	<meta name="twitter:image" content="{{ url($_shopSettings->thumbnail) }}"/>

	<!-- Preload Css -->

	<link rel="preload" href="//fonts.googleapis.com/css?family=Roboto:100,300,400,500,700&display=swap" as="style">
	<link rel="preload" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" as="style">
	<link rel="preload" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" as="style">
	<link rel="preload" href="{{ 'pv_tmp/css/owl.carousel.min.css' }}" as="style">
	<link rel="preload" href="{{ 'pv_tmp/css/fonts.css' }}" as="style">
    <link rel="preload" href="{{ '/refactorcss/themes.css' . '?v=' . uniqid() }}" as="style">
	<link rel="preload" href="{{ mix('/css/shop_app.css') }}" as="style">
	<!-- Preload js -->
	<link rel="preconnect" href="{{ 'pv_tmp/js/jquery.js' }}">
	<link rel="preconnect" href="{{ 'pv_tmp/js/jquery.ui.js' }}">
	<link rel="preconnect" href="{{ 'pv_tmp/js/owl.carousel.min.js' }}">
	<link rel="preconnect" href="{{ 'pv_tmp/js/k2-custom.js'}}">
	<link rel="preconnect" href="//code.jquery.com/jquery-3.2.1.slim.min.js">
	<link rel="preconnect" href="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js">
	<link rel="preconnect" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
	<link rel="preconnect" href="{{ mix('js/shop_app.js') }}">

	<!-- Load css -->
	<link href="//fonts.googleapis.com/css?family=Roboto:100,300,400,500,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" type="text/css">
	<link rel="stylesheet" href="{{ 'pv_tmp/css/fonts.css' }}" type="text/css">
    <link rel="stylesheet" href="{{ '/refactorcss/themes.css' . '?v=' . uniqid() }}" type="text/css">
	<link rel="stylesheet" href="{{ 'pv_tmp/css/owl.carousel.min.css' }}" type="text/css" media="all">
	<link rel="stylesheet" href="{{ mix('/css/shop_app.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ '/refactorcss/themes.css' }}" type="text/css">
	@yield('styles')
</head>

<body>
@include('shop.layouts.ads_banner_slider')
@include('shop.layouts.nav_mobile')
<div class="template-pv {{ $class_name }}" id="app">
	@include('shop.layouts.social_network')
	@include('shop.layouts.call_phone_info')
	@yield('header')
	<div class="container nav-fix">
		@yield('content')
	</div>
{{--	<products-recently></products-recently>--}}
	@include('shop.layouts.footer')
    @include('shop.layouts.nav_bottom_mobile')
</div>
<!-- App -->
<script src="//code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ 'pv_tmp/js/jquery.js' }}"></script>
<script type="text/javascript" src="{{ 'pv_tmp/js/jquery.ui.js' }}"></script>
<script type="text/javascript" src="{{ 'pv_tmp/js/jquery.countdown.min.js' }}"></script>
<script type="text/javascript" src="{{ 'pv_tmp/js/owl.carousel.min.js' }}"></script>
<script type="text/javascript" src="{{ 'pv_tmp/js/k2-custom.js' . '?v=' . uniqid() }}"></script>
<script src="{{ mix('js/shop_app.js') }}" defer></script>

@yield('scripts')
</body>
</html>

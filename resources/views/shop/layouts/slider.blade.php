<div class="s-slider">
    <div class="sli-ban">
        <div class="sli-slider">
            <div>
                <div class="tab-content">
                    @foreach($_slider as $key => $tab)
                        <div class="tab-pane fade @if($key === 0)show active @endif" id="slider-{{ $key }}" role="tabpanel" aria-labelledby="slider-{{ $key }}">
                            <div class="banner-slider owl-carousel owl-theme" data-margin="0" data-loop="true" data-nav="true" data-dots="false" data-autoplay="true" data-large-items="1" data-medium-items="1" data-small-items="1" data-xsmall-items="1">
                                @foreach($tab->slides as $slide)
                                    <div class="item">
                                        <div class="wrap-k2-image">
                                            <a href="{{ $slide->link }}">
                                                <img src="{{ asset($slide->image) }}" alt="{{ $tab->name  }}">
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="sli-banner">
            <div class="item">
                <a href="{{ $_shopSettings->banner_top_link }}">
                    <img src="{{ asset($_shopSettings->banner_top) }}" alt="Banner Top">
                </a>
            </div>
            <div class="item">
                <a href="{{ $_shopSettings->banner_mid_link }}">
                    <img src="{{ asset($_shopSettings->banner_mid) }}" alt="Banner mid">
                </a>
            </div>
            <div class="item">
                <a href="{{ $_shopSettings->banner_bot_link }}">

                    <img src="{{ asset($_shopSettings->banner_bot) }}" alt="Banner Bot">
                </a>
            </div>
        </div>
    </div>
    <div class="ss-count-down">
        <div class="count-wrap-tab">
            <div class="title">
                <p><i class="fa fa-bolt" aria-hidden="true"></i> {{ $_shopSettings->main_product_title }}</p>
            </div>
        </div>
        <div class="tab-content" id="myTabContent">
            <div class="tab-ct">
                <div class="ct-tab">
                    <div class="tab-carousel hot-products owl-carousel owl-theme">
                        @foreach($_hotProducts as $product)
                            <product-item :product="{{ json_encode($product) }}"></product-item>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

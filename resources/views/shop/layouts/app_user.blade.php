<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Locale -->
    <meta name="locale" content="{{ app()->getLocale() }}">

    <title>{{ config('app.name') }}</title>

    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}">


    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" >
    <link href="{{ asset('pv_tmp/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('pv_tmp/css/fonts.css') }}" rel="stylesheet">
    <link href="{{ asset('pv_tmp/css/backend.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/element-ui.css') }}" rel="stylesheet">
    <link href="{{ mix('css/shop_app.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body>
<div class="template-pv editer-info wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <a href="{{ url('') }}"><h3>{{ config('app.name') }}</h3></a>
            <strong>KT</strong>
        </div>

        <ul class="list-unstyled components">
            <li>
                <a href="{{ route('index') }}">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    Trang Chủ
                </a>
            </li>
            <li  class="{{ request()->is('user/info') ? 'active' : '' }}">
                <a href="{{ route('user.info') }}">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    Sửa Thông Tin Cá Nhân
                </a>
            </li>
            <li class="{{ request()->is('user/orders') ? 'active' : '' }}">
                <a href="{{ route('user.order') }}">
                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                    Kiểm Tra Đơn Hàng
                </a>
            </li>
            <li class="{{ request()->is('user/products-purchased') ? 'active' : '' }}">
                <a href="{{ route('user.purchased') }}">
                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                    Sản phẩm đã mua
                </a>
            </li>

            <li class="{{ request()->is('user/tickets') ? 'active' : '' }}">
                <a href="{{ route('user.ticket_index') }}">
                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                    Các yêu cầu hỗ trợ
                </a>
            </li>
            <li>
                <a href="{{ route('auth.user_logout') }}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i>
                    Thoát
                </a>
            </li>
        </ul>
    </nav>

    <!-- Page Content Holder -->
    <div id="content" style="width: 100%;">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        @guest
                            <li><a href="{{ route('auth.user_login') }}" class="btn btn-primary border-radius-0 text-white">ĐĂNG NHẬP</a></li>
                        @endguest
                        @auth
                        <li><p>Xin chào <span> {{ $_user->name }}</span></p></li>
                        <li><p>SĐT <span>{{ $_user->phone }}</span></p></li>
                        <li><a href="{{ route('auth.user_logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i> Thoát</a></li>
                        @endauth
                    </ul>
                </div>
            </div>
        </nav>
        <div id="app">
            @yield('content')
        </div>

    </div>
</div>
<script type='text/javascript' src='{{ mix('js/shop_app.js') }}'></script>
<script type='text/javascript' src='{{ asset('pv_tmp/js/jquery.js') }}'></script>
<script type="text/javascript" src="{{ asset('pv_tmp/js/bootstrap.min.js') }}"></script>

</body>
</html>

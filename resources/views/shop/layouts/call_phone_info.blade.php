<!--Modal: modalPush-->
<div class="modal fade" id="modalPush" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content text-center">
      <!--Header-->
      <div class="modal-header d-flex justify-content-center">
        <p class="heading">Hotline Liên Hệ Mua Hàng</p>
      </div>

      <!--Body-->
      <div class="modal-body">

		<i class="fa fa-info-circle fa-4x animated rotateIn mb-4" aria-hidden="true"></i>
        <p><i class="fa fa-user-circle-o" aria-hidden="true"></i> Mr Hiệp : 1900636101</p>
		<p><i class="fa fa-user-circle-o" aria-hidden="true"></i> Mr Việt : 0938 496 680</p>
      </div>

      <!--Footer-->
      <div class="modal-footer flex-center">
        <!--<a href="https://mdbootstrap.com/products/jquery-ui-kit/" class="btn btn-info">Yes</a>-->
        <a type="button" class="btn btn-outline-info waves-effect" data-dismiss="modal">Close</a>
      </div>
    </div>
    <!--/.Content-->
  </div>
  <div id="snackbar"><a class="phone_mobile" href="tel:0987 653 333">0987 653 333</a></div>
</div>

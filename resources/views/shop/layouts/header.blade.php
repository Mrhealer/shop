<div class="site-header">
    <div class="header-middle d-none d-lg-block d-md-none d-sm-none d-xs-none" id = "myTopBar">
        @include('shop.layouts.top-navbar-banner')
        <div class="container">
            <div class="header-logo-block">
                <a href="{{ url('') }}">
                    <img src="{{ asset($_shopSettings->logo) }}" alt="Logo"/>
                </a>
            </div>
            <div class="header-search">
               <search-bar :keyword="{{ json_encode($params['search'] ?? '') }}"></search-bar>
            </div>
            <div class="header-right-block">
                <a href="{{ route('build_config') }}" class="main-bar-news">
                    <i class="fas fa-tools"></i>
                    <div class="ri-info">
                        <p>Xây dựng cấu hình</p>
                    </div>
                </a>
                <a href="{{ route('user.warranty') }}" class="main-bar-news">
                    <i class="fas fa-recycle"></i>
                    <div class="ri-info">
                        <p>Kiểm tra bảo hành</p>
                    </div>
                </a>
                <a href="{{ route('blogs') }}" class="main-bar-news">
                    <i class="far fa-newspaper" area-hidden="true"></i>
                    <div class="ri-info">
                        <p>Tin tức</p>
                    </div>
                </a>
                <login-button :user="{{ json_encode($_user) }}"></login-button>
                <cart></cart>
            </div>
        </div>
        @include('shop.layouts.submenu-nav')
    </div>
</div>
<div class="nav-button d-md-block d-sm-block d-xs-block d-lg-none ">
    <button id="bars"><i class="fas fa-bars"></i></button>
    <a href="{{ url('') }}" class="mobi-logo">
        <img src="{{ asset($_shopSettings->logo) }}" alt="Logo"/>
    </a>
    <form action="{{ route('search') }}" class="mobile-search form-search-auto-width">
        <input type="text" autocomplete="off" name="search" class="input-search search-auto-width" value="{{ $params['search'] ?? '' }}"></input>
    </form>
    <a href="{{ route('cart') }}" class="mobile-cart">
        <div>
            <i class="fas fa-shopping-cart"></i>
        </div>
    </a>
</div>

<div class="ss-laptop-new">
    <div class="laptop-new-title">
        <h2>SẢN PHẨM MỚI</h2>
    </div>
    <div class="ss-product">
        <div class="product-content">
            <div class="product-carousel cms-carousel owl-carousel owl-theme" data-margin="10" data-loop="true" data-nav="true" data-dots="false" data-autoplay="false" data-large-items="5" data-medium-items="4" data-small-items="3" data-xsmall-items="2">
                @foreach($_latestProducts as $product)
                    <product-item :product="{{ json_encode($product) }}"></product-item>
                @endforeach
            </div>
        </div>
    </div>
</div>

<!-- ---------Modal--------- -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog modal-lg my-modal">
		<div class="modal-content">
			<div class="modal-header mod-head">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="wrap-img">
						<div class="show-img">
							<img src="images/item_1.jpg" alt="">
						</div>
						<div class="thumb-img">
							<div class="thumb-item">
								<img src="images/item_1.jpg" alt="">
							</div>
							<div class="thumb-item thumb-hover">
								<img src="images/laptop_km.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="mod-title">
						<h5>SAMSUNG GALAXY S7</h5>
					</div>
					<div class="mod-desc">
						<ul>
							<li>Technology GSM/ CDMA/ HSPA/ EVDO/ LTE</li>
							<li>Dimension 1920 x 1080</li>
							<li>Weight: 125g</li>
							<li>Display: Super AMOLED</li>
							<li>Resolution: 1440 x 2560</li>
							<li>Technology: GSM/ CDMA/ HSPA/ EVDO/ LTE</li>
							<li>Technology: GSM/ CDMA/ HSPA/ EVDO/ LTE</li>
						</ul>
					</div>
					<div class="mod-price"><h3>$899.0</h3></div>

					<span>Chọn số lượng: </span>
					<div class="qty-item">
						<button class="item-bt minus-item">
							<i class="fa fa-minus" aria-hidden="true"></i>
						</button>
						<input type="text" class="count" name="qty" value="1"  min="1" max="10000">
						<button class="item-bt plus-item">
							<i class="fa fa-plus" aria-hidden="true"></i>
						</button>
					</div>
					<div class="mod-btn">
						<button href="" title="Thêm vào giỏ hàngm-btn">
							<i class="icon-basket-3"></i>
							<span>ADD TO CART</span>
						</button>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<!-- ---------Modal--------- -->
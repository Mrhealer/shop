<!-- ----- NAV MOBILE ----- -->
<div class="nav-mobile d-block d-md-block d-sm-block d-xs-block d-lg-none">
	<div class="overlay"></div>
	<nav id="sidebar" class="">
		<div class="sidebar-content">
			<div class="sidebar-header">
				<a href="{{ url('') }}"><h5>{{ $_shopSettings->name }}</h5></a>
			</div>
			<div class="reg-login-header">
				@auth('user')
					<a href="{{ route('user.info') }}" class="sidebar-register">
						<strong>
							<i class="fas fa-user"  style="color: red"></i>
							 Chào! {{ $_user->name }}.</strong>
					</a>
					<a href="{{ route('auth.user_logout') }}" class="sidebar-login" >
						<i class="fas fa-eject"></i></a>
				@else
				<a href="{{ route('auth.register_user') }}" class="sidebar-register">
					<i class="fas fa-signing"></i>
					<div>Đăng ký</div>
				</a>
				<a href="{{ route('auth.user_login') }}" class="sidebar-login" >
					<i class="fas fa-sign-in-alt"></i>
					<div>Đăng nhập</div>
				</a>
				@endauth
			</div>

			<ul class="list-unstyled components">
				<li>
					<a href="#laptop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle collapsed">
						<i class="fas fa-list"></i>
						Danh mục
					</a>
					<ul class="collapse list-unstyled" id="laptop">
						@foreach($_menusTree as $menuLevel1)
                            @if ($menuLevel1->object_type === \App\Enums\CustomMenuObjType::PRODUCT_CATEGORY)
                                @php
                                    $url = route('category', ['category' => $menuLevel1->prodCategory->slug]);
                                    $icon = $menuLevel1->prodCategory->icon;
                                @endphp
                            @else
                                @php
                                    $url = $menuLevel1->url;
                                    $icon = '/images/link.png';
                                @endphp
                            @endif

							<li>
								<a href="{{ $url }}">
									<lazy-image img-alt="{{ $menuLevel1->name }}" img-url="{{ $icon }}" img-class="icon-category-mobile"/>
									{{ $menuLevel1->name }}
								</a>
							</li>
						@endforeach
					</ul>
				</li>
                <li>
                    <a href="{{ route('build_config') }}">
                        <i class="fas fa-tools"></i>
                        Xây dựng cấu hình
                    </a>
                </li>
                <li>
                    <a href="{{ route('blogs') }}">
                        <i class="far fa-newspaper"></i>
                        Tin tức
                    </a>
                </li>
                <li>
                    <a href="{{ route('cart') }}">
                        <i class="fas fa-shopping-cart"></i>
                        Giỏ hàng
                    </a>
                </li>

				<li class="info">
					<ul class="menu-info">
						<li>Thông tin</li>
						<li>
							<i class="fas fa-phone"></i>
							{{ $_shopSettings->hotline }}
						</li>
						<li>
							<a href=""><i class="fas fa-phone"></i>
								Tư vấn mua hàng</a>
						</li>
						<li>
							<a href=""><i class="fas fa-phone"></i>
								Tổng hợp khuyến mãi</a>
						</li>
						<li>
							<a href=""><i class="fas fa-phone"></i>
								Chính sách vận chuyển</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</div>
<!-- ------ NAV MOBILE ------ -->

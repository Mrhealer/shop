@extends('shop.layouts.app', ['class_name' => 'page-checkout'])
@section('header')
	@include('shop.layouts.header')
@endsection
@section('content')
	<div class="site-body" id="site-body">
		<div class="entry-checkout">
			<cart-checkout
					:user="{{ json_encode($_user) }}"
					:settings="{{ json_encode($_shopSettings) }}"
            ></cart-checkout>
		</div>
	</div>
@endsection

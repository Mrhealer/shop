@extends('shop.layouts.app', ['class_name' => 'page-blog'])
@section('header')
	@include('shop.layouts.header')
@endsection
@section('content')
	<warranty
	:product-warranty-status="{{ json_encode($productWarrantyStatus) }}"
	>
	</warranty>
@endsection
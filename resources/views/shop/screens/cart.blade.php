@extends('shop.layouts.app', ['class_name' => 'page-cart'])
@section('header')
    @include('shop.layouts.header')
@endsection
@section('content')
    <div class="site-body" id="site-body">
        <cart-list></cart-list>
    </div>
@endsection

@extends('shop.layouts.app_user')
@section('content')
   <order-list
           :orders="{{ json_encode($orders) }}"
           :order-status="{{ json_encode($_orderStatus) }}"
           :order-status-enum="{{ json_encode($_orderStatusEnum) }}"
           :shipping-status="{{ json_encode($_shippingStatus) }}"
   ></order-list>
@endsection

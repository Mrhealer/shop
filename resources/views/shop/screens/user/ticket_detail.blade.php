@extends('shop.layouts.app_user')
@section('content')
<user-ticket-detail
	:ticket="{{ json_encode($ticket) }}"
	:ticket-status-enum="{{ json_encode($_ticketStatusEnum) }}"
>

</user-ticket-detail>
@stop
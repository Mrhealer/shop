@extends('shop.layouts.app_user')
@section('content')
    <warranty
    :warranty-status="{{ json_encode($warrantyStatus) }}"
    ></warranty>
@endsection

@extends('shop.layouts.app_user')
@section('content')
<user-profile :user="{{ json_encode($_user) }}"></user-profile>
@endsection
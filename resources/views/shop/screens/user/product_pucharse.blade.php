@extends('shop.layouts.app_user')
@section('styles')
	<style>
		.el-upload__input {
			display: none !important;
		}
		.el-button--primary {
			color: #FFF;
			background-color: #409EFF;
			border-color: #409EFF;
		}
	</style>
@stop
@section('content')
	<product-pucharsed
			:inventories="{{ json_encode($inventories) }}"
			:user="{{ json_encode($_user) }}"
	>
	</product-pucharsed>
@endsection

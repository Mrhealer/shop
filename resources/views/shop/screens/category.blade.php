@extends('shop.layouts.app', ['class_name' => 'page-cat'])
@section('header')
	@include('shop.layouts.header')
@endsection
@section('content')
	<div class="site-body" id="site-body-sub">
		<div class="wrap-chuyen-muc">
			<filter-product
					:category="{{ json_encode($category) }}"
                    @if(!empty($category))
					:brands="{{ json_encode($category->brands) }}"
                    @else
                    :brands="{{ json_encode($_allBrand) }}"
                    @endif
                    :attributes="{{ json_encode($attributes) }}"
                    :price-range-checkbox="{{ json_encode($_priceRange) }}"
					:params="{{ json_encode($params) }}"
			></filter-product>
			<div class="cm-content">
				@if(!empty($category->slider))
				<div class="banner-cm">
					<div class="banner-slider owl-carousel owl-theme">
						@foreach($category->sliderImages() as $image)
						<div class="item">
							<div class="wrap-k2-image">
								<lazy-image img-url="{{ asset($image) }}" img-alt="{{ $category->name }}">
							</div>
						</div>
						@endforeach
					</div>
				</div>
				@endif
				<div class="content-ft">
					<sort
						:params="{{ json_encode($params) }}"
						:sort-type="{{ json_encode($_sortType) }}"
						:route-name="{{ json_encode( Route::currentRouteName()) }}"
						:category="{{ json_encode($category) }}"
					></sort>
					<div class="content-item">
						@foreach($cat_products as $product)
							<product-item :product="{{ json_encode($product) }}"></product-item>
						@endforeach
					</div>

					{{ $cat_products->appends($params)->links('shop.layouts.paginate_custom') }}

				</div>
			</div>
		</div>
	</div>
@endsection

@extends('shop.layouts.app', ['class_name' => 'page-home'])
@section('header')
    @include('shop.layouts.header')
@endsection
@section('content')
    <div class="site-body rmv">
        <div class="s-body">
            @include('shop.layouts.category')
            @include('shop.layouts.slider')
        </div>
        @include('shop.layouts.lastest_products')
        @foreach($_topCategories as $category)
           <product-tab-list :category="{{ json_encode($category) }}"></product-tab-list>
        @endforeach

    </div>
@endsection

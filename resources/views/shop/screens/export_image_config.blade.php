<html lang="vi">
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
    <title>kowcomputer.com - Xây dựng cấu hình PC</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: Arial;
            font-size: 14px;
            line-height: 1.5;
        }

        table {
            padding: 5px;
        }
    </style>
</head>
<body>
<div id="config-export_image" style="width:800px; margin:auto; background: url(/images/logo_background.png) center no-repeat #ffffff;">
    <img src="/images/build_config.jpg" alt="header" style="width:100%; height:auto; display:block;">
    <table style="width:100%;">
        <tbody>
            @php
                $totalMoney = 0;
            @endphp
            @foreach($data as $item)
                @php
                    $totalMoney += $item->price * $item->quantity;
                @endphp
                <tr>
                    <td width="200">
                        <a >
                            <img
                                src="{{ $item->image }}"
                                alt=""
                                style="width: 200px; height: 200px;"
                            >
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('product', ['product' => $item->slug]) }}"
                           style="text-decoration:none; color:#333; font-weight:bold; font-size:19px;">
                            {{ $item->name }}
                        </a>
                        <br>
                        SKU: {{ $item->sku }} <br>
                        <b>{{ number_format($item->price) }} x {{ $item->quantity }}</b>
                    </td>
                    <td width="180">
                        <b style="color:#e00; font-size:18px;">= {{ number_format($item->price *  $item->quantity) }} đ</b>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <p style="font-size:21px; color:#e00; font-weight:bold; text-align:center;">Tổng chi phí: {{ number_format($totalMoney) }}</p>
    <table style="text-align:center; width:100%;">
        <tbody>
        <tr>
            <td colspan="8" style="font-size:21px;">CHÂN THÀNH CẢM ƠN !</td>
        </tr>
        <tr>
            <td colspan="8">Để biết thêm chi tiết, vui lòng liên hệ</td>
        </tr>
        <tr>
            <td colspan="8"><b>Hotline:</b> {{ $setting->hotline }}</td>
        </tr>
        <tr>
            <td colspan="8"><a href="https://kowcomputer.com">https://kowcomputer.com</a></td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>

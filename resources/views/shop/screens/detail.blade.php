@extends('shop.layouts.app', ['class_name' => 'page-single'])
@section('header')
	@include('shop.layouts.header')
@endsection
@section('title',"$product->name | kowcomputer.com")
@section('content')
	<div class="site-body" id="site-body">
		<div class="breadcrumb">
			<ul>
				<li><a href="{{ route('index') }}">Trang chủ</a></li>
				<li>{{ $product->name }}</li>
			</ul>
		</div>

		<div class="k2-single">
			<div class="single-product">
				<product-image-detail
						:product="{{ json_encode($product) }}"
						:images="{{ json_encode($product->imageList()) }}"
						:thumbnail="{{ json_encode($product->thumbnail()) }}"
                ></product-image-detail>
                <div class="single-summany">
                    <h3>{{ $product->name }}</h3>
                    <div class="summanys">
                        <div class="summany-left">
                            <h5>Thông Số Cơ Bản:</h5>
                            @if($product->prod_category_id === 4)
                                <div class="wrap-atribute">
                                    <div class="attribute-item">
                                        <p>Thương hiệu: {{ $product->brand->name }}</p>
                                    </div>

                                    @foreach($productAttributes as $productAttribute)
                                        @if(!empty($productAttribute['values']))
                                            <div class="attribute-item">
                                                @if($productAttribute['attribute']->id ===5
                                                    ||$productAttribute['attribute']->id ===11 || $productAttribute['attribute']->id === 1)
                                                    <p>
                                                        {{ $productAttribute['attribute']->name }} : {{ \App\Helpers\Helper::implodeAttributeValues($productAttribute['values']) }}
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @elseif($product->prod_category_id === 9)
                                <div class="wrap-atribute">
                                    <div class="attribute-item">
                                        <p>- {{ $product->brand->name }}</p>
                                    </div>
                                    @foreach($productAttributes as $productAttribute)
                                        @if(!empty($productAttribute['values']))
                                            <div class="attribute-item">
                                                @if($productAttribute['attribute']->id === 20)
                                                    <p>
                                                        {{ $productAttribute['attribute']->name }} : {{ \App\Helpers\Helper::implodeAttributeValues($productAttribute['values']) }}
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @elseif($product->prod_category_id === 10)
                                <div class="wrap-atribute">
                                    <div class="attribute-item">
                                        <p>- {{ $product->brand->name }}</p>
                                    </div>
                                    @foreach($productAttributes as $productAttribute)
                                        @if(!empty($productAttribute['values']))
                                            <div class="attribute-item">
                                                @if($productAttribute['attribute']->id === 79)
                                                    <p>
                                                        {{ $productAttribute['attribute']->name }} : {{ \App\Helpers\Helper::implodeAttributeValues($productAttribute['values']) }}
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @elseif($product->prod_category_id === 11)
                                <div class="wrap-atribute">
                                    <div class="attribute-item">
                                        <p>- {{ $product->brand->name }}</p>
                                    </div>
                                    @foreach($productAttributes as $productAttribute)
                                        @if(!empty($productAttribute['values']))
                                            <div class="attribute-item">
                                                @if($productAttribute['attribute']->id === 46 || $productAttribute['attribute']->id === 45
                                                    || $productAttribute['attribute']->id === 47 || $productAttribute['attribute']->id === 48)
                                                    <p>
                                                        {{ $productAttribute['attribute']->name }} : {{ \App\Helpers\Helper::implodeAttributeValues($productAttribute['values']) }}
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @elseif($product->prod_category_id === 12)
                                <div class="wrap-atribute">
                                    <div class="attribute-item">
                                        <p>- {{ $product->brand->name }}</p>
                                    </div>
                                    @foreach($productAttributes as $productAttribute)
                                        @if(!empty($productAttribute['values']))
                                            <div class="attribute-item">
                                                @if($productAttribute['attribute']->id === 65 || $productAttribute['attribute']->id === 69 )
                                                    <p>
                                                        {{ $productAttribute['attribute']->name }} : {{ \App\Helpers\Helper::implodeAttributeValues($productAttribute['values']) }}
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @elseif($product->prod_category_id === 13)
                                <div class="wrap-atribute">
                                    <div class="attribute-item">
                                        <p>- {{ $product->brand->name }}</p>
                                    </div>
                                    @foreach($productAttributes as $productAttribute)
                                        @if(!empty($productAttribute['values']))
                                            <div class="attribute-item">
                                                @if($productAttribute['attribute']->id === 53 || $productAttribute['attribute']->id === 51
                                                    || $productAttribute['attribute']->id === 59 || $productAttribute['attribute']->id === 60)
                                                    <p>
                                                        {{ $productAttribute['attribute']->name }} : {{ \App\Helpers\Helper::implodeAttributeValues($productAttribute['values']) }}
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @elseif($product->prod_category_id === 15)
                                <div class="wrap-atribute">
                                    <div class="attribute-item">
                                        <p>- {{ $product->brand->name }}</p>
                                    </div>
                                    @foreach($productAttributes as $productAttribute)
                                        @if(!empty($productAttribute['values']))
                                            <div class="attribute-item">
                                                @if($productAttribute['attribute']->id === 37)
                                                    <p>
                                                        {{ $productAttribute['attribute']->name }} :  {{ \App\Helpers\Helper::implodeAttributeValues($productAttribute['values']) }}
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @elseif($product->prod_category_id === 25)
                                <div class="wrap-atribute">
                                    <div class="attribute-item">
                                        <p>- {{ $product->brand->name }}</p>
                                    </div>
                                    @foreach($productAttributes as $productAttribute)
                                        @if(!empty($productAttribute['values']))
                                            <div class="attribute-item">
                                                @if($productAttribute['attribute']->id === 96)
                                                    <p>
                                                        {{ $productAttribute['attribute']->name }} : {{ \App\Helpers\Helper::implodeAttributeValues($productAttribute['values']) }}
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @elseif($product->prod_category_id === 30)
                                <div class="wrap-atribute">
                                    <div class="attribute-item">
                                        <p>- {{ $product->brand->name }}</p>
                                    </div>
                                    @foreach($productAttributes as $productAttribute)
                                        @if(!empty($productAttribute['values']))
                                            <div class="attribute-item">
                                                @if($productAttribute['attribute']->id === 145 || $productAttribute['attribute']->id === 146
                                                    ||$productAttribute['attribute']->id === 147 || $productAttribute['attribute']->id === 150)
                                                    <p>
                                                        {{ $productAttribute['attribute']->name }} : {{ \App\Helpers\Helper::implodeAttributeValues($productAttribute['values']) }}
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @else
                                <div class="wrap-atribute">
                                    <div class="attribute-item">
                                        <p>Thương hiệu: {{ $product->brand->name }}</p>
                                    </div>
                                </div>
                            @endif
                            <br>
                            <div class="price-detail">
                                <div class="price">
                                    @if($product->listed_price)
                                        @if($product->listed_price > $product->price)
                                            <div class="price-old">
                                                <p>Giá niêm yết</p>
                                                <p class="price-ol">{{ number_format($product->listed_price) }}đ</p>
                                            </div>
                                            <div class="price-km">
                                                <p>Giá khuyến mãi</p>
                                                <p class="price-k">{{ number_format($product->price) }}đ
                                                    {{--<small>Áp dụng khi mua từ 1 sản phẩm sử dụng mã <span>PAY022</span></small>--}}
                                                </p>
                                            </div>
                                        @else
                                            <div class="price-km">
                                                <p>Giá</p>
                                                <p class="price-k">{{ number_format($product->price) }}đ
                                                </p>
                                            </div>
                                        @endif
                                    @else
                                        <div class="price-km">
                                            <p>Giá bán lẻ</p>
                                            <p class="price-k">{{ number_format($product->price) }}đ</p>
                                        </div>
                                    @endif
                                </div>
                                @if($product->listed_price > $product->price)
                                    <div class="pomo">
                                        <p>Khuyến mãi</p>
                                        <div class="pomo-detail">
                                            <p class="fs13 dark-grey-text mr5">Giảm <span class="highlight">
										@if($product->show_percent_discount)
                                                        {{ ($product->listed_price - $product->price) / $product->listed_price * 100}}
                                                        %
                                                    @else
                                                        {{ number_format($product->listed_price - $product->price ) }}đ
                                                    @endif
									</span></p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <cart-plugin :product="{{ json_encode($product) }}"/>
                        </div>



                        <div class="summany-right" style="margin-top: 0px !important; margin-left: 10px;">
                            @if(!empty($setting_sale_policy))
                                <div class="b-item">
                                    <div class="b-title custom"><p><i aria-hidden="true" class="fa fa-money"></i>CHÍNH SÁCH BÁN HÀNG</p></div>
                                    <div class="b-content">
                                        {!! $setting_sale_policy !!}
                                    </div>
                                </div>
                            @endif
                            @if(!empty($product->promotions))
                                <div class="b-item">
                                    <div class="b-title custom"><p><i aria-hidden="true" class="fa fa-bullhorn"></i>CHƯƠNG TRÌNH KHUYẾN MÃI</p></div>
                                    <div class="b-content">
                                        {!! $product->promotions !!}
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
		</div>

        @if(count($on_brand_products) >=5)
            <div class="recen-product">
                <!-- ************ NAVTAB SP CÙNG THƯƠNG HIỆU ***********  -->
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#sp-brand">Sản phẩm tương tự</a>
                    </li>
                    {{--				<li class="nav-item">--}}
                    {{--					<a class="nav-link" data-toggle="tab" href="#sp-combo">Sản phẩm cùng thương hiệu</a>--}}
                    {{--				</li>--}}
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane container active" id="sp-brand">
                        <div class="tab-carousel cms-carousel owl-carousel owl-theme" data-margin="10" data-loop="true"
                             data-nav="true" data-dots="false" data-autoplay="true" data-large-items="5"
                             data-medium-items="3"
                             data-small-items="2" data-xsmall-items="2">
                            @foreach($on_brand_products as $onBrand)
                                <product-item :product="{{ json_encode($onBrand) }}"></product-item>
                            @endforeach
                        </div>
                    </div>
                    <!-- ----- sp brand ------ -->
                    {{--				<div class="tab-pane container fade" id="sp-combo">--}}
                    {{--					<div class="tab-carousel cms-carousel owl-carousel owl-theme" data-margin="10" data-loop="true"--}}
                    {{--					     data-nav="true" data-dots="false" data-autoplay="true" data-large-items="5"--}}
                    {{--					     data-medium-items="3"--}}
                    {{--					     data-small-items="2" data-xsmall-items="2">--}}
                    {{--						@foreach($similar_products as $similar)--}}
                    {{--							<product-item :product="{{ json_encode($similar) }}"></product-item>--}}
                    {{--						@endforeach--}}
                    {{--					</div>--}}
                    {{--				</div> <!-- ----- sp combo ------ -->--}}
                </div>
            </div>
        @endif

		<div class="about-product">
			<h3>Về sản phẩm</h3>
			<div class="wrap-about">
				<div class="title-about">
					<p class="title-left">THÔNG TIN CHI TIẾT</p>
					<p class="title-right">THÔNG SỐ KỸ THUẬT</p>
				</div>
				<div class="about-ct">
					<div class="about-left">
						<div class="about-content">
							{!!  $product->description !!}
						</div>
						{{--						<div class="wrap-button">--}}
						{{--							<button class="read-more">Xem đầy đủ <i class="fa fa-angle-down" aria-hidden="true"></i>--}}
						{{--							</button>--}}
						{{--							<button class="read-g">Thu gọn <i class="fa fa-angle-up" aria-hidden="true"></i></button>--}}
						{{--						</div>--}}
					</div>
					<div class="about-rigt">
						<div class="wrap-atribute">
							<div class="attribute-item">
								<p>Thương hiệu</p>
								<p>{{ $product->brand->name }}</p>
							</div>
							@foreach($productAttributes as $productAttribute)
                                @if(!empty($productAttribute['values']))
								<div class="attribute-item">
									<p>{{ $productAttribute['attribute']->name }}</p>
									<p>
                                      {{ \App\Helpers\Helper::implodeAttributeValues($productAttribute['values']) }}
									</p>
								</div>
                                @endif
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>

        <div class="k2-comment">
            <h2><i class="fa fa-smile-o" aria-hidden="true"></i> Chấm điểm & Đánh giá từ người mua</h2>

            <div class="wrap-comment">
                <div class="wrap-pro">
                    <product-item :product="{{ json_encode($product) }}"></product-item>
                </div>
                <div class="wrap-com">
                    <div class="fb-comment-embed"
                         data-href="{{ route('product', ['product' => $product->slug ?? $product->id]) }}"
                         data-width="1034"
                         data-include-parent="true">
                        <div class="fb-comments"
                             data-href="{{ route('product', ['product' => $product->slug ?? $product->id]) }}"
                             data-numposts="5" data-width=""></div>
                    </div>
                </div>
            </div>
        </div>
	</div>
@endsection
@section('scripts')
    @parent
    <div id="fb-root"></div>
    <!-- <script async defer crossorigin="anonymous"
            src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=174855319597604&autoLogAppEvents=1"
            nonce="iJso0yUv"></script> -->
@stop

@extends('shop.layouts.app', ['class_name' => 'page-blog'])
@section('header')
	@include('shop.layouts.header')
@endsection
@section('content')
	<div class="site-body" id="site-body">
        <div class="blog">
            <div class="row">
                <div class="col-sm-12">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="{{ route('index') }}">Trang chủ</a></li>
                            <li>{{ $post->name }}</li>
                        </ul>
                    </div>

                </div>
                <div class="col-sm-12">
                    <p></p>
                    <h3>{{ $post->name }}</h3>
                    <p><small>{{ \App\Helpers\DatetimeHelper::formatDatetime('H:i d/m/Y', strtotime($post->updated_at)) }}</small></p>
                    <hr>
                    <p>{!!  $post->content !!}</p>
                </div>
            </div>
        </div>
	</div>
@endsection

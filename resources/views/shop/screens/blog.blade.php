@extends('shop.layouts.app', ['class_name' => 'page-blog'])
@section('header')
	@include('shop.layouts.header')
@endsection
@section('content')
	<div class="site-body" id="site-body">
        <div class="blog rmv">
            <div class="row">
                <div class="col-sm-12">
                    <div class="breadcrumb rmv">
                        <ul>
                            <li><a href="{{ route('index') }}">Trang chủ</a></li>
                            <li>{{ $category->name ?? 'Tin Tức' }}</li>
                        </ul>
                    </div>

                </div>
                <div class="col-sm-12">
                    <p></p>
                    <h3>{{ $category->name ?? 'Tin Tức'  }}</h3>
                </div>
                <div class="col-lg-9">
                    <div class="blogs">
                        @foreach($posts as $post)
                            <div class="blog-item">
                                <a href="{{ route('post', ['post' => $post->slug]) }}">
                                    <lazy-image img-url="{{ asset($post->image) }}" />
                                </a>
                                <div class="blog-title">
                                    <div>
                                        <h4><a href="{{ route('post', ['post' => $post->slug]) }}">{{ $post->name }}</a></h4>
                                        <p><span>{{ \App\Helpers\DatetimeHelper::formatDatetime('H:i d/m/Y', strtotime($post->updated_at)) }}</span></p>
                                    </div>
                                    <p>{{ $post->description }}</p>
                                    <a href="{{ route('post', ['post' => $post->slug]) }}">Đọc thêm</a>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    {{ $posts->appends($params)->links('shop.layouts.paginate_custom') }}
                </div>
                <div class="col-lg-3 d-lg-block d-none d-xs-none">
                    <div class="new-blogs">
                        <h4>Tin mới nhất</h4>
                        @foreach($_lastestNews as $post)
                            <div class="blog-item">
                                <a href="{{ route('post', ['post' => $post->slug]) }}">
                                    <lazy-image img-url="{{ asset($post->image) }}" />
                                </a>
                                <div class="blog-title">
                                    <div>
                                        <a href="{{ route('post', ['post' => $post->slug]) }}">{{ $post->name }}</a>
                                        <p><span>{{ \App\Helpers\DatetimeHelper::formatDatetime('H:i d/m/Y', strtotime($post->updated_at)) }}</span></p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
	</div>
@endsection

@extends('shop.layouts.app', ['class_name' => 'page-cat'])
@section('header')
    @include('shop.layouts.header')
@endsection
@section('content')
    <div class="site-body">
        <div class="build-screen">
            <build-config
                :config="{{ json_encode($config) }}"
                :request-params="{{ json_encode($params) }}"
                :sort-type="{{ json_encode($_sortType) }}"
                :settings="{{ json_encode($_shopSettings) }}"
            ></build-config>
        </div>
    </div>
@endsection

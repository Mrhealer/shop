<?php


namespace App\Enums;


class TicketStatus extends \MyCLabs\Enum\Enum
{
	const OPEN = 0;
	const ANSWERED = 1;
	const CLOSED = 2;
	const WAIT_CLOSE = 3;
}
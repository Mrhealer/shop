<?php


namespace App\Enums;

use MyCLabs\Enum\Enum;

class AdminPermissions extends Enum
{
	const SYSTEM_MANAGER = 11;
	const VIEW_REVENUE = 12;
	const SETTING = 13;


	const VIEW_MENU = 14;
	const EDIT_MENU = 15;
	const DELETE_MENU = 16;

	const VIEW_HORIZON = 17;

	const VIEW_BLOG_CATEGORY = 21;
	const EDIT_BLOG_CATEGORY = 22;
	const DELETE_BLOG_CATEGORY = 23;

	const VIEW_POST = 31;
	const EDIT_POST = 32;
	const DELETE_POST = 33;

	const VIEW_PROD_CATEGORY = 41;
	const EDIT_PROD_CATEGORY = 42;
	const DELETE_PROD_CATEGORY = 43;

	const VIEW_PRODUCT = 51;
	const EDIT_PRODUCT = 52;
	const DELETE_PRODUCT = 53;
	const IMPORT_PRODUCT = 54;
	const EXPORT_PRODUCT = 55;

	const VIEW_PROD_ATTRIBUTE = 54;
	const EDIT_PROD_ATTRIBUTE = 55;
	const DELETE_PROD_ATTRIBUTE = 56;

	const VIEW_SUPPLY_UNIT = 57;
	const EDIT_SUPPLY_UNIT = 58;
	const DELETE_SUPPLY_UNIT = 59;

	const VIEW_BRAND = 61;
	const EDIT_BRAND = 62;
	const DELETE_BRAND = 63;

	const VIEW_WAREHOUSE = 71;
	const EDIT_WAREHOUSE = 72;
	const DELETE_WAREHOUSE = 73;

	const VIEW_INVENTORY = 81;
	const EDIT_INVENTORY = 82;
	const DELETE_INVENTORY = 83;
	const EXPORT_INVENTORY = 84;
	const IMPORT_INVENTORY = 85;


	const VIEW_USER = 91;
	const EDIT_USER = 92;
	const DELETE_USER = 93;
	const EXPORT_USER = 94;

	const VIEW_ADMIN = 101;
	const EDIT_ADMIN = 102;
	const DELETE_ADMIN = 103;

	const VIEW_ROLE = 110;
	const EDIT_ROLE = 111;
	const DELETE_ROLE = 112;



	const VIEW_ORDER = 201;
	const EDIT_ORDER = 202;
	const HANDLE_ORDER = 203;
	const DELETE_ORDER = 204;
	const CHANGE_SHIP_ORDER = 205;

	const VIEW_SHIPPING_BILL = 301;
	const HANDLE_SHIPPING_BILL = 302;

	const VIEW_TICKET = 401;
	const EDIT_TICKET = 402;
	const HANDLE_TICKET = 403;

	const VIEW_TICKET_CAT = 411;
	const EDIT_TICKET_CAT = 412;
	const DELETE_TICKET_CAT = 413;

	const VIEW_WARRANTY = 501;
	const EDIT_WARRANTY  = 502;
	const HANDLE_WARRANTY  = 503;
	const VIEW_ALL_WARRANTY_PRODS = 504;

	const VIEW_VOUCHER = 601;
	const EDIT_VOUCHER = 602;
	const DELETE_VOUCHER = 603;

	const VIEW_FILE = 611;
	const EDIT_FILE = 612;
	const DELETE_FILE = 613;
	const UPLOAD_FILE = 614;
	const EDIT_DIR = 615;


}
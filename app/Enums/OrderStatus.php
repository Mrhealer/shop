<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class OrderStatus extends Enum
{
    const ERROR = 0;
    const SUCCESS = 1;
    const PENDING = 2;
    const PROCESSING = 3;
//	const WAIT_SHIPPER = 4;
//	const SHIPPING = 5;
    const RETURN = 6;

}

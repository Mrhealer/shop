<?php


namespace App\Enums;

use MyCLabs\Enum\Enum;

class UserPermissions extends Enum
{
	const VIEW_TICKET = 1101;
	const CLOSE_TICKET = 1102;
	const SEND_TICKET = 1103;
}
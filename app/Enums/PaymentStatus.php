<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class PaymentStatus extends Enum
{
	const PAID = 0;
	const UNPAID = 1;
	const REFUND = 2;
}

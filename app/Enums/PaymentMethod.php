<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class PaymentMethod extends Enum
{
    const COD = 1; // Thanh toan tien mat khi nhan hang
    const ATM = 2; // Chuyen khoan
    const CC = 3; // Tra gop qua credit cart
}

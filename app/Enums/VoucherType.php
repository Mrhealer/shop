<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class VoucherType extends Enum
{
    const PRODUCT = 1;
    const BRAND = 2;
    const CATEGORY = 3;
}

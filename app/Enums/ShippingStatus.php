<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class ShippingStatus extends Enum
{
	const WAIT_SHIPPER = 1;
	const SHIPPING = 2;
    const SUCCESS = 3;
    const FAILED = 4;
}

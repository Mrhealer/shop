<?php
namespace App\Enums;
use MyCLabs\Enum\Enum;

class ExportInventoryType extends Enum
{
    const ORDER = 'order';
    const WAREHOUSE = 'warehouse';
}

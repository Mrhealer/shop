<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class Warehouse extends Enum
{
    const HN = 'HN';
    const HCM = 'HCM';
}

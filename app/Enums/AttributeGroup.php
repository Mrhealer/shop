<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class AttributeGroup extends Enum
{
    const GENERAL = 1; // Thong tin chung
    const CONFIG = 2; // Cau hinh
    const OTHER = 3; // Thong tin khac
}

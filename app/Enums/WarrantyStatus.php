<?php


namespace App\Enums;

use MyCLabs\Enum\Enum;

class WarrantyStatus extends Enum
{
	const WAIT_HANDLE = 0;
	const CHECKING = 1;
	const UNDER_WARRANTY = 2;
	const PROCESSED = 3;
	const RETURNED = 4;
	const CANCELED = 5;
}

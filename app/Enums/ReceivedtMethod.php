<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class ReceivedtMethod extends Enum
{
    const SHIP = 1; // Giao hang
    const STORE = 2; // Nhan tai cua hang
}

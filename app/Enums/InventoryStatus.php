<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class InventoryStatus extends Enum
{
    const IN_WAREHOUSE = 1;
    const TEMP_EXPORTED = 2;
    const EXPORTED = 3;
}

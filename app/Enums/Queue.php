<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class Queue extends Enum
{
    const DEFAULT = 'default';
    const HIGH = 'high';
    const LOW = 'low';
}

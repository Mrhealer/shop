<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class OrderAction extends Enum
{
    const UPDATE_STATUS = 1;
    const EXPORT_INVENTORY = 2;

}

<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class InventoriesHasSerial extends Enum
{
    const TRUE = 1;
    const FALSE = 2;
}

<?php


namespace App\Enums;


class TicketPriority extends \MyCLabs\Enum\Enum
{
	const LOW = 0;
	const MEDIUM = 1;
	const HIGHT = 2;
}
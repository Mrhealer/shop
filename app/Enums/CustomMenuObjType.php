<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class CustomMenuObjType extends Enum
{
    const PRODUCT_CATEGORY = 1;
    const CUSTOM_LINK = 4;
}

<?php


namespace App\Enums;


class ProductWarrantyStatus extends \MyCLabs\Enum\Enum
{
	const PENDING = 0;
	const DELIVERY_TECH = 1;
	const TECH_PROCESSING = 2;
	const SUPPLY_UNIT_PROCESSING = 3;
	const WAITING_CUSTOMER_RECEIVE = 4;
	const SUCCESS = 5;
	const CANCELED = 6;
}
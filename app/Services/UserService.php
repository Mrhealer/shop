<?php

namespace App\Services;

use App\Enums\UserRole;
use App\Models\FbAccount;
use App\Models\Order;
use App\Models\Ticket;
use App\Models\TicketMessage;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserService
{

    /**
     * @param $params
     * @return mixed
     */
    public function searchUser($params)
    {
        $query = User::with('fbAccount', 'roles');
        if(!empty($params['id'])) {
            $query->where('id', $params['id']);
        }
        if(!empty($params['email'])) {
            $query->where('email', $params['email']);
        }
        if(!empty($params['phone'])) {
            $query->where('phone', $params['phone']);
        }
        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])
                    ->orWhere('email', 'like', '%' . $params['search'] . '%')
                    ->orWhere('phone', 'like', '%' . $params['search'] . '%')
                    ->orWhere('name', 'like', '%' . $params['search'] . '%');
            });
        }
        if(!empty($params['except_id'])) {
            $query->where('id', '<>', $params['except_id']);
        }
        return $query->orderBy('id', 'DESC')->paginate(config('app.pagination'));
    }
    /**
     * @param $attributes
     * @return mixed
     */
    public function createUser(array $attributes)
    {
        if (empty($attributes['password'])) {
            $attributes['password'] = Hash::make(Str::random(16));
        } else {
            $attributes['password'] = Hash::make($attributes['password']);
        }

        return DB::transaction(function () use ($attributes) {
            $user = new User($attributes);
            $user->save();

	        if(!empty($attributes['role_ids'])) {
		        $user->roles()->sync($attributes['role_ids']);
	        }
	        return $user->load('roles');
        });
    }

    /**
     * @param $fbUser
     * @return mixed
     */
    public function registerUserByFacebook($fbUser)
    {
        $fbAccount = new FbAccount([
            'fid' => $fbUser->id,
            'name' => $fbUser->name,
            'image' => $fbUser->avatar,
        ]);
        $fbAccount->save();

        $attributes = [
            'name' => $fbUser->name,
            'email' => $fbUser->email ?? null,
            'phone' => $fbUser->email ?? null,
            'image' => $fbUser->avatar,
            'role' =>  UserRole::NORMAL,
            'password' => Str::random(20),
            'fb_account_id' => $fbAccount->id
        ];
        return $this->createUser($attributes);
    }

    /**
     * @param User $user
     * @param array $attributes
     * @return mixed
     */
    public function updateUser(User $user, array $attributes)
    {
        $currentUser = Auth::user();

        if($currentUser->id === $user->id) {
            unset($attributes['password']);
        } else {
            if (empty($attributes['password'])) {
                unset($attributes['password']);
            } else {
                $attributes['password'] = Hash::make($attributes['password']);
            }
        }

        return DB::transaction(function () use ($attributes, $user) {
            $user->update($attributes);

	        if(!empty($attributes['role_ids'])) {
		        $user->roles()->sync($attributes['role_ids']);
	        }

	        return $user->load('roles');
        });
    }

    /**
     * @param  User  $user
     * @return mixed
     */
    public function deleteUser(User $user)
    {
        $order = Order::where('user_id', $user->id)->first();
        if(!empty($order)) {
            abort(400, 'Người dùng này đã có đơn hàng, vui lòng không được xóa.');
        }

        $ticket = Ticket::where('user_id', $user->id)->first();
        if(!empty($ticket)) {
            abort(400, 'Người dùng này đã có ticket, vui lòng không được xóa.');
        }

        $ticketMessage = TicketMessage::where('user_id', $user->id)->where('is_customer', true)->first();
        if(!empty($ticketMessage)) {
            abort(400, 'Người dùng này đã có xử lý ticket, vui lòng không được xóa.');
        }

        return DB::transaction(function () use ($user) {
	        $user->roles()->sync([]);
            return $user->delete();
        });
    }

}

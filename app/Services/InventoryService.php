<?php

namespace App\Services;

use App\Enums\InventoryStatus;
use App\Enums\OrderAction;
use App\Enums\OrderStatus;
use App\Helpers\Helper;
use App\Imports\Admin\InventoriesImport;
use App\Imports\Admin\SerialImport;
use App\Models\Admin;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\OrderHistory;
use App\Models\Product;
use App\Models\User;
use App\Models\Warehouse;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Enums\InventoriesHasSerial;

class InventoryService
{

	/**
	 * @param $params
	 * @return mixed
	 */
	public function searchInventory($params)
	{
	    $ids = Inventory::select(DB::raw('max(id) as id'))
            ->where('is_deleted', false)
            ->groupBy('serial')
            ->get()
            ->pluck('id')
            ->toArray();

        $query = Inventory::with('product', 'order', 'warehouse', 'supply')
            ->whereIn('id', $ids)
            ->whereIsDeleted(false);

		if (!empty($params['include_exported'])) {
			$query->whereNotNull('exported_at');
		}

        if (!empty($params['status'])) {
            $query->where('status', $params['status']);
        }

		if (!empty($params['id'])) {
			$query->where('id', $params['id']);
		}

		if (!empty($params['serial'])) {
			$query->where('serial', $params['serial']);
		}

		if (!empty($params['product_id'])) {
			$query->where('product_id', $params['product_id']);
		}

		if (!empty($params['import_to_warehouse_id'])) {
			$query->where('import_to_warehouse_id', $params['import_to_warehouse_id']);
		}

		if (!empty($params['search'])) {
			$query->where(function ($query) use ($params) {
				$query->where('id', $params['search'])
					->orWhere('serial', 'like', '%' . $params['search'] . '%');
			});
		}

		if (!empty($params['except_id'])) {
			$query->where('id', '<>', $params['except_id']);
		}

		return $query->orderBy('id', 'DESC')->paginate(config('app.medium_pagination'));
	}


	/**
	 * @param array $inventoryAttributes
	 * @param string $type // import|export_to_warehouse
	 * @return mixed
	 */
	public function createInventory(array $inventoryAttributes, $type = 'import')
	{
		return DB::transaction(function () use ($inventoryAttributes, $type) {
			$inventory = new Inventory($inventoryAttributes);
			// Always must be have serial number
			$inventory->has_serial = InventoriesHasSerial::TRUE;

            $admin = admin();

			// Create inventory
            $inventory->import_admin_id = $admin->id;
            $inventory->status = InventoryStatus::IN_WAREHOUSE;
			$inventory->save();

			// Update quantity_goods of warehouse
			$warehouseService = new WarehouseService();

			$warehouse = Warehouse::find($inventory->import_to_warehouse_id);
			$warehouseService->updateWarehouse($warehouse, ['quantity_goods' => DB::raw('quantity_goods + 1')]);

			if ($type == 'import') {
				// Update qty_in_warehouses of product
				$productService = new ProductService();

				$product = Product::find($inventory->product_id);
				$productService->updateProduct($product, ['qty_in_warehouses' => DB::raw('qty_in_warehouses + 1')]);
			}

			return true;
		});
	}

	/**
	 * @param Inventory $inventory
	 * @param array $inventoryAttributes
	 * @return mixed
	 */
	public function updateInventory(Inventory $inventory, array $inventoryAttributes)
	{
        if (!empty($inventory->exported_at)) {
            abort(422, 'Bạn không thể cập nhật thông tin của hàng đã xuất');
        }

		return DB::transaction(function () use ($inventoryAttributes, $inventory) {
			$warehouseService = new WarehouseService();
			$productService = new ProductService();

			// Update quantity_goods of warehouse
			if ($inventoryAttributes['import_to_warehouse_id'] !== $inventory->import_to_warehouse_id) {
				$oldWarehouse = Warehouse::find($inventory->import_to_warehouse_id);
				$warehouseService->updateWarehouse($oldWarehouse, ['quantity_goods' => DB::raw('quantity_goods - 1')]);

				$newWarehouse = Warehouse::find($inventoryAttributes['import_to_warehouse_id']);
				$warehouseService->updateWarehouse($newWarehouse, ['quantity_goods' => DB::raw('quantity_goods + 1')]);
			}

			// Update qty_in_warehouses of product
			if ($inventoryAttributes['product_id'] !== $inventory->product_id) {
				$oldProduct = Product::find($inventory->product_id);
				$productService->updateProduct($oldProduct, ['qty_in_warehouses' => DB::raw('qty_in_warehouses - 1')]);

				$newProduct = Product::find($inventoryAttributes['product_id']);
				$productService->updateProduct($newProduct, ['qty_in_warehouses' => DB::raw('qty_in_warehouses + 1')]);
			}

			$inventory->update($inventoryAttributes);

			return $inventory->load('product', 'warehouse');
		});
	}

	/**
	 * @param Order $order
	 * @param Admin $admin
	 * @param array $params
	 * @return mixed
	 */
	public function exportInventoryByOrder(Order $order, Admin $admin, array $params)
	{
		// Check order status
		if (!in_array($order->status, [OrderStatus::PENDING, OrderStatus::PROCESSING])) {
			return abort('403', 'Đơn hàng này không hợp lệ, vui lòng chọn đơn hàng khác!');
		}

		// Check order history
		$orderService = new OrderService(new UserService());
        $orderExportedInventory = $orderService->checkExportedInventoryByOrder($order->id);
		if ($orderExportedInventory) {
			return abort('403', 'Đơn hàng này đã được xuất hàng, vui lòng chọn đơn hàng khác!');
		}
		// Check order history

		$products = $order->orderProducts()->get();
		$productIds = [];
		$totalInventories = 0;
		$orderProductQty = [];
		foreach ($products as $product) {
			$productIds[] = $product->product_id;
			$totalInventories += $product->quantity;
			$orderProductQty[$product->product_id] = $product->quantity;
		}

		$inventorySerials = array_unique($params['inventories']);
		$inventories = Inventory::whereIn('serial', $inventorySerials)
			->where('exported_at', null)
			->where('quantity', 1)
			->where('is_deleted', false)
			->whereIn('product_id', $productIds)
			->get();

		if (count($inventories) === 0) {
			return abort('403', 'Serial nhập vào không đúng');
		}

		if ($totalInventories !== count($inventories)) {
			return abort('403', 'Số lượng serial nhập vào không khớp với đơn hàng');
		}

		$inputProductQty = [];
		foreach ($inventories as $inventory) {
			if (!in_array($inventory->product_id, $productIds, true)) {
				return abort('403', 'Serial ' . $inventory->serial . ' không hợp lệ');
			}
			if (($key = array_search($inventory->serial, $inventorySerials, true)) !== false) {
				unset($inventorySerials[$key]);
			}

			if (empty($inputProductQty[$inventory->product_id])) {
				$inputProductQty[$inventory->product_id] = 1;
			} else {
				$inputProductQty[$inventory->product_id] += 1;
			}
		}

		if (!empty($inventorySerials)) {
			return abort('403', 'Serial không hợp lệ: ' . implode(',', $inventorySerials));
		}

		// Check qty of products
		foreach ($productIds as $productId) {
			if ($orderProductQty[$productId] !== $inputProductQty[$productId]) {
				return abort('403', 'Những serial nhập vào không khớp với sản phẩm của đơn hàng');
			}
		}

		// Check export price of serials
//        $inventorySerialsHaveNotExportPrice = $inventories->whereIn('export_price', [null, 0])->pluck('serial')->toArray();
//        if(!empty($inventorySerialsHaveNotExportPrice)) {
//            return abort('403', 'Vui lòng nhập giá xuất cho những serial này:</br> <b>'. implode('</br>', $inventorySerialsHaveNotExportPrice) .'</b>');
//        }

		$inventoryClassAIds = $inventoryClassB = $orderInventories = [];
		$dataUpdateWarehouse = $dataUpdateProduct = [];
		foreach ($inventories as $inventory) {
			if ($inventory->has_serial) {
				$inventoryClassAIds[] = $inventory->id;

				// Build data to update warehouse
				if (empty($dataUpdateWarehouse[$inventory->import_to_warehouse_id])) {
					$dataUpdateWarehouse[$inventory->import_to_warehouse_id] = [
						'id' => $inventory->import_to_warehouse_id,
						'qty' => 1
					];
				} else {
					$dataUpdateWarehouse[$inventory->import_to_warehouse_id]['qty'] += 1;
				}

				// Build data to update product
				if (empty($dataUpdateProduct[$inventory->product_id])) {
					$dataUpdateProduct[$inventory->product_id] = [
						'id' => $inventory->product_id,
						'qty' => 1
					];
				} else {
					$dataUpdateProduct[$inventory->product_id]['qty'] += 1;
				}
			} else {
				$inventoryClassB[] = $inventory;
				$orderInventories[] = [
					'inventory_id' => $inventory->id,
					'order_id' => $order->id,
					'exported_at' => now(),
				];
			}
		}

		return DB::transaction(function () use (
			$order,
			$admin,
			$inventoryClassAIds,
			$inventoryClassB,
			$orderInventories,
			$dataUpdateProduct,
			$params
		) {
			// Create order history
			$orderHistory = new OrderHistory([
				'order_id' => $order->id,
				'admin_id' => $admin->id,
				'action' => OrderAction::EXPORT_INVENTORY,
				'status' => OrderStatus::PROCESSING,
                'payment_status' => $order->payment_status,
                'note' => $params['export_note']
			]);
			$orderHistory->save();

            $oldOrder = $order->toArray();
			// Update order status
			$order->update(['status' => OrderStatus::PROCESSING]);

			// Update inventories
			if (!empty($inventoryClassAIds)) {
				Inventory::whereIn('id', $inventoryClassAIds)->update([
					'export_admin_id' => $admin->id,
					'export_note' => $params['export_note'],
					'order_id' => $order->id,
					'exported_at' => now(),
                    'status' => InventoryStatus::TEMP_EXPORTED,
				]);
			}

			// Begin: For case has not serial
			DB::table('order_inventory')->insert($orderInventories);

			foreach ($inventoryClassB as $inventory) {
				$inventory->quantity--;
				$inventory->save();
			}
			// End: For case has not serial

            $discordMsg = Helper::getDiscordMsgTrackingExportInventoryOfOrder($oldOrder, $order->toArray(), $params['export_note']);
            if(!empty($discordMsg)) {
                Helper::discordNotify(config('discord-config.sale_chanel'), $discordMsg);
            }

            return $order->load(
                [
                    'histories' => function ($query) {
                        $query->with('admin');
                    }
                ]
            );
		});
	}

	/**
	 * @param Warehouse $exportToWarehouse
	 * @param User $user
	 * @param array $params
	 * @return mixed
	 */
	public function exportInventoryToWarehouse(Warehouse $exportToWarehouse, Admin $admin, array $params)
	{
		$inputInventorySerials = array_unique($params['inventories']);
		$inventories = Inventory::whereIn('serial', $inputInventorySerials)
			->where('exported_at', null)
			->where('quantity', 1)
			->where('is_deleted', false)
			->where('import_to_warehouse_id', '<>', $exportToWarehouse->id)
			->get();

		if (count($inventories) === 0) {
			return abort('403', 'Serial nhập vào không hợp lệ.');
		}

		// Check input serials
		$inventorySerials = $inventories->pluck('serial')->toArray();
		$invalidSerials = array_diff($inputInventorySerials, $inventorySerials);
		if (!empty($invalidSerials)) {
			return abort('403', 'Những serial này không hợp lệ: <b>' . implode('</br>',
					$invalidSerials) . '</b></br> Vui lòng nhâp lại!');
		}

		$inventoryIdsToUpdate = [];
		$dataUpdateOldWarehouse = [];
		foreach ($inventories as $inventory) {
			$inventoryIdsToUpdate[] = $inventory->id;

			// Build data to update warehouse
			if (empty($dataUpdateOldWarehouse[$inventory->import_to_warehouse_id])) {
				$dataUpdateOldWarehouse[$inventory->import_to_warehouse_id] = [
					'id' => $inventory->import_to_warehouse_id,
					'qty' => 1
				];
			} else {
				$dataUpdateOldWarehouse[$inventory->import_to_warehouse_id]['qty'] += 1;
			}
		}

		return DB::transaction(function () use (
			$exportToWarehouse,
			$admin,
			$inventoryIdsToUpdate,
			$dataUpdateOldWarehouse,
			$inventories,
			$params
		) {
			// Update inventories
			if (!empty($inventoryIdsToUpdate)) {
				Inventory::whereIn('id', $inventoryIdsToUpdate)->update([
					'export_admin_id' => $admin->id,
					'export_note' => $params['export_note'],
					'export_to_warehouse_id' => $exportToWarehouse->id,
					'exported_at' => now(),
                    'status' => InventoryStatus::EXPORTED
				]);
			}

			// Update quantity_goods of old warehouses
			foreach ($dataUpdateOldWarehouse as $item) {
				Warehouse::where('id',
					$item['id'])->update(['quantity_goods' => DB::raw('quantity_goods - ' . $item['qty'])]);
			}

			// Add inventory
			foreach ($inventories as $inventory) {
				$this->createInventory([
					'product_id' => $inventory->product_id,
					'supply_unit_id' => $inventory->supply_unit_id,
					'import_to_warehouse_id' => $exportToWarehouse->id,
					'import_admin_id' => $admin->id,
					'serial' => $inventory->serial,
					'import_price' => $inventory->import_price,
					'brand_warranty_period' => $inventory->brand_warranty_period,
					'warranty_period' => $inventory->warranty_period
				], 'export_to_warehouse');
			}

			return true;
		});
	}

	/**
	 * @param $file
	 * @return \Maatwebsite\Excel\Excel
	 */
	public function importExcelInventory($file)
	{
		return Excel::import(new InventoriesImport, $file);
	}

    /**
     * @param $file
     * @return array
     */
    public function importExcelSerial($file)
    {
        $result = Excel::toArray([], $file)[0];

        $serialList = [];
        foreach ($result as $index => $item) {
            if ($index == 0) {
                continue;
            }

            if (isset($item[1])) {
                $serialList[] = (string)$item[1];
            }
        }

        return $serialList;
    }

    /**
     * Check inventory is enough by order
     *
     * @param Order $order
     *
     * @return array
     */
	public function checkInventoryIsEnoughByOrder(Order $order) {
	    $res = true;

        $products = $order->orderProducts()->get();
        $productIds = [];
        $orderProductQty = [];
        foreach ($products as $product) {
            $productIds[] = $product->product_id;
            $orderProductQty[$product->product_id] = $product->quantity;
        }

        $productInventories = Inventory::select('product_id', DB::raw('sum(quantity) as qty'))
            ->where('exported_at', null)
            ->where('quantity', 1)
            ->where('is_deleted', false)
            ->whereIn('product_id', $productIds)
            ->groupBy('product_id')
            ->get();

        $productInventoriesKeyed = $productInventories->keyBy('product_id')->toArray();

        $orderProductsQtyRes = [];
        foreach ($productIds as $productId) {
            if(empty($productInventoriesKeyed[$productId])) {
                $res = false;
                $orderProductsQtyRes[$productId]['goods_missing'] = $orderProductQty[$productId];
                $orderProductsQtyRes[$productId]['inventories'] = 0;
            } else {
                $orderProductsQtyRes[$productId]['inventories'] = $productInventoriesKeyed[$productId]['qty'];
                if($productInventoriesKeyed[$productId]['qty'] < $orderProductQty[$productId]) {
                    $res = false;
                    $orderProductsQtyRes[$productId]['goods_missing'] = $orderProductQty[$productId] - $productInventoriesKeyed[$productId]['qty'];
                } else {
                    $orderProductsQtyRes[$productId]['goods_missing'] = 0;
                }
            }
        }

        if (!$res) {
            return [
                'status' => $res,
                'order_products_qty' => $orderProductsQtyRes
            ];
        }

        return ['status' => $res];
    }
}

<?php


namespace App\Services;


use App\Enums\TicketStatus;
use App\Models\TicketCategory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class TicketCategoryService
{

	public function searchTicketCategory(array $params)
	{
		$query = TicketCategory::withCount([
			'tickets',
			'tickets as open_ticket_count' => function (Builder $query) {
				$query->where('status', TicketStatus::OPEN);
			},
			'tickets as closed_ticket_count' => function (Builder $query) {
				$query->where('status', TicketStatus::CLOSED);
			},
			'tickets as answered_ticket_count' => function (Builder $query) {
				$query->where('status', TicketStatus::ANSWERED);
			}
		]);
		if (!empty($params['id'])) {
			$query->where('id', $params['id']);
		}
		if (!empty($params['status'])) {
			$query->where('status', $params['status']);
		}
		if (!empty($params['search'])) {
			$query->where(function ($query) use ($params) {
				$query->where('id', $params['search'])
					->orWhere('name', 'like', '%' . $params['search'] . '%');
			});
		}

		return $query->paginate(config('app.pagination'));
	}


	public function createTicketCategory(array $attributes)
	{
		return DB::transaction(function () use ($attributes) {
			$category = new TicketCategory($attributes);
			$category->save();
			return $category;
		});
	}

	public function updateTicketCategory(TicketCategory $ticketCategory, array $attributes)
	{
		return DB::transaction(function () use ($ticketCategory, $attributes) {
			$ticketCategory->update($attributes);
			return $ticketCategory;
		});
	}


	public function deleteTicketCategory(TicketCategory $ticketCategory)
	{
		return DB::transaction(function () use ($ticketCategory) {
			return $ticketCategory->delete();
		});
	}
}
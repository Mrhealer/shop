<?php


namespace App\Services;


use App\Enums\TicketStatus;
use App\Helpers\Helper;
use App\Jobs\SendDiscordNotification;
use App\Jobs\SendTicketMailJob;
use App\Models\Ticket;
use App\Models\TicketCategory;
use App\Models\TicketMessage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class TicketService
{
	public function searchTicket(array $params)
	{
		$query = Ticket::with('category', 'user', 'admin');

		if (isset($params['id'])) {
			$query->where('id', $params['id']);
		}
		if (isset($params['ticket_category_id'])) {
			$query->where('ticket_category_id', $params['ticket_category_id']);
		}
		if (isset($params['status'])) {
			$query->where('status', $params['status']);
		}
		if (isset($params['user_id'])) {
			$query->where('user_id', $params['user_id']);
		}
		if (isset($params['admin_id'])) {
			$query->where('admin_id', $params['admin_id']);
		}
		if (isset($params['priority'])) {
			$query->where('priority', $params['priority']);
		}
		if (isset($params['search'])) {
			$query->where(function ($query) use ($params) {
				$query->where('id', $params['search'])
					->orWhere('title', 'like', '%' . $params['search'] . '%');
			});
		}
		return $query->orderByDesc('created_at')->paginate(20);
	}


	public function searchTicketMessage(array $params)
	{
		$query = TicketMessage::with('ticket', 'user', 'admin');
		if (isset($params['id'])) {
			$query->where('id', $params['id']);
		}
		if (isset($params['status'])) {
			$query->where('status', $params['status']);
		}
		if (isset($params['search'])) {
			$query->where(function ($query) use ($params) {
				$query->where('id', $params['search'])
					->orWhere('name', 'like', '%' . $params['search'] . '%');
			});
		}

		return $query->paginate(config('app.pagination'));
	}

	public function createTicket(array $attributes)
	{
		return DB::transaction(function () use ($attributes) {
			$ticket = new Ticket($attributes['ticket']);
			$ticket->save();
			$ticket->messages()->create($attributes['message']);
			$ticket->load(['category', 'user','admin']);
			SendDiscordNotification::dispatch($ticket->category->discord_webhook,
				'Có yêu cầu hỗ trợ mới : ***#' . $ticket->id . '*** từ khách hàng *' . $ticket->user->name .
				'* đang chờ xử lý. Vui lòng [Kiểm tra](' . route('admin.ticket.index',
					['id' => $ticket->id]) . ') !')->onQueue('default');
			SendTicketMailJob::dispatch($ticket,
				'Chúng tôi vừa nhận được yêu cầu hỗ trợ ***' . $ticket->title . '*** từ quý khách ! \n Chúng tôi sẽ sớm xử lý yêu cầu này!')->onQueue('high');
			return $ticket;
		});
	}


	public function createTicketMessage(Ticket $ticket, array $attributes)
	{
		return DB::transaction(function () use ($ticket, $attributes) {
			$ticket->messages()->create($attributes);
			$ticket->load([
				'category',
				'user',
				'admin',
				'messages' => function ($query) {
					$query->orderByDesc('created_at');
				},
				'messages.user',
				'messages.admin'
			]);
			if ($attributes['is_customer']) {
				
				$ticket->update([
					'status' => TicketStatus::OPEN,
					'completed_at' => null,
				]);

				SendDiscordNotification::dispatch($ticket->category->discord_webhook,
					'Khách hàng đã trả lời ticket: ***#' . $ticket->id . '*** . Vui lòng [Kiểm tra ngay](' . route('admin.ticket.index',
						['id' => $ticket->id]) . ') !')->onQueue('default');

			} else {
				$ticket->update([
					'status' => TicketStatus::ANSWERED
				]);

				SendTicketMailJob::dispatch($ticket,
					'Chúng tôi vừa trả lời yêu cầu hỗ trợ ***#' . $ticket->id . '*** ! Vui lòng kiểm tra!')->onQueue('default');

			}
			return $ticket;
		});
	}


	public function updateTicket(Ticket $ticket, array $attributes)
	{	

		return DB::transaction(function () use ($ticket, $attributes) {
			if (isset($attributes['status'])){
				if ($attributes['status'] === TicketStatus::WAIT_CLOSE){

					$attributes['completed_at'] = Carbon::now()->format('Y-m-d H:i:s');

					$message = [];

					$message['admin_id'] = admin()->id;
					$message['is_customer'] = false;
					$message['message'] = $ticket->category->close_message_default;

					$this->createTicketMessage($ticket, $message);

				}	

				if ($attributes['status'] === TicketStatus::CLOSED){

					$attributes['completed_at'] = Carbon::now()->format('Y-m-d H:i:s');
				}			
			}	

			$ticket->update($attributes);
			return $ticket->load('messages', 'user',
				'admin','category');
		});
	}


	public function updateTicketMessage(TicketMessage $ticketMessage, array $attributes)
	{
		return DB::transaction(function () use ($ticketMessage, $attributes) {
			$ticketMessage->update($attributes);
			return $ticketMessage;
		});
	}


	public function deleteTicket(Ticket $ticket)
	{
		return DB::transaction(function () use ($ticket) {
			$ticket->delete();
			return $ticket;
		});
	}


	public function deleteTicketMessage(TicketMessage $ticketMessage)
	{
		return DB::transaction(function () use ($ticketMessage) {
			return $ticketMessage->delete();
		});
	}
}
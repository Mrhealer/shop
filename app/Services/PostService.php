<?php

namespace App\Services;

use App\Helpers\SlugHelper;
use App\Models\Post;
use Illuminate\Support\Facades\DB;

class PostService
{

    /**
     * @param $params
     * @return mixed
     */
    public function searchPost($params)
    {
        $query =  Post::with('category')->whereIsDeleted(false);
        if(!empty($params['id'])) {
            $query->where('id', $params['id']);
        }
        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])
                    ->orWhere('name', 'like', '%' . $params['search'] . '%');
            });
        }
        if(!empty($params['except_id'])) {
            $query->where('id', '<>', $params['except_id']);
        }
        return $query->paginate(config('app.pagination'));
    }

    /**
     * @param array $postAttributes
     * @return mixed
     */
    public function createPost(array $postAttributes)
    {
        return DB::transaction(function () use ($postAttributes) {
            $post = new Post($postAttributes);
            $post->save();
            $post->slug = SlugHelper::genSlug($post->name).'-'.$post->id;
            $post->save();
            return $post->load('category');
        });
    }

    /**
     * @param Post $post
     * @param array $postAttributes
     * @return mixed
     */
    public function updatePost(Post $post, array $postAttributes)
    {
        return DB::transaction(function () use ($postAttributes, $post) {
            $post->update($postAttributes);
            return $post->load('category');
        });
    }

    /**
     * @param Post $post
     * @return mixed
     */
    public function deletePost(Post $post)
    {
        return DB::transaction(function () use ($post) {
            $post->is_deleted = true;
            $post->save();
            return $post;
        });
    }
}

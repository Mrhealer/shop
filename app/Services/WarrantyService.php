<?php


namespace App\Services;


use App\Enums\ProductWarrantyStatus;
use App\Helpers\Helper;
use App\Helpers\SlugHelper;
use App\Models\Warranty;
use App\Models\WarrantyProduct;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class WarrantyService
{
	public function searchWarranty($params)
	{
		$query = Warranty::with(['products.product','products.technician', 'products.order', 'histories', 'products.inventory', 'products.inventory.order']);
		if (isset($params['id'])) {
			$query->where('id', $params['id']);
		}
		if (isset($params['search'])) {
			$query->where(function ($query) use ($params) {
				$query->where('id', $params['search'])->orWhere('name', 'like', '%' . $params['search'] . '%');
			});
		}
		if (isset($params['except_id'])) {
			$query->where('id', '<>', $params['except_id']);
		}
		if(isset($params['status'])) {
			$query->where('status', $params['status']);
		}
		return $query->orderBy('id', 'asc')->paginate(config('app.pagination'));
	}

	/**
	 * @param array $parametes
	 * @return mixed
	 */
	public function createWarranty(array $parametes)
	{
		$warrantyAttributes = [
			'customer_name' => $parametes['customer_name'],
			'customer_email' => $parametes['customer_email'],
			'customer_phone' => $parametes['customer_phone'],
			'customer_address' => $parametes['customer_address'],
			'images' => $parametes['images'],
			'status' => $parametes['status'],
		];

		$products = [];

		foreach ($parametes['products'] as $product){
			$products[] = Arr::except($product, ['product','technician', 'order', 'product_name']);
		}

		return DB::transaction(function () use ($warrantyAttributes, $products) {
			$warranty = new Warranty($warrantyAttributes);
			$warranty->save();
			$warranty->products()->createMany($products);
			$warranty->histories()->create([
				'action' => 'Khởi tạo Phiếu bảo hành'
			]);
			return $warranty->load(['products.product','products.order','products.technician', 'histories']);
		});
	}

	/**
	 * @param Warranty $warranty
	 * @param array $parametes
	 * @return mixed
	 */
	public function updateWarranty(Warranty $warranty, array $parametes)
	{

		$action = $parametes['action'];

		$warrantyAttributes = [
			'customer_name' => $parametes['customer_name'],
			'customer_email' => $parametes['customer_email'],
			'customer_phone' => $parametes['customer_phone'],
			'customer_address' => $parametes['customer_address'],
			'images' => $parametes['images'],
			'status' => $parametes['status'],
		];

		$products = [];

		foreach ($parametes['products'] as $product){
			$products[] = Arr::except($product, ['product','technician', 'order', 'status_name']);
		}

		return DB::transaction(function () use ($warrantyAttributes, $products,$warranty,$action) {
			$warranty->update($warrantyAttributes);

			foreach ($products as $item){
				$product = WarrantyProduct::find($item['id']);
				$product->update($item);

				if ($product->status === ProductWarrantyStatus::DELIVERY_TECH) {
					Helper::discordNotify(config('discord-config.warranty_chanel'),
						'Yêu cầu bảo hành : *#' . $product->warranty_id .
						'* có sản phẩm *'. $product->product->name
						.'* đang chờ kỹ thuật *'. $product->technician->name
						.'* xử lý. Vui lòng kiểm tra! [Chi tiết]('.route('admin.warranty_index', ['id' => $product->warranty_id]).')');
				}

			}

			$warranty->histories()->create([
				'action' => $action
			]);
			return $warranty->load(['products.product','products.technician', 'histories']);
		});
	}

	/**
	 * @param Warranty $warranty
	 * @return mixed
	 */
	public function deleteWarranty(Warranty $warranty)
	{
		return DB::transaction(function () use ($warranty) {
			$warranty->is_deleted = true;
			$warranty->save();
			return $warranty;
		});
	}


	public function searchProduct($params)
	{
		$query = WarrantyProduct::with('inventory','product');
		if (isset($params['id'])) {
			$query->where('id', $params['id']);
		}
		if (isset($params['search'])) {
			$query->where(function ($query) use ($params) {
				$query->where('id', $params['search'])->orWhere('name', 'like', '%' . $params['search'] . '%');
			});
		}
		if (isset($params['except_id'])) {
			$query->where('id', '<>', $params['except_id']);
		}
		return $query->orderBy('_lft', 'asc')->paginate(config('app.pagination'));
	}

	/**
	 * @param array $productAttributes
	 * @return mixed
	 */
	public function createProduct(array $productAttributes)
	{
		return DB::transaction(function () use ($productAttributes) {
			$warrantyProduct = new WarrantyProduct($productAttributes);
			$warrantyProduct->save();
			return $warrantyProduct;
		});
	}

	/**
	 * @param WarrantyProduct $warrantyProduct
	 * @param array $productAttributes
	 * @return mixed
	 */
	public function updateProduct(WarrantyProduct $warrantyProduct, array $productAttributes)
	{
		return DB::transaction(function () use ($warrantyProduct, $productAttributes) {
			$warrantyProduct->update($productAttributes);
			return $warrantyProduct;
		});
	}

	/**
	 * @param WarrantyProduct $warrantyProduct
	 * @return mixed
	 */
	public function deleteProduct(WarrantyProduct $warrantyProduct)
	{
		return DB::transaction(function () use ($warrantyProduct) {
			$warrantyProduct->save();
			return $warrantyProduct;
		});
	}
}
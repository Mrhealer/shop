<?php
namespace App\Services;

use App\Models\AttributeValue;

class AttributeValueService
{
    /**
     * @param $params
     * @return mixed
     */
    public function searchAttributeValue($params)
    {
        $query = AttributeValue::orderBy('id', 'DESC');

        if(!empty($params['id'])) {
            $query->where('id', $params['id']);
        }

        if(isset($params['pagination']) && $params['pagination'] == 'false') {
            return $query->get();
        }

        return $query->paginate(config('app.pagination'));
    }
}

<?php

namespace App\Services;

use App\Models\Role;
use Illuminate\Support\Facades\DB;

class RoleService
{

    public function createRole(array $attributes)
    {
        return DB::transaction(function () use ($attributes) {
            $role = new Role($attributes);
            $role->save();
            $role->permissions()->sync($attributes['permission_ids']);

            return $role->load('permissions');
        });
    }

    public function updateRole(Role $role, array $attributes)
    {
        return DB::transaction(function () use ($attributes, $role) {
            $role->update($attributes);
            $role->permissions()->sync($attributes['permission_ids']);

            return $role->load('permissions');
        });
    }

    public function deleteRole(Role $role)
    {
        return DB::transaction(function () use ($role) {
            return $role->delete();
        });
    }

}

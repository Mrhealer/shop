<?php

namespace App\Services;

use App\Repositories\Files\Interfaces\FileRepositoryInterface;
use Illuminate\Http\Request;


class FileService
{

	private $_repository;

	public function __construct(FileRepositoryInterface $repository)
	{
		$this->_repository = $repository;
	}

	public function uploadAnonymous(Request $request)
	{
		$data = [];
		$file = $request->file('upload');

		if ($file->isValid()) {
			$path = $this->_repository->upload($this->_repository->getSharePath(), $file);
			$url = $this->_repository->getUrl($path);
			$name = $file->hashName();
			$data = [
				'name' => $name,
				'url' => asset($url),
			];
		}

		return $data;

	}

	public function upload(Request $request, $user)
	{
		$this->getDirectorySize($user);
		$data = [];
		$file = $request->file('upload');
		if ($file->isValid()) {
			$rootDirectory = $this->_repository->getPath($user);
			if(strpos($request->input('directory'), $rootDirectory) !== false) {
				$path = $this->_repository->upload($request->input('directory'), $file);
				$url = $this->_repository->getUrl($path);
				$name = $file->hashName();
				$data = ($request->has('editor'))
					? [
						'uploaded' => 1,
						'fileName' => $name,
						'url' => $url,
					]
					: [
						'name' => $name,
						'url' => $url,
						'path' => $path,
						'size' => $this->_repository->getSize($path),
						'last_modified' => $this->_repository->getLastModified($path),
						'is_selected' => false,

					];
			} else {
				abort(422, 'Destination directory wrong.');
			}

		} else {
			abort(422, 'File invaild.');
		}
		return $data;

	}

	public function resizeImage($files, $width, $height, $user)
	{
		$directory = $this->_repository->getPath($user);
		$files = array_map(function ($file) use($width, $height, $directory) {
			$path = $this->_repository->resizeImage($file['path'], $width, $height);
			return [
				'name' => str_replace($directory . '/', '', $path),
				'url' => $this->_repository->getUrl($path),
				'path' => $path,
				'size' => $this->_repository->getSize($path),
				'mime' => $this->_repository->getMimeType($path),
				'last_modified' => $this->_repository->getLastModified($path),
				'is_selected' => false,
			];
		}, $files);
		return $files;
	}

	public function delete($files, $directories)
	{
		$files = array_map(function ($file) {
			return $file['path'];
		}, $files);

		if(!empty($files)) {
			$this->_repository->delete($files);
		}

		foreach ($directories as $directory) {
			$this->_repository->deleteDirectory($directory['path']);
		}
		return true;
	}

	public function makeDirectory($params, $user)
	{
		$rootDirectory = $this->_repository->getPath($user);

		if(strpos($params['directory'], $rootDirectory) !== false) {
			$directory = $params['directory'].'/'.$params['name'];
			$this->_repository->makeDir($directory);

			return [
				'name' => $params['name'],
				'path' => $directory,
			];
		}

		return abort(403);

	}

	public function browser($user, $directory = '')
	{
		if(empty($directory)) {
			$directory = $this->_repository->getPath($user);
		}

		$directories = array_map(function ($path) {
			$separates = explode('/', $path);
			return [
				'name' => end($separates),
				'path' => $path
			];

		},
			$this->_repository->getDirectories($directory));

		$files = array_map(function ($file) use ($directory) {
			return [
				'name' => str_replace($directory . '/', '', $file),
				'url' => $this->_repository->getUrl($file),
				'path' => $file,
				'size' => $this->_repository->getSize($file),
				'mime' => $this->_repository->getMimeType($file),
				'last_modified' => $this->_repository->getLastModified($file),
				'is_selected' => false,
			];
		}, $this->_repository->getFiles($directory));

		return [
			'directory' => $directory,
			'directories' => $directories,
			'files' => $files,
		];

	}
	public function getDirectorySize($user)
	{
		$directory = $this->_repository->getPath($user);
		$files = $this->_repository->getFiles($directory);
		$size  = 0;
		foreach ($files as $file) {
			$size += $this->_repository->getSize($file);
		}
		return $size;
	}
}

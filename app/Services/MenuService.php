<?php

namespace App\Services;

use App\Enums\CustomMenuObjType;
use App\Models\CustomMenus;
use App\Models\ProdCategory;

class MenuService
{
    /**
     * @param array $menuAttributes
     * @return mixed
     */
    public function createMenu(array $menuAttributes)
    {
        $this->checkParentIdForProdCategory($menuAttributes);

        $menu = new CustomMenus($menuAttributes);
        $menu->save();

        return $menu;
    }

    /**
     * @param CustomMenus $menu
     * @param array $menuAttributes
     * @return mixed
     */
    public function updateMenu(CustomMenus $menu, array $menuAttributes)
    {
        $this->checkParentIdForProdCategory($menuAttributes);

        $menu->update($menuAttributes);

        return $menu;
    }

    /**
     * @param CustomMenus $menu
     * @return bool
     * @throws \Exception
     */
    public function deleteMenu(CustomMenus $menu)
    {
        $menu->delete();

        return true;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchMenu($params)
    {
        $query = CustomMenus::select('*', 'name as label')->orderBy('order', 'ASC');
        if (!empty($params['id'])) {
            $query->where('id', $params['id']);
        }

        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])
                    ->orwhere('name', 'like', '%' . $params['search'] . '%')
                    ->orWhere('url', 'like', '%' . $params['search'] . '%');
            });
        }

        return $query->get()->toTree();
    }

    /**
     * Check parent id for prod category
     *
     * @param $menuAttributes
     */
    private function checkParentIdForProdCategory($menuAttributes) {
        if(!empty($menuAttributes['parent_id']) && $menuAttributes['object_type'] === CustomMenuObjType::PRODUCT_CATEGORY) {
            $parent = CustomMenus::find($menuAttributes['parent_id']);
            if($parent->object_type === CustomMenuObjType::PRODUCT_CATEGORY) {
                $prodCategory = ProdCategory::find($menuAttributes['object_id']);
                if($prodCategory->parent_id !== $parent->object_id) {
                    abort(422, 'Vui lòng chọn menu cha đúng với danh mục cha.');
                }
            }
        }
    }
}

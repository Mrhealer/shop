<?php

namespace App\Services;

use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\ProductAttributeValue;
use Illuminate\Support\Facades\DB;

class AttributeService
{
    /**
     * @param $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function searchAttribute($params)
    {
        $query =  Attribute::with('values', 'categories');
        if(!empty($params['id'])) {
            $query->where('id', $params['id']);
        }

        if(!empty($params['group_type'])) {
            $query->where('group_type', $params['group_type']);
        }

        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])
                    ->orWhere('name', 'like', '%' . $params['search'] . '%');
            });
        }

        if(!empty($params['except_id'])) {
            $query->where('id', '<>', $params['except_id']);
        }

        if(!empty($params['category_id'])) {
            $query->whereHas('categories', function ($q) use($params) {
                $q->where('prod_category_id', $params['category_id']);
            });
        }

        return $query->paginate(config('app.pagination'));
    }
    /**
     * @param  array  $attributes
     * @return mixed
     */
    public function createAttribute(array $attributes)
    {
        return DB::transaction(function () use ($attributes) {
            $attribute = new Attribute($attributes);
            $attribute->save();
            $attribute->categories()->sync($attributes['prod_category_ids']);

            return $attribute->load('categories');
        });
    }

    /**
     * @param Attribute $attribute
     * @param array $attributes
     * @return mixed
     */
    public function updateAttribute(Attribute $attribute, array $attributes)
    {
        return DB::transaction(function () use ($attribute, $attributes) {
            $attribute->update($attributes);
            $attribute->categories()->sync($attributes['prod_category_ids']);

            return $attribute->load('values', 'categories');
        });
    }

    /**
     * @param Attribute $attribute
     * @return mixed
     */
    public function deleteAttribute(Attribute $attribute)
    {
        return DB::transaction(function () use ($attribute) {
            $attribute->categories()->sync([]);
            $attributeValueIds = $attribute->values()->get()->pluck('id');
            ProductAttributeValue::whereIn('attribute_value_id', $attributeValueIds)->delete();

            return $attribute->delete();
        });
    }

    /**
     * @param Attribute $attribute
     * @param array $attributes
     * @return mixed
     */
    public function createAttributeValue(Attribute $attribute, array $attributes)
    {
        return DB::transaction(function () use ($attribute, $attributes) {
            $attributeValue = $attribute->values()->newModelInstance($attributes);
            $attributeValue->save();
            return $attribute->load('values');
        });
    }

    /**
     * @param Attribute $attribute
     * @param AttributeValue $attributeValue
     * @param array $attributes
     * @return mixed
     */
    public function updateAttributeValue(Attribute $attribute, AttributeValue $attributeValue, array $attributes)
    {
        return DB::transaction(function () use ($attribute, $attributeValue, $attributes) {
            $attributeValue->update($attributes);
            return $attribute->load('values');
        });
    }

    /**
     * @param AttributeValue $attributeValue
     *
     * @return mixed
     */
    public function deleteAttributeValue(AttributeValue $attributeValue) {
        return DB::transaction(function () use ($attributeValue) {
            $attributeValue->products()->sync([]);

            return $attributeValue->delete();
        });
    }

    /**
     * @param  Attribute  $attribute
     * @return Attribute
     */
    public function getAttributeValues(Attribute $attribute)
    {
        return $attribute->load('values');
    }
}

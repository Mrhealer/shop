<?php
namespace App\Services;

use App\Helpers\SlugHelper;
use App\Models\BlogCategory;
use Illuminate\Support\Facades\DB;

class BlogCategoryService
{
    /**
     * @param $params
     * @return mixed
     */
    public function searchCategory($params)
    {
        $query = BlogCategory::with('parentCategory')->whereIsDeleted(false);
        if(!empty($params['id'])) {
            $query->where('id', $params['id']);
        }
        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])->orWhere('name', 'like', '%' . $params['search'] . '%');
            });
        }
        if(!empty($params['except_id'])) {
            $query->where('id', '<>', $params['except_id']);
        }
        return $query->orderBy('_lft', 'asc')->paginate(config('app.pagination'));
    }

    /**
     * @param  array  $categoryAttributes
     * @return mixed
     */
    public function createCategory(array $categoryAttributes)
    {
        return DB::transaction(function () use ($categoryAttributes) {
            $category = new BlogCategory($categoryAttributes);
            $category->save();
            $category->slug = SlugHelper::genSlug($category->name).'-'.$category->id;
            $category->save();
            return $category;
        });
    }

    /**
     * @param BlogCategory $category
     * @param array $categoryAttributes
     * @return mixed
     */
    public function updateCategory(BlogCategory $category, array $categoryAttributes)
    {
        return DB::transaction(function () use ($category, $categoryAttributes) {
            $category->update($categoryAttributes);
            return $category;
        });
    }

    /**
     * @param  BlogCategory  $category
     * @return mixed
     */
    public function deleteCategory(BlogCategory $category)
    {
        return DB::transaction(function () use ($category) {
            $category->is_deleted = true;
            $category->save();
            return $category;
        });
    }
}

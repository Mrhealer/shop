<?php

namespace App\Services;

use App\Models\Voucher;
use Illuminate\Support\Facades\DB;

class VoucherService
{

    /**
     * @param $params
     * @return mixed
     */
    public function searchVoucher($params)
    {
        $query =  Voucher::with('product', 'brand', 'category');
        if(!empty($params['id'])) {
            $query->where('id', $params['id']);
        }
        if(!empty($params['code'])) {
            $query->where('code', $params['code']);
        }
        if(!empty($params['product_id'])) {
            $query->where('product_id', $params['product_id']);
        }
        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])
                    ->orWhere('code', $params['search'])
                    ->orWhere('product_id', $params['search']);
            });
        }
        if(!empty($params['except_id'])) {
            $query->where('id', '<>', $params['except_id']);
        }
        return $query->paginate(config('app.pagination'));
    }

    /**
     * @param array $voucherAttribute
     * @return mixed
     */
    public function createVoucher(array $voucherAttribute)
    {
        return DB::transaction(function () use ($voucherAttribute) {
            $voucher = new Voucher($voucherAttribute);
            $voucher->save();
            return $voucher->load('product', 'brand', 'category');
        });
    }

    /**
     * @param Voucher $voucher
     * @param array $voucherAttribute
     * @return mixed
     */
    public function updateVoucher(Voucher $voucher, array $voucherAttribute)
    {
        return DB::transaction(function () use ($voucherAttribute, $voucher) {
            $voucher->update($voucherAttribute);
            return $voucher->load('product', 'brand', 'category');
        });
    }

}

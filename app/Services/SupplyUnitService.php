<?php

namespace App\Services;

use App\Imports\Admin\SupplyUnitsImport;
use App\Models\SupplyUnit;
use App\Models\Inventory;
use Maatwebsite\Excel\Facades\Excel;

class SupplyUnitService
{
    /**
     * @param array $supplyUnitAttributes
     * @return mixed
     */
    public function createSupplyUnit(array $supplyUnitAttributes)
    {
        $supplyUnit = new SupplyUnit($supplyUnitAttributes);
        $supplyUnit->save();

        return $supplyUnit;
    }

    /**
     * @param SupplyUnit $unit
     * @param array $supplyUnits
     * @return mixed
     */
    public function updateSupplyUnit(SupplyUnit $unit, array $supplyUnits)
    {
        $unit->update($supplyUnits);

        return $unit;
    }

    /**
     * @param  SupplyUnit  $unit
     * @return mixed
     */
    public function deleteSupplyUnit(SupplyUnit $unit)
    {
        // Check inventories of this supply unit
        $inventory = Inventory::where('supply_unit_id', $unit->id)->first();
        if($inventory) {
            return abort(422, 'Đơn vị cung cấp này đã có hàng nhập, vui lòng không được xóa!');
        }

        $unit->is_deleted =  true;
        $unit->save();

        return $unit;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchSupplyUnit($params)
    {
        $query = SupplyUnit::whereIsDeleted(false);

        if (!empty($params['id'])) {
            $query->where('id', $params['id']);
        }

        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])
                    ->orwhere('name', 'like', '%' . $params['search'] . '%')
                    ->orWhere('address', 'like', '%' . $params['search'] . '%')
                    ->orWhere('description', 'like', '%' . $params['search'] . '%');
            });
        }

        return $query->orderBy('id', 'DESC')->paginate(config('app.pagination'));
    }

    /**
     * @param $file
     * @return \Maatwebsite\Excel\Excel
     */
    public function importSupplyUnitExcel($file) {
        return Excel::import(new SupplyUnitsImport, $file);
    }
}

<?php

namespace App\Services;

use App\Enums\PriceRange;
use App\Enums\SortType;
use App\Enums\VoucherType;
use App\Jobs\SendDiscordNotification;
use App\Jobs\SendOrderMail;
use App\Models\Attribute;
use App\Models\AttributeCategory;
use App\Models\AttributeValue;
use App\Models\BlogCategory;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Post;
use App\Models\ProdCategory;
use App\Models\Product;
use App\Models\Voucher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ShopService
{

	/**
	 * @param $params
	 * @return Inventory|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|void|null
	 */
	public function checkWarranty($params)
	{
		if (empty($params['serial'])) {
			abort(422, 'Vui lòng nhập đầy đủ serial và mã đơn hàng.');
		}
		$inventory = Inventory::select([
			'id',
			'exported_at',
			'order_id',
			'product_id',
			'serial',
			'has_serial',
			'brand_warranty_period',
			'warranty_period'
			])->with([
				'warranties.warranty'=> function ($order) {
					$order->select('id', 'customer_name');
				},
			'order' => function ($order) {
				$order->select('id', 'name');
			},
			'product' =>
				function ($product) {
					$product->select('id', 'name', 'slug', 'sku','images');
				}
		])
			->whereIsDeleted(false)
			->whereSerial($params['serial'])
			->first();

		if (!$inventory) {
			return abort(422, 'Serial nhập vào không đúng.');
		}

		$warrantyDays = $inventory->warranty_period * 30;
		$expiredAt = null;
		$now = time();

		if ($inventory->has_serial) {

			$expiredAt = strtotime($inventory->exported_at . '+' . $warrantyDays . 'days');

		} else {


			$expiredAt = strtotime($inventory->orderInventories[0]->exported_at . '+' . $warrantyDays . 'days');
		}

		if ($now > $expiredAt) {
			return abort(422, 'Serial hết hạn bảo hành.');
		}

		return $inventory;

	}

	/**
	 * @param Voucher $voucher
	 * @param $params
	 * @return array
	 */
	public function checkVoucher($params)
	{
		if (empty($params['voucher'])) {
			abort(422, 'Vui lòng nhập mã khuyển mãi.');
		}

		$voucher = Voucher::whereCode($params['voucher'])->first();
		$msgVoucherInvalid = 'Mã khuyến mãi này không hợp lệ, vui lòng nhập lại.';

		if ($voucher === null) {
			abort(422, $msgVoucherInvalid);
		}

		if (!$voucher->active) {
			abort(403, $msgVoucherInvalid);
		}

		if ($voucher->isExpired()) {
			abort(403, $msgVoucherInvalid);
		}

		if ($voucher->number_of_uses >= $voucher->limit) {
			abort(403, $msgVoucherInvalid);
		}

//		if (!empty($params['user_id'])) {
//			$order = Order::whereVoucherCode($params['voucher'])->where('user_id', $params['user_id'])->first();
//			if ($order) {
//				abort(403, 'Mã khuyến mãi đã được dùng cho tài khoản này');
//			}
//		}
//
//		if (!empty($params['phone'])) {
//			$order = Order::whereVoucherCode($params['voucher'])->where('phone', $params['phone'])->first();
//			if ($order) {
//				abort(403, 'Mã khuyến mãi đã được dùng cho số điện thoại này');
//			}
//		}
//
//		if (!empty($params['email'])) {
//			$order = Order::whereVoucherCode($params['voucher'])->where('email', $params['email'])->first();
//			if ($order) {
//				abort(403, 'Mã khuyến mãi đã được dùng cho email này');
//			}
//		}

		$discountMoney = 0;
		if (!empty($params['products'])) {
			$productIds = [];
			$numberOfProducts = [];
			$brandIds = [];
			$categoryIds = [];
			$numberOfBrands = [];
			$numberOfCategory = [];
			$tempMoney = 0;
			$voucherAccepted = false;

			foreach ($params['products'] as $product) {
				$productIds[] = $product['id'];
				$numberOfProducts[$product['id']] = $product['quantity'];
			}

			$products = Product::whereIsDeleted(false)->whereIn('id', $productIds)->get();

			foreach ($products as $product) {
				if (isset($brandIds[$product->brand_id])) {
					$numberOfBrands[$product->brand_id] += $numberOfProducts[$product->id];
				} else {
					$brandIds[] = $product->brand_id;
					$numberOfBrands[$product->brand_id] = $numberOfProducts[$product->id];
				}

				if (isset($categoryIds[$product->prod_category_id])) {
					$numberOfCategory[$product->prod_category_id] += $numberOfProducts[$product->id];
				} else {
					$categoryIds[] = $product->prod_category_id;
					$numberOfCategory[$product->prod_category_id] = $numberOfProducts[$product->id];
				}

				$tempMoney += $product->price * $numberOfProducts[$product->id];

			}

			if ($tempMoney < $voucher->min_money_required) {
				abort(403, $msgVoucherInvalid);
			}

			if ($voucher->type === VoucherType::PRODUCT && in_array($voucher->product_id, $productIds)) {
				$voucherAccepted = true;
			}

			if ($voucher->type === VoucherType::BRAND && in_array($voucher->brand_id, $brandIds)) {
				$voucherAccepted = true;
			}

			if ($voucher->type === VoucherType::CATEGORY && in_array($voucher->prod_category_id, $categoryIds)) {
				$voucherAccepted = true;
			}

			if ($voucherAccepted) {
				if ($voucher->type === VoucherType::PRODUCT) {
					if ($voucher->use_percent_discount) {
						foreach ($products as $product) {
							if ($product->id === $voucher->product_id) {
								$discountMoney = ($product->price * $voucher->value / 100) * $numberOfProducts[$product->id];
								break;
							}
						}
					} else {
						$discountMoney = $voucher->value * $numberOfProducts[$voucher->product_id];
					}
				}

				if ($voucher->type === VoucherType::BRAND) {
					if ($voucher->use_percent_discount) {
						foreach ($products as $product) {
							if ($product->brand_id === $voucher->brand_id) {
								$discountMoney += ($product->price * $voucher->value / 100) * $numberOfProducts[$product->id];
							}
						}
					} else {
						$discountMoney = $voucher->value * $numberOfBrands[$voucher->brand_id];
					}
				}

				if ($voucher->type === VoucherType::CATEGORY) {
					if ($voucher->use_percent_discount) {
						foreach ($products as $product) {
							if ($product->prod_category_id === $voucher->prod_category_id) {
								$discountMoney += ($product->price * $voucher->value / 100) * $numberOfProducts[$product->id];
							}
						}
					} else {
						$discountMoney = $voucher->value * $numberOfCategory[$voucher->prod_category_id];
					}
				}

				if ($discountMoney > $voucher->max_money_discount) {
					$discountMoney = $voucher->max_money_discount;
				}
			}
		}

		if ($discountMoney === 0) {
			abort(403, $msgVoucherInvalid);
		}

		return [
			'voucher' => $voucher,
			'discount_money' => $discountMoney,
		];
	}

	/**
	 * @param $params
	 * @return mixed
	 *
	 */
	public function createOrder($params)
	{
		$productIds = [];
		$numberOfProducts = [];
		$brandIds = [];
		$categoryIds = [];
		$numberOfBrands = [];
		$numberOfCategory = [];
		$tempMoney = 0;
		$orderProducts = [];
		$shipMoney = 0; // TODO get ship money by address
		$discountMoney = 0;
		$voucher = null;
		$voucherAccepted = false;

		foreach ($params['products'] as $product) {
			$productIds[] = $product['id'];
			$numberOfProducts[$product['id']] = $product['quantity'];
		}

		$products = Product::whereIsDeleted(false)->whereIn('id', $productIds)->get();

		foreach ($products as $product) {
			if (isset($brandIds[$product->brand_id])) {
				$numberOfBrands[$product->brand_id] += $numberOfProducts[$product->id];
			} else {
				$brandIds[] = $product->brand_id;
				$numberOfBrands[$product->brand_id] = $numberOfProducts[$product->id];
			}

			if (isset($categoryIds[$product->prod_category_id])) {
				$numberOfCategory[$product->prod_category_id] += $numberOfProducts[$product->id];
			} else {
				$categoryIds[] = $product->prod_category_id;
				$numberOfCategory[$product->prod_category_id] = $numberOfProducts[$product->id];
			}

			$tempMoney += $product->price * $numberOfProducts[$product->id];
			$orderProducts[] = new OrderProduct([
				'product_id' => $product->id,
				'price' => $product->price,
				'real_price' => $product->price,
				'quantity' => $numberOfProducts[$product->id],
				'name' => $product->name,
				'image' => json_decode($product->images)[0]
			]);
		}

		if (!empty($params['voucher_code'])) {
			$voucher = Voucher::whereActive('true')
				->whereCode($params['voucher_code'])
				->first();

			if (empty($voucher)) {
				abort(403, 'Mã khuyến mãi này không hợp lệ, vui lòng nhập lại.');
			}

			if ($voucher->type === VoucherType::PRODUCT && in_array($voucher->product_id, $productIds)) {
				$voucherAccepted = true;
			}

			if ($voucher->type === VoucherType::BRAND && in_array($voucher->brand_id, $brandIds)) {
				$voucherAccepted = true;
			}

			if ($voucher->type === VoucherType::CATEGORY && in_array($voucher->prod_category_id, $categoryIds)) {
				$voucherAccepted = true;
			}

			if ($voucher->isExpired()
				|| $voucher->number_of_uses >= $voucher->limit
				|| $tempMoney < $voucher->min_money_required
			) {
				$voucherAccepted = false;
			}

			if (!$voucherAccepted) {
				abort(403, 'Mã khuyến mãi này không hợp lệ, vui lòng nhập lại.');
			}

			if ($voucher->type === VoucherType::PRODUCT) {
				if ($voucher->use_percent_discount) {
					foreach ($products as $product) {
						if ($product->id === $voucher->product_id) {
							$discountMoney = ($product->price * $voucher->value / 100) * $numberOfProducts[$product->id];
							break;
						}
					}
				} else {
					$discountMoney = $voucher->value * $numberOfProducts[$voucher->product_id];
				}
			}

			if ($voucher->type === VoucherType::BRAND) {
				if ($voucher->use_percent_discount) {
					foreach ($products as $product) {
						if ($product->brand_id === $voucher->brand_id) {
							$discountMoney += ($product->price * $voucher->value / 100) * $numberOfProducts[$product->id];
						}
					}
				} else {
					$discountMoney = $voucher->value * $numberOfBrands[$voucher->brand_id];
				}
			}

			if ($voucher->type === VoucherType::CATEGORY) {
				if ($voucher->use_percent_discount) {
					foreach ($products as $product) {
						if ($product->prod_category_id === $voucher->prod_category_id) {
							$discountMoney += ($product->price * $voucher->value / 100) * $numberOfProducts[$product->id];
						}
					}
				} else {
					$discountMoney = $voucher->value * $numberOfCategory[$voucher->prod_category_id];
				}
			}

			if ($discountMoney > $voucher->max_money_discount) {
				$discountMoney = $voucher->max_money_discount;
			}
		}

		$payMoney = $tempMoney + $shipMoney - $discountMoney;

		$orderAttributes = [
			'temp_money' => $tempMoney,
			'real_temp_money' => $tempMoney,
			'ship_money' => $shipMoney,
			'voucher_discount_value' => $discountMoney,
			'pay_money' => $payMoney,
			'real_pay_money' => $payMoney,
		];
		if (Auth::check()) {
			$orderAttributes['user_id'] = Auth::user()->id;
		}
		unset($params['products']);
		$orderAttributes = array_merge($params, $orderAttributes);

		return DB::transaction(function () use ($orderAttributes, $orderProducts, $voucher) {
            $orderAttributes['track_code'] = 'TC' . strtoupper(uniqid());
			$order = new Order($orderAttributes);
			$order->save();
			$order->orderProducts()->saveMany($orderProducts);

			if (!empty($voucher)) {
				// Update number of uses of voucher
				$voucher->number_of_uses++;
				$voucher->save();
			}

			SendDiscordNotification::dispatchNow(config('discord-config.sale_chanel'),
				'Có đơn hàng mới : *#' . $order->id . '* từ khách hàng *' . $order->name .
				'* đang chờ xử lý. Vui lòng [Kiểm tra](' . route('admin.orders_index',
					['id' => $order->id]) . ') !');

			return SendOrderMail::dispatch($order);
		});
	}

	/**
	 * @param $params
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
	 */
	public function getUserOrders($params)
	{
//       ['product', 'inventory' => function($inventory){
//           return $inventory->with('product', 'order');
//       }]
		$query = Order::with(['orderProducts.product' => function($query){
			$query->select('id','slug');
		}, 'inventories', 'shippingHistories']);
		if (!empty($params['id'])) {
			$query->whereKey($params['id']);
		}
		if (!empty($params['user_id'])) {
			$query->where('user_id', $params['user_id']);
		}
		return $query->orderBy('created_at', 'DESC')->paginate(config('app.pagination'));
	}

	public function getCategoryAttributes(ProdCategory $category, $params)
	{
		$attributeIds = AttributeCategory::whereProdCategoryId($category->id)
			->pluck('attribute_id');

		$attributes = Attribute::with('values')->whereIn('id', $attributeIds);
		if(isset($params['is_show_on_filter'])) {
            $attributes->where('is_show_on_filter', ($params['is_show_on_filter'] === 'true'));
        }

        $attributes = $attributes->orderBy('name', 'ASC')->get();
		$brands = $category->brands()->get();

		$priceRange = [];
		foreach (PriceRange::values() as $range) {
			$minMax = explode("|", $range->getValue());
			$min = $minMax[0];
			$max = $minMax[1];

			$priceRange[] = [
				'name' => trans('enum.price_range.' . strtolower($range->getKey())),
				'value' => $range->getValue(),
				'min' => $min,
				'max' => $max
			];
		}

		return [
			'attributes' => $attributes,
			'brands' => $brands,
			'priceRange' => $priceRange
		];
	}

	public function searchProduct(array $params, ProdCategory $category = null)
	{
		$query = Product::with(Product::queryIssetInventory())
			->whereIsDeleted(false);

		if (!empty($params['attributes'])) {
            $attributeValues = AttributeValue::whereIn('id', $params['attributes'])->get();
            $groupByAttributeId = $attributeValues->groupBy('attribute_id')->toArray();

            $query->whereIn('product_attribute.attribute_value_id', $params['attributes'])
                ->join('product_attribute','products.id','=','product_attribute.product_id')
                ->groupBy('products.id');

            if (count($params['attributes']) > 1 && count($groupByAttributeId) > 1) {
                $query->havingRaw(
                    'COUNT(distinct product_attribute.attribute_value_id) = ' . count($groupByAttributeId)
                );
            }
		}

		if ($category) {
			$category->load('descendants');
			$categoryIds[] = $category->id;

			foreach ($category->descendants as $child) {
				$categoryIds[] = $child->id;
			}

			$query->whereIn('products.prod_category_id', $categoryIds);
		}

		if (!empty($params['search'])) {
			$query->where(function ($query) use ($params) {
				$query->orWhere('products.sku', $params['search'])
					->orWhere('products.name', 'like', '%' . $params['search'] . '%');
			});
		}

		if (!empty($params['brand'])) {
			$query->where('products.brand_id', $params['brand']);
		}

		if (!empty($params['brands'])) {
			$query->whereIn('products.brand_id', $params['brands']);
		}

		if (!empty($params['min'])) {
			$query->where('products.price', '>=', (int)$params['min']);
		}

		if (!empty($params['max'])) {
			$query->where('products.price', '<=', (int)$params['max']);
		}

		if (!empty($params['sort'])) {
			switch ($params['sort']) {
				case SortType::LATEST:
					$query->orderBy('products.id', 'DESC');
					break;
				case SortType::OLDEST:
					$query->orderBy('products.id', 'ASC');
					break;
				case SortType::PRICE_DESC:
					$query->orderBy('products.price', 'DESC');
					break;
				case SortType::PRICE_ASC:
					$query->orderBy('products.price', 'ASC');
					break;
			}
		}
		return $query->paginate($params['limit'] ?? config('app.pagination'));
	}

	/**
	 * @param ProdCategory $category
	 * @return Product[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|void
	 */

	public function searchPost(array $params, BlogCategory $category = null)
	{
		$query = Post::whereIsDeleted(false);

		if ($category) {
			$category->load('descendants');
			$categoryIds[] = $category->id;
			foreach ($category->descendants as $child) {
				$categoryIds[] = $child->id;
			}
			$query->whereIn('blog_category_id', $categoryIds);
		}

		if (!empty($params['search'])) {
			$query->where('name', 'like', '%' . $params['search'] . '%');;
		}

		if (!empty($params['sort'])) {
			switch ($params['sort']) {
				case SortType::LATEST:
					$query->orderBy('id', 'DESC');
					break;
				case SortType::OLDEST:
					$query->orderBy('id', 'ASC');
					break;
			}
		} else {
			$query->orderBy('updated_at', 'DESC');
		}
		return $query->paginate($params['limit'] ?? config('app.pagination'));
	}
}

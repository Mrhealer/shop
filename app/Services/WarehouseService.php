<?php

namespace App\Services;

use App\Models\Warehouse;

class WarehouseService
{
    /**
     * @param array $warehouseAttributes
     * @return mixed
     */
    public function createWarehouse(array $warehouseAttributes)
    {
        $warehouse = new Warehouse($warehouseAttributes);
        $warehouse->save();

        // Add default value of quantity_goods to return data
        $warehouse->quantity_goods = 0;

        return $warehouse;
    }

    /**
     * @param Warehouse $warehouse
     * @param array $warehoues
     * @return mixed
     */
    public function updateWarehouse(Warehouse $warehouse, array $warehouses)
    {
        $warehouse->update($warehouses);

        return $warehouse;
    }

    /**
     * @param  Warehouse  $warehouse
     * @return mixed
     */
    public function deleteWarehouse(Warehouse $warehouse)
    {
        if($warehouse->quantity_goods > 0) {
            return abort(422, 'Kho hàng này đang có hàng tồn kho, vui lòng không được xóa!');
        }

        $warehouse->is_deleted =  true;
        $warehouse->save();

        return $warehouse;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchWarehouse($params)
    {
        $query = Warehouse::whereIsDeleted(false);

        if (!empty($params['id'])) {
            $query->where('id', $params['id']);
        }

        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])
                    ->orwhere('name', 'like', '%' . $params['search'] . '%')
                    ->orWhere('address', 'like', '%' . $params['search'] . '%')
                    ->orWhere('description', 'like', '%' . $params['search'] . '%');
            });
        }

        return $query->orderBy('id', 'DESC')->paginate(config('app.pagination'));
    }
}

<?php
namespace App\Services;

use App\Models\Brand;
use Illuminate\Support\Facades\DB;

class BrandService
{
    /**
     * @param $params
     * @return mixed
     */
    public function searchBrand($params)
    {
        $query = Brand::with('categories')->whereIsDeleted(false);

        if(!empty($params['id'])) {
            $query->where('id', $params['id']);
        }

        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])->orWhere('name', 'like', '%' . $params['search'] . '%');
            });
        }

        if(!empty($params['except_id'])) {
            $query->where('id', '<>', $params['except_id']);
        }

        if(!empty($params['category_id'])) {
            $query->whereHas('categories', function ($q) use($params) {
                $q->where('prod_category_id', $params['category_id']);
            });
        }

        if(isset($params['pagination']) && $params['pagination'] == 'false') {
            return $query->get();
        }

        return $query->paginate(config('app.pagination'));
    }

    /**
     * @param  array  $brandAttributes
     * @return mixed
     */
    public function createBrand(array $brandAttributes)
    {
        return DB::transaction(function () use ($brandAttributes) {
            $brand = new Brand($brandAttributes);
            $brand->save();
            $brand->categories()->sync($brandAttributes['prod_category_ids']);
            return $brand->load('categories');
        });
    }

    /**
     * @param Brand $brand
     * @param array $brandAttributes
     * @return mixed
     */
    public function updateBrand(Brand $brand, array $brandAttributes)
    {
        return DB::transaction(function () use ($brand, $brandAttributes) {
            $brand->update($brandAttributes);
            $brand->categories()->sync($brandAttributes['prod_category_ids']);
            return $brand->load('categories');
        });
    }

    /**
     * @param  Brand  $brand
     * @return mixed
     */
    public function deleteBrand(Brand $brand)
    {
        return DB::transaction(function () use ($brand) {
            $brand->categories()->sync([]);
            $brand->is_deleted = true;
            $brand->save();
            return $brand;
        });
    }
}

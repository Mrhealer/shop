<?php
namespace App\Services;

use App\Enums\CustomMenuObjType;
use App\Helpers\SlugHelper;
use App\Models\CustomMenus;
use App\Models\ProdCategory;
use Illuminate\Support\Facades\DB;

class ProdCategoryService
{
    /**
     * @param $params
     * @return mixed
     */
    public function searchCategory($params)
    {
        $query = ProdCategory::with('parentCategory')
            ->whereIsDeleted(false)
            ->orderBy('_lft', 'asc');

        if(!empty($params['id'])) {
            $query->where('id', $params['id']);
        }

        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])->orWhere('name', 'like', '%' . $params['search'] . '%');
            });
        }

        if(!empty($params['except_id'])) {
            $query->where('id', '<>', $params['except_id']);
        }

        if (!empty($params['parent_id'])) {
            $query->where('parent_id', $params['parent_id'] == 'null' ? null : (int)$params['parent_id']);
        }

        if(isset($params['pagination']) && $params['pagination'] == 'false') {
            return $query->get();
        }

        return $query->paginate(config('app.pagination'));
    }

    /**
     * @param  array  $categoryAttributes
     * @return mixed
     */
    public function createCategory(array $categoryAttributes)
    {
        return DB::transaction(function () use ($categoryAttributes) {
            $category = new ProdCategory($categoryAttributes);
            $category->save();
            $category->slug = SlugHelper::genSlug($category->name).'-'.$category->id;
            $category->save();
            return $category;
        });
    }

    /**
     * @param ProdCategory $category
     * @param array $categoryAttributes
     * @return mixed
     */
    public function updateCategory(ProdCategory $category, array $categoryAttributes)
    {
        return DB::transaction(function () use ($category, $categoryAttributes) {
            $categoryAttributes['slug'] = SlugHelper::genSlug($categoryAttributes['name']).'-'.$category->id;
            $category->update($categoryAttributes);
            return $category;
        });
    }

    /**
     * @param  ProdCategory  $category
     * @return mixed
     */
    public function deleteCategory(ProdCategory $category)
    {
        return DB::transaction(function () use ($category) {
            // Delete menus
            CustomMenus::where([
                ['object_id', $category->id],
                ['object_type', CustomMenuObjType::PRODUCT_CATEGORY],
            ])->delete();

            $category->is_deleted = true;
            $category->save();

            return $category;
        });
    }
}

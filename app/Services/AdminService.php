<?php

namespace App\Services;

use App\Models\Admin;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\OrderHistory;
use App\Models\ShippingTrack;
use App\Models\Ticket;
use App\Models\TicketMessage;
use App\Models\Warranty;
use App\Models\WarrantyProduct;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminService
{

	public function searchAdmin($params)
	{
		$query =  Admin::with('roles');
		if(!empty($params['id'])) {
			$query->where('id', $params['id']);
		}
		if(!empty($params['email'])) {
			$query->where('email', $params['email']);
		}
		if(!empty($params['phone'])) {
			$query->where('phone', $params['phone']);
		}
		if(!empty($params['search'])) {
			$query->where(function ($query) use ($params) {
				$query->where('id', $params['search'])
					->orWhere('email', 'like', '%' . $params['search'] . '%')
					->orWhere('phone', 'like', '%' . $params['search'] . '%')
					->orWhere('name', 'like', '%' . $params['search'] . '%');
			});
		}
		if(!empty($params['except_id'])) {
			$query->where('id', '<>', $params['except_id']);
		}
		return $query->orderBy('id', 'DESC')->paginate(config('app.pagination'));
	}


	public function createAdmin(array $attributes)
	{
		if (empty($attributes['password'])) {
			$attributes['password'] = Hash::make(Str::random(16));
		}else{
			$attributes['password'] = Hash::make($attributes['password']);
		}

		return DB::transaction(function () use ($attributes) {
			$admin = new Admin($attributes);
			$admin->save();
			$admin->roles()->sync($attributes['role_ids']);

			return $admin->load('roles');
		});
	}

	public function updateAdmin(Admin $admin, array $attributes)
	{
		if (!empty($attributes['password'])) {
			$attributes['password'] = Hash::make($attributes['password']);
 		} else {
		    unset($attributes['password']);
        }

		return DB::transaction(function () use ($attributes, $admin) {
			$admin->update($attributes);
			$admin->roles()->sync($attributes['role_ids']);

			return $admin->load('roles');
		});
	}


	public function deleteAdmin(Admin $admin)
	{
        $order = Order::where('shipper_id', $admin->id)->orWhere('admin_id', $admin->id)->first();
        if(!empty($order)) {
            abort(400, 'Quản trị viên này đã có xử lý đơn hàng, vui lòng không được xóa.');
        }

        $orderHistory = OrderHistory::where('admin_id', $admin->id)->first();
        if(!empty($orderHistory)) {
            abort(400, 'Quản trị viên này đã có xử lý đơn hàng, vui lòng không được xóa.');
        }

        $shippingTrack = ShippingTrack::where('admin_id', $admin->id)->orWhere('shipper_id', $admin->id)->first();
        if(!empty($shippingTrack)) {
            abort(400, 'Quản trị viên này đã có xử lý đơn hàng, vui lòng không được xóa.');
        }

        $inventory = Inventory::where('import_admin_id', $admin->id)->orWhere('export_admin_id', $admin->id)->first();
        if(!empty($inventory)) {
            abort(400, 'Quản trị viên này đã có xử lý hàng trong kho, vui lòng không được xóa.');
        }

        $warranty = Warranty::where('admin_id', $admin->id)->first();
        if(!empty($warranty)) {
            abort(400, 'Quản trị viên này đã có xử lý bảo hành, vui lòng không được xóa.');
        }

        $warrantyProduct = WarrantyProduct::where('tech_id', $admin->id)->first();
        if(!empty($warrantyProduct)) {
            abort(400, 'Quản trị viên này đã có xử lý bảo hành, vui lòng không được xóa.');
        }

        $ticket = Ticket::where('admin_id', $admin->id)->first();
        if(!empty($ticket)) {
            abort(400, 'Quản trị viên này đã có xử lý ticket, vui lòng không được xóa.');
        }

        $ticketMessage = TicketMessage::where('user_id', $admin->id)->where('is_customer', false)->first();
        if(!empty($ticketMessage)) {
            abort(400, 'Quản trị viên này đã có xử lý ticket, vui lòng không được xóa.');
        }

		return DB::transaction(function () use ($admin) {
            $admin->roles()->sync([]);
			return $admin->delete();
		});
	}

}

<?php

namespace App\Services;

use App\Imports\Admin\ProductAttributeImport;
use App\Models\OrderProduct;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class ProductService
{

	/**
	 * @param $params
	 * @return mixed
	 */
	public function searchProduct($params)
	{
		$query = Product::with('category', 'attributeValues')->whereIsDeleted(false);
		if (!empty($params['id'])) {
			$query->where('id', $params['id']);
		}
		if (!empty($params['sku'])) {
			$query->where('sku', $params['sku']);
		}
        if (!empty($params['brand_id'])) {
            $query->where('brand_id', $params['brand_id']);
        }
        if (!empty($params['category_id'])) {
            $query->where('prod_category_id', $params['category_id']);
        }
		if (!empty($params['search'])) {
			$query->where(function ($query) use ($params) {
				$query->where('id', $params['search'])
					->orWhere('sku', $params['search'])
					->orWhere('name', 'like', '%' . $params['search'] . '%');
			});
		}
		if (!empty($params['except_id'])) {
			$query->where('id', '<>', $params['except_id']);
		}
		return $query->orderBy('updated_at', 'DESC')->paginate(config('app.pagination'));
	}

	/**
	 * @param array $productAttributes
	 * @return mixed
	 */
	public function createProduct(array $productAttributes)
	{
		return DB::transaction(function () use ($productAttributes) {
			$product = new Product($productAttributes);
			$product->save();
			$product->slug = Str::slug($product->name) . '-' . $product->sku;
			$product->save();

            // Add default value of qty_in_warehouses to return data
            $product->qty_in_warehouses = 0;

			return $product->load('category', 'attributeValues');
		});
	}

	/**
	 * @param Product $product
	 * @param array $productAttributes
	 * @return mixed
	 */
	public function updateProduct(Product $product, array $productAttributes)
	{
		return DB::transaction(function () use ($productAttributes, $product) {
		    if(isset($productAttributes['name']) && $product->sku) {
                $productAttributes['slug'] = Str::slug($productAttributes['name']).'-'.$product->sku;
            }

			$product->update($productAttributes);
			return $product->load('category', 'attributeValues');
		});
	}

	/**
	 * @param Product $product
	 * @param array $prams
	 * @return mixed
	 */
	public function updateProductAttributes(Product $product, array $prams)
	{
		return DB::transaction(function () use ($prams, $product) {
			$product->attributeValues()->sync($prams['attribute_value_ids']);
			return $product->load('category', 'attributeValues');
		});
	}

	/**
	 * @param Product $product
	 * @return mixed
	 */
	public function deleteProduct(Product $product)
	{
        if($product->qty_in_warehouses > 0) {
            return abort(422, 'Sản phẩm này đang có hàng tồn kho, vui lòng không được xóa!');
        }

        $orderProduct = OrderProduct::where('product_id', $product->id)->first();
        if(!empty($orderProduct)) {
            return abort(422, 'Sản phẩm này đã có trong một đơn hàng, vui lòng không được xóa!');
        }

		return DB::transaction(function () use ($product) {
			$product->is_deleted = true;
			$product->save();
			return $product;
		});
	}

	/**
	 * @param $file
	 * @return \Maatwebsite\Excel\Excel
	 */
	public function importProductExcel($file)
	{
		return Excel::import(new ProductAttributeImport(), $file);
	}
}

<?php

namespace App\Services;

use App\Enums\InventoryStatus;
use App\Enums\OrderAction;
use App\Enums\OrderStatus;
use App\Enums\ShippingStatus;
use App\Enums\UserRole;
use App\Helpers\Helper;
use App\Jobs\SendTrackingOrderShippingMail;
use App\Jobs\SendTrackingOrderStatusMail;
use App\Models\Admin;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\OrderHistory;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\Setting;
use App\Models\ShippingTrack;
use App\Models\User;
use App\Models\UserPoint;
use App\Models\Voucher;
use App\Models\Warehouse;
use Illuminate\Support\Facades\DB;

class OrderService
{

    private $_userService;

    public function __construct(UserService $userService)
    {
        $this->_userService = $userService;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchOrder($params)
    {
        $query =  Order::with(
            [
                'user',
                'orderProducts',
                'shippingHistories' => function ($query) {
                    $query->with('admin')->with('shipper')->with('transportCompany');
                },
                'histories'  => function ($query) {
                    $query->with('admin');
                }
            ]
        );

        if(isset($params['id'])) {
            $query->where('id', $params['id']);
        }
        if(isset($params['shipper_id'])) {
            $query->where('shipper_id', $params['shipper_id']);
        }
        if(isset($params['phone'])) {
            $query->where('phone', $params['phone']);
        }
	    if(isset($params['email'])) {
		    $query->where('phone', $params['email']);
	    }
	    if(isset($params['status'])) {
		    $query->where('status', $params['status']);
	    }

	    if(isset($params['shipping_status'])) {
		    $query->where('shipping_status', $params['shipping_status']);
	    }
        if(isset($params['payment_status'])) {
            $query->where('payment_status', $params['payment_status']);
        }
	    if(isset($params['date_from']) && $params['date_from'] != 'null') {
		    $query-> where('created_at', '>=', $params['date_from'] . ' 00:00:00');
	    }

	    if(isset($params['date_to']) && $params['date_to'] != 'null') {
		    $query->where('created_at', '<=', $params['date_to'] . ' 23:59:59');
	    }
        if(isset($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])
                    ->orWhere('phone', $params['search'])
                    ->orWhere('email', $params['search'])
                    ->orWhere('name', 'like', '%' . $params['search'] . '%');
            });
        }
        if(isset($params['except_id'])) {
            $query->where('id', '<>', $params['except_id']);
        }
        return $query->orderBy('created_at', 'DESC')->paginate(config('app.pagination'));
    }


	/**
	 * @param $params
	 * @return mixed
	 */
	public function searchAllOrder($params)
	{
		$query =  Order::with(
			[
				'user',
				'orderProducts',
				'shippingHistories' => function ($query) {
					$query->with('admin')->with('shipper')->with('transportCompany');
				},
				'histories'  => function ($query) {
					$query->with('admin');
				}
			]
		);

		if(isset($params['id'])) {
			$query->where('id', $params['id']);
		}
		if(isset($params['shipper_id'])) {
			$query->where('shipper_id', $params['shipper_id']);
		}
		if(isset($params['phone'])) {
			$query->where('phone', $params['phone']);
		}
		if(isset($params['email'])) {
			$query->where('phone', $params['email']);
		}
		if(isset($params['status'])) {
			$query->where('status', $params['status']);
		}

		if(isset($params['shipping_status'])) {
			$query->where('shipping_status', $params['shipping_status']);
		}
		if(isset($params['payment_status'])) {
			$query->where('payment_status', $params['payment_status']);
		}
		if(isset($params['date_from']) && $params['date_from'] != 'null') {
			$query-> where('created_at', '>=', $params['date_from'] . ' 00:00:00');
		}

		if(isset($params['date_to']) && $params['date_to'] != 'null') {
			$query->where('created_at', '<=', $params['date_to'] . ' 23:59:59');
		}
		if(isset($params['search'])) {
			$query->where(function ($query) use ($params) {
				$query->where('id', $params['search'])
					->orWhere('phone', $params['search'])
					->orWhere('email', $params['search'])
					->orWhere('name', 'like', '%' . $params['search'] . '%');
			});
		}
		if(isset($params['except_id'])) {
			$query->where('id', '<>', $params['except_id']);
		}
		return $query->orderBy('created_at', 'DESC')->get();
	}

    /**
     * @param  Order  $order
     * @param  User  $user
     * @param  array  $attributes
     * @return mixed
     */
    public function updateOrder(Order $order, Admin $admin, array $attributes)
    {
        $orderCompleted = $this->checkOrderCompleted($order->id);
        if ($orderCompleted) {
            abort('422','Đơn hàng này đã hoàn thành, bạn không thể cập nhật thông tin.');
        }

        $orderExportedInventory = $this->checkExportedInventoryByOrder($order->id);

        if (
            !$orderExportedInventory
            &&
            in_array($attributes['status'], [OrderStatus::SUCCESS, OrderStatus::RETURN])
        ) {
            abort('422', 'Bạn vui lòng xuất hàng trước khi cập nhật sang trạng thái này.');
        }

        if($orderExportedInventory && $attributes['status'] === OrderStatus::ERROR) {
            abort('422', 'Đơn hàng này đã được xuất hàng, bạn không thể hủy đơn hàng chỉ có thể cập nhật sang trạng thái đơn hoàn.');
        }

        if(
            in_array($attributes['status'], [OrderStatus::SUCCESS, OrderStatus::RETURN])
            &&
            !in_array($order->shipping_status, [ShippingStatus::SUCCESS, ShippingStatus::FAILED])
        ) {
            abort('422', 'Vui lòng cập nhật trạng thái giao hàng sang thành công hoặc thất bại trước khi hoàn thành đơn hàng này.');
        }

        if (
            $attributes['status'] === $order->status
            &&
            $attributes['payment_status'] === $order->payment_status
            &&
            empty($attributes['note'])
        ) {
            return $order->load(
                [
	                'admin',
	                'user',
                    'orderProducts',
                    'histories' => function ($query) {
                        $query->with('admin');
                    }
                ]
            );
        }

        $customer = $order->user()->first();
        $setting = Setting::first();
        $userPoint = UserPoint::whereOrderId($order->id)->first();
	    $attributes['admin_id'] = $admin->id;

        return DB::transaction(function () use ($attributes, $order, $admin, $customer, $setting, $userPoint) {
            $oldOrder = $order->toArray();
            $order->update($attributes);
            $orderHistory = new OrderHistory([
                'order_id' => $order->id,
                'admin_id' => $admin->id,
                'action' => OrderAction::UPDATE_STATUS,
                'status' => $order->status,
                'payment_status' => $order->payment_status,
                'note' => $attributes['note']
            ]);
            $orderHistory->save();

            if(!empty($customer)) {
                if ($order->status === OrderStatus::SUCCESS) {
                    if($userPoint === null) {
                        $userPoint = new UserPoint([
                            'order_id' => $order->id,
                            'user_id' => $customer->id,
                            'point' =>  $order->pay_money * $setting->order_rate_point
                        ]);
                        $userPoint->save();
                        $customer->point += $userPoint->point;
                        $customer->save();
                    }
                } else {
                    if($userPoint !== null) {
                        $userPoint->delete();
                        $customer->point -= $order->pay_money * $setting->order_rate_point;
                        $customer->save();
                    }
                }
            }

            switch ($order->status) {
                case OrderStatus::SUCCESS:
                    Inventory::where('order_id', $order->id)->update([
                        'status' => InventoryStatus::EXPORTED
                    ]);

                    $inventoriesExported = Inventory::where('order_id', $order->id)->get();

                    $dataUpdateWarehouse = $dataUpdateProduct = [];
                    foreach ($inventoriesExported as $inventory) {
                        // Build data to update warehouse
                        if (empty($dataUpdateWarehouse[$inventory->import_to_warehouse_id])) {
                            $dataUpdateWarehouse[$inventory->import_to_warehouse_id] = [
                                'id' => $inventory->import_to_warehouse_id,
                                'qty' => 1
                            ];
                        } else {
                            $dataUpdateWarehouse[$inventory->import_to_warehouse_id]['qty'] += 1;
                        }

                        // Build data to update product
                        if (empty($dataUpdateProduct[$inventory->product_id])) {
                            $dataUpdateProduct[$inventory->product_id] = [
                                'id' => $inventory->product_id,
                                'qty' => 1
                            ];
                        } else {
                            $dataUpdateProduct[$inventory->product_id]['qty'] += 1;
                        }
                    }

                    // Update quantity_goods of warehouses
                    foreach ($dataUpdateWarehouse as $item) {
                        Warehouse::where('id', $item['id'])
                            ->update(['quantity_goods' => DB::raw('quantity_goods - ' . $item['qty'])]);
                    }

                    // Update qty_in_warehouses of products
                    foreach ($dataUpdateProduct as $item) {
                        Product::where('id', $item['id'])
                            ->update(['qty_in_warehouses' => DB::raw('qty_in_warehouses - ' . $item['qty'])]);
                    }
                    break;
                case OrderStatus::RETURN:
                    Inventory::where('order_id', $order->id)->update([
                        'export_user_id' => null,
                        'export_note' => null,
                        'order_id' => null,
                        'exported_at' => null,
                        'status' => InventoryStatus::IN_WAREHOUSE
                    ]);
                    break;
            }

            // Send discord msg to group
            $discordMsg = Helper::getDiscordMsgTrackingOrderStatus($oldOrder, $attributes);
            if(!empty($discordMsg)) {
                Helper::discordNotify(config('discord-config.sale_chanel'), $discordMsg);
            }

            // Send email to customer
//            if (!empty($orderHistory)) {
//                SendTrackingOrderStatusMail::dispatch($order);
//            }

            return $order->load(
                [
                    'user',
                    'orderProducts',
                    'histories'  => function ($query) {
                        $query->with('admin');
                    }
                ]
            );
        });
    }

	/**
	 * @param Order $order
	 * @param array $attributes
	 * @param Admin $admin
	 * @return Order
	 */
    public function updateShippingInfoOrder(Order $order, array $attributes, Admin $admin) {
        $orderExportedInventory = $this->checkExportedInventoryByOrder($order->id);
        if (
            !$orderExportedInventory
            &&
            in_array($attributes['shipping_status'], [ShippingStatus::SHIPPING, ShippingStatus::SUCCESS, ShippingStatus::FAILED])
        ) {
            abort('422', 'Bạn vui lòng xuất hàng trước khi cập nhật trạng thái giao hàng.');
        }

        $orderCompleted = $this->checkOrderCompleted($order->id);
        if($orderCompleted) {
            abort('422','Đơn hàng này đã hoàn thành, bạn không thể cập nhật thông tin giao hàng.');
        }

        $orderShippingCompleted = $this->checkOrderShippingCompleted($order->id);
        if($orderShippingCompleted) {
            abort('422','Quá trình giao hàng đã hoàn thành, bạn không thể cập nhật thông tin giao hàng.');
        }

        return DB::transaction(function () use ($order, $attributes, $admin) {
            $oldOrder = $order->toArray();
            if (
                !empty($attributes['note'])
                ||
                $order->shipping_status != $attributes['shipping_status']
                ||
                $order->shipper_id != $attributes['shipper_id']
                ||
                $order->transport_company_id != $attributes['transport_company_id']
            ) {
                $shippingTrack = new ShippingTrack([
                    'order_id' => $order->id,
                    'admin_id' => $admin->id,
                    'shipper_id' => $attributes['shipper_id'],
                    'transport_company_id' => $attributes['transport_company_id'],
                    'note' => $attributes['note'],
                    'status' => $attributes['shipping_status']
                ]);
                $shippingTrack->save();
            }

            $payMoney = $order->pay_money + ($attributes['ship_money'] - $order->ship_money);

            $order->update([
                'shipper_id' => $attributes['shipper_id'],
                'transport_company_id' => $attributes['transport_company_id'],
                'track_code' => $attributes['track_code'],
                'ship_money' => $attributes['ship_money'],
                'pay_money' => $payMoney,
                'real_pay_money' => $payMoney,
                'shipping_status' => $attributes['shipping_status']
            ]);

            $discordMsg = Helper::getDiscordMsgTrackingOrderShipping($oldOrder, $attributes);
            if(!empty($discordMsg)) {
                Helper::discordNotify(config('discord-config.shipping_chanel'), $discordMsg);
            }

            // Send email to customer
//            if (!empty($shippingTrack)) {
//                SendTrackingOrderShippingMail::dispatch($order);
//            }

            return $order->load(
                [
                    'user',
                    'orderProducts',
                    'shippingHistories' => function ($query) {
                        $query->with('admin')->with('shipper')->with('transportCompany');
                    }
                ]
            );
        });
    }

    /**
     * @param Order $order
     * @param array $attributes
     * @param  User  $user
     * @return Order
     */
    public function shipperUpdateShippingInfoOrder(Order $order, array $attributes, Admin $admin) {
        $orderExportedInventory = $this->checkExportedInventoryByOrder($order->id);
        if (
            !$orderExportedInventory
            &&
            in_array($attributes['shipping_status'], [ShippingStatus::SHIPPING, ShippingStatus::SUCCESS, ShippingStatus::FAILED])
        ) {
            abort('422', 'Bạn vui lòng xuất hàng trước khi cập nhật trạng thái giao hàng.');
        }

        $orderCompleted = $this->checkOrderCompleted($order->id);
        if($orderCompleted) {
            abort('422','Đơn hàng này đã hoàn thành, bạn không thể cập nhật thông tin giao hàng.');
        }

        $orderShippingCompleted = $this->checkOrderShippingCompleted($order->id);
        if($orderShippingCompleted) {
            abort('422','Quá trình giao hàng đã hoàn thành, bạn không thể cập nhật thông tin giao hàng.');
        }

        return DB::transaction(function () use ($order, $attributes, $admin) {
            $oldOrder = $order->toArray();
            if (
                !empty($attributes['note'])
                ||
                $order->shipping_status != $attributes['shipping_status']
            ) {
                $shippingTrack = new ShippingTrack([
                    'order_id' => $order->id,
                    'admin_id' => $admin->id,
                    'shipper_id' => $order->shipper_id,
                    'transport_company_id' => $order->transport_company_id,
                    'note' => $attributes['note'],
                    'status' => $attributes['shipping_status']
                ]);
                $shippingTrack->save();
            }

            $order->update([
                'shipping_status' => $attributes['shipping_status']
            ]);

            $discordMsg = Helper::getDiscordMsgTrackingOrderShipping(
                $oldOrder,
                [
                    'note' => $attributes['note'],
                    'shipping_status' => $attributes['shipping_status'],
                    'shipper_id' => $order->shipper_id,
                    'transport_company_id' => $order->transport_company_id
                ]
            );

            if(!empty($discordMsg)) {
                Helper::discordNotify(config('discord-config.shipping_chanel'), $discordMsg);
            }

            // Send email to customer
//            if (!empty($shippingTrack)) {
//                SendTrackingOrderShippingMail::dispatch($order);
//            }

            return $order->load(
                [
                    'user',
                    'orderProducts',
                    'shippingHistories' => function ($query) {
                        $query->with('admin')->with('shipper')->with('transportCompany');
                    }
                ]
            );
        });
    }


    /**
     * @param  Order  $order
     * @return mixed
     */
    public function syncOrderUser(Order $order)
    {
        $orderCompleted = $this->checkOrderCompleted($order->id);
        if($orderCompleted) {
            abort('422','Đơn hàng này đã hoàn thành, bạn không thể cập nhật thông tin.');
        }

        $user = User::whereEmail($order->email)->orWhere('phone', $order->phone)->first();

        return DB::transaction(function () use ($user, $order) {
            if($user === null) {
                $user = $this->_userService->createUser([
                    'email' => $order->email,
                    'phone' => $order->phone,
                    'name' => $order->name,
                    'role' => UserRole::NORMAL,
                ]);
            }
            $order->update([
                'user_id' => $user->id,
            ]);

            return $order->load('user', 'orderProducts');
        });
    }

    public function updateProduct(OrderProduct $orderProduct, array $attributes) {
        $order = Order::find($orderProduct->order_id);

        $orderCompleted = $this->checkOrderCompleted($order->id);
        if($orderCompleted) {
            abort('422','Đơn hàng này đã hoàn thành, bạn không thể cập nhật thông tin.');
        }

        $orderExportedInventory = $this->checkExportedInventoryByOrder($order->id);
        if ($orderExportedInventory) {
            abort('422', 'Hệ thống đã xuất hàng cho đơn hàng này, bạn không thể cập nhật sản phẩm.');
        }

        // return true when nothing to changes
        if($orderProduct->product_id === $attributes['product_id'] && $orderProduct->quantity === $attributes['quantity']) {
            return [
                'order' => $order,
                'orderProduct' => $orderProduct
            ];
        }

        $orderProductDuplicated = OrderProduct::where('product_id', $attributes['product_id'])
            ->where('id', '<>', $orderProduct->id)
            ->where('order_id', $order->id)
            ->first();

        if(!empty($orderProductDuplicated)) {
            return abort(422, 'Sản phẩm này đã có trong đơn hàng, vui lòng chọn sản phẩm khác.');
        }

        return DB::transaction(function () use ($orderProduct, $attributes, $order) {
            $product = Product::find($attributes['product_id']);

            // Update order product
            $orderProduct->update([
                'product_id' => $attributes['product_id'],
                'name' => $product->name,
                'image' => json_decode($product->images)[0],
                'price' => $product->price,
                'quantity' => $attributes['quantity'],
                'real_price' => $product->price,
            ]);

            $orderProducts = $order->orderProducts()->get();

            $productIds = [];
            $numberOfProducts = [];
            $discountMoney = 0;
            $tempMoney = 0;
            foreach ($orderProducts as $itemOrderProduct) {
                $productIds[] = $itemOrderProduct['product_id'];
                $numberOfProducts[$itemOrderProduct['product_id']] = $itemOrderProduct['quantity'];
            }

            $products = Product::whereIn('id', $productIds)->whereIsDeleted(false)->get();
            foreach ($products as $product) {
                $tempMoney += $product->price * $numberOfProducts[$product->id];
            }

            // Update number of uses of voucher
            if(!empty($order->voucher_code)) {
                $voucher = Voucher::whereCode($order->voucher_code)->first();

                $voucher->number_of_uses--;
                $voucher->save();
            }

            $payMoney = $tempMoney + $order->ship_money - $discountMoney;

            // Update order
            $order->update([
                'voucher_code' => null,
                'voucher_discount_value' => $discountMoney,
                'temp_money' => $tempMoney,
                'real_temp_money' => $tempMoney,
                'pay_money' => $payMoney,
                'real_pay_money' => $payMoney,
            ]);

            return [
                'order' => $order,
                'orderProduct' => $orderProduct
            ];
        });
    }

    /**
     * Check this order exported inventory?
     *
     * @param $orderId
     *
     * @return bool
     */
    public function checkExportedInventoryByOrder($orderId)
    {
        $orderExportInventoryHistory = OrderHistory::where('action', OrderAction::EXPORT_INVENTORY)
            ->where('order_id', $orderId)
            ->first();

        return !empty($orderExportInventoryHistory);
    }

    /**
     * Get serial list of this order
     *
     * @param $orderId
     *
     * @return array
     */
    public function getSerialList($orderId) {
        $inventories = Inventory::select('serial')
            ->where('order_id', $orderId)
            ->whereNotNull('exported_at')
            ->get();

        return empty($inventories) ? [] : $inventories->pluck('serial')->all();
    }

    /**
     * Check order completed
     *
     * @param $orderId
     *
     * @return bool
     */
    public function checkOrderCompleted($orderId)
    {
        $order = Order::where('id', '=', $orderId)->firstOrFail();

        if (in_array($order->status, [OrderStatus::SUCCESS, OrderStatus::ERROR, OrderStatus::RETURN])) {
            return true;
        }

        return false;
    }

    /**
     * Check order shipping completed
     *
     * @param $orderId
     *
     * @return bool
     */
    public function checkOrderShippingCompleted($orderId)
    {
        $order = Order::where('id', '=', $orderId)->firstOrFail();

        if (in_array($order->shipping_status, [ShippingStatus::SUCCESS, ShippingStatus::FAILED])) {
            return true;
        }

        return false;
    }
}



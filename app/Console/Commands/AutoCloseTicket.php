<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Enums\TicketStatus;
use App\Models\Ticket;
use App\Jobs\CloseTicketJob;


class AutoCloseTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ticket:auto-close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Close Ticket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $completed_at = Carbon::now()->format('Y-m-d H:i:s');

        $tickets = Ticket::whereStatus(TicketStatus::WAIT_CLOSE)->where('completed_at', '<=' , $completed_at)->get();


        foreach($tickets as $ticket){      

             CloseTicketJob::dispatchNow($ticket);

        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SetRealMoney extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deploy:real_money_order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $this->info('Update real_temp_money and real_pay_money in order table');
	    DB::statement('UPDATE orders SET real_temp_money = temp_money, real_pay_money = pay_money');
	    $this->info('Update real_price in order_product table');
	    DB::statement('UPDATE order_product SET real_price = price');
	    $this->info('All done!');
    }
}

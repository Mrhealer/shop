<?php

namespace App\Console\Commands;

use App\Models\Brand;
use App\Models\ProdCategory;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixDataDuplicateForBrandsAndProdCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:duplicate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function () {
            $this->info('Start fix prodCategories!');
            $prodCategoriesXemTatCa = ProdCategory::where('name', 'XEM TẤT CẢ')->get();
            $prodCategoriesXemTatCaKeyed = $prodCategoriesXemTatCa->keyBy('id')->toArray();
            $prodCategoriesIdXemTatCa = $prodCategoriesXemTatCa->pluck('id')->toArray();

            // Update prod_category_id of products
            $productsToFixProdCategories = Product::whereIn('prod_category_id', $prodCategoriesIdXemTatCa)->get();
            foreach ($productsToFixProdCategories as $product) {
                $product->prod_category_id = $prodCategoriesXemTatCaKeyed[$product->prod_category_id]['parent_id'];
                $product->save();
            }

            // Delete brand_category
            DB::table('brand_category')->whereIn('prod_category_id', $prodCategoriesIdXemTatCa)->delete();

            // Delete attribute_category
            DB::table('attribute_category')->whereIn('prod_category_id', $prodCategoriesIdXemTatCa)->delete();

            ProdCategory::whereIn('id', $prodCategoriesIdXemTatCa)->delete();
            $this->info('Fixed prodCategories!');


            $this->info('Start fix brands!');
            $brandsDuplicated = Brand::whereIn('name', ['E-BLUE', 'PHILIPS', 'Team'])->get();
            $brandsDuplicatedGroupByName = $brandsDuplicated->groupBy('name')->toArray();

            foreach ($brandsDuplicatedGroupByName as $brandGroup) {
                foreach ($brandGroup as $key => $brand) {
                    if($key == 0) {
                        continue;
                    }

                    Product::where('brand_id', $brand['id'])->update([
                        'brand_id' => $brandGroup[0]['id']
                    ]);

                    // Delete brand_category
                    DB::table('brand_category')->where('brand_id', $brand['id'])->delete();

                    // Delete brand duplicated
                    DB::table('brands')->where('id', $brand['id'])->delete();
                }
            }

            $this->info('Fixed brands!');
        });
    }
}

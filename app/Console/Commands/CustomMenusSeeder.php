<?php

namespace App\Console\Commands;

use App\Enums\CustomMenuObjType;
use App\Models\CustomMenus;
use App\Models\ProdCategory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CustomMenusSeeder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seeder:custom_menus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start seeder:custom_menus!');
        DB::transaction(function () {
            $prodCategoriesLevel1 = ProdCategory::whereNull('parent_id')->where('is_deleted', false)->orderBy('id', 'ASC')->get();

            foreach ($prodCategoriesLevel1 as $prodCate) {
                $menuLevel1 = new CustomMenus([
                    'name' => $prodCate->name,
                    'object_id' => $prodCate->id,
                    'object_type' => CustomMenuObjType::PRODUCT_CATEGORY,
                    'url' => '',
                    'parent_id' => null,
                    'order' => $prodCate->order
                ]);
                $menuLevel1->save();

                $childCategories = ProdCategory::where('parent_id', $prodCate->id)->where('is_deleted', false)->orderBy('id', 'ASC')->get();
                if(count($childCategories) == 0) {
                    $brands = $prodCate->brands()->where('is_deleted', false)->get();
                    if(count($brands) > 0) {
                        $menuLevel2 = new CustomMenus([
                            'name' => 'Thương hiệu',
                            'object_id' => 0,
                            'object_type' => CustomMenuObjType::CUSTOM_LINK,
                            'url' => route('category', ['category' => $prodCate->slug]),
                            'parent_id' => $menuLevel1->id
                        ]);
                        $menuLevel2->save();

                        foreach ($brands as $item) {
                            $menuLevel3 = new CustomMenus([
                                'name' => $item->name,
                                'object_id' => 0,
                                'object_type' => CustomMenuObjType::CUSTOM_LINK,
                                'url' => route('category', ['category' => $prodCate->slug, 'brands[]' => $item->id]),
                                'parent_id' => $menuLevel2->id
                            ]);
                            $menuLevel3->save();
                        }
                    }
                } else {
                    foreach ($childCategories as $itemCate) {
                        $menuLevel2 = new CustomMenus([
                            'name' => $itemCate->name,
                            'object_id' => $itemCate->id,
                            'object_type' => CustomMenuObjType::PRODUCT_CATEGORY,
                            'url' => '',
                            'parent_id' => $menuLevel1->id,
                            'order' => $itemCate->order
                        ]);
                        $menuLevel2->save();

                        $brands = $itemCate->brands()->where('is_deleted', false)->get();
                        foreach ($brands as $itemBrand) {
                            $menuLevel3 = new CustomMenus([
                                'name' => $itemBrand['name'],
                                'object_id' => 0,
                                'object_type' => CustomMenuObjType::CUSTOM_LINK,
                                'url' => route('category', ['category' => $itemCate->slug, 'brands[]' => $itemBrand['id']]),
                                'parent_id' => $menuLevel2->id
                            ]);
                            $menuLevel3->save();
                        }
                    }
                }
            }
        });

        $this->info('End seeder:custom_menus!');
    }
}

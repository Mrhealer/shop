<?php

namespace App\Console\Commands;

use App\Enums\AdminPermissions;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class MakePermissionForVue extends Command
{
	protected $signature = 'permission:generate {path=./resources/js/admin_permissions.js}';

	protected $description = 'Generate js file for including in build process';

	protected $files;

	public function __construct(Filesystem $files)
	{
		parent::__construct();
		$this->files = $files;
	}

	public function handle()
	{
		$path = $this->argument('path');

		$this->makeDirectory($path);

		$this->files->put(base_path($path), $this->generate());

		$this->info('AdminPermissions generated!');
	}

	public function generate()
	{
		$json = json_encode(AdminPermissions::values());

		return <<<EOT
export const AdminPermissions = $json;
EOT;
	}

	protected function makeDirectory($path)
	{
		if (!$this->files->isDirectory(dirname(base_path($path)))) {
			$this->files->makeDirectory(dirname(base_path($path)), 0777, true, true);
		}
		return $path;
	}
}

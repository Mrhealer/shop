<?php

namespace App\Console;

use App\Console\Commands\CreateAdminData;
use App\Console\Commands\MakePermissionForVue;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
	    //Commands\SetRealMoney::class,
        //Commands\FixDataDuplicateForBrandsAndProdCategories::class,
        //Commands\CustomMenusSeeder::class,
        Commands\AutoCloseTicket::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('ticket:auto-close')
                ->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

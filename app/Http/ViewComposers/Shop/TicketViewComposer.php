<?php

namespace App\Http\ViewComposers\Shop;

use App\Enums\TicketPriority;
use App\Enums\TicketStatus;
use App\Models\TicketCategory;
use Illuminate\View\View;

class TicketViewComposer
{
	public function compose(View $view)
	{
		$_ticketStatus = [];
		foreach (TicketStatus::values() as $type) {
			$_ticketStatus[] = [
				'name' => trans('enum.ticket_status.'. strtolower($type->getKey())),
				'value' => $type->getValue(),
			];
		}
		$_ticketPriority = [];
		foreach (TicketPriority::values() as $type) {
			$_ticketPriority[] = [
				'name' => trans('enum.ticket_priority.'. strtolower($type->getKey())),
				'value' => $type->getValue(),
			];
		}
		$categories = TicketCategory::whereStatus(true)->get();

		$view->with([
			'_categories' => $categories,
			'_ticketStatus' => $_ticketStatus,
			'_ticketPriority' => $_ticketPriority,
			'_ticketPriorityEnum' => TicketPriority::values(),
			'_ticketStatusEnum' => TicketStatus::values(),
		]);
	}
}
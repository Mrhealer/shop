<?php

namespace App\Http\ViewComposers\Shop;

use App\Models\CustomMenus;
use App\Models\ProdCategory;
use App\Models\Product;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;

class HomeComposer
{

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
//        $prodCategories = ProdCategory::with('brands')->whereIsDeleted(false)->orderBy('order', 'ASC')->get();
        $hotProducts = Product::with(Product::queryIssetInventory())
            ->whereIsDeleted(false)
            ->whereIsHot(true)
            ->orderBy('updated_at', 'DESC')
            ->limit(config('app.pagination'))
            ->get();

        if ($hotProducts->isEmpty()) {
            $hotProducts = Product::with(Product::queryIssetInventory())
                ->whereIsDeleted(false)
                ->inRandomOrder()
                ->limit(config('app.pagination'))
                ->get();
        }

        $topOrderProducts = DB::table('order_product')
            ->select('product_id', DB::raw('count(*) as total'))
            ->groupBy('product_id')
            ->orderBy('total', 'DESC')
            ->limit(config('app.pagination'))
            ->get();

        $productIds = [];
        foreach ($topOrderProducts as $product) {
            $productIds[] = $product->product_id;
        }

        $bestSellerProducts = Product::with(Product::queryIssetInventory())
            ->whereIsDeleted(false)
            ->whereIn('id', $productIds)
            ->orderBy('updated_at', 'DESC')
            ->limit(config('app.pagination'))
            ->get();

        $bestSellerProducts = $bestSellerProducts->merge($hotProducts);

        $latestProducts = Product::with(Product::queryIssetInventory())
            ->whereIsDeleted(false)
            ->orderBy('updated_at', 'DESC')
            ->limit(config('app.pagination'))
            ->get();

        $topCategories = ProdCategory::with(['descendants' => function($query) {
            return $query->where('is_deleted', false);
        }, 'products' => function($query){
            return $query->where('is_deleted', false);
        }])
            ->whereIsDeleted(false)
            ->where('is_show_home', true)
            ->whereIsRoot()
            ->orderByDesc('updated_at')
            ->limit(config('app.pagination'))
            ->get();

        $view->with([
            '_menusTree' => CustomMenus::with('prodCategory')->orderBy('order', 'ASC')->get()->toTree(),
            '_hotProducts' => $hotProducts,
            '_latestProducts' => $latestProducts,
            '_topCategories' => $topCategories,
            '_bestSellerProducts' => $bestSellerProducts,
        ]);
    }
}

<?php

namespace App\Http\ViewComposers\Shop;

use App\Enums\OrderStatus;
use App\Enums\PriceRange;
use App\Enums\ShippingStatus;
use App\Enums\SortType;
use App\Models\Attribute;
use App\Models\Brand;
use App\Models\ProdCategory;
use App\Models\Product;
use App\Models\Setting;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class ShopComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $_orderStatus = [];
        foreach (OrderStatus::values() as $type) {
            $_orderStatus[] = [
                'name' => trans('enum.order_status.'. strtolower($type->getKey())),
                'value' => $type->getValue(),
            ];

        }
        $_shippingStatus = [];
	    foreach (ShippingStatus::values() as $type) {
		    $_shippingStatus[] = [
			    'name' => trans('enum.shipping_status.'. strtolower($type->getKey())),
			    'value' => $type->getValue(),
		    ];

	    }
        $_sortType = [];
        foreach (SortType::values() as $type) {
            $_sortType[] = [
                'name' => trans('enum.sort_type.'. strtolower($type->getKey())),
                'value' => $type->getValue(),
            ];

        }

        $_priceRange = [];
        foreach (PriceRange::values() as $range) {
            $minMax = explode("|", $range->getValue());
            $min = $minMax[0];
            $max = $minMax[1];

            $_priceRange[] = [
                'name' => trans('enum.price_range.'. strtolower($range->getKey())),
                'value' => $range->getValue(),
                'min' => $min,
                'max' => $max
             ];
        }

        $_setting = Setting::first();
        $_slider = !empty($_setting->slider) ? json_decode($_setting->slider) : [];
        $view->with([
            '_user' => user() ?? null,
            '_allBrand' => Brand::whereIsDeleted(false)->get(),
            '_shopSettings' => $_setting,
            '_slider' => $_slider,
	        '_orderStatus' => $_orderStatus,
	        '_shippingStatus' => $_shippingStatus,
            '_orderStatusEnum' => OrderStatus::values(),
            '_sortType' => $_sortType,
            '_priceRange' => $_priceRange
        ]);
    }
}

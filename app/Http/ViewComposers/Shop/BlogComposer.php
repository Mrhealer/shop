<?php

namespace App\Http\ViewComposers\Shop;

use App\Models\Post;
use Illuminate\Contracts\View\View;


class BlogComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            '_lastestNews' => Post::whereIsDeleted(false)->orderBy('updated_at', 'DESC')->limit(10)->get(),
        ]);
    }
}

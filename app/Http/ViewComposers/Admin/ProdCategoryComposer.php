<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\Contracts\View\View;

class ProdCategoryComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            '_activeRoute' => 'admin.prod_categories_index',
            '_groupRoute' => 'product',
            '_breadcrumb' => [
               [
                   'label' => 'Danh mục sản phẩm',
                   'route' => route('admin.prod_categories_index')
               ],
            ],
        ]);
    }
}

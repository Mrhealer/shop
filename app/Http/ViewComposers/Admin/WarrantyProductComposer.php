<?php

namespace App\Http\ViewComposers\Admin;

use App\Enums\AdminPermissions;
use App\Enums\InventoryStatus;
use App\Enums\ProductWarrantyStatus;
use App\Enums\WarrantyStatus;
use Illuminate\View\View;

class WarrantyProductComposer
{
	public function compose(View $view)
	{
		$productWarrantyStatus = [];
		foreach (ProductWarrantyStatus::values() as $type) {
			$productWarrantyStatus[] = [
				'name' => trans('enum.product_warranty_status.' . strtolower($type->getKey())),
				'value' => $type->getValue(),
			];

		}

		$view->with([
			'_activeRoute' => 'admin.warranty_prod_index',
			'_groupRoute' => 'warranty',
			'_breadcrumb' => [
				[
					'label' => 'Quản lý Sản phẩm Bảo hành',
					'route' => route('admin.warranty_prod_index')
				],
			],
			'technicians' => get_admins_via_permission([AdminPermissions::HANDLE_WARRANTY,AdminPermissions::VIEW_WARRANTY]),
			'productWarrantyStatus' => $productWarrantyStatus,
			'productWarrantyStatusEnum' => ProductWarrantyStatus::values(),
			'inventoryStatusEnum' => InventoryStatus::values(),
		]);
	}
}
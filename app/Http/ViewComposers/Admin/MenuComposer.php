<?php

namespace App\Http\ViewComposers\Admin;

use App\Enums\CustomMenuObjType;
use Illuminate\Contracts\View\View;

class MenuComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $menuObjType = [];
        foreach (CustomMenuObjType::values() as $type) {
            $menuObjType[] = [
                'name' => trans('enum.menu_obj_type.'. strtolower($type->getKey())),
                'value' => $type->getValue()
            ];
        }

        $view->with([
            '_activeRoute' => 'admin.menu_index',
            '_groupRoute' => '',
            '_breadcrumb' => [
                [
                    'label' => 'Menus',
                    'route' => route('admin.menu_index')
                ]
            ],
            '_menuObjType' => $menuObjType,
            '_menuObjTypeEnums' => CustomMenuObjType::values(),
        ]);
    }
}

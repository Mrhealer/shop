<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\View\View;

class TicketCategoryComposer
{
	public function compose(View $view)
	{
		$view->with([
			'_activeRoute' => 'admin.ticket_cat.index',
			'_groupRoute' => 'ticket',
			'_breadcrumb' => [
				[
					'label' => 'Phòng ban hỗ trợ',
					'route' => route('admin.ticket_cat.index')
				],
			]
		]);
	}
}
<?php

namespace App\Http\ViewComposers\Admin;

use App\Models\Attribute;
use App\Models\Brand;
use App\Models\ProdCategory;
use Illuminate\Contracts\View\View;

class ProductComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            '_activeRoute' => 'admin.products_index',
            '_groupRoute' => 'product',
            '_breadcrumb' => [
                [
                    'label' => 'Sản phẩm',
                    'route' => route('admin.products_index')
                ],
            ],
            '_attributes' => Attribute::with('categories')->get(),
            '_brands' => Brand::whereIsDeleted(false)->get(),
            '_categories' => ProdCategory::with('brands')->whereIsDeleted(false)->get()->toFlatTree(),
        ]);
    }
}

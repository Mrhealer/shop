<?php

namespace App\Http\ViewComposers\Admin;

use App\Enums\InventoryStatus;
use App\Enums\AdminPermissions;
use App\Enums\ProductWarrantyStatus;
use App\Enums\UserRole;
use App\Enums\WarrantyStatus;
use App\Models\User;
use Illuminate\Contracts\View\View;

class WarrantyComposer
{

	/**
	 * Bind data to the view.
	 *
	 * @param View $view
	 * @return void
	 */
	public function compose(View $view)
	{

		$warrantyStatus = [];
		foreach (WarrantyStatus::values() as $type) {
			$warrantyStatus[] = [
				'name' => trans('enum.warranty_status.' . strtolower($type->getKey())),
				'value' => $type->getValue(),
			];

		}

		$productWarrantyStatus = [];
		foreach (ProductWarrantyStatus::values() as $type) {
			$productWarrantyStatus[] = [
				'name' => trans('enum.product_warranty_status.' . strtolower($type->getKey())),
				'value' => $type->getValue(),
			];

		}
		$view->with([
			'_activeRoute' => 'admin.warranty_index',
			'_groupRoute' => 'warranty',
			'_breadcrumb' => [
				[
					'label' => 'Quản lý Bảo hành',
					'route' => route('admin.warranty_index')
				],
			],
			'technicians' => get_admins_via_permission([AdminPermissions::HANDLE_WARRANTY,AdminPermissions::VIEW_WARRANTY]),
			'warrantyStatus' => $warrantyStatus,
			'warrantyStatusEnum' => WarrantyStatus::values(),
			'productWarrantyStatus' => $productWarrantyStatus,
			'productWarrantyStatusEnum' => ProductWarrantyStatus::values(),
            'inventoryStatusEnum' => InventoryStatus::values(),
		]);
	}
}

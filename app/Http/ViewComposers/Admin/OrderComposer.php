<?php

namespace App\Http\ViewComposers\Admin;

use App\Enums\OrderAction;
use App\Enums\PaymentStatus;
use App\Enums\OrderStatus;
use App\Enums\PaymentMethod;
use App\Enums\AdminPermissions;
use App\Enums\ReceivedtMethod;
use App\Enums\ShippingStatus;
use App\Enums\UserRole;
use App\Models\Permission;
use App\Models\Role;
use App\Models\TransportCompany;
use App\Models\User;
use Illuminate\Contracts\View\View;

class OrderComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
	    $_orderStatus = [];
	    foreach (OrderStatus::values() as $type) {
		    $_orderStatus[] = [
			    'name' => trans('enum.order_status.'. strtolower($type->getKey())),
			    'value' => $type->getValue(),
		    ];
        }

        $_shippingStatus = [];
        foreach (ShippingStatus::values() as $type) {
            $_shippingStatus[] = [
                'name' => trans('enum.shipping_status.'. strtolower($type->getKey())),
                'value' => $type->getValue(),
            ];
        }

        $_orderActions = [];
        foreach (OrderAction::values() as $type) {
            $_orderActions[] = [
                'name' => trans('enum.order_actions.'. strtolower($type->getKey())),
                'value' => $type->getValue(),
            ];
        }

        $_paymentMethod = [];
        foreach (PaymentMethod::values() as $type) {
            $_paymentMethod[] = [
                'name' => trans('enum.payment_method.'. strtolower($type->getKey())),
                'value' => $type->getValue(),
            ];
        }
        $_paymentStatus = [];
        foreach (PaymentStatus::values() as $type) {
            $_paymentStatus[] = [
                'name' => trans('enum.payment_status.'. strtolower($type->getKey())),
                'value' => $type->getValue(),
            ];
        }

        $_receivedMethod = [];
        foreach (ReceivedtMethod::values() as $type) {
            $_receivedMethod[] = [
                'name' => trans('enum.received_method.'. strtolower($type->getKey())),
                'value' => $type->getValue(),
            ];

        }

        $view->with([
            '_activeRoute' => 'admin.orders_index',
            '_groupRoute' => '',
            '_breadcrumb' => [
                [
                    'label' => 'Đơn hàng',
                    'route' => route('admin.orders_index')
                ],
            ],
            '_orderStatus' => $_orderStatus,
            '_shippingStatus' => $_shippingStatus,
            '_orderStatusEnum' => OrderStatus::values(),
            '_orderShippingStatusEnum' => ShippingStatus::values(),
            '_orderActions'   => $_orderActions,
            '_paymentMethod' => $_paymentMethod,
            '_paymentStatus' => $_paymentStatus,
            '_receivedMethod' => $_receivedMethod,
            '_shippers' => get_admins_via_permission([AdminPermissions::HANDLE_SHIPPING_BILL]),
            '_transportCompanies' => TransportCompany::whereIsDeleted(false)->get()
        ]);
    }
}

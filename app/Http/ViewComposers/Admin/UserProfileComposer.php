<?php

namespace App\Http\ViewComposers\Admin;

use App\Models\User;
use Illuminate\Contracts\View\View;

class UserProfileComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
           '_activeRoute' => 'admin.user_profile',
            '_groupRoute' => '',
           '_breadcrumb' => [
               [
                   'label' => 'Trang cá nhân',
                   'route' => route('admin.user_profile')
               ],
           ]
        ]);
    }
}

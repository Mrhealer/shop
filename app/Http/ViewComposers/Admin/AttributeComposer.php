<?php

namespace App\Http\ViewComposers\Admin;

use App\Enums\AttributeGroup;
use App\Models\ProdCategory;
use Illuminate\Contracts\View\View;

class AttributeComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $_attributeTypes = [];
        foreach (AttributeGroup::values() as $type) {
            $_attributeTypes[] = [
                'name' => trans('enum.attribute_group.'. strtolower($type->getKey())),
                'value' => $type->getValue(),
            ];

        }
        $view->with([
            '_activeRoute' => 'admin.attributes_index',
            '_groupRoute' => 'product',
            '_breadcrumb' => [
                [
                    'label' => 'Thông số',
                    'route' => route('admin.attributes_index')
                ],
            ],
            '_attributeTypes' => $_attributeTypes,
            '_categories' => ProdCategory::whereIsDeleted(false)->get()
        ]);
    }
}

<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\Contracts\View\View;

class RevenueComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $view->with([

            '_activeRoute' => 'admin.revenue_index',
            '_groupRoute' => '',
            '_breadcrumb' => [
               [
                   'label' => 'Doanh thu',
                   'route' => route('admin.revenue_index')
               ],
            ],
        ]);
    }
}

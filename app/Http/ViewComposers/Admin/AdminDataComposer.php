<?php

namespace App\Http\ViewComposers\Admin;

use App\Enums\InventoryStatus;
use App\Enums\OrderStatus;
use App\Enums\AdminPermissions;
use App\Enums\TicketStatus;
use App\Enums\WarrantyStatus;
use App\Models\Order;
use App\Models\Role;
use App\Models\Ticket;
use App\Models\Warranty;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class AdminDataComposer
{
	/**
	 * Bind data to the view.
	 *
	 * @param View $view
	 * @return void
	 */
	public function compose(View $view)
	{
		$adminRoles =  Role::whereGuard('admin')->get();
		$admin = admin();
		$view->with([
			'_user' => $admin,
			'_availableRoles' => admin_avaiable_roles($admin, $adminRoles),
			'_adminRoles' => $adminRoles,
			'_availablePermissions' => admin_avaiable_permissions($admin),
			'_allPermissionsEnum' => AdminPermissions::values(),
			'_notifications' => [],
			'_orderCount' => Order::whereStatus(OrderStatus::PENDING)->count(),
			'_ticketCount' => Ticket::whereStatus(TicketStatus::OPEN)->count(),
			'_warrantyCount' => Warranty::whereStatus(WarrantyStatus::WAIT_HANDLE)->count(),
            '_inventoryStatusEnums' => InventoryStatus::values(),
		]);
	}
}

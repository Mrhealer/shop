<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\Contracts\View\View;

class BrandComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            '_activeRoute' => 'admin.brands_index',
            '_groupRoute' => 'product',
            '_breadcrumb' => [
               [
                   'label' => 'Thương hiệu',
                   'route' => route('admin.brands_index')
               ],
            ],
        ]);
    }
}

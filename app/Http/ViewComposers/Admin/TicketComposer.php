<?php

namespace App\Http\ViewComposers\Admin;

use App\Enums\TicketPriority;
use App\Enums\TicketStatus;
use Illuminate\View\View;

class TicketComposer
{
	public function compose(View $view)
	{
		$_ticketStatus = [];
		foreach (TicketStatus::values() as $type) {
			$_ticketStatus[] = [
				'name' => trans('enum.ticket_status.'. strtolower($type->getKey())),
				'value' => $type->getValue(),
			];
		}
		$_ticketPriority = [];
		foreach (TicketPriority::values() as $type) {
			$_ticketPriority[] = [
				'name' => trans('enum.ticket_priority.'. strtolower($type->getKey())),
				'value' => $type->getValue(),
			];
		}

		$view->with([
			'_activeRoute' => 'admin.ticket.index',
			'_groupRoute' => 'ticket',
			'_ticketStatus' => $_ticketStatus,
			'_ticketPriority' => $_ticketPriority,
			'_ticketPriorityEnum' => TicketPriority::values(),
			'_ticketStatusEnum' => TicketStatus::values(),
			'_breadcrumb' => [
				[
					'label' => 'Danh sách Ticket',
					'route' => route('admin.ticket.index')
				],
			]
		]);
	}
}
<?php

namespace App\Http\ViewComposers\Admin;

use App\Enums\VoucherType;
use Illuminate\Contracts\View\View;

class VoucherComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $_voucherTypes = [];
        foreach (VoucherType::values() as $type) {
            $_voucherTypes[] = [
                'name' => trans('enum.voucher_type.'. strtolower($type->getKey())),
                'value' => $type->getValue(),
            ];

        }
        $view->with([
            '_activeRoute' => 'admin.vouchers_index',
            '_groupRoute' => '',
            '_breadcrumb' => [
               [
                   'label' => 'Mã khuyến mãi',
                   'route' => route('admin.vouchers_index')
               ],
            ],
            '_voucherTypes' => $_voucherTypes,
            '_voucherTypeEnum' => VoucherType::toArray(),
        ]);
    }
}

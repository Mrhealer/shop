<?php

namespace App\Http\ViewComposers\Admin;

use App\Enums\ExportInventoryType;
use App\Enums\InventoryStatus;
use App\Models\SupplyUnit;
use App\Models\User;
use App\Models\Warehouse;
use Illuminate\Contracts\View\View;

class InventoryComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $exportInventoryTypes = [];
        foreach (ExportInventoryType::values() as $type) {
            $exportInventoryTypes[] = [
                'name' => trans('enum.export_inventory_type.'. strtolower($type->getKey())),
                'value' => $type->getValue()
            ];
        }

        $inventoryStatus = [];
        foreach (InventoryStatus::values() as $status) {
            $inventoryStatus[] = [
                'name' => trans('enum.inventory_status.'. strtolower($status->getKey())),
                'value' => $status->getValue()
            ];
        }

        $view->with([
            '_activeRoute' => 'admin.inventories_index',
            '_groupRoute' => 'product',
            '_breadcrumb' => [
               [
                   'label' => 'Tổng kho',
                   'route' => route('admin.inventories_index')
               ],
            ],
            '_warehouses' => Warehouse::whereIsDeleted(false)->get(),
            '_supplyunits' => SupplyUnit::whereIsDeleted(false)->get(),
            '_exportinventorytypes' => $exportInventoryTypes,
            '_inventoryStatus' => $inventoryStatus
        ]);
    }
}

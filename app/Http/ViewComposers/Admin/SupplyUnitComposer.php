<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\Contracts\View\View;

class SupplyUnitComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            '_activeRoute' => 'admin.supply_unit_index',
            '_groupRoute' => 'product',
            '_breadcrumb' => [
                [
                    'label' => 'Đơn vị cung cấp',
                    'route' => route('admin.supply_unit_index')
                ]
            ]
        ]);
    }
}

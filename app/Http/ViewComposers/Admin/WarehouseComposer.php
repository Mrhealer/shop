<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\Contracts\View\View;

class WarehouseComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            '_activeRoute' => 'admin.warehouse_index',
            '_groupRoute' => 'product',
            '_breadcrumb' => [
                [
                    'label' => 'Kho hàng',
                    'route' => route('admin.warehouse_index')
                ]
            ]
        ]);
    }
}

<?php

namespace App\Http\ViewComposers\Shipper;

use App\Enums\AdminPermissions;
use App\Enums\UserRole;
use App\Models\Role;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class ShipperComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
	    $adminRoles =  Role::whereGuard('admin')->get();
	    $admin = admin();

        $view->with([
	        '_admin' => $admin,
	        '_availableRoles' => admin_avaiable_roles($admin, $adminRoles),
	        '_adminRoles' => $adminRoles,
	        '_availablePermissions' => admin_avaiable_permissions($admin),
	        '_allPermissionsEnum' => AdminPermissions::values(),
            '_notifications' => []
        ]);
    }
}

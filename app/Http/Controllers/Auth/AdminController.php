<?php
namespace App\Http\Controllers\Auth;

use App\Enums\Queues;
use App\Enums\VerificationType;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\ForgetAdminPasswordRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Http\Requests\Auth\UpdateAdminProfileRequest;
use App\Jobs\SendResetPasswordMail;
use App\Models\Admin;
use App\Models\UserVerification;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class AdminController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectTo = '/admin';
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout', 'profile', 'updateProfile', 'changePassword');
    }

    public function guard()
    {
        return Auth::guard('admin');
    }

	/**
	 * Get the needed authorization credentials from the request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	protected function credentials(Request $request)
	{
		$field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
			? $this->username()
			: 'username';
		return [
			$field => $request->get($this->username()),
			'password' => $request->input('password'),
		];
	}

    public function showLoginForm()
    {
        return view('auth.index', [
            'guard' => 'admin',
            'screen' => 'login',
            'title' => 'Đăng nhập quản trị'
        ]);
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if ($request->ajax() || $request->wantsJson()) {
            return response()->json([
                'success' => true,
                'user' => $this->guard()->user(),
            ]);
        }

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectTo);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return $this->loggedOut($request) ?: redirect(route('auth.show_admin_login_form'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showForgetPasswordForm()
    {
        return view('auth.index', [
            'guard' => 'admin',
            'screen' => 'forget',
            'title' => 'Quên mật khẩu'
        ]);
    }

    /**
     * @param ForgetAdminPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function createResetPasswordToken(ForgetAdminPasswordRequest $request)
    {
        try {
            $admin = Admin::whereEmail($request->input('email'))->first();
	        $userVerification = new UserVerification([
                'admin_id' => $admin->id,
                'expired_at' => date('Y-m-d H:i:s', strtotime('+1 days')),
                'token' =>  Str::random(20) . uniqid(),
                'verification_type' => VerificationType::RESET_PASSWORD,
            ]);
	        $userVerification->save();
            SendResetPasswordMail::dispatch($userVerification, $admin, 'admin')->onQueue(Queues::HIGH);

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            Log::error('Cannot create reset password token for user: '.$request->input('email'));
            Log::error($e->getMessage());
        }

        return abort(500);

    }


	/**
	 * @param UserVerification $userVerification
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function showResetPasswordForm(UserVerification $userVerification)
    {
        if($userVerification->isExpired() || $userVerification->verification_type !== VerificationType::RESET_PASSWORD) {
            abort(403);
        }

        return view('auth.index', [
            'guard' => 'admin',
            'screen' => 'reset',
            'token' => $userVerification->token,
            'title' => 'Đặt lại mật khẩu'
        ]);
    }

	/**
	 * @param UserVerification $userVerification
	 * @param ResetPasswordRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
    public function resetPassword(UserVerification $userVerification, ResetPasswordRequest $request)
    {
        if($userVerification->isExpired() || $userVerification->verification_type !== VerificationType::RESET_PASSWORD || $userVerification->admin_id === null) {
            abort(403);
        }

        $admin = $userVerification->admin()->first();

        DB::transaction(function () use ($admin, $request, $userVerification) {
            $admin->update($request->parameters());
	        $userVerification->delete();
        });

        $this->guard()->login($admin);

        return redirect(route('admin.index'));
    }


}

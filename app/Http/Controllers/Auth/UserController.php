<?php
namespace App\Http\Controllers\Auth;

use App\Enums\Queues;
use App\Enums\VerificationType;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\ForgetUserPasswordRequest;
use App\Http\Requests\Auth\RegisterUserRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Http\Requests\Auth\UpdateUserProfileRequest;
use App\Jobs\SendResetPasswordMail;
use App\Mail\OrderMail;
use App\Models\FbAccount;
use App\Models\Order;
use App\Models\Product;
use App\Models\UserVerification;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class UserController extends Controller
{
	use AuthenticatesUsers;

	protected $_userService;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */

	protected $redirectTo = '/user';

	/**
	 * Create a new controller instance.
	 *
	 * @param  UserService  $userService
	 */

	public function __construct(UserService $userService)
	{
		$this->middleware('guest:user')->except('logout', 'profile', 'updateProfile', 'password', 'changePassword');
		$this->_userService = $userService;
	}

	/**
	 * @return mixed
	 */
	public function guard()
	{
		return Auth::guard('user');
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showLoginForm()
	{
		return view('auth.index', [
			'guard' => 'user',
			'screen' => 'login',
			'title' => 'Đăng nhập'
		]);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
	 */
	protected function sendLoginResponse(Request $request)
	{
		$request->session()->regenerate();

		$this->clearLoginAttempts($request);

		if ($request->ajax() || $request->wantsJson()) {
			return response()->json([
				'success' => true,
				'user' => $this->guard()->user(),
			]);
		}

		return $this->authenticated($request, $this->guard()->user())
			?: redirect()->intended($this->redirectPath());
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function logout(Request $request)
	{
		$this->guard()->logout();

		$request->session()->invalidate();

		$request->session()->regenerateToken();

		return $this->loggedOut($request) ?: redirect(route('auth.show_user_login_form'));
	}


	/**
	 * Get the needed authorization credentials from the request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	protected function credentials(Request $request)
	{
		$field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
			? $this->username()
			: 'username';
		return [
			$field => $request->get($this->username()),
			'password' => $request->password,
		];
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showRegisterForm()
	{
		return view('auth.index', [
			'guard' => 'user',
			'screen' => 'register',
			'title' => 'Đăng ký'
		]);
	}

	/**
	 * @param RegisterUserRequest $request
	 * @param UserService $userService
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function registerUser(RegisterUserRequest $request, UserService $userService)
	{
		$attributes = $request->parameters();
		$user = $userService->createUser($attributes);
		$this->guard()->login($user);

		return response()->json([
			'success' => true,
			'user' => $user,
		]);
	}

	/**
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function facebookLogin()
	{
		return Socialite::driver('facebook')->redirect();
	}

	/**
	 * @param UserService $userService
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
	 */
	public function callbackFacebookLogin(UserService $userService)
	{
		$fbUser = Socialite::driver('facebook')->user();

		if(empty($fbUser)) {
			return abort(422, 'Cannot get user information.');
		}

		$fbAccount = FbAccount::whereFid($fbUser->id)->first();
		$user = ($fbAccount) ? $fbAccount->user()->first() : $userService->registerUserByFacebook($fbUser);
		$this->guard()->login($user);

		return redirect('index');
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showForgetPasswordForm()
	{
		return view('auth.index', [
			'guard' => 'user',
			'screen' => 'forget',
			'title' => 'Quên mật khẩu'
		]);
	}

	/**
	 * @param ForgetUserPasswordRequest $request
	 * @return \Illuminate\Http\JsonResponse|void
	 */
	public function createResetPasswordToken(ForgetUserPasswordRequest $request)
	{
		try {
			$user = User::whereEmail($request->input('email'))->first();
			$userVerification = new UserVerification([
				'user_id' => $user->id,
				'expired_at' => date('Y-m-d H:i:s', strtotime('+1 days')),
				'token' =>  Str::random(20) . uniqid(),
				'verification_type' => VerificationType::RESET_PASSWORD,
			]);
			$userVerification->save();
			SendResetPasswordMail::dispatch($userVerification, $user, 'user')->onQueue(Queues::HIGH);

			return response()->json(['success' => true]);
		} catch (\Exception $e) {
			Log::error('Không thể tạo mã thông báo đặt lại mật khẩu cho người dùng : '.$request->input('email'));
			Log::error($e->getMessage());
		}

		return abort(500);
	}

	/**
	 * @param UserVerification $userVerification
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function verifyEmail(UserVerification $userVerification)
	{
		if($userVerification->isExpired() || $userVerification->verification_type !== VerificationType::EMAIL) {
			abort(403);
		}

		$user =  $userVerification->user()->first();

		if($user === null) {
			abort(404);
		}

		$user->email_verified_at = now();
		$user->save();
		$this->guard()->login($user);

		return redirect(route('index'));
	}

	/**
	 * @param UserVerification $userVerification
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showResetPasswordForm(UserVerification $userVerification)
	{
		if($userVerification->isExpired() || $userVerification->verification_type !== VerificationType::RESET_PASSWORD) {
			abort(403);
		}

		return view('auth.index', [
			'guard' => 'user',
			'screen' => 'reset',
			'token' => $userVerification->token,
			'title' => 'Đặt lại mật khẩu',
		]);
	}

	/**
	 * @param UserVerification $userVerification
	 * @param ResetPasswordRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function resetPassword(UserVerification $userVerification, ResetPasswordRequest $request)
	{
		if($userVerification->isExpired() || $userVerification->verification_type !== VerificationType::RESET_PASSWORD || $userVerification->user_id === null) {
			abort(403);
		}

		$user = $userVerification->user()->first();

		DB::transaction(function () use ($user, $request, $userVerification) {
			$user->update($request->parameters());
			$userVerification->delete();
		});

		$this->guard()->login($user);

		return redirect(route('index'));
	}

	/**
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function profile(){

		return view('shop.screens.user.change_profile');
	}

	/**
	 * @param UpdateUserProfileRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function updateProfile(UpdateUserProfileRequest $request)
	{
		$this->_userService->updateUser(user(), $request->parameters());

		return response()->json(['success' => true]);
	}

	/**
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function password()
	{
		return view('shop.screens.user.change_password');
	}

	/**
	 * @param ChangePasswordRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function changePassword(ChangePasswordRequest $request)
	{
		$user = user();
		$user->update($request->parameters());

		return response()->json(['success' => true]);
	}

	/**
	 * @param Request $request
	 * @return void
	 */
	public function sendEmailOrder(Request $request){

		$newOrder = $request->data;
		$oldProduct = $request->oldProduct;
		$order = new Order($newOrder);
		$productDetail = [];
		for ($i=0;$i<count($oldProduct);$i++){
			$productDetail = new Product($oldProduct[$i]);
		}

		return Mail::to( $order->email)->send(new OrderMail($order, $productDetail));
	}
}

<?php

namespace App\Http\Controllers\Shop;

use App\Enums\ProductWarrantyStatus;
use App\Enums\WarrantyStatus;
use App\Exports\Shop\BuildConfigExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Shop\StoreOrderRequest;
use App\Http\Requests\Shop\UpdateUserInfoRequest;
use App\Http\Requests\Shop\WarrantyReportRequest;
use App\Models\Attribute;
use App\Models\AttributeCategory;
use App\Models\BlogCategory;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\Post;
use App\Models\ProdCategory;
use App\Models\Product;
use App\Models\Setting;
use App\Services\InventoryService;
use App\Services\ShopService;
use App\Services\UserService;
use App\Services\WarrantyService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
	private $shopService;
	private $userService;
	private $inventoryService;
	private $warrantyService;

	public function __construct(
		ShopService $shopService,
		UserService $userService,
		InventoryService $inventoryService,
		WarrantyService $warrantyService

	)
	{
		$this->shopService = $shopService;
		$this->userService = $userService;
		$this->inventoryService = $inventoryService;
		$this->warrantyService = $warrantyService;
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(Request $request)
	{
		return view('shop.screens.home', [
			'params' => $request->all(),
		]);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function buildConfig(Request $request)
	{
	    $categoryIds = json_decode(Setting::select('build_categories')->first()->build_categories);
	    $categories = ProdCategory::whereIsDeleted(false)->whereIn('id', $categoryIds)->get();
	    $categoriesKeyed = $categories->keyBy('id')->toArray();

        $config = [];
        foreach ($categoryIds as $categoryId) {
            $config[] = [
                'category' => $categoriesKeyed[$categoryId],
                'product' => null,
            ];
        }

		return view('shop.screens.build', [
			'params' => $request->all(),
            'config' => $config,
		]);
	}

	public function exportConfig(Request $request)
    {
        return Excel::download(new BuildConfigExport($request->all()), 'kowcomputer-xay-dung-cau-hinh.xlsx');
    }

    public function exportImageConfigView(Request $request)
    {
        return view('shop.screens.export_image_config', [
            'data' => json_decode($_COOKIE['kow_build_config']),
            'setting' => Setting::first()
        ]);
    }

	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
	 */
	public function search(Request $request)
	{
		$params = $request->all();

		if ($request->ajax() || $request->wantsJson()) {
			return response()->json([
				'success' => true,
				'data' => $this->shopService->searchProduct($params),
			]);
		}

		return view('shop.screens.category', [
			'params' => $params,
			'category' => null,
			'attributes' =>  Attribute::with('values')->orderBy('name', 'ASC')->get(),
			'cat_products' => $this->shopService->searchProduct($params)
		]);
	}

    /**
     * @param  ProdCategory  $category
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function getCategoryProducts(ProdCategory $category, Request $request)
    {
        $params = $request->all();

        return response()->json($this->shopService->searchProduct($params, $category));
    }

    /**
     * @param  ProdCategory  $category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategoryAttributes(ProdCategory $category, Request $request)
    {
        return response()->json($this->shopService->getCategoryAttributes($category, $request->all()));
    }

	/**
	 * @param ProdCategory $category
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function category(ProdCategory $category, Request $request)
	{
		$params = $request->all();
        $category->load([
            'brands' => function ($query) {
                $query->where('is_deleted', false);
            },
            'children' => function ($query) {
                $query->where('is_deleted', false);
            }
        ]);
		$attributeIds = AttributeCategory::whereProdCategoryId($category->id)->get()->pluck('attribute_id');
		$attributes = Attribute::with('values')
            ->where('is_show_on_filter', true)
            ->whereIn('id', $attributeIds)
            ->orderBy('name', 'ASC')
            ->get();

		return view('shop.screens.category', [
			'params' => $params,
			'category' => $category,
			'attributes' => $attributes,
			'cat_products' => $this->shopService->searchProduct($params, $category)
		]);
	}


	/**
	 * @param BlogCategory $blogCategory
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function blogCategory(BlogCategory $blogCategory, Request $request)
	{
		$params = $request->all();
		return view('shop.screens.blog', [
			'params' => $params,
			'category' => $blogCategory,
			'posts' => $this->shopService->searchPost($params, $blogCategory)
		]);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function allBlog(Request $request)
	{
		$params = $request->all();
		return view('shop.screens.blog', [
			'params' => $params,
			'posts' => $this->shopService->searchPost($params)
		]);
	}


	/**
	 * @param Product $product
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function product(Product $product, Request $request)
	{
	    if($product->is_deleted) {
	        return view('errors.404');
        }

	    $setting = Setting::select('sales_policy')->first();

		return view('shop.screens.detail', [
			'params' => $request->all(),
			'product' => $product,
			'productRecently' => $product,
			'productAttributes' => $product->attributes(),
			'similar_products' => $product->similarProducts(),
			'on_brand_products' => $product->brandProducts(),
            'setting_sale_policy' => $setting->sales_policy
		]);
	}

	/**
	 * @param Post $post
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function post(Post $post, Request $request)
	{
		$blade = $post->is_custom_page ? 'custom_page' : 'post';
		return view('shop.screens.' . $blade, [
			'params' => $request->all(),
			'post' => $post,
		]);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function checkVoucher(Request $request)
	{
		return response()->json($this->shopService->checkVoucher($request->all()));
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function cart(Request $request)
	{
		return view('shop.screens.cart', [
			'params' => $request->all()
		]);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function checkout(Request $request)
	{
		return view('shop.screens.checkout', [
			'params' => $request->all()
		]);
	}

	/**
	 * @param StoreOrderRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function order(StoreOrderRequest $request)
	{
		return response()->json($this->shopService->createOrder($request->parameters()));
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showUserOrder(Request $request)
	{
		$user = user();
		$params = $request->all();
		$params['user_id'] = $user->id;
		return view('shop.screens.user.order', [
			'params' => $params,
			'orders' => $this->shopService->getUserOrders($params)
		]);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showWarranty(Request $request)
	{
		$user = user();
		$params = $request->all();
		$params['user_id'] = $user->id;

		$productWarrantyStatus = [];
		foreach (ProductWarrantyStatus::values() as $type) {
			$productWarrantyStatus[] = [
				'name' => trans('enum.product_warranty_status.' . strtolower($type->getKey())),
				'value' => $type->getValue(),
			];

		}
		return view('shop.screens.warranty', [
			'params' => $params,
			'productWarrantyStatus' => $productWarrantyStatus
		]);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function checkWarranty(Request $request)
	{
		$params = $request->all();
		return response()->json($this->shopService->checkWarranty($params));
	}

	public function showUserInfo(Request $request)
	{

		$params = $request->all();
		return view('shop.screens.user.profile',
			[
				'params' => $params
			]);
	}

	public function updateUserInfo(UpdateUserInfoRequest $request)
	{
		$user = user();
		return response()->json($this->userService->updateUser($user, $request->parameters()));
	}

	public function updatePassword(ChangePasswordRequest $request)
	{
		$user = user();
		return response()->json($this->userService->updateUser($user, $request->parameters()));
	}

	public function productsPurchased(){
		$user = user();
		$orders = Order::whereUserId($user->id)->pluck('id');

		$inventories = Inventory::with('product')->whereIn('order_id', $orders)->get();

		return view('shop.screens.user.product_pucharse', compact('inventories'));
	}

	public function storeWarrantyReport(WarrantyReportRequest $request)
	{
		$params = $request->parameters();
		$params['status'] = WarrantyStatus::WAIT_HANDLE;
		return response()->json($this->warrantyService->createWarranty($params));
	}

}

<?php

namespace App\Http\Controllers\Shop;

use App\Enums\TicketStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\Shop\Ticket\CreateMessageRequest;
use App\Http\Requests\Shop\Ticket\CreateTicketRequest;
use App\Http\Requests\Shop\Ticket\UpdateTicketRequest;
use App\Models\Ticket;
use App\Models\TicketMessage;
use App\Services\TicketService;
use Illuminate\Http\Request;

class TicketController extends Controller
{
	protected $ticketService;

	public function __construct(TicketService $ticketService)
	{
		$this->ticketService = $ticketService;
	}

	public function index(Request $request)
	{
		$params = $request->all();
		$params['user_id'] = user()->id;

		return view('shop.screens.user.ticket',
			[
				'tickets' => $this->ticketService->searchTicket($params),
				'keyword' => $request->input('search' . '')
			]
		);
	}

	public function show(Ticket $ticket)
	{
		if($ticket->user_id != user()->id) {
			return abort(404, 'Không tìm thấy');
		}
		$ticket->load(['category', 'user', 'messages' => function($query) {
			$query->orderByDesc('created_at');
		}, 'messages.user', 'messages.admin']);

		return view('shop.screens.user.ticket_detail', compact('ticket'));
	}

	public function store(CreateTicketRequest $request)
	{
		$params = [
			'ticket' => [
				'ticket_category_id' => $request->input('ticket_category_id'),
				'user_id' =>  user()->id,
				'title' =>  $request->input('title'),
				'priority' =>  $request->input('priority'),
				'status' => TicketStatus::OPEN
			],
			'message' => [
				'user_id' =>  user()->id,
				'message' =>$request->input('message'),
			]
		];
		return response()->json($this->ticketService->createTicket($params));
	}

	public function update(Ticket $ticket, UpdateTicketRequest $request)
	{
		if ($ticket->user_id != user()->id){
			return abort(403, 'Bạn không có quyền cập nhật ticket này');
		}

		return response()->json($this->ticketService->updateTicket($ticket, $request->parameters()));
	}

	public function createMessage(Ticket $ticket,CreateMessageRequest $request)
	{
		if ($ticket->user_id != user()->id){
			return abort(403, 'Bạn không có quyền trả lời ticket này');
		}
		$params = $request->parameters();
		$params['user_id'] = user()->id;
		$params['is_customer'] = true;
		return response()->json($this->ticketService->createTicketMessage($ticket, $params));
	}

	public function rateMessage(TicketMessage $message, Request $request)
	{
		$message->load('ticket');
		if ($message->ticket->user_id != user()->id){
			return abort(403, 'Bạn không có quyền đánh giá tin nhắn này');
		}
		return response()->json($this->ticketService->updateTicketMessage($message, $request->all()));

	}

}
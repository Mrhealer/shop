<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Voucher\StoreVoucherRequest;
use App\Http\Requests\Admin\Voucher\UpdateVoucherRequest;
use App\Models\Voucher;
use App\Services\VoucherService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class VoucherController extends Controller
{
    /** @var VoucherService */
    protected $_voucherService;

    /**
     * VoucherController constructor.
     * @param  VoucherService  $voucherService
     */
    public function __construct(VoucherService $voucherService)
    {
        $this->_voucherService = $voucherService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_VOUCHER)) {
		    return abort(403, 'Không có quyền');
	    }
	    return view('admin.screens.vouchers', [
            'vouchers' => $this->_voucherService->searchVoucher($request->all()),
            'keyword' => $request->input('search', ''),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_VOUCHER)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_voucherService->searchVoucher($request->all()));
    }

    /**
     * @param  StoreVoucherRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreVoucherRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_VOUCHER)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_voucherService->createVoucher($request->parameters()));
    }

    /**
     * @param  Voucher  $voucher
     * @param  UpdateVoucherRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
	public function update(Voucher $voucher, UpdateVoucherRequest $request)
	{
		if (Gate::denies(AdminPermissions::EDIT_VOUCHER)) {
			return abort(403, 'Không có quyền');
        }
        
		return response()->json($this->_voucherService->updateVoucher($voucher, $request->parameters()));
	}


}

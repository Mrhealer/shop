<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Services\AttributeValueService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class AttributeValueController extends Controller
{
    /** @var AttributeValueService */
    protected $_attributeValueService;

    /**
     * AttributeValueController constructor.
     * @param  AttributeValueService  $attributeValueService
     */
    public function __construct(AttributeValueService $attributeValueService)
    {
        $this->_attributeValueService = $attributeValueService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {

	    if (Gate::denies(AdminPermissions::VIEW_PROD_ATTRIBUTE)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_attributeValueService->searchAttributeValue($request->all()));
    }
}

<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;;
use App\Http\Requests\Admin\Warehouse\StoreWarehouseRequest;
use App\Http\Requests\Admin\Warehouse\UpdateWarehouseRequest;
use App\Models\Warehouse;
use App\Services\WarehouseService;
use Gate;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    /** @var WarehouseService */
    protected $warehouseService;

    /**
     * WarehouseController constructor.
     * @param  WarehouseService  $warehouseService
     */
    public function __construct(WarehouseService $warehouseService)
    {
        $this->warehouseService = $warehouseService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
	    if(Gate::denies(AdminPermissions::VIEW_WAREHOUSE)) {
		    return abort(403, 'Không có quyền');
	    }
        $search = $request->input('search', '');
        return view('admin.screens.warehouses', [
            'warehouses' => $this->warehouseService->searchWarehouse($request->all()),
            'keyword' => $search
        ]);
    }

    /**
     * @param  StoreWarehouseRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreWarehouseRequest $request)
    {
	    if(Gate::denies(AdminPermissions::EDIT_WAREHOUSE)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->warehouseService->createWarehouse($request->parameters()));
    }

    /**
     * @param Warehouse $warehouse
     * @param UpdateWarehouseRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Warehouse $warehouse, UpdateWarehouseRequest $request)
    {
	    if(Gate::denies(AdminPermissions::EDIT_WAREHOUSE)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->warehouseService->updateWarehouse($warehouse, $request->parameters()));
    }

    /**
     * @param  Warehouse  $warehouse
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Warehouse $warehouse)
    {
	    if(Gate::denies(AdminPermissions::DELETE_WAREHOUSE)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->warehouseService->deleteWarehouse($warehouse));
    }
}

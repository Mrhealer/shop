<?php
namespace App\Http\Controllers\Admin;

use App\Enums\ExportInventoryType;
use App\Enums\AdminPermissions;
use App\Exports\Admin\InventoriesExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Inventory\ExportInventoryRequest;
use App\Http\Requests\Admin\Inventory\StoreInventoryRequest;
use App\Http\Requests\Admin\Inventory\UpdateInventoryRequest;
use App\Http\Requests\Admin\Upload\ImportExcelRequest;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\Warehouse;
use App\Services\InventoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class InventoryController extends Controller
{
    /** @var InventoryService */
    protected $_inventoryService;

    /**
     * InventoryController constructor.
     * @param  InventoryService  $inventoryService
     */
    public function __construct(InventoryService $inventoryService)
    {
        $this->_inventoryService = $inventoryService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_INVENTORY)) {
		    return abort(403, 'Không có quyền');
	    }
        $params = $request->all();
        return view('admin.screens.inventories', [
            'inventories' => $this->_inventoryService->searchInventory($params),
            'params' => empty($params) ? ['search' => ''] : $params,
        ]);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_INVENTORY)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_inventoryService->searchInventory($request->all()));
    }

    /**
     * @param StoreInventoryRequest $request
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function store(StoreInventoryRequest $request)
    {
	    if (Gate::denies(AdminPermissions::IMPORT_INVENTORY)) {
		    return abort(403, 'Không có quyền');
	    }

        $params = $request->parameters();

        // Check serial is unique
        $inventories = Inventory::whereIn('serial', $params['serial_list'])->get();
        if(count($inventories) > 0) {
            return abort('403', 'Những serial này đã có trong hệ thống: <b>' . implode('</br>',
                    $inventories->pluck('serial')->all()) . '</b></br> Vui lòng nhâp lại!');
        }

        $user = admin();
        $params['import_user_id'] = $user->id;

        try {
            foreach ($params['serial_list'] as $serial) {
                $params['serial'] = $serial;
                $this->_inventoryService->createInventory($params);
            }

            return response()->json(true);
        }
        catch(\Exception $e) {
            return abort(500, 'Internal server error');
        }
    }

    /**
     * @param $inventory
     * @param  UpdateInventoryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Inventory $inventory, UpdateInventoryRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_INVENTORY)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_inventoryService->updateInventory($inventory, $request->parameters()));
    }

    /**
     * @param  ExportInventoryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function export(ExportInventoryRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EXPORT_INVENTORY)) {
		    return abort(403, 'Không có quyền');
	    }

        $admin = admin();

        if ($request->input('export_type') === ExportInventoryType::ORDER) {
            $order = Order::find($request->input('order_id'));
            $response = $this->_inventoryService->exportInventoryByOrder($order, $admin, $request->parameters());
        } else {
            $exportToWarehouse = Warehouse::find($request->input('export_to_warehouse_id'));
            $response = $this->_inventoryService->exportInventoryToWarehouse($exportToWarehouse, $admin, $request->parameters());
        }

        return response()->json($response);
    }
    public function importExcel(ImportExcelRequest $request)
    {
	    if (Gate::denies(AdminPermissions::IMPORT_INVENTORY)) {
		    return abort(403, 'Không có quyền');
	    }
		return response()->json($this->_inventoryService->importExcelInventory($request->file('file')));
    }

    /**
     * Check inventory is enough by order
     *
     * @param Order $order
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkInventoryIsEnoughByOrder(Order $order) {

	    if (Gate::denies(AdminPermissions::HANDLE_ORDER)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_inventoryService->checkInventoryIsEnoughByOrder($order));
    }

    public function importExcelSerial(ImportExcelRequest $request)
    {
	    if (Gate::denies(AdminPermissions::IMPORT_INVENTORY)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_inventoryService->importExcelSerial($request->file('file')));
    }

    public function exportExcel()
    {
        if (Gate::denies(AdminPermissions::VIEW_INVENTORY)) {
            return abort(403, 'Không có quyền');
        }

        return (new InventoriesExport)->download('tongkho.xlsx');
    }
}

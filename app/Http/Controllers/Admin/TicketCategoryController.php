<?php

namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Ticket\CreateCategoryRequest;
use App\Http\Requests\Admin\Ticket\UpdateCategoryRequest;
use App\Models\TicketCategory;
use App\Services\MenuService;
use App\Services\TicketCategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class TicketCategoryController extends Controller
{
	/** @var ticketCatService */
	protected $ticketCatService;

	/**
	 * TicketCategoryController constructor.
	 * @param TicketCategoryService $ticketCatService
	 */
	public function __construct(TicketCategoryService $ticketCatService)
	{
		$this->ticketCatService = $ticketCatService;
	}

	public function index(Request $request)
	{

		if (Gate::denies(AdminPermissions::VIEW_TICKET_CAT)) {
			return abort(403, 'Không có quyền');
		}

		return view('admin.screens.ticket_category', [
			'categories' => $this->ticketCatService->searchTicketCategory($request->all()),
			'keyword' => $request->input('search', '')
		]);
	}

	/**
	 * @param  Request  $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function search(Request $request)
	{
		if (Gate::denies(AdminPermissions::VIEW_TICKET_CAT)) {
			return abort(403, 'Không có quyền');
		}

		return response()->json($this->ticketCatService->searchTicketCategory($request->all()));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(CreateCategoryRequest $request)
	{
		if (Gate::denies(AdminPermissions::EDIT_TICKET_CAT)) {
			return abort(403, 'Không có quyền');
		}

		return response()->json($this->ticketCatService->createTicketCategory($request->parameters()));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param UpdateCategoryRequest $request
	 * @param TicketCategory $category
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(UpdateCategoryRequest $request, TicketCategory $category)
	{
		if (Gate::denies(AdminPermissions::EDIT_TICKET_CAT)) {
			return abort(403, 'Không có quyền');
		}

		return response()->json($this->ticketCatService->updateTicketCategory($category, $request->parameters()));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param TicketCategory $category
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function destroy(TicketCategory $category)
	{
		if (Gate::denies(AdminPermissions::DELETE_TICKET_CAT)) {
			return abort(403, 'Không có quyền');
		}

		return response()->json($this->ticketCatService->deleteTicketCategory($category));
	}
}

<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Setting\UpdateSettingRequest;
use App\Models\ProdCategory;
use App\Models\Setting;
use Illuminate\Support\Facades\Gate;

class SettingController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

	    if (Gate::denies(AdminPermissions::SETTING)) {
		    return abort(403, 'Không có quyền');
	    }

	    $categoryIds = json_decode(Setting::select('build_categories')->first()->build_categories);
	    $buildCategories = ProdCategory::whereIsDeleted(false)->whereIn('id', $categoryIds)->get();
        $buildCategoriesKeyed = $buildCategories->keyBy('id')->toArray();

        $buildCategoriesResponse = [];
        foreach ($categoryIds as $categoryId) {
            $buildCategoriesResponse[] = $buildCategoriesKeyed[$categoryId];
        }

        return view('admin.screens.setting', [
            'setting' => Setting::first(),
	        'buildCategories' => $buildCategoriesResponse
        ]);
    }

    /**
     * @param Setting $setting
     * @param UpdateSettingRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Setting $setting, UpdateSettingRequest $request)
    {

	    if (Gate::denies(AdminPermissions::SETTING)) {
		    return abort(403, 'Không có quyền');
	    }
        $setting->update($request->parameters());
        return response()->json($setting);
    }
}

<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Order\UpdateOrderProductRequest;
use App\Http\Requests\Admin\Order\UpdateOrderRequest;
use App\Http\Requests\Admin\Order\UpdateShippingInfoOrderRequest;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class OrderController extends Controller
{
    /** @var OrderService */
    protected $_orderService;

    /**
     * OrderController constructor.
     * @param  OrderService  $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->_orderService = $orderService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_ORDER)) {
		    return abort(403, 'Không có quyền');
	    }
        return view('admin.screens.orders', [
            'orders' => $this->_orderService->searchOrder($request->all()),
            'params' => $request->all(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_ORDER)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_orderService->searchOrder($request->all()));
    }


	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function searchAll(Request $request)
	{
		if (Gate::denies(AdminPermissions::VIEW_ORDER)) {
			return abort(403, 'Không có quyền');
		}
		return response()->json($this->_orderService->searchAllOrder($request->all()));
	}



    /**
     * @param  Order  $order
     * @param  UpdateOrderRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Order $order, UpdateOrderRequest $request)
    {

	    if (Gate::denies(AdminPermissions::HANDLE_ORDER)) {
		    return abort(403, 'Không có quyền');
	    }

        return response()->json($this->_orderService->updateOrder($order, admin(), $request->parameters()));
    }

    /**
     * @param Order $order
     * @param UpdateShippingInfoOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateShippingInfo(Order $order, UpdateShippingInfoOrderRequest $request)
    {
	    if (Gate::denies(AdminPermissions::CHANGE_SHIP_ORDER)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_orderService->updateShippingInfoOrder($order, $request->parameters(), admin()));
    }

    /**
     * @param  Order  $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function syncOrderUser(Order $order)
    {
	    if (Gate::denies(AdminPermissions::HANDLE_ORDER)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_orderService->syncOrderUser($order));
    }

    public function updateProduct(OrderProduct $orderProduct, UpdateOrderProductRequest $request) {

	    if (Gate::denies(AdminPermissions::HANDLE_ORDER)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_orderService->updateProduct($orderProduct, $request->parameters()));
    }

    public function checkExportedInventory(Order $order) {

	    if (Gate::denies(AdminPermissions::HANDLE_ORDER)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_orderService->checkExportedInventoryByOrder($order->id));
    }

    public function getSerialList(Order $order) {
        if (Gate::denies(AdminPermissions::HANDLE_ORDER)) {
            return abort(403, 'Không có quyền');
        }

        return response()->json($this->_orderService->getSerialList($order->id));
    }
}

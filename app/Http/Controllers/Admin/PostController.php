<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Post\StorePostRequest;
use App\Models\Post;
use App\Services\PostService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PostController extends Controller
{
    /** @var PostService */
    protected $_postService;

    /**
     * PostController constructor.
     * @param  PostService  $postService
     */
    public function __construct(PostService $postService)
    {
        $this->_postService = $postService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_POST)) {
		    return abort(403, 'Không có quyền');
	    }
        return view('admin.screens.posts', [
            'posts' => $this->_postService->searchPost($request->all()),
            'keyword' => $request->input('search', ''),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_POST)) {
		    return abort(403, 'Không có quyền');
	    }
	    return response()->json($this->_postService->searchPost($request->all()));
    }

    /**
     * @param StorePostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StorePostRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_POST)) {
		    return abort(403, 'Không có quyền');
	    }
	    return response()->json($this->_postService->createPost($request->parameters()));
    }

    /**
     * @param Post $post
     * @param StorePostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Post $post, StorePostRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_POST)) {
		    return abort(403, 'Không có quyền');
	    }
	    return response()->json($this->_postService->updatePost($post, $request->parameters()));
    }


    /**
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Post $post)
    {
	    if (Gate::denies(AdminPermissions::DELETE_POST)) {
		    return abort(403, 'Không có quyền');
	    }
	    return response()->json($this->_postService->deletePost($post));
    }

}

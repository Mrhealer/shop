<?php

namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Warranty\StoreWarrantyRequest;
use App\Http\Requests\Admin\Warranty\UpdateWarrantyRequest;
use App\Models\Warranty;
use App\Services\WarrantyService;
use Gate;
use Illuminate\Http\Request;

class WarrantyController extends Controller
{
	private $warrantyService;

	public function __construct(WarrantyService $warrantyService)
	{
		$this->warrantyService = $warrantyService;
	}

	public function index(Request $request)
	{
		if (Gate::denies(AdminPermissions::VIEW_WARRANTY)) {
			return abort(403, 'Không có quyền');
		}

		return view('admin.screens.warranty', [
			'warranties' => $this->warrantyService->searchWarranty($request->all()),
			'params' => $request->all()
		]);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse|void
	 */
	public function search(Request $request)
	{
		if (Gate::denies(AdminPermissions::VIEW_WARRANTY)) {
			return abort(403, 'Không có quyền');
		}
		return response()->json($this->warrantyService->searchWarranty($request->all()));
	}

	/**
	 * @param StoreWarrantyRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(StoreWarrantyRequest $request)
	{
		if (Gate::denies(AdminPermissions::EDIT_WARRANTY)) {
			return abort(403, 'Không có quyền');
		}
		return response()->json($this->warrantyService->createWarranty($request->parameters()));
	}

	/**
	 * @param Warranty $warranty
	 * @param UpdateWarrantyRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(Warranty $warranty, UpdateWarrantyRequest $request)
	{
		if (Gate::denies(AdminPermissions::EDIT_WARRANTY)) {
			return abort(403, 'Không có quyền');
		}
		return response()->json($this->warrantyService->updateWarranty($warranty, $request->parameters()));
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
	 */
	public function productList(Request $request)
	{
		$params = $request->all();

		if (Gate::denies(AdminPermissions::HANDLE_WARRANTY)) {
			return abort(403, 'Bạn không có quyền!');
		}
		if (Gate::denies(AdminPermissions::VIEW_ALL_WARRANTY_PRODS)){
			$params['tech_id'] = admin()->id;
		}
		return view('admin.screens.warranty_prod', [
			'products' => $this->warrantyService->searchProduct($params),
			'params' => $params
		]);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse|void
	 */
	public function getProductList(Request $request)
	{
		$params = $request->all();

		if (Gate::denies(AdminPermissions::HANDLE_WARRANTY)) {
			return abort(403, 'Bạn không có quyền!');
		}

		if (Gate::denies(AdminPermissions::VIEW_ALL_WARRANTY_PRODS)){
			$params['tech_id'] = admin()->id;
		}

		return response()->json($this->warrantyService->searchProduct($params));
	}
}
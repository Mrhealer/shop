<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Exports\Admin\UsersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\StoreUserRequest;
use App\Http\Requests\Admin\User\UpdateUserRequest;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;


class UserController extends Controller
{
    /** @var UserService */
    protected $_userService;

    /**
     * UserController constructor.
     * @param  UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->_userService = $userService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
	    if(Gate::denies(AdminPermissions::VIEW_USER)) {
		    return abort(403, 'Không có quyền');
	    }

        return view('admin.screens.users', [
            'users' => $this->_userService->searchUser($request->all()),
            'keyword' => $request->input('search', ''),
        ]);
    }

	public function search(Request $request)
	{
		if(Gate::denies(AdminPermissions::VIEW_USER)) {
			return abort(403, 'Không có quyền');
		}

		return response()->json( $this->_userService->searchUser($request->all()));
	}

    public function profile()
    {
        return view('admin.screens.profile');
    }

    /**
     * @param StoreUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreUserRequest $request)
    {
	    if(Gate::denies(AdminPermissions::EDIT_USER)) {
		    return abort(403, 'Không có quyền');
	    }

        return response()->json($this->_userService->createUser($request->parameters()));
    }

    /**
     * @param User $user
     * @param UpdateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(User $user, UpdateUserRequest $request)
    {
	    if(Gate::denies(AdminPermissions::EDIT_USER)) {
		    return abort(403, 'Không có quyền');
	    }

        return response()->json($this->_userService->updateUser($user, $request->parameters()));
    }

    /**
     * @param User $user
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(User $user, ChangePasswordRequest $request)
    {
	    if(Gate::denies(AdminPermissions::EDIT_USER)) {
		    return abort(403, 'Không có quyền');
	    }

        return response()->json($this->_userService->updateUser($user, $request->parameters()));
    }

	/**
	 * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
	 */
	public function export()
	{
		if(Gate::denies(AdminPermissions::EXPORT_USER)) {
			return abort(403, 'Không có quyền');
		}
		return (new UsersExport)->download('user_info.xlsx');
	}

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(User $user)
    {
	    if(Gate::denies(AdminPermissions::DELETE_USER)) {
		    return abort(403, 'Không có quyền');
	    }

        return response()->json($this->_userService->deleteUser($user));
    }
}

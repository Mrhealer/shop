<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BlogCategory\StoreBlogCategoryRequest;
use App\Models\BlogCategory;
use App\Services\BlogCategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class BlogCategoryController extends Controller
{
    /** @var BlogCategoryService */
    protected $_categoryService;

    /**
     * ProdCategoryController constructor.
     * @param  BlogCategoryService  $categoryService
     */
    public function __construct(BlogCategoryService $categoryService)
    {
        $this->_categoryService = $categoryService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

	    if (Gate::denies(AdminPermissions::VIEW_BLOG_CATEGORY)) {
		    return abort(403, 'Không có quyền');
	    }

        $search = $request->input('search', '');
        return view('admin.screens.blog_categories', [
            'categories' => $this->_categoryService->searchCategory($request->all()),
            'keyword' => $search
        ]);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {

	    if (Gate::denies(AdminPermissions::VIEW_BLOG_CATEGORY)) {
		    return abort(403, 'Không có quyền');
	    }

	    return response()->json($this->_categoryService->searchCategory($request->all()));
    }

    /**
     * @param  StoreBlogCategoryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreBlogCategoryRequest $request)
    {

	    if (Gate::denies(AdminPermissions::EDIT_BLOG_CATEGORY)) {
		    return abort(403, 'Không có quyền');
	    }

	    return response()->json($this->_categoryService->createCategory($request->parameters()));
    }

    /**
     * @param $category
     * @param  StoreBlogCategoryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BlogCategory $category, StoreBlogCategoryRequest $request)
    {

	    if (Gate::denies(AdminPermissions::EDIT_BLOG_CATEGORY)) {
		    return abort(403, 'Không có quyền');
	    }

	    return response()->json($this->_categoryService->updateCategory($category, $request->parameters()));
    }

    /**
     * @param  BlogCategory  $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(BlogCategory $category)
    {

	    if (Gate::denies(AdminPermissions::DELETE_BLOG_CATEGORY)) {
		    return abort(403, 'Không có quyền');
	    }

	    return response()->json($this->_categoryService->deleteCategory($category));
    }
}

<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Attribute\StoreAttributeRequest;
use App\Http\Requests\Admin\AttributeValue\StoreAttributeValueRequest;
use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Services\AttributeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class AttributeController extends Controller
{
    /** @var AttributeService */
    protected $_attributeService;

    public function __construct(AttributeService $attributeService)
    {
        $this->_attributeService = $attributeService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_PROD_ATTRIBUTE)) {
		    return abort(403, 'Không có quyền');
	    }

        $params = $request->all();
        return view('admin.screens.attributes', [
            'attributes' => $this->_attributeService->searchAttribute($request->all()),
            'params' => empty($params) ? ['search' => ''] : $params,
        ]);
    }

    /**
     * @param StoreAttributeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreAttributeRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_PROD_ATTRIBUTE)) {
		    return abort(403, 'Không có quyền');
	    }

	    return response()->json($this->_attributeService->createAttribute($request->parameters()));
    }

    /**
     * @param Attribute $attribute
     * @param StoreAttributeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Attribute $attribute, StoreAttributeRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_PROD_ATTRIBUTE)) {
		    return abort(403, 'Không có quyền');
	    }

        return response()->json($this->_attributeService->updateAttribute($attribute, $request->parameters()));
    }

    /**
     * @param Attribute $attribute
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Attribute $attribute)
    {
	    if (Gate::denies(AdminPermissions::DELETE_PROD_ATTRIBUTE)) {
		    return abort(403, 'Không có quyền');
	    }

        return response()->json($this->_attributeService->deleteAttribute($attribute));
    }


    /**
     * @param Attribute $attribute
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addValue(Attribute $attribute, StoreAttributeValueRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_PROD_ATTRIBUTE)) {
		    return abort(403, 'Không có quyền');
	    }

        return response()->json($this->_attributeService->createAttributeValue($attribute, $request->all()));
    }

    /**
     * @param  Attribute  $attribute
     * @return \Illuminate\Http\JsonResponse
     */
    public function getValues(Attribute $attribute)
    {

	    if (Gate::denies(AdminPermissions::EDIT_PROD_ATTRIBUTE)) {
		    return abort(403, 'Không có quyền');
	    }

        return response()->json($this->_attributeService->getAttributeValues($attribute));
    }

    /**
     * @param Attribute $attribute
     * @param AttributeValue $attributeValue
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateValue(Attribute $attribute, AttributeValue $attributeValue, StoreAttributeValueRequest $request)
    {

	    if (Gate::denies(AdminPermissions::EDIT_PROD_ATTRIBUTE)) {
		    return abort(403, 'Không có quyền');
	    }

        return response()->json($this->_attributeService->updateAttributeValue($attribute, $attributeValue, $request->all()));
    }

    /**
     * @param AttributeValue $attributeValue
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteValue(AttributeValue $attributeValue) {

	    if (Gate::denies(AdminPermissions::EDIT_PROD_ATTRIBUTE)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_attributeService->deleteAttributeValue($attributeValue));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Enums\TicketStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Ticket\CreateMessageRequest;
use App\Http\Requests\Admin\Ticket\CreateTicketRequest;
use App\Http\Requests\Admin\Ticket\UpdateTicketRequest;
use App\Models\Ticket;
use App\Models\TicketCategory;
use App\Services\TicketService;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;

class TicketController extends Controller
{
	private $ticketService;


	public function __construct(TicketService $ticketService)
	{
		$this->ticketService = $ticketService;
	}


	public function index(Request $request)
	{
		if (Gate::denies(AdminPermissions::VIEW_TICKET)) {
			return abort(403, 'Không có quyền');
		}

		$categories = TicketCategory::whereStatus(true)->get();

		return view('admin.screens.ticket', [
			'tickets' => $this->ticketService->searchTicket($request->all()),
			'categories' => $categories,
			'keyword' => $request->input('search', ''),
		]);
	}

	
	public function search(Request $request)
	{
		if (Gate::denies(AdminPermissions::VIEW_TICKET)) {
			return abort(403, 'Không có quyền');
		}

		return response()->json($this->ticketService->searchTicket($request->all()));
	}

	
	public function show(Ticket $ticket)
	{

		if (Gate::denies(AdminPermissions::HANDLE_TICKET)) {
			return abort(403, 'Không có quyền');
		}

		return response()->json($ticket->load(['category', 'user', 'messages' => function($query) {
			$query->orderByDesc('created_at');
		}, 'messages.user', 'messages.admin']));
	}



	public function store(CreateTicketRequest $request)
	{

		if (Gate::denies(AdminPermissions::HANDLE_TICKET)) {
			return abort(403, 'Không có quyền');
		}

		$category = TicketCategory::find($request->input('ticket_category_id'));

		$params = [
			'ticket' => [
				'ticket_category_id' => $category->id,
				'user_id' =>  $request->input('user_id'),
				'title' =>  $request->input('title'),
				'priority' =>  $request->input('priority'),
	    		'time_handle' => $category->time_handle_default,
	    		'received_at' => null,
				'status' => TicketStatus::OPEN
			],
			'message' => [
				'user_id' =>  $request->input('user_id'),
				'message' =>$request->input('message'),
			]
		];
		return response()->json($this->ticketService->createTicket($params));
	}

	
	public function update(UpdateTicketRequest $request, Ticket $ticket)
	{
		if (Gate::denies(AdminPermissions::HANDLE_TICKET)) {
			return abort(403, 'Không có quyền');
		}

		return response()->json($this->ticketService->updateTicket($ticket, $request->parameters()));
	}


	public function destroy(Ticket $ticket)
	{

		if (Gate::denies(AdminPermissions::EDIT_TICKET)) {
			return abort(403, 'Không có quyền');
		}

		return response()->json($this->ticketService->deleteTicket($ticket));
	}


	public function receiveTicket(Ticket $ticket){


		if (Gate::denies(AdminPermissions::HANDLE_TICKET)) {
			return abort(403, 'Không có quyền');
		}

		if ($ticket->received_at != null) {
			return abort(403, 'Ticket đã được nhận trước đó!');
		}

		$attributes['received_at'] = Carbon::now()->format('Y-m-d H:i:s');
		$attributes['admin_id'] = admin()->id;

		$countReceive_time = $ticket->created_at->diffInRealMinutes($attributes['received_at']);
		
		if ($countReceive_time > $ticket->category->time_receiver_default) {
			$attributes['is_violated_receive'] = true;
		}

		return response()->json($this->ticketService->updateTicket($ticket, $attributes));
	}


	public function createMessage(Ticket $ticket,CreateMessageRequest $request)
	{

		if (Gate::denies(AdminPermissions::HANDLE_TICKET)) {
			return abort(403, 'Không có quyền');
		}

		$params = $request->parameters();
		$params['admin_id'] = admin()->id;
		$params['is_customer'] = false;
		return response()->json($this->ticketService->createTicketMessage($ticket, $params));
	}
}

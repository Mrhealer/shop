<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Brand\StoreBrandRequest;
use App\Models\Brand;
use App\Services\BrandService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class BrandController extends Controller
{
    /** @var BrandService */
    protected $_brandService;

    /**
     * ProdBrandController constructor.
     * @param  BrandService  $brandService
     */
    public function __construct(BrandService $brandService)
    {
        $this->_brandService = $brandService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

	    if (Gate::denies(AdminPermissions::VIEW_BRAND)) {
		    return abort(403, 'Không có quyền');
	    }

	    $search = $request->input('search', '');
        return view('admin.screens.brands', [
            'brands' => $this->_brandService->searchBrand($request->all()),
            'keyword' => $search
        ]);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {

	    if (Gate::denies(AdminPermissions::VIEW_BRAND)) {
		    return abort(403, 'Không có quyền');
	    }

	    return response()->json($this->_brandService->searchBrand($request->all()));
    }

    /**
     * @param  StoreBrandRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreBrandRequest $request)
    {

	    if (Gate::denies(AdminPermissions::EDIT_BRAND)) {
		    return abort(403, 'Không có quyền');
	    }

	    return response()->json($this->_brandService->createBrand($request->parameters()));
    }

    /**
     * @param $brand
     * @param  StoreBrandRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Brand $brand, StoreBrandRequest $request)
    {

	    if (Gate::denies(AdminPermissions::EDIT_BRAND)) {
		    return abort(403, 'Không có quyền');
	    }

	    return response()->json($this->_brandService->updateBrand($brand, $request->parameters()));
    }

    /**
     * @param  Brand  $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Brand $brand)
    {

	    if (Gate::denies(AdminPermissions::DELETE_BRAND)) {
		    return abort(403, 'Không có quyền');
	    }

	    return response()->json($this->_brandService->deleteBrand($brand));
    }
}

<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProdCategory\StoreProdCategoryRequest;
use App\Models\ProdCategory;
use App\Services\ProdCategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ProdCategoryController extends Controller
{
    /** @var ProdCategoryService */
    protected $_categoryService;

    /**
     * ProdCategoryController constructor.
     * @param  ProdCategoryService  $categoryService
     */
    public function __construct(ProdCategoryService $categoryService)
    {
        $this->_categoryService = $categoryService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_PROD_CATEGORY)) {
		    return abort(403, 'Không có quyền');
	    }
        $search = $request->input('search', '');
        return view('admin.screens.prod_categories', [
            'categories' => $this->_categoryService->searchCategory($request->all()),
            'keyword' => $search
        ]);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_PROD_CATEGORY)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_categoryService->searchCategory($request->all()));
    }

    /**
     * @param  StoreProdCategoryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreProdCategoryRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_PROD_CATEGORY)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_categoryService->createCategory($request->parameters()));
    }

    /**
     * @param $category
     * @param  StoreProdCategoryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProdCategory $category, StoreProdCategoryRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_PROD_CATEGORY)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_categoryService->updateCategory($category, $request->parameters()));
    }

    /**
     * @param  ProdCategory  $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(ProdCategory $category)
    {
	    if (Gate::denies(AdminPermissions::DELETE_PROD_CATEGORY)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_categoryService->deleteCategory($category));
    }
}

<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\File\UploadFileRequest;
use App\Http\Requests\File\ResizeImageRequest;
use App\Http\Requests\File\DeleteFileRequest;
use App\Services\FileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class FileController extends Controller
{
    protected $_fileService;

    public function __construct(FileService $fileService)
    {
      $this->_fileService = $fileService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

	    if (Gate::denies(AdminPermissions::VIEW_FILE)) {
		    return abort(403, 'Không có quyền');
	    }

        $hasEditor = $request->has('CKEditor');
        $hasSelector = $request->has('selector');
        $layout = ($hasEditor || $hasSelector) ? 'iframe' : 'app';
        return view('admin.screens.files', [
            'layout'      => $layout,
            'hasEditor'   => $hasEditor,
            'hasSelector' => $hasSelector
        ]);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showUploadAnonymousForm(Request $request)
    {
        return view('files.upload_anonymous');
    }

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function browser(Request $request) {

	    if (Gate::denies(AdminPermissions::VIEW_FILE)) {
		    return abort(403, 'Không có quyền');
	    }
        $user = admin();
	    return response()->json($this->_fileService->browser($user, $request->input('directory', '')));
    }

	public function makeDirectory(Request $request)
	{
		if (Gate::denies(AdminPermissions::EDIT_DIR)) {
			return abort(403, 'Không có quyền');
		}

		$user = admin();
		return response()->json($this->_fileService->makeDirectory($request->all(), $user));
	}

    /**
     * @param  UploadFileRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(UploadFileRequest $request)
    {
	    if (Gate::denies(AdminPermissions::UPLOAD_FILE)) {
		    return abort(403, 'Không có quyền');
	    }

        $user = admin();
        return response()->json($this->_fileService->upload($request, $user));
    }

    /**
     * @param  UploadFileRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadAnonymous(UploadFileRequest $request)
    {
        return response()->json($this->_fileService->uploadAnonymous($request));
    }

    /**
     * @param  ResizeImageRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resize(ResizeImageRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_FILE)) {
		    return abort(403, 'Không có quyền');
	    }
        $user = admin();
        return response()->json($this->_fileService->resizeImage(
            $request->input('files', []),
            $request->input('width'),
            $request->input('height'),
            $user
        ));
    }

    /**;
     * @param  DeleteFileRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(DeleteFileRequest $request)
    {
	    if (Gate::denies(AdminPermissions::DELETE_FILE)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_fileService->delete($request->input('files', []),$request->input('directories', []) ));
    }

}

<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;;
use App\Http\Requests\Admin\SupplyUnit\StoreSupplyUnitRequest;
use App\Http\Requests\Admin\SupplyUnit\UpdateSupplyUnitRequest;
use App\Models\SupplyUnit;
use App\Services\SupplyUnitService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\Upload\ImportExcelRequest;

class SupplyUnitController extends Controller
{
    /** @var SupplyUnitService */
    protected $supplyUnitService;

    /**
     * WarehouseController constructor.
     * @param  SupplyUnitService  $supplyUnitService
     */
    public function __construct(SupplyUnitService $supplyUnitService)
    {
        $this->supplyUnitService = $supplyUnitService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_SUPPLY_UNIT)) {
		    return abort(403, 'Không có quyền');
	    }
        dd($this->supplyUnitService->searchSupplyUnit($request->all())->toArray());
	    $search = $request->input('search', '');
        return view('admin.screens.supply_units', [
            'units' => $this->supplyUnitService->searchSupplyUnit($request->all()),
            'keyword' => $search
        ]);
    }

    /**
     * @param  StoreSupplyUnitRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreSupplyUnitRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_SUPPLY_UNIT)) {
		    return abort(403, 'Không có quyền');
	    }

	    return response()->json($this->supplyUnitService->createSupplyUnit($request->parameters()));
    }

    /**
     * @param SupplyUnit $unit
     * @param UpdateSupplyUnitRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SupplyUnit $unit, UpdateSupplyUnitRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_SUPPLY_UNIT)) {
		    return abort(403, 'Không có quyền');
	    }

	    return response()->json($this->supplyUnitService->updateSupplyUnit($unit, $request->parameters()));
    }

    /**
     * @param  SupplyUnit  $unit
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(SupplyUnit $unit)
    {
	    if (Gate::denies(AdminPermissions::DELETE_SUPPLY_UNIT)) {
		    return abort(403, 'Không có quyền');
	    }

	    return response()->json($this->supplyUnitService->deleteSupplyUnit($unit));
    }

    /**
     * @param ImportExcelRequest $request
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function importExcel(ImportExcelRequest $request)
    {
        if (Gate::denies(AdminPermissions::EDIT_SUPPLY_UNIT)) {
            return abort(403, 'Không có quyền');
        }

        return response()->json($this->supplyUnitService->importSupplyUnitExcel($request->file('file')));
    }
}

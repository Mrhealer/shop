<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;;

use App\Http\Requests\Admin\Menu\StoreMenuRequest;
use App\Http\Requests\Admin\Menu\UpdateMenuRequest;
use App\Http\Requests\Admin\SupplyUnit\StoreSupplyUnitRequest;
use App\Http\Requests\Admin\SupplyUnit\UpdateSupplyUnitRequest;
use App\Models\CustomMenus;
use App\Models\SupplyUnit;
use App\Services\MenuService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class MenuController extends Controller
{
    /** @var MenuService */
    protected $menuService;

    /**
     * MenuController constructor.
     * @param MenuService $menuService
     */
    public function __construct(MenuService $menuService)
    {
        $this->menuService = $menuService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

	    if (Gate::denies(AdminPermissions::VIEW_MENU)) {
		    return abort(403, 'Không có quyền');
	    }

        $search = $request->input('search', '');
        return view('admin.screens.menus', [
            'menus' => $this->menuService->searchMenu($request->all()),
            'keyword' => $search
        ]);
    }

    /**
     * @param  StoreMenuRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreMenuRequest $request)
    {

	    if (Gate::denies(AdminPermissions::EDIT_MENU)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->menuService->createMenu($request->parameters()));
    }

    /**
     * @param CustomMenus $menu
     * @param UpdateMenuRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CustomMenus $menu, UpdateMenuRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_MENU)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->menuService->updateMenu($menu, $request->parameters()));
    }

    /**
     * @param CustomMenus $menu
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail(CustomMenus $menu) {
	    if (Gate::denies(AdminPermissions::VIEW_MENU)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($menu);
    }

    /**
     * @param  CustomMenus  $menu
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(CustomMenus $menu)
    {
	    if (Gate::denies(AdminPermissions::DELETE_MENU)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->menuService->deleteMenu($menu));
    }
}

<?php
namespace App\Http\Controllers\Admin;

use App\Enums\OrderStatus;
use App\Enums\AdminPermissions;
use App\Enums\ShippingStatus;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Support\Facades\Gate;


class RevenueController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
	    if (Gate::denies(AdminPermissions::VIEW_REVENUE)) {
		    return abort(403, 'Không có quyền');
	    }
        $today = date("Y-m-d"). ' 00:00:00';
        $month = date('Y-m-d', strtotime('-30 days')). ' 00:00:00';
        $quarter = date('Y-m-d', strtotime('-120 days')). ' 00:00:00';

        // TODO: Tạm thời sau này sẽ fix lại = DB raw
        $pendingOrders = Order::whereStatus(OrderStatus::PENDING)->count();
        $processingOrders = Order::whereStatus(OrderStatus::PROCESSING)->count();
        $shippingOrders = Order::whereShippingStatus(ShippingStatus::SHIPPING)->count();
        $todaySuccessOrders = Order::whereStatus(OrderStatus::SUCCESS)->where('created_at', '>=', $today)->count();
        $revenueToday = Order::whereStatus(OrderStatus::SUCCESS)->where('created_at', '>=', $today)->sum('pay_money');
        $monthSuccessOrders = Order::whereStatus(OrderStatus::SUCCESS)->where('created_at', '>=', $month)->count();
        $revenueMonth = Order::whereStatus(OrderStatus::SUCCESS)->where('created_at', '>=', $month)->sum('pay_money');
        $quarterSuccessOrders = Order::whereStatus(OrderStatus::SUCCESS)->where('created_at', '>=', $quarter)->count();
        $revenueQuarter = Order::whereStatus(OrderStatus::SUCCESS)->where('created_at', '>=', $quarter)->sum('pay_money');

        return view('admin.screens.revenue', compact(
            'pendingOrders',
            'processingOrders',
            'shippingOrders',
            'todaySuccessOrders',
            'revenueToday',
            'monthSuccessOrders',
            'revenueMonth',
            'quarterSuccessOrders',
            'revenueQuarter',
            'today',
            'month',
            'quarter'
        ));
    }
}

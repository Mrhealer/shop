<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Role\StoreRoleRequest;
use App\Models\Role;
use App\Services\RoleService;
use Illuminate\Support\Facades\Gate;

class RoleController extends Controller
{
    /** @var RoleService */
    protected $roleService;

    /**
     * RoleController constructor.
     * @param  RoleService  $roleService
     */
    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

	    if (Gate::denies(AdminPermissions::VIEW_ROLE)) {
		    return abort(403, 'Không có quyền');
	    }
        return view('admin.screens.roles');
    }

    /**
     * @param StoreRoleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRoleRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_ROLE)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->roleService->createRole($request->parameters()));
    }

    /**
     * @param Role $role
     * @param StoreRoleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Role $role, StoreRoleRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_ROLE)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->roleService->updateRole($role, $request->parameters()));
    }


    /**
     * @param Role $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Role $role)
    {
	    if (Gate::denies(AdminPermissions::DELETE_ROLE)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->roleService->deleteRole($role));
    }
}

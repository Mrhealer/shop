<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Exports\Admin\ProductsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Product\StoreProductRequest;
use App\Http\Requests\Admin\Product\UpdateProductRequest;
use App\Http\Requests\Admin\Upload\ImportExcelRequest;
use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ProductController extends Controller
{
    /** @var ProductService */
    protected $_productService;

    /**
     * ProductController constructor.
     * @param  ProductService  $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->_productService = $productService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_PRODUCT)) {
		    return abort(403, 'Không có quyền');
	    }
        $params = $request->all();
        return view('admin.screens.products', [
            'products' => $this->_productService->searchProduct($params),
            'params' => empty($params) ? ['search' => ''] : $params,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
	    if (Gate::denies(AdminPermissions::VIEW_PRODUCT)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_productService->searchProduct($request->all()));
    }

    /**
     * @param StoreProductRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreProductRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_PRODUCT)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_productService->createProduct($request->parameters()));
    }

    /**
     * @param Product $product
     * @param UpdateProductRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Product $product, UpdateProductRequest $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_PRODUCT)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_productService->updateProduct($product, $request->parameters()));
    }

    /**
     * @param Product $product
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAttributes(Product $product, Request $request)
    {
	    if (Gate::denies(AdminPermissions::EDIT_PRODUCT)) {
		    return abort(403, 'Không có quyền');
	    }

        return response()->json($this->_productService->updateProductAttributes($product, $request->all()));
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Product $product)
    {
	    if (Gate::denies(AdminPermissions::DELETE_PRODUCT)) {
		    return abort(403, 'Không có quyền');
	    }

        return response()->json($this->_productService->deleteProduct($product));
    }

    /**
     * @param ImportExcelRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importExcel(ImportExcelRequest $request)
    {

	    if (Gate::denies(AdminPermissions::IMPORT_PRODUCT)) {
		    return abort(403, 'Không có quyền');
	    }

    	return response()->json($this->_productService->importProductExcel($request->file('file')));
    }

    /**
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportExcel()
    {
	    if (Gate::denies(AdminPermissions::EXPORT_PRODUCT)) {
		    return abort(403, 'Không có quyền');
	    }
        return (new ProductsExport)->download('products.xlsx');
    }
}

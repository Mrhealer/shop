<?php

namespace App\Http\Controllers\Admin;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Admin\StoreAdminRequest;
use App\Http\Requests\Admin\Admin\UpdateAdminRequest;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\UpdateAdminProfileRequest;
use App\Models\Admin;
use App\Services\AdminService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class AdminController extends Controller
{

	protected $adminService;

	public function __construct(AdminService $adminService)
	{
		$this->adminService = $adminService;
	}

	public function index(Request $request)
	{
		if (Gate::denies(AdminPermissions::VIEW_ADMIN)) {
			return abort(403, 'Không có quyền');
		}

		return view('admin.screens.admins', [
			'admins' => $this->adminService->searchAdmin($request->all()),
			'keyword' => $request->input('search', ''),
		]);
	}

	public function profile()
	{
		return view('admin.screens.profile');
	}

	public function store(StoreAdminRequest $request)
	{
		if (Gate::denies(AdminPermissions::EDIT_ADMIN)) {
			return abort(403, 'Không có quyền');
		}

		return response()->json($this->adminService->createAdmin($request->parameters()));
	}

	public function update(Admin $admin, UpdateAdminRequest $request)
	{
		if (Gate::denies(AdminPermissions::EDIT_ADMIN)) {
			return abort(403, 'Không có quyền');
		}

		return response()->json($this->adminService->updateAdmin($admin, $request->parameters()));
	}

	/**
	 * @param ChangePasswordRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function changePassword(ChangePasswordRequest $request)
	{
		$admin = admin();
		$admin->update($request->parameters());

		return response()->json(['success' => true]);
	}

	/**
	 * @param UpdateAdminProfileRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function updateProfile(UpdateAdminProfileRequest $request)
	{
		$admin = admin();
		$admin->update($request->parameters());

		return response()->json(['success' => true]);
	}

	public function delete(Admin $admin)
	{
		if (Gate::denies(AdminPermissions::DELETE_ADMIN)) {
			return abort(403, 'Không có quyền');
		}
		return response()->json($this->adminService->deleteAdmin($admin));
	}
}

<?php
namespace App\Http\Controllers\Staff;

use App\Enums\AdminPermissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Shipper\Order\UpdateShippingInfoOrderRequest;
use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class OrderController extends Controller
{
    /** @var OrderService */
    protected $_orderService;

    /**
     * OrderController constructor.
     * @param  OrderService  $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->_orderService = $orderService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
	    if (Gate::denies(AdminPermissions::HANDLE_SHIPPING_BILL)) {
		    return abort(403, 'Không có quyền');
	    }
        $params = $request->all();
        $params['shipper_id'] = admin()->id;

        return view('staff.screens.orders', [
            'orders' => $this->_orderService->searchOrder($params),
            'params' => $params,
        ]);
    }

    /**
     * @param Order $order
     * @param UpdateShippingInfoOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateShippingInfo(Order $order, UpdateShippingInfoOrderRequest $request)
    {
	    if (Gate::denies(AdminPermissions::HANDLE_SHIPPING_BILL)) {
		    return abort(403, 'Không có quyền');
	    }
        return response()->json($this->_orderService->shipperUpdateShippingInfoOrder($order, $request->parameters(), admin()));
    }
}

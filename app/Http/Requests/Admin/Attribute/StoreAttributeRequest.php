<?php

namespace App\Http\Requests\Admin\Attribute;

use App\Http\Requests\BaseRequest;
use App\Enums\AttributeGroup;
use Illuminate\Validation\Rule;

class StoreAttributeRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name'              => 'required|max:255',
            'prod_category_ids' => 'required|array',
            'prod_category_ids.*' => 'required|exists:prod_categories,id',
            'multi_values'      => 'required',
            'is_show_on_filter'      => 'required',
            'group_type'              => [
                'required',
                Rule::in(AttributeGroup::values())
            ],
        ]);
    }
}

<?php

namespace App\Http\Requests\Admin\Warranty;

use App\Enums\ProductWarrantyStatus;
use App\Enums\WarrantyStatus;
use App\Http\Requests\BaseRequest;

class UpdateWarrantyRequest extends BaseRequest
{
	public function rules()
	{
		return array_merge(parent::rules(), [
			'customer_name' => 'required',
			'customer_email' => 'required',
			'customer_phone' => 'required',
			'customer_address' => 'required',
			'status' => 'required|in:' . implode(',', WarrantyStatus::toArray()),
			'action' => 'required',
			'products.*.order_id' => 'required|exists:orders,id',
			'products' => 'required|array|min:1',
			'products.*.product_id' => 'required|exists:products,id',
			'products.*.before' => 'required',
			'products.*.cost' => 'required|numeric|min:0',
			'products.*.expect_return_at' => 'required',
			'products.*.tech_id' => 'required|exists:users,id',
			'products.*.status' => 'required|in:' . implode(',', ProductWarrantyStatus::toArray()),
			'products.*.new_serial' => 'required_if:products.*.is_refund_new,1',
			'products.*.after' => 'required_if:products.*.status,'.ProductWarrantyStatus::SUCCESS,

		]);
	}

	public function messages()
	{
		return array_merge(parent::messages(), [
			'products.*.product_id.exists' => 'Mã sản phẩm của trường :attribute không tồn tại.',
			'products.*.serial.exists' => 'Số serial trong trường :attribute không đúng.',
			'before_status.required' => 'Trạng thái sản phẩm không được bỏ trống.',
		]);
	}
}
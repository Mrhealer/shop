<?php

namespace App\Http\Requests\Admin\Order;

use App\Enums\OrderStatus;
use App\Enums\PaymentStatus;
use App\Http\Requests\BaseRequest;

class UpdateOrderRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'status'  => 'required|in:' . implode(',', OrderStatus::toArray()),
            'payment_status'  => 'required|in:' . implode(',', PaymentStatus::toArray()),
            'note'  => 'max:255',
        ]);
    }

    public function parameters()
    {
        return [
            'status' => $this->input('status'),
            'payment_status' => $this->input('payment_status'),
            'note' => $this->input('note')
        ];
    }
}

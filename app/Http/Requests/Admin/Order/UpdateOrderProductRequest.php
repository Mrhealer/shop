<?php

namespace App\Http\Requests\Admin\Order;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class UpdateOrderProductRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'product_id' => [
                'required',
                'integer',
                Rule::exists('products', 'id')
                    ->where('is_deleted', false)
            ],
            'quantity'   => 'required|integer|min:1',
        ]);
    }

    public function parameters()
    {
        return [
            'product_id' => $this->input('product_id'),
            'quantity' => $this->input('quantity')
        ];
    }
}

<?php

namespace App\Http\Requests\Admin\Order;

use App\Enums\ShippingStatus;
use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class UpdateShippingInfoOrderRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'shipper_id' => [
                'required',
                'integer',
            ],
            'transport_company_id' => [
                'required',
                'integer',
                Rule::exists('transport_companies', 'id')
                    ->where('is_deleted', false)
            ],
            'track_code' => 'required|string|max:255|unique:orders,track_code,' .$this->route('order')->id,
            'ship_money' => 'required|integer|min:0',
            'shipping_status'  => 'required|in:' . implode(',', ShippingStatus::toArray()),
            'note'  => 'max:255',
        ]);
    }

    public function parameters()
    {
        return [
            'shipper_id' => $this->input('shipper_id'),
            'transport_company_id' => $this->input('transport_company_id'),
            'track_code' => $this->input('track_code'),
            'ship_money' => $this->input('ship_money'),
            'shipping_status' => $this->input('shipping_status'),
            'note' => $this->input('note'),
        ];
    }
}

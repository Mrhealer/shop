<?php

namespace App\Http\Requests\Admin\Inventory;

use App\Enums\ExportInventoryType;
use Illuminate\Validation\Rule;
use App\Http\Requests\BaseRequest;

class ExportInventoryRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'export_type' => [
                'required',
                Rule::in(ExportInventoryType::toArray()),
            ],
            'inventories' => 'required|array',
            'export_note' => 'max:255'
        ]);
    }

    public function messages()
    {
        return [
            'inventories.required' => 'Vui lòng nhập serial',
            'inventories.array' => 'Danh sách serial phải là một array'
        ];
    }
}

<?php

namespace App\Http\Requests\Admin\Inventory;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class StoreInventoryRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'product_id' => [
                'required',
                'integer',
                Rule::exists('products', 'id')
                    ->where('is_deleted', false)
            ],
            'supply_unit_id' => [
                'required',
                'integer',
                Rule::exists('supply_units', 'id')
                    ->where('is_deleted', false)
            ],
            'import_to_warehouse_id' => [
                'required',
                'integer',
                Rule::exists('warehouses', 'id')
                    ->where('is_deleted', false)
            ],
            'serial_list' => 'required|array',
            'brand_warranty_period' => 'required|date_format:Y-m-d',
            'warranty_period' => 'required|integer|min:0',
            'note' => 'max:255',
        ]);
    }

    public function parameters()
    {
        return [
            'serial_list' => $this->input('serial_list'),
            'product_id' => $this->input('product_id'),
            'supply_unit_id' => $this->input('supply_unit_id'),
            'import_to_warehouse_id' => $this->input('import_to_warehouse_id'),
            'brand_warranty_period' => $this->input('brand_warranty_period'),
            'warranty_period' => $this->input('warranty_period'),
            'note' => $this->input('note'),
        ];
    }
}

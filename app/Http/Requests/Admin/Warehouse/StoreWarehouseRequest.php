<?php

namespace App\Http\Requests\Admin\Warehouse;

use App\Http\Requests\BaseRequest;

class StoreWarehouseRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name' => 'required|unique:warehouses|max:255',
            'address' => 'required|max:255',
            'description' => 'max:255'
        ]);
    }

    public function parameters()
    {
        return [
            'name' => $this->input('name'),
            'address' => $this->input('address'),
            'description' => $this->input('description')
        ];
    }
}

<?php

namespace App\Http\Requests\Admin\AttributeValue;

use App\Http\Requests\BaseRequest;

class StoreAttributeValueRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'value' => 'required|max:255'
        ]);
    }
}

<?php

namespace App\Http\Requests\Admin\Voucher;

use App\Enums\VoucherType;
use App\Http\Requests\BaseRequest;

class StoreVoucherRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'type'  => 'required|in:' . implode(',', VoucherType::toArray()),
            'product_id' => 'required_if:type,1|exists:products,id',
            'prod_category_id' => 'required_if:type,3|exists:prod_categories,id',
            'brand_id' => 'required_if:type,2|exists:brands,id',
            'code' => 'required|max:50|unique:vouchers,code',
            'value' => 'required|integer|min:0',
            'limit' => 'required|integer|min:0',
            'min_money_required' => 'required|integer|min:0',
            'max_money_discount' => 'required|integer|min:0',
            'expired_at' => 'required',
            'active' => 'required'
        ]);
    }

    public function messages()
    {
        return array_merge(parent::messages(), [
            'product_id.required_if' => 'Chưa chọn sản phẩm',
            'prod_category_id.required_if' => 'Chưa chọn danh mục sản phẩm',
            'brand_id.required_if' => 'Chưa chọn thương hiệu',
        ]);
    }

}

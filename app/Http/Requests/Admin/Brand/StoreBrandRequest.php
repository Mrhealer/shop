<?php

namespace App\Http\Requests\Admin\Brand;

use App\Http\Requests\BaseRequest;

class StoreBrandRequest extends BaseRequest
{
    public function rules()
    {
        if ($this->input('id')) {
            $ruleName = 'required|max:255|unique:brands,name,' . $this->input('id');
        } else {
            $ruleName = 'required|max:255|unique:brands';
        }

        return array_merge(parent::rules(), [
            'name' => $ruleName,
            'prod_category_ids' => 'required|array',
            'prod_category_ids.*' => 'required|exists:prod_categories,id',
            'url' => 'required|max:255',
            'image' => 'required|max:255',
        ]);
    }

}

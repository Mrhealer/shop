<?php

namespace App\Http\Requests\Admin\Product;

use App\Http\Requests\BaseRequest;

class StoreProductRequest extends BaseRequest
{
	public function rules()
	{
		return array_merge(parent::rules(), [
			'prod_category_id' => 'required|integer|exists:prod_categories,id',
			'brand_id' => 'required|integer|exists:brands,id',
			'name' => 'required|max:255|unique:products',
			'sku' => 'required|max:255|unique:products,sku',
			'price' => 'required|integer|min:0',
			'description' => 'required',
			'images' => 'required|array',
		]);
	}

	public function parameters()
	{
		$params = parent::parameters();
		if (!empty($this->input('images'))) {
			$params['images'] = json_encode($this->input('images'));
		}
		return $params;
	}
}

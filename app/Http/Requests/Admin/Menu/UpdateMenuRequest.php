<?php

namespace App\Http\Requests\Admin\Menu;

use App\Enums\CustomMenuObjType;
use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class UpdateMenuRequest extends BaseRequest
{
    public function rules()
    {
        $ruleObjectId = [
            'nullable',
            'integer'
        ];

        if($this->input('object_type') === CustomMenuObjType::PRODUCT_CATEGORY) {
            $ruleObjectId[] = 'required';
            $ruleObjectId[] = Rule::exists('prod_categories', 'id')->where('is_deleted', false);
        }

        return array_merge(parent::rules(), [
            'name' => [
                'required',
                'max:255'
            ],
            'object_type' => 'required|in:' . implode(',', CustomMenuObjType::toArray()),
            'object_id' => $ruleObjectId,
            'order' => 'integer',
            'url' => "nullable|url|max:255",
            'parent_id' => [
                'nullable',
                'integer',
                Rule::exists('custom_menus', 'id')
            ]
        ]);
    }

    public function parameters()
    {
        return [
            'name' => $this->input('name'),
            'object_id' => $this->input('object_id'),
            'object_type' => $this->input('object_type'),
            'url' => $this->input('url'),
            'order' => empty($this->input('order')) ? 1 : $this->input('order'),
            'parent_id' => $this->input('parent_id')
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'object_id' => 'danh mục',
        ];
    }
}

<?php

namespace App\Http\Requests\Admin\Upload;

use App\Http\Requests\BaseRequest;

class ImportExcelRequest extends BaseRequest
{
	public function rules()
	{
		return [
			'file'  => 'required|max:500000'
		];
	}
}

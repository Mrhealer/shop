<?php

namespace App\Http\Requests\Admin\SupplyUnit;

use App\Http\Requests\BaseRequest;

class StoreSupplyUnitRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name' => 'required|unique:supply_units|max:255',
            'tax_code' => 'nullable|unique:supply_units|max:255',
            'address' => 'required|max:255',
            'description' => 'max:255'
        ]);
    }

    public function parameters()
    {
        return [
            'name' => $this->input('name'),
            'address' => $this->input('address'),
            'description' => $this->input('description'),
            'tax_code' => empty($this->input('tax_code')) ? '' : $this->input('tax_code')
        ];
    }
}

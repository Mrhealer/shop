<?php

namespace App\Http\Requests\Admin\SupplyUnit;

use App\Http\Requests\BaseRequest;

class UpdateSupplyUnitRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name' => 'required|max:255|unique:supply_units,name,' .$this->input('id'),
            'tax_code' => 'nullable|max:255|unique:supply_units,tax_code,' .$this->input('id'),
            'address' => 'required|max:255',
            'description' => 'max:255'
        ]);
    }

    public function parameters()
    {
        return [
            'name' => $this->input('name'),
            'address' => $this->input('address'),
            'description' => $this->input('description'),
            'tax_code' => empty($this->input('tax_code')) ? '' : $this->input('tax_code')
        ];
    }
}

<?php

namespace App\Http\Requests\Admin\Ticket;

use App\Http\Requests\BaseRequest;

class CreateMessageRequest extends BaseRequest
{
	public function rules()
	{
		return [
			'message' => 'required',
		];
	}
}
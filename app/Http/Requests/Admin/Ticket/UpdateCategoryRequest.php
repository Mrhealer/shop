<?php

namespace App\Http\Requests\Admin\Ticket;

use App\Http\Requests\BaseRequest;

class UpdateCategoryRequest extends BaseRequest
{
	public function rules()
	{
		return [
			"name" => "required|unique:ticket_categories,name," .$this->input('id'),
			"discord_webhook" => "required|unique:ticket_categories,discord_webhook," .$this->input('id'),
			"signature" => "required",
		    "time_handle_default" => "required|numeric|min:1",
		    "time_receiver_default" => "required|numeric|min:1",
		    "close_message_default" => "required",
		    "status" => "required",
		];
	}
}
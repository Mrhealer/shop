<?php

namespace App\Http\Requests\Admin\Ticket;

use App\Http\Requests\BaseRequest;

class UpdateTicketRequest extends BaseRequest
{
	public function rules()
	{
		return [
			"ticket_category_id" => "required",
			"priority" => "required",
			"status" => "required",
			"title" => "required",
		];
	}
}
<?php

namespace App\Http\Requests\Admin\ProdCategory;

use App\Http\Requests\BaseRequest;

class StoreProdCategoryRequest extends BaseRequest
{
    public function rules()
    {
        if ($this->input('id')) {
            $ruleName = 'required|max:255|unique:prod_categories,name,' . $this->input('id');
        } else {
            $ruleName = 'required|max:255|unique:prod_categories';
        }

        return array_merge(parent::rules(), [
            'name' => $ruleName,
            'order' => 'integer',
            'icon' => 'required|max:255',
            'banner' => 'max:255',
            'banner_link' => 'required_if:banner,url',
            'slider' => 'array',
        ]);
    }

    public function parameters()
    {
        $params =  parent::parameters();
        if (!empty($this->input('slider')) && $this->input('slider') !== '') {
            $params['slider'] = json_encode($this->input('slider'));
        } else {
            $params['slider'] = json_encode([]);
        }

        return $params;
    }
}

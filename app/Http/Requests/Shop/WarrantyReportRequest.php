<?php

namespace App\Http\Requests\Shop;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class WarrantyReportRequest extends BaseRequest
{
	public function rules()
	{
		return array_merge(parent::rules(), [
			'customer_name' => 'required|max:255',
			'customer_email' => 'required|max:255|email',
			'customer_address' => 'required|max:255',
			'customer_phone' => [
				'required',
				'regex:/(0[1|2|3|4|5|6|8|9])+([0-9]{8})\b/'
			],
			'images' => 'required|array',
			'products' => 'required|array',
			'products.*.serial' => 'required|exists:inventories,serial',
			'products.*.product_id' => 'required|exists:products,id',
			'products.*.before' => 'required',
		]);
	}


	public function attributes()
	{
		return array_merge(parent::parameters(), [
			'products.*.before' => 'trạng thái lỗi sản phẩm '. $this->input('products.*.*')[3],
			'images' => 'ảnh mô tả lỗi',
			'products.*.serial' => 'mã serial',
			'customer_name' => 'Họ và tên',
			'customer_phone' => 'Số điện thoại',
			'customer_email' => 'Email',
			'customer_address' => 'Địa chỉ',
		]);
	}

	public function authorize()
	{
		return true;
	}
}
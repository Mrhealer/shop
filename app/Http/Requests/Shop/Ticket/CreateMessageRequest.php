<?php

namespace App\Http\Requests\Shop\Ticket;

use App\Http\Requests\BaseRequest;

class CreateMessageRequest extends BaseRequest
{
	public function rules()
	{
		return [
			'message' => 'required'
		];
	}
}
<?php

namespace App\Http\Requests\Shop\Ticket;

use App\Http\Requests\BaseRequest;

class CreateTicketRequest extends BaseRequest
{
	public function rules()
	{
		return [
			"ticket_category_id" => "required",
			"priority" => "required",
			"title" => "required",
			"message" => "required",
		];
	}
}
<?php

namespace App\Http\Requests\Shop\Ticket;

use App\Enums\TicketStatus;
use App\Http\Requests\BaseRequest;

class UpdateTicketRequest extends BaseRequest
{
	public function rules()
	{
		return [
			'status'  => 'in:' . implode(',', TicketStatus::toArray()),
			'star' => 'numeric|min:0|max:5'
		];
	}
}
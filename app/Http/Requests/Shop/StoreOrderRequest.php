<?php

namespace App\Http\Requests\Shop;

use App\Http\Requests\BaseRequest;
use App\Enums\PaymentMethod;
use App\Enums\ReceivedtMethod;

class StoreOrderRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'products'   => 'required|array',
            'products.*' => 'required',

            'payment_method'  => 'required|in:' . implode(',', PaymentMethod::toArray()),
            'received_method' => 'required|in:' . implode(',', ReceivedtMethod::toArray()),

            'name'  => 'required|max:255',
            'phone' => [
                'required',
                'regex:/(0[1|2|3|4|5|6|8|9])+([0-9]{8})\b/'
            ],
            'email' => 'required|email',

            'province'      => 'required|max:255',
            'province_name' => 'required|max:255',
            'district'      => 'required|max:255',
            'district_name' => 'required|max:255',
            'address'       => 'required|max:255',
            'note'          => 'max:255',

            'has_bill'                   => 'required',
            'company_name'               => 'required_if:has_bill,true|max:255',
            'company_address'            => 'required_if:has_bill,true|max:255',
            'company_tax_identification' => 'required_if:has_bill,true|max:255',
            'bill_received_address'      => 'required_if:has_bill,true|max:255',

        ]);
    }
    public function messages()
    {
        return array_merge(parent::messages(), [
            'products.required' => 'Bạn chưa chọn sản phẩm nào.',
            'company_name.required_if' => 'Bạn chưa nhập tên công ty.',
            'company_address.required_if' => 'Bạn chưa nhập địa chỉ công ty.',
            'company_tax_identification.required_if' => 'Bạn chưa nhập mã số thuế.',
            'bill_received_address.required_if' => 'Bạn chưa nhập địa chỉ nhận hóa đơn.',
        ]);
    }
}

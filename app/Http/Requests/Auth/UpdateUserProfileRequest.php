<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;

class UpdateUserProfileRequest extends BaseRequest
{
    const protectFields = [
        'cash',
        'fb_account_id',
        'email_verified_at',
        'password',
        'role_ids',
    ];

	public function rules()
	{
		return [
			'name' => 'required|string|max:255',
			'username' => 'required|email|unique:users,username,'.user()->id,
			'email' => 'required|email|unique:users,email,'.user()->id,
			'phone' => 'required|unique:users,phone,'.user()->id,
			'image' => 'max:255',
			'facebook' => 'url|max:255',
			'youtube_intro' => 'url|max:255',
			'skype' => 'max:255',

		];
	}

	public function parameters()
    {
        $params = parent::parameters();

        foreach (self::protectFields as $field) {
            if(isset($params[$field])) {
                unset($params[$field]);
            }
        }

        return $params;
    }

    public function authorize()
	{
		return true;
	}
}

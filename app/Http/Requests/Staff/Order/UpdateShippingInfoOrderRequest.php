<?php

namespace App\Http\Requests\Shipper\Order;

use App\Enums\ShippingStatus;
use App\Http\Requests\BaseRequest;

class UpdateShippingInfoOrderRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'shipping_status'  => 'required|in:' . implode(',', ShippingStatus::toArray()),
            'note'  => 'max:255',
        ]);
    }

    public function parameters()
    {
        return [
            'shipping_status' => $this->input('shipping_status'),
            'note' => $this->input('note')
        ];
    }
}

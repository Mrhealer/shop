<?php

namespace App\Mail;

use App\Models\Ticket;
use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class SendTicketMail extends Mailable implements ShouldQueue
{
	use Queueable, SerializesModels;

	protected $ticket;

	protected $message;

	public function __construct(Ticket $ticket, $message)
	{
		$this->ticket = $ticket;
		$this->message = $message;
	}

	public function build()
	{
		return $this->markdown('emails.send-ticket', [
			'ticket' => $this->ticket,
			'message' => $this->message
		]);
	}
}
<?php

namespace App\Mail;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $productDetail = [];

	/**
	 * Create a new message instance.
	 *
	 * @param Order $order
	 * @param Product $productDetail
	 */
    public function __construct(Order $order,Product $productDetail)
    {
        $this->order = $order->load('orderProducts');
	    $this->productDetail = $productDetail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.order', ['order' => $this->order])
            ->subject('Kowcomputer Thông Báo Đặt Hàng');
    }
}

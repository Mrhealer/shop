<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Shop
	    view()->composer(['shop.screens.*'], 'App\Http\ViewComposers\Shop\ShopComposer');
	    view()->composer(['auth.*'], 'App\Http\ViewComposers\Shop\ShopComposer');
        view()->composer(['shop.screens.home', 'shop.layouts.nav_mobile'], 'App\Http\ViewComposers\Shop\HomeComposer');
        view()->composer(['shop.screens.blog'], 'App\Http\ViewComposers\Shop\BlogComposer');

	    view()->composer(['shop.screens.user.ticket','shop.screens.user.ticket_detail'], 'App\Http\ViewComposers\Shop\TicketViewComposer');

        // Admin
        view()->composer(['admin.screens.*'], 'App\Http\ViewComposers\Admin\AdminDataComposer');
        view()->composer(['admin.screens.files'], 'App\Http\ViewComposers\Admin\FileComposer');
        view()->composer(['admin.screens.attributes'], 'App\Http\ViewComposers\Admin\AttributeComposer');
        view()->composer(['admin.screens.products'], 'App\Http\ViewComposers\Admin\ProductComposer');
        view()->composer(['admin.screens.orders'], 'App\Http\ViewComposers\Admin\OrderComposer');
        view()->composer(['admin.screens.users'], 'App\Http\ViewComposers\Admin\UserComposer');
        view()->composer(['admin.screens.profile'], 'App\Http\ViewComposers\Admin\ProfileComposer');
        view()->composer(['admin.screens.prod_categories'], 'App\Http\ViewComposers\Admin\ProdCategoryComposer');
        view()->composer(['admin.screens.blog_categories'], 'App\Http\ViewComposers\Admin\BlogCategoryComposer');
        view()->composer(['admin.screens.posts'], 'App\Http\ViewComposers\Admin\PostComposer');
        view()->composer(['admin.screens.vouchers'], 'App\Http\ViewComposers\Admin\VoucherComposer');
        view()->composer(['admin.screens.brands'], 'App\Http\ViewComposers\Admin\BrandComposer');
        view()->composer(['admin.screens.setting'], 'App\Http\ViewComposers\Admin\SettingComposer');
        view()->composer(['admin.screens.warehouses'], 'App\Http\ViewComposers\Admin\WarehouseComposer');
        view()->composer(['admin.screens.supply_units'], 'App\Http\ViewComposers\Admin\SupplyUnitComposer');
        view()->composer(['admin.screens.menus'], 'App\Http\ViewComposers\Admin\MenuComposer');
        view()->composer(['admin.screens.inventories'], 'App\Http\ViewComposers\Admin\InventoryComposer');
	    view()->composer(['admin.screens.warranty'], 'App\Http\ViewComposers\Admin\WarrantyComposer');
	    view()->composer(['admin.screens.warranty_prod'], 'App\Http\ViewComposers\Admin\WarrantyProductComposer');
	    view()->composer(['admin.screens.revenue'], 'App\Http\ViewComposers\Admin\RevenueComposer');


        // Staff
        view()->composer(['staff.screens.*'], 'App\Http\ViewComposers\Shipper\ShipperComposer');
        view()->composer(['staff.screens.orders'], 'App\Http\ViewComposers\Shipper\OrderComposer');

        //Ticket    
	    view()->composer(['admin.screens.ticket_category'], 'App\Http\ViewComposers\Admin\TicketCategoryComposer');
	    view()->composer(['admin.screens.ticket'], 'App\Http\ViewComposers\Admin\TicketComposer');


	    //Role
	    view()->composer(['admin.screens.roles'], 'App\Http\ViewComposers\Admin\RoleComposer');

	    view()->composer(['admin.screens.admins'], 'App\Http\ViewComposers\Admin\AdminComposer');
    }
}

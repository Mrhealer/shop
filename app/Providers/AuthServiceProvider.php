<?php

namespace App\Providers;

use App\Enums\AdminPermissions;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
	    $this->registerPolicies();

	    // Define permissions via gate
	    foreach (AdminPermissions::toArray() as $permission) {
		    Gate::define($permission, function ($user) use ($permission) {
			    return $user->canDo($permission);
		    });
	    }
    }
}

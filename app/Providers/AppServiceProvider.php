<?php

namespace App\Providers;

use App\Models\Warranty;
use App\Models\WarrantyProduct;
use App\Observers\WarrantyObserver;
use App\Observers\WarrantyProductObserver;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Set https
        if ($this->app['config']['app.force_https']) {
            $this->app['request']->server->set('HTTPS', 'on');
        }
        // Listen user events
        //User::observe(UserObserver::class);
		Warranty::observe(WarrantyObserver::class);
        WarrantyProduct::observe(WarrantyProductObserver::class);
        // Set default pagination view
        Paginator::defaultView('shop.layouts.pagination');
    }
}

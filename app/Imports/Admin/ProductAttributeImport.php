<?php

namespace App\Imports\Admin;

use App\Enums\AttributeGroup;
use App\Helpers\SlugHelper;
use App\Models\Attribute;
use App\Models\AttributeCategory;
use App\Models\AttributeValue;
use App\Models\Brand;
use App\Models\ProdCategory;
use App\Models\Product;
use App\Models\ProductAttributeValue;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Row;
use Illuminate\Support\Facades\Validator;

class ProductAttributeImport implements OnEachRow, WithChunkReading, WithHeadingRow, WithBatchInserts
{
    private $row = 0;

    public function headingRow(): int
	{
		return 1;
	}

	public function onRow(Row $row)
	{
        $row = $row->toArray();
        $this->row++;

        Validator::make($row, [
            'ten_danh_muc' => [
                'required',
                'max:255',
                Rule::exists('prod_categories','name')->where(function ($query) {
                    $query->where('is_deleted', false);
                }),
            ],
            'ten_thuong_hieu' => [
                'required',
                'max:255',
                Rule::exists('brands','name')->where(function ($query) {
                    $query->where('is_deleted', false);
                }),
            ],
            'ten' => ['required', 'max:255', Rule::unique('products', 'name')->whereNot('sku', $row['sku_dinh_danh'])],
            'sku_dinh_danh' => 'required|max:255',
            'gia_ban' => 'required|integer|min:0',
            'gia_niem_yet' => 'required|integer|min:0',
            'danh_sach_anh_san_pham' => 'required',
            'hien_thi_phan_tram_giam_gia' => 'required',
            'san_pham_hot' => 'required'
        ],[
            'required' => '[Hàng ' . $this->row . '] Trường :attribute không được bỏ trống.',
            'max' => '[Hàng ' . $this->row . '] Trường :attribute không được lớn hơn :max ký tự.',
            'min' => '[Hàng ' . $this->row . '] Trường :attribute phải có tối thiểu :min ký tự.',
            'exists' => '[Hàng ' . $this->row . '] Giá trị đã chọn trong trường :attribute không hợp lệ.',
            'unique' => '[Hàng ' . $this->row . '] Trường :attribute đã có trong hệ thống.',
            'integer' => '[Hàng ' . $this->row . '] Trường :attribute phải là một số nguyên.',
        ])->validate();

        $product = Product::firstOrCreate([
            'sku' => $row['sku_dinh_danh'],
            'is_deleted' => false
        ]);

        $productCategory = ProdCategory::where('name', $row['ten_danh_muc'])->where('is_deleted', false)->first();
        $brand = Brand::where('name', $row['ten_thuong_hieu'])->where('is_deleted', false)->first();

        $productAttributes = [
            'prod_category_id' => $productCategory->id,
            'brand_id' => $brand->id,
            'name' => $row['ten'],
            'slug' => SlugHelper::genSlug($row['ten']) . '-' . $row['sku_dinh_danh'],
            'sku' => $row['sku_dinh_danh'],
            'price' => $row['gia_ban'],
            'listed_price' => $row['gia_niem_yet'],
            'images' => json_encode(explode(',', $row['danh_sach_anh_san_pham'])),
            'show_percent_discount' => $row['hien_thi_phan_tram_giam_gia'] === "Yes",
            'is_hot' => $row['san_pham_hot'] === "Yes",
            'is_deleted' => false,
        ];
        $product->update($productAttributes);
        $product->save();

        // Prepare raw data
//        try {
//            $attributes = explode(';', $row['attributes']);
//        } catch (\Exception $e) {
//            return abort(422, 'Lỗi thông số sản phẩm số '.$this->row);
//        }


//        foreach ($attributes as $attribute) {
//            if(empty($attribute)) {
//                continue;
//            }
//            try {
//                $temp = explode(':', $attribute);
//                $values = explode('|', $temp[1]);
//            } catch (\Exception $e) {
//                return abort(422, 'Lỗi thông số "'.$attribute.'" sản phẩm '.$this->row);
//            }
//
//            //Get or insert new attribute
//            $attributeExists = Attribute::whereId($temp[0])->first();
//            if(!$attributeExists) {
//                return abort(422, 'Lỗi mã thông số "'.$temp[0].'" sản phẩm '.$this->row);
//            }
//
//            $attributeCategory = AttributeCategory::firstOrCreate([
//                'attribute_id' => $attributeExists->id,
//                'prod_category_id' => $product->prod_category_id,
//            ]);
//            $attributeCategory->save();
//
//            foreach ($values as $value) {
//                // Get or insert new attribute value
//                $attributeValue = AttributeValue::firstOrCreate([
//                    'attribute_id' => $attributeExists->id,
//                    'value' => $value,
//                ]);
//
//                // Mapping product attribute value
//                $productAttribute = new ProductAttributeValue([
//                    'product_id' => $product->id,
//                    'attribute_value_id' => $attributeValue->id,
//                ]);
//                $productAttribute->save();
//            }
//        }
	}

	public function batchSize(): int
	{
		return 1000;
	}

	/**
	 * @inheritDoc
	 */
	public function chunkSize(): int
	{
		return 1000;
	}
}

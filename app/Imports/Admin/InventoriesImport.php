<?php

namespace App\Imports\Admin;

use App\Models\Inventory;
use App\Models\Product;
use App\Models\SupplyUnit;
use App\Models\Warehouse;
use App\Services\ProductService;
use App\Services\WarehouseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class InventoriesImport implements ToModel, WithChunkReading, WithHeadingRow, WithBatchInserts, WithValidation
{
	use  SkipsFailures;

	public function headingRow(): int
	{
		return 1;
	}

	/**
	 * @param array $row
	 * @return mixed
	 */
	public function model(array $row)
	{
		return DB::transaction(function () use ($row) {
			try {
				$product = Product::where([
                    ['name', '=', $row['ten_san_pham']],
                    ['sku', '=', $row['sku']],
                    ['is_deleted', '=', false],
                ])->first();

                if (empty($product)) {
                    return abort('422', 'Tên sản phẩm và sku không khớp với nhau, vui lòng nhập lại.');
                }

                if(!empty($row['ma_so_thue_don_vi_cung_cap'])) {
                    $supply_unit = SupplyUnit::where([
                        ['name', '=', $row['ten_don_vi_cung_cap']],
                        ['tax_code', '=', $row['ma_so_thue_don_vi_cung_cap']],
                        ['is_deleted', '=', false],
                    ])->first();

                    if (empty($supply_unit)) {
                        return abort('422', 'Tên đơn vị cung cấp và mã số thuế không khớp với nhau, vui lòng nhập lại.');
                    }
                } else {
                    $supply_unit = SupplyUnit::where([
                        ['name', '=', $row['ten_don_vi_cung_cap']],
                        ['is_deleted', '=', false],
                    ])->first();
                }

                $warehouse = Warehouse::where('name', $row['ten_kho'])->where('is_deleted', false)->first();

                $admin = admin();

                $inventory =  new Inventory([
					'product_id' => $product->id,
					'import_to_warehouse_id' => $warehouse->id,
					'serial' => $row['serial'],
					'has_serial' => true,
					'quantity' => 1,
					'warranty_period' => $row['thoi_gian_bao_hanh'],
					'brand_warranty_period' => Date::excelToDateTimeObject($row['bao_hanh_hang_den_ngay']),
					'note' => $row['note'],
                    'import_admin_id' => $admin->id,
                    'supply_unit_id' => $supply_unit->id
				]);

                // Update quantity_goods of warehouse
                $warehouseService = new WarehouseService();

                $warehouse = Warehouse::find($inventory->import_to_warehouse_id);
                $warehouseService->updateWarehouse($warehouse, ['quantity_goods' => DB::raw('quantity_goods + 1')]);

                // Update qty_in_warehouses of product
                $productService = new ProductService();

                $product = Product::find($inventory->product_id);
                $productService->updateProduct($product, ['qty_in_warehouses' => DB::raw('qty_in_warehouses + 1')]);

                return $inventory;
			} catch (\Exception $exception) {
				return abort('422', $exception->getMessage());
			}
		});
	}

	public function rules(): array
	{
		return [
			'sku' => [
			    'required',
                'max:255',
                Rule::exists('products','sku')->where(function ($query) {
                    $query->where('is_deleted', false);
                }),
            ],
            'ten_san_pham' => [
                'required',
                'max:255',
                Rule::exists('products','name')->where(function ($query) {
                    $query->where('is_deleted', false);
                }),
            ],
            'ten_don_vi_cung_cap' => [
                'required',
                'max:255',
                Rule::exists('supply_units','name')->where(function ($query) {
                    $query->where('is_deleted', false);
                }),
            ],
            'ma_so_thue_don_vi_cung_cap' => [
                'nullable',
                'max:255',
                Rule::exists('supply_units', 'tax_code')->where(function ($query) {
                    $query->where('is_deleted', false);
                }),
            ],
            'ten_kho' => [
                'required',
                'max:255',
                Rule::exists('warehouses','name')->where(function ($query) {
                    $query->where('is_deleted', false);
                }),
            ],
			'serial' => ['required', 'max:255', Rule::unique('inventories', 'serial')],
			'thoi_gian_bao_hanh' => 'required|integer|min:0',
			'bao_hanh_hang_den_ngay' => 'required',
            'note' => 'max:255'
		];
	}

	public function batchSize(): int
	{
		return 1000;
	}

	/**
	 * @inheritDoc
	 */
	public function chunkSize(): int
	{
		return 1000;
	}
}

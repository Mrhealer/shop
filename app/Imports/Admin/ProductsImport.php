<?php

namespace App\Imports\Admin;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;


class ProductsImport implements WithMultipleSheets
{
    public function sheets(): array
    {
        return [
            'Products' => new ProductImport(),
            'Attributes' => new ProductAttributeImport(),
        ];
    }

}

<?php

namespace App\Imports\Admin;

use App\Helpers\SlugHelper;
use App\Models\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ProductImport implements ToModel, WithChunkReading, WithHeadingRow, WithBatchInserts, WithValidation
{
	public function headingRow(): int
	{
		return 1;
	}

	/**
	 * @param array $row
	 *
	 * @return string
	 */
	public function model(array $row)
	{

        try {
            return new Product([
                'prod_category_id' => $row['category_id'],
                'brand_id' => $row['brand_id'],
                'name' => $row['name'],
                'slug' => SlugHelper::genSlug($row['name']) . '-' . $row['sku'],
                'sku' => $row['sku'],
                'price' => $row['price'],
                'listed_price' => $row['listed_price'],
                'images' => json_encode(explode(',', $row['images'])),
                'short_description' => $row['short_description'],
                'description' => $row['description'],
                'show_percent_discount' => $row['show_percent_discount'] === "Yes",
                'is_hot' => $row['hot'] === "Yes",
            ]);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

	}

	public function batchSize(): int
	{
		return 1000;
	}

	/**
	 * @inheritDoc
	 */
	public function chunkSize(): int
	{
		return 1000;
	}

	/**
	 * @inheritDoc
	 */
	public function rules(): array
	{
		return [
			'category_id' => 'required|exists:prod_categories,id',
			'brand_id' => 'required|exists:brands,id',
			'name' => 'required|max:255',
			'sku' => 'required|max:255|unique:products,sku',
			'price' => 'required|integer|min:0',
			'images' => 'required',
			'show_percent_discount' => 'required',
			'hot' => 'required',
		];
	}

}

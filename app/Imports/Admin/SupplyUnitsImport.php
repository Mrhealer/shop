<?php

namespace App\Imports\Admin;

use App\Models\SupplyUnit;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class SupplyUnitsImport implements ToModel, WithChunkReading, WithHeadingRow, WithBatchInserts, WithValidation
{
    use  SkipsFailures;

    public function headingRow(): int
    {
        return 1;
    }

    /**
     * @param array $row
     * @return mixed
     */
    public function model(array $row)
    {
        return DB::transaction(function () use ($row) {
            try {
                $supplyUnit =  new SupplyUnit([
                    'name' => $row['ten'],
                    'address' => $row['dia_chi'],
                    'description' => $row['mo_ta'],
                    'tax_code' => empty($row['ma_so_thue']) ? '' : $row['ma_so_thue']
                ]);

                return $supplyUnit;
            } catch (\Exception $exception) {
                return abort('422', $exception->getMessage());
            }
        });
    }

    public function rules(): array
    {
        return [
            'ten' => 'required|unique:supply_units,name|max:255',
            'ma_so_thue' => 'nullable|unique:supply_units,tax_code|max:255',
            'dia_chi' => 'required|max:255',
            'mo_ta' => 'max:255'
        ];
    }

    public function batchSize(): int
    {
        return 1000;
    }

    /**
     * @inheritDoc
     */
    public function chunkSize(): int
    {
        return 1000;
    }
}

<?php

namespace App\Observers;

use App\Enums\Queue;
use App\Enums\VerificationType;
use App\Jobs\SendVerificationMail;
use App\Models\User;

class UserObserver
{

    /**
     * Handle the user "created" event.
     *
     * @param User $user
     * @return void
     */
    public function created(User $user)
    {
        $userVerification = $user->createVerification(VerificationType::EMAIL, 3);
        SendVerificationMail::dispatch($user, $userVerification)->onQueue(Queue::HIGH);
    }
}

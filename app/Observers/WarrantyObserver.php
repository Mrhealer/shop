<?php

namespace App\Observers;

use App\Enums\WarrantyStatus;
use App\Helpers\Helper;
use App\Models\Warranty;
use Illuminate\Support\Facades\Log;

class WarrantyObserver
{
	/**
	 * Handle the inventory warranty "created" event.
	 *
	 * @param Warranty $warranty
	 */
	public function created(Warranty $warranty)
	{
		if ($warranty->status === WarrantyStatus::WAIT_HANDLE) {
			Helper::discordNotify(config('discord-config.warranty_chanel'),
				'Yêu cầu bảo hành : *#' . $warranty->id . '* từ khách hàng *'. $warranty->customer_name .'* đang chờ xử lý. Vui lòng kiểm tra! [Chi tiết]('.route('admin.warranty_index', ['id' => $warranty->id]).')');
		}
	}

	/**
	 * Handle the inventory warranty "updated" event.
	 *
	 * @param Warranty $warranty
	 */
	public function updated(Warranty $warranty)
	{
		//
	}

	/**
	 * Handle the inventory warranty "deleted" event.
	 *
	 * @param Warranty $warranty
	 */
	public function deleted(Warranty $warranty)
	{
		//
	}

	/**
	 * Handle the inventory warranty "restored" event.
	 *
	 * @param Warranty $warranty
	 */
	public function restored(Warranty $warranty)
	{
		//
	}

	/**
	 * Handle the inventory warranty "force deleted" event.
	 *
	 * @param Warranty $warranty
	 */
	public function forceDeleted(Warranty $warranty)
	{
		//
	}
}
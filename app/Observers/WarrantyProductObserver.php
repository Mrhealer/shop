<?php

namespace App\Observers;

use App\Enums\ProductWarrantyStatus;
use App\Helpers\Helper;
use App\Models\WarrantyProduct;

class WarrantyProductObserver
{
	/**
	 * Handle the inventory warranty "created" event.
	 *
	 * @param WarrantyProduct $warrantyProduct
	 */
	public function created(WarrantyProduct $warrantyProduct)
	{
		if ($warrantyProduct->status === ProductWarrantyStatus::DELIVERY_TECH) {
			Helper::discordNotify(config('discord-config.warranty_chanel'),
				'Yêu cầu bảo hành : *#' . $warrantyProduct->warranty_id .
				'* có sản phẩm *'. $warrantyProduct->product->name
				.'* đang chờ kỹ thuật *'. $warrantyProduct->technician->name
				.'* xử lý. Vui lòng kiểm tra! [Chi tiết]('.route('admin.warranty_index', ['id' => $warrantyProduct->warranty_id]).')');
		}
	}

	/**
	 * Handle the inventory warranty "updated" event.
	 *
	 * @param WarrantyProduct $warrantyProduct
	 */
	public function updated(WarrantyProduct $warrantyProduct)
	{
		if ($warrantyProduct->status > ProductWarrantyStatus::PENDING) {
			Helper::discordNotify(config('discord-config.warranty_chanel'),
				'Yêu cầu bảo hành : *#' . $warrantyProduct->warranty_id .
				'* có sản phẩm *'. $warrantyProduct->product->name
				.'* đang chờ kỹ thuật *'. $warrantyProduct->technician->name
				.'* xử lý. Vui lòng kiểm tra! [Chi tiết]('.route('admin.warranty_index', ['id' => $warrantyProduct->warranty_id]).')');
		}
	}

}
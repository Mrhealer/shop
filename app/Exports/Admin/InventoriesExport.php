<?php

namespace App\Exports\Admin;

use App\Enums\InventoryStatus;
use App\Models\Inventory;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class InventoriesExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    use Exportable;

    public function collection()
    {
        $ids = Inventory::select(DB::raw('max(id) as id'))
            ->where('is_deleted', false)
            ->groupBy('serial')
            ->get()
            ->pluck('id')
            ->toArray();

        $inventories = DB::table('inventories')->select(
            'inventories.id', 'products.sku', 'products.name as product_name', 'inventories.serial', 'inventories.status',
            'inventories.warranty_period', 'inventories.brand_warranty_period', 'supply_units.name as supply_units_name',
            'supply_units.tax_code as supply_units_tax_code', 'warehouses.name as warehouse_name', 'inventories.note'
        )
            ->where('inventories.is_deleted', false)
            ->whereIn('inventories.id', $ids)
            ->join('products', 'inventories.product_id', '=', 'products.id')
            ->join('supply_units', 'inventories.supply_unit_id', '=', 'supply_units.id')
            ->join('warehouses', 'inventories.import_to_warehouse_id', '=', 'warehouses.id')
            ->orderBy('inventories.created_at', 'asc')
            ->get();

        foreach ($inventories as $key => $inventory) {
            $inventory->id = $key + 1;
            $inventory->status = $this->getInventoryStatusName($inventory->status);
        }

        return $inventories;
    }

    private function getInventoryStatusName($keyStatus)
    {
        $inventoryStatus = [];
        foreach (InventoryStatus::values() as $status) {
            $inventoryStatus[$status->getValue()] = [
                'name' => trans('enum.inventory_status.'. strtolower($status->getKey())),
                'value' => $status->getValue()
            ];
        }

        return $inventoryStatus[$keyStatus]['name'];
    }

    public function headings(): array
    {
        return [
            'STT',
            'SKU',
            'Tên sản phẩm',
            'Serial',
            'Trạng thái',
            'Thời gian bảo hành',
            'Bảo hành hãng đến ngày',
            'Tên đơn vị cung cấp',
            'Mã số thuế đơn vị cung cấp',
            'Tên kho',
            'Note'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:K1'; // All headers
                $event->sheet->getDelegate()
                    ->getStyle($cellRange)
                    ->getFont()
                    ->setSize(12)
                    ->setBold(true);

                $event->sheet->getDelegate()
                    ->getStyle($cellRange)
                    ->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('ef620b');

                $event->sheet->getDelegate()
                    ->getStyle($cellRange)
                    ->getBorders()
                    ->getBottom()
                    ->setBorderStyle('thin');
            },
        ];
    }

}

<?php

namespace App\Exports\Admin;

use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class ProductsExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    use Exportable;

    public function collection()
    {
        $products = DB::table('products')->select(
            'products.id', 'prod_categories.name as prod_category_name', 'brands.name as brand_name', 'products.name', 'products.sku',
            'products.price', 'products.listed_price', 'products.show_percent_discount',
            'products.is_hot', 'products.images'
        )
            ->where('products.is_deleted', false)
            ->join('prod_categories', 'products.prod_category_id', '=', 'prod_categories.id')
            ->join('brands', 'products.brand_id', '=', 'brands.id')
            ->orderBy('products.created_at', 'asc')
            ->get();

        foreach ($products as $key => $product) {
            $product->id = $key + 1;
            $product->show_percent_discount = $product->show_percent_discount ? 'YES' : 'NO';
            $product->is_hot = $product->is_hot ? 'YES' : 'NO';
            $product->images = implode(',', json_decode($product->images));
        }

        return $products;
    }

    public function headings(): array
    {
        return [
            'STT',
            'Tên danh mục',
            'Tên thương hiệu',
            'Tên',
            'SKU (định danh)',
            'Giá bán',
            'Giá niêm yết',
            'Hiển thị phần trăm giảm giá',
            'Sản phẩm hot',
            'Danh sách ảnh sản phẩm'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:J1'; // All headers
                $event->sheet->getDelegate()
                    ->getStyle($cellRange)
                    ->getFont()
                    ->setSize(12)
                    ->setBold(true);

                $event->sheet->getDelegate()
                    ->getStyle($cellRange)
                    ->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('ef620b');

                $event->sheet->getDelegate()
                    ->getStyle($cellRange)
                    ->getBorders()
                    ->getBottom()
                    ->setBorderStyle('thin');
            },
        ];
    }

}

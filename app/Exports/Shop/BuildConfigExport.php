<?php

namespace App\Exports\Shop;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Color;

class BuildConfigExport implements FromView, ShouldAutoSize, WithEvents
{
    const FORMAT_CURRENCY_VND = '_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)';

    private $products;
    private $lastRow;

    public function __construct($productsData)
    {
        $this->products = $productsData;
        $this->lastRow = count($productsData) + 2;

    }

    public function view(): View
    {
        return view('exports.build_config', [
            'products' => $this->products,
        ]);
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $heading = $event->sheet->getDelegate()->getStyle('A1:F1');
                $heading->getAlignment()->setHorizontal('center');
                $heading->getFont()
                    ->setSize(15)
                    ->setBold(true);

                $header =  $event->sheet->getDelegate()->getStyle('A2:F2');
                $header->getFill()->setFillType('solid')->setStartColor(new Color(Color::COLOR_BLUE));
                $header->getAlignment()->setHorizontal('center');
                $header->getFont()
                    ->setSize(13)
                    ->setColor(new Color(Color::COLOR_WHITE))
                    ->setBold(true);

                $colNo =  $event->sheet->getDelegate()->getStyle('A2:A'.$this->lastRow);
                $colNo->getAlignment()->setHorizontal('center');

                $colSku =  $event->sheet->getDelegate()->getStyle('B2:B'.$this->lastRow);
                $colSku->getAlignment()->setHorizontal('center');

                $colQuantity =  $event->sheet->getDelegate()->getStyle('D2:D'.$this->lastRow);
                $colQuantity->getAlignment()->setHorizontal('center');

                $colPrice = $event->sheet->getDelegate()->getStyle('E2:E'.$this->lastRow);
                $colPrice->getNumberFormat()->setFormatCode('#,##0.00');

                $colMoney = $event->sheet->getDelegate()->getStyle('F2:F'.$this->lastRow);
                $colMoney->getNumberFormat()->setFormatCode('#,##0.00');

                $colTotalMoney = $event->sheet->getDelegate()->getStyle('F'.($this->lastRow + 1) . ':F'. ($this->lastRow + 3));
                $colTotalMoney->getNumberFormat()->setFormatCode('#,##0.00');
                $colTotalMoney->getFill()->setFillType('solid')->setStartColor(new Color(Color::COLOR_YELLOW));

                $colTotalMoneyLabel = $event->sheet->getDelegate()->getStyle('D'.($this->lastRow + 1) . ':D'. ($this->lastRow + 3));
                $colTotalMoneyLabel->getFont()->setBold(true);
                $colTotalMoneyLabel->getFill()->setFillType('solid')->setStartColor(new Color(Color::COLOR_YELLOW));

            },
        ];
    }

}

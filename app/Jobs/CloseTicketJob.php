<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Ticket;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Enums\TicketStatus;

class CloseTicketJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $ticket;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
          return DB::transaction(function (){
               try{

                    $attributes['status'] = TicketStatus::CLOSED;

                    if ($this->ticket->created_at->diffInRealMinutes($this->ticket->completed_at) > $this->ticket->time_handle) {
                        $attributes['is_violated_complete'] = true;
                    }

                    return $this->ticket->update($attributes);
            

                } catch (\Exception $e) {

                    \Log::info('message:'. $e);
                }

            });
    }
}

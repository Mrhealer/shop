<?php

namespace App\Jobs;

use App\Mail\SendTicketMail;
use App\Models\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendTicketMailJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	use Queueable, SerializesModels;

	protected $ticket;

	protected $message;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 5;

	public function __construct(Ticket $ticket, $message)
	{
		$this->ticket = $ticket->load('user');
		$this->message = $message;
	}


	public function handle()
	{
		Mail::to($this->ticket->user->email)->send(new SendTicketMail($this->ticket, $this->message));
	}
}
<?php

namespace App\Jobs;

use App\Helpers\Helper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendDiscordNotification implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $webhook;
	protected $message;

	public function __construct($webhook,$message)
	{
		$this->webhook = $webhook;
		$this->message = $message;
	}

	public function handle()
	{
		Helper::discordNotify($this->webhook,$this->message);
	}
}
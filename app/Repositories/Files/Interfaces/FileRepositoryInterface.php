<?php
namespace App\Repositories\Files\Interfaces;


interface FileRepositoryInterface
{
	public function upload(String $path, $file);

	public function getUrl(String $file);

	public function makeDir(String $dir);

	public function isExist(String $path);

	public function getPath($user);

	public function getFiles(String $directory);

	public function delete(Array $files);

	public function deleteDirectory(String $directory);

	public function getSize(String $path);

	public function getLastModified(String $path);

	public function resizeImage(String $file, $width, $height);

	public function getMimeType(String $path);

}

<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @method static whereEmail($input)
 */
class Admin extends Authenticatable implements MustVerifyEmail
{
	use Notifiable;

	/** @var string */
	protected $guard = 'admin';

	/** @var string */
	protected $table = 'admins';

	/** @var array */

	protected $hidden = [
		'password',
		'remember_token',
	];

	/** @var array */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	/** @var array $fillable */
	protected $fillable = [
		'name',
		'username',
		'email',
		'phone',
		'image',
		'fb_account_id',
		'role',
		'email_verified_at',
		'password',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function roles()
	{
		return $this->belongsToMany(Role::class, 'admin_role');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function verifications()
	{
		return $this->hasMany(UserVerification::class);
	}


	/**
	 * @param $permissionKey
	 * @return bool
	 */
	public function canDo($permissionKey)
	{
		$user = $this->load('roles.permissions');

		foreach ($user->roles as $role) {
			foreach ($role->permissions as $permission) {
				if ($permission->key === $permissionKey) {
					return true;
				}
			}
		}

		return false;
	}
}

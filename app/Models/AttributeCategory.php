<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\AttributeCategory
 *
 * @property int $id
 * @property int $attribute_id
 * @property int $prod_category_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Attribute $attribute
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeCategory whereAttributeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeCategory whereProdCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AttributeCategory extends Model
{

    /** @var string $table */
    protected $table = 'attribute_category';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'attribute_id',
        'prod_category_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }
}

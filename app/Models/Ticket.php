<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';

    protected $fillable = [
	    "ticket_category_id",
	    "id",
	    "priority",
	    "status",
	    "title",
	    "user_id",
	    "admin_id",
	    "star",
	    "completed_at",
	    "time_handle",
	    "received_at",
	    "created_at",
	    "is_violated_complete",
	    "is_violated_receive"

    ];

	protected $appends = ['first_message'];

    public function category()
    {
    	return $this->belongsTo(TicketCategory::class, 'ticket_category_id', 'id');
    }

    public function messages()
    {
    	return $this->hasMany(TicketMessage::class);
    }

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function admin()
	{
		return $this->belongsTo(Admin::class);
	}

	public function getFirstMessageAttribute()
	{
		return $this->messages()->select('id','message')->first()->message;
	}


	public static function boot() {
		parent::boot();
		static::deleting(function($ticket) {
			$ticket->messages()->delete();
		});
	}
}

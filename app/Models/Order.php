<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $payment_method
 * @property int $received_method
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $province
 * @property string $province_name
 * @property string|null $district
 * @property string|null $district_name
 * @property string|null $address
 * @property string|null $note
 * @property string|null $showroom
 * @property string|null $received_time
 * @property int $has_bill
 * @property string|null $company_name
 * @property string|null $company_address
 * @property string|null $company_tax_identification
 * @property string|null $bill_received_address
 * @property string|null $referral_code
 * @property string|null $voucher_code
 * @property int|null $voucher_discount_value
 * @property int $temp_money
 * @property int $ship_money
 * @property int $pay_money
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderProduct[] $orderProducts
 * @property-read int|null $order_products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereBillReceivedAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCompanyAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCompanyTaxIdentification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereDistrictName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereHasBill($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePayMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereProvinceName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereReceivedMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereReceivedTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereReferralCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereShipMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereShowroom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereTempMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereVoucherCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereVoucherDiscountValue($value)
 * @mixin \Eloquent
 * @property int $status
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereShippingStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order where(string $string, string $string1, string $week)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Inventory[] $inventories
 * @property-read int|null $inventories_count
 * @property-read \App\Models\UserPoint $userPoint
 */
class Order extends Model
{
    /** @var string $table */
    protected $table = 'orders';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'user_id',
        'admin_id',
        'payment_method',
        'received_method',
        'name',
        'phone',
        'province',
        'province_name',
        'district',
        'district_name',
        'address',
        'email',
        'note',
        'showroom',
        'received_time',
        'has_bill',
        'company_name',
        'company_address',
        'company_tax_identification',
        'bill_received_address',
        'referral_code',
        'voucher_code',
        'voucher_discount_value',
        'temp_money',
        'real_temp_money',
        'ship_money',
        'pay_money',
        'real_pay_money',
        'shipper_id',
        'transport_company_id',
        'track_code',
        'payment_status',
        'status',
        'shipping_status'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class);
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function admin()
	{
		return $this->belongsTo(Admin::class, 'admin_id');
	}

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function inventories()
    {
        return $this->hasMany(Inventory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userPoint()
    {
        return $this->hasOne(UserPoint::class, 'id', 'order_id');
    }

    public function histories()
    {
        // TODO:: fix history handle order
        return $this->hasMany(OrderHistory::class)->orderBy('created_at', 'DESC');
    }

    public function shippingHistories()
    {
        return $this->hasMany(ShippingTrack::class)->orderBy('created_at', 'DESC');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderHistory
 *
 * @property int $id
 * @property int $order_id
 * @property int $admin_id
 * @property int $action
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Order $order
 * @property-read \App\Models\Admin $admin
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderHistory whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderHistory whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderHistory whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderHistory whereAdminId($value)
 * @mixin \Eloquent
 */
class OrderHistory extends Model
{
    /** @var string $table */
    protected $table = 'order_histories';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
            'admin_id',
            'order_id',
            'action',
            'status',
            'payment_status',
            'note'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }


}

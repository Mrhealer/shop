<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Setting
 *
 * @property int $id
 * @property string $logo
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $hotline
 * @property string $email
 * @property string $title
 * @property string $keyword
 * @property string $description
 * @property string $thumbnail
 * @property mixed|null $banner
 * @property string|null $gg_analytic_id
 * @property float $order_rate_point
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereBanner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereGgAnalyticId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereHotline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereThumbnail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting first()
 * @mixin \Eloquent
 * @property mixed|null $slider
 * @property string|null $banner_top
 * @property string|null $banner_mid
 * @property string|null $banner_bot
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereBannerBot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereBannerMid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereBannerTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereSlider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereOrderRatePoint($value)
 * @property string $main_product_title
 * @property string|null $banner_top_link
 * @property string|null $banner_mid_link
 * @property string|null $banner_bot_link
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereBannerBotLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereBannerMidLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereBannerTopLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereMainProductTitle($value)
 */
class Setting extends Model
{
    /** @var string $table */
    protected $table = 'setting';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'logo',
        'name',
        'address',
        'phone',
        'hotline',
        'email',
        'title',
        'keyword',
        'description',
        'thumbnail',
        'slider',
        'banner_top',
        'banner_top_link',
        'banner_mid',
        'banner_mid_link',
        'banner_bot',
        'banner_bot_link',
        'gg_analytic_id',
        'order_rate_point',
        'main_product_title',
        'sales_policy',
	    'build_categories'
    ];

}

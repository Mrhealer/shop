<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketCategory extends Model
{
    protected $table = 'ticket_categories';

    protected $fillable =  [
	    "id",
	    "name",
	    "signature",
	    "discord_webhook",
	    "time_handle_default",
	    "time_receiver_default",
	    "close_message_default",
	    "status",
    ];

    public function tickets()
    {
    	return $this->hasMany(Ticket::class);
    }

	public static function boot() {
		parent::boot();

		static::deleting(function($category) {
			$category->tickets()->delete();
		});
	}
}

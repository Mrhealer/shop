<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserPoint
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPoint firstOrCreate(array $array)
 * @property int $id
 * @property int $user_id
 * @property int $order_id
 * @property float $point
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Order $order
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPoint newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPoint newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPoint query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPoint whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPoint whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPoint whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPoint wherePoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPoint whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPoint whereUserId($value)
 * @mixin \Eloquent
 */
class UserPoint extends Model
{
    /** @var string $table */
    protected $table = 'user_points';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
            'user_id',
            'order_id',
            'point',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WarrantyProduct extends Model
{
	protected $table = 'warranty_products';

	protected $fillable = [
		'warranty_id',
		'product_id',
		'tech_id',
		'serial',
		'new_serial',
		'name',
		'image',
		'descriptions',
		'before',
		'after',
		'cost',
		'order_id',
		'retun_at',
		'expect_return_at',
		'status'
	];


	public function warranty()
	{
		return $this->belongsTo(Warranty::class);
	}

	public function product()
	{
		return $this->belongsTo(Product::class);
	}

	public function technician()
	{
		return $this->hasOne(Admin::class, 'id', 'tech_id');
	}

	public function inventory()
	{
		return $this->belongsTo(Inventory::class, 'serial', 'serial');
	}

	public function order()
	{
		return $this->belongsTo(Order::class);
	}
}

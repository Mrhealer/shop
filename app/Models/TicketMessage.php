<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketMessage extends Model
{
    protected $table = 'ticket_messages';

    protected $fillable =  [
	    "attached",
	    "id",
	    "is_customer",
	    "message",
	    "star",
	    "ticket_id",
	    "user_id",
	    "admin_id",
    ];

    protected $casts = [
        'attached'	=> 'array'
    ];


    public function ticket()
    {
    	return $this->belongsTo(Ticket::class);
    }

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id')->withDefault();
	}

	public function admin()
	{
		return $this->belongsTo(Admin::class, 'admin_id')->withDefault();
	}


}

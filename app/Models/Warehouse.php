<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Warehouse
 *
 */
class Warehouse extends Model
{
    /** @var string $table */
    protected $table = 'warehouses';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'name',
        'address',
        'description',
        'quantity_goods',
        'is_deleted'
    ];
}

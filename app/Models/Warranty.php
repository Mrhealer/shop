<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{
    protected $table = 'warranties';

    protected $casts = [
    	'images' => 'array'
    ];

    protected $fillable = [
    	'customer_name',
	    'customer_phone',
	    'customer_email',
	    'customer_address',
	    'text',
	    'images',
	    'refund_date',
	    'expect_return_at',
	    'action',
	    'admin_id',
	    'status'
    ];

    public function products()
    {
    	return $this->hasMany(WarrantyProduct::class);
    }

    public function histories()
    {
    	return $this->hasMany(WarrantyHistory::class);
    }

    public function admin()
    {
    	return $this->belongsTo(Admin::class);
    }
}

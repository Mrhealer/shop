<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\ProductAttributeValue
 *
 * @property int $id
 * @property int $product_id
 * @property int $attribute_value_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\AttributeValue $attributeValue
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductAttributeValue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductAttributeValue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductAttributeValue query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductAttributeValue whereAttributeValueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductAttributeValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductAttributeValue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductAttributeValue whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductAttributeValue whereUpdatedAt($value)
 * @method static whereIn(string $string, \Illuminate\Support\Collection $attributeValueIds)
 * @mixin \Eloquent
 */
class ProductAttributeValue extends Model
{

    /** @var string $table */
    protected $table = 'product_attribute';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'attribute_value_id',
        'product_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attributeValue()
    {
        return $this->belongsTo(AttributeValue::class);
    }


}

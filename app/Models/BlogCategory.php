<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

/**
 * App\Models\ProdCategory
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $is_deleted
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Kalnoy\Nestedset\Collection|\App\Models\ProdCategory[] $children
 * @property-read int|null $children_count
 * @property-read \App\Models\ProdCategory|null $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory d()
 * @method static \Kalnoy\Nestedset\QueryBuilder|\App\Models\ProdCategory newModelQuery()
 * @method static \Kalnoy\Nestedset\QueryBuilder|\App\Models\ProdCategory newQuery()
 * @method static \Kalnoy\Nestedset\QueryBuilder|\App\Models\ProdCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereUpdatedAt($value)
 * @method static withDepth()
 * @mixin \Eloquent
 * @property-read \App\Models\ProdCategory|null $parentCategory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlogCategory whereSlug($value)
 */
class BlogCategory extends Model
{
    use NodeTrait;

    /** @var string $table */
    protected $table = 'blog_categories';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'name',
        'slug',
        'image',
        '_lft',
        '_rgt',
        'parent_id',
        'is_deleted'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentCategory()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value)
    {
        // Retrieve by id or key.
        return is_numeric($value)
            ? $this->where('id', $value)->first()
            : $this->where('slug', $value)->first();
    }
}

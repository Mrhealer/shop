<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ShippingTrack
 *
 */
class ShippingTrack extends Model
{
    /** @var string $table */
    protected $table = 'shipping_tracks';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'order_id',
        'admin_id',
        'shipper_id',
        'transport_company_id',
        'note',
        'status'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function shipper()
    {
        return $this->belongsTo(Admin::class, 'shipper_id');
    }

    public function transportCompany()
    {
        return $this->belongsTo(TransportCompany::class);
    }
}

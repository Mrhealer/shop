<?php

namespace App\Models;

use App\Enums\UserRole;
use App\Enums\VerificationType;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $phone
 * @property string|null $image
 * @property int $role
 * @property float $point
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserVerification[] $verifications
 * @property-read int|null $verifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\FbAccount $fbAccount
 * @property int|null $fb_account_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFbAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePoint($value)
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'name',
	    'username',
        'email',
        'phone',
        'image',
        'password',
        'email_verified_at',
        'fb_account_id',
        'point',
        'discord_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function verifications()
    {
        return $this->hasMany(UserVerification::class);
    }


    /**
     * @param $type
     * @param  int  $dayExpired
     * @return UserVerification
     */
    public function createVerification($type, $dayExpired = 1)
    {
        $userVerification = new UserVerification([
            'user_id' => $this->id,
            'expired_at' => date('Y-m-d H:i:s', strtotime('+'.$dayExpired.' days')),
            'token' => Str::random(20) . uniqid(),
            'verification_type' => $type
        ]);
        $userVerification->save();
        return $userVerification;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fbAccount()
    {
        return $this->belongsTo(FbAccount::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'orders', 'user_id');
    }


	public function roles()
	{
		return $this->belongsToMany(Role::class, 'user_role');
	}


	public function canDo($permissionKey)
	{
		$user = $this->load('roles.permissions');

		foreach ($user->roles as $role) {
			foreach ($role->permissions as $permission) {
				if ($permission->key === $permissionKey) {
					return true;
				}
			}
		}

		return false;
	}
}

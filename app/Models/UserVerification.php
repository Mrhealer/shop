<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\UserVerification
 *
 * @property int $id
 * @property int $user_id
 * @property string $expired_at
 * @property string $token
 * @property int $verification_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserVerification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserVerification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserVerification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserVerification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserVerification whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserVerification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserVerification whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserVerification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserVerification whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserVerification whereVerificationType($value)
 * @mixin \Eloquent
 */
class UserVerification extends Model
{
    /** @var string $table */
    protected $table = 'user_verifications';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
	    'user_id',
	    'admin_id',
        'expired_at',
        'token',
        'verification_type'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function admin()
	{
		return $this->belongsTo(Admin::class);
	}

    /**
     * @return bool
     */
    public function isExpired()
    {
        $now = strtotime(now());
        $expired = strtotime($this->expired_at);
        return $now > $expired;
    }

    /**
     * @param  mixed  $value
     * @return Model|null
     */
    public function resolveRouteBinding($value)
    {
        return $this->where('token', $value)->first();
    }


}

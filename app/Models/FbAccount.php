<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FbAccount
 *
 * @property int $id
 * @property string $name
 * @property string $fid
 * @property string $image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FbAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FbAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FbAccount query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FbAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FbAccount whereFid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FbAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FbAccount whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FbAccount whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FbAccount whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FbAccount extends Model
{
    /** @var string $table */
    protected $table = 'fb_accounts';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'fid',
        'name',
        'image',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TransportCompany
 *
 */
class TransportCompany extends Model
{
    /** @var string $table */
    protected $table = 'transport_companies';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'name',
        'address',
        'description',
        'is_deleted'
    ];
}

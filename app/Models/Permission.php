<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	/** @var string $table */
	protected $table = 'permissions';

	/** @var array $hidden */
	protected $hidden = [];

	/** @var array $fillable */
	protected $fillable = [
		'key',
		'name',
		'guard',
		'level',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function roles()
	{
		return $this->belongsToMany(Role::class, 'role_permission');
	}


}

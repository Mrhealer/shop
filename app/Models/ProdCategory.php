<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;


/**
 * App\Models\ProdCategory
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $is_deleted
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $icon
 * @property mixed|null $slider
 * @property string|null $banner
 * @property string|null $banner_link
 * @property-read \Kalnoy\Nestedset\Collection|\App\Models\ProdCategory[] $children
 * @property-read int|null $children_count
 * @property-read \App\Models\ProdCategory|null $parent
 * @property-read \App\Models\ProdCategory|null $parentCategory
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory d()
 * @method static \Kalnoy\Nestedset\QueryBuilder|\App\Models\ProdCategory newModelQuery()
 * @method static \Kalnoy\Nestedset\QueryBuilder|\App\Models\ProdCategory newQuery()
 * @method static \Kalnoy\Nestedset\QueryBuilder|\App\Models\ProdCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereBanner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereBannerLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereSlider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory descendantsOf(int $id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory with($relations)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereIsRoot()
 * @mixin \Eloquent
 * @property int $is_best_seller
 * @property-read \Kalnoy\Nestedset\Collection|\App\Models\ProdCategory[] $childrenCategories
 * @property-read int|null $children_categories_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdCategory whereIsBestSeller($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Attribute[] $attributes
 * @property-read int|null $attributes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Brand[] $brands
 * @property-read int|null $brands_count
 */
class ProdCategory extends Model
{
    use NodeTrait;

    /** @var string $table */
    protected $table = 'prod_categories';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'name',
        'slug',
        'icon',
        'order',
        'slider',
        'banner',
        'banner_link',
        'is_best_seller',
        '_lft',
        '_rgt',
        'parent_id',
        'is_deleted',
        'is_show_home'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentCategory()
    {
        return $this->belongsTo(ProdCategory::class, 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childrenCategories()
    {
        return $this->hasMany(ProdCategory::class, 'id', 'parent_id');
    }

    public function children()
    {
	    return $this->hasMany(ProdCategory::class, 'parent_id', 'id');
    }

	/**
     * @return mixed
     */
    public function sliderImages()
    {
        return json_decode($this->slider);
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value)
    {
        // Retrieve by id or key.
        return is_numeric($value)
            ? $this->where('id', $value)->first()
            : $this->where('slug', $value)->first();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function brands()
    {
        return $this->belongsToMany(Brand::class, 'brand_category', 'prod_category_id', 'brand_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'attribute_category', 'prod_category_id', 'attribute_id');
    }
}

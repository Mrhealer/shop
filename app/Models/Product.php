<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Product
 *
 * @property int $id
 * @property int $prod_category_id
 * @property int $brand_id
 * @property string $name
 * @property string $sku
 * @property int $price
 * @property int $listed_price
 * @property mixed $images
 * @property string $description
 * @property int $is_deleted
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $show_percent_discount
 * @property int $is_hot
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AttributeValue[] $attributeValues
 * @property-read int|null $attribute_values_count
 * @property-read \App\Models\Brand $brand
 * @property-read \App\Models\ProdCategory $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereIsHot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereListedPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereProdCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereShowPercentDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product with($value)
 * @mixin \Eloquent
 * @property string|null $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Inventory[] $inventory
 * @property-read int|null $inventory_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereSlug($value)
 * @property string|null $short_description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereShortDescription($value)
 */
class Product extends Model
{
    /** @var string $table */
    protected $table = 'products';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'prod_category_id',
        'brand_id',
        'name',
        'slug',
        'sku',
        'price',
        'listed_price',
        'images',
        'description',
	    'short_description',
        'sales_policy',
        'promotions',
        'show_percent_discount',
        'is_hot',
        'qty_in_warehouses',
        'is_deleted'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(ProdCategory::class, 'prod_category_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inventory()
    {
    	return $this->hasMany(Inventory::class);
    }

    /**
     * @return array
     */
    public static function queryIssetInventory()
    {
        return [
            'inventory' => function ($query) {
                $query->where(function ($query2) {
                    $query2->where('has_serial', true)
                        ->whereNull('order_id');
                })->orWhere(function ($query2) {
                    $query2->where('has_serial', false)
                        ->where('quantity', '>', 0);
                });
            },
        ];
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attributeValues()
    {
        return $this->belongsToMany(AttributeValue::class, 'product_attribute');
    }

    /**
     * @return array
     */
    public function attributes()
    {
        $categoryAttributes = AttributeCategory::with('attribute')->where('prod_category_id', $this->prod_category_id)->get();
        $productAttributes = ProductAttributeValue::with('attributeValue')->where('product_id', $this->id)->get();
        $data = [];
        foreach ($categoryAttributes as $categoryAttribute) {
            $data['attr_'.$categoryAttribute->attribute->id] = [
                'attribute' => $categoryAttribute->attribute,
                'values' => [],
            ];
        }
        foreach ($productAttributes as $productAttribute) {
            if(isset($data['attr_'.$productAttribute->attributeValue->attribute_id])) {
                $data['attr_'.$productAttribute->attributeValue->attribute_id]['values'][] = $productAttribute->attributeValue;
            }
        }
        return $data;

    }

    /**
     * @return array
     */
    public function similarProducts()
    {
        $attributeValueIds = ProductAttributeValue::where('product_id', $this->id)->get('id')->toArray();
        return Product::whereIsDeleted(false)
            ->where('products.prod_category_id', $this->prod_category_id)
            ->whereIn('product_attribute.attribute_value_id', $attributeValueIds)
            ->join('product_attribute', 'products.id', 'product_attribute.product_id')
            ->orderBy('products.updated_at', 'DESC')
            ->limit(8)->get();

    }

    /**
     * @return array
     */
    public function brandProducts()
    {
        return Product::whereIsDeleted(false)
            ->whereProdCategoryId($this->prod_category_id)
            ->where('id', '<>', $this->id)
            ->orderBy('products.price', 'asc')
            ->limit(8)
            ->get();
    }

    /**
     * @return string
     */
    public function thumbnail()
    {
        $images = json_decode($this->images);
        return $images[0] ?? 'images/placeholder-img.jpg';
    }

    /**
     * @return mixed
     */
    public function imagesExceptFirst()
    {
        $images = json_decode($this->images);
        if(isset($images[0])) {
            unset($images[0]);
        }
        return $images;

    }

    /**
     * @return mixed
     */
    public function imageList()
    {
        return json_decode($this->images);
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value)
    {
        // Retrieve by id or key.
        return is_numeric($value)
            ? $this->where('id', $value)->first()
            : $this->where('slug', $value)->first();
    }
}

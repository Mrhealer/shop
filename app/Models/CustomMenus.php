<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

/**
 * App\Models\CustomMenus
 *
 */
class CustomMenus extends Model
{
    use NodeTrait;

    /** @var string $table */
    protected $table = 'custom_menus';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'name',
        'object_id',
        'object_type',
        'url',
        '_lft',
        '_rgt',
        'parent_id',
        'order'
    ];

    public function prodCategory()
    {
        return $this->hasOne(ProdCategory::class, 'id','object_id');
    }
}

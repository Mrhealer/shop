<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Voucher
 *
 * @property int $id
 * @property int $type
 * @property string $code
 * @property int|null $product_id
 * @property int|null $prod_category_id
 * @property int|null $brand_id
 * @property int $limit
 * @property int $number_of_uses
 * @property int $value
 * @property int|null $min_money_required
 * @property int|null $max_money_discount
 * @property int $use_percent_discount
 * @property int $active
 * @property string $expired_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Brand|null $brand
 * @property-read \App\Models\ProdCategory $category
 * @property-read \App\Models\Product|null $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereMaxMoneyDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereMinMoneyRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereNumberOfUses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereProdCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereUsePercentDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Voucher whereValue($value)
 * @mixin \Eloquent
 */
class Voucher extends Model
{
    /** @var string $table */
    protected $table = 'vouchers';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'code',
        'type',
        'product_id',
        'prod_category_id',
        'brand_id',
        'limit',
        'number_of_uses',
        'min_money_required',
        'max_money_discount',
        'use_percent_discount',
        'value',
        'expired_at',
        'active',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(ProdCategory::class, 'prod_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        $now = strtotime(now());
        $expired = strtotime($this->expired_at);
        return $now > $expired;
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value)
    {
        // Retrieve by id or key.
        return is_numeric($value)
            ? $this->where('id', $value)->first()
            : $this->where('code', $value)->first();
    }
}

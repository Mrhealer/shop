<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	/** @var string $table */
	protected $table = 'roles';

	/** @var array $hidden */
	protected $hidden = [];

	/** @var array $fillable */
	protected $fillable = [
		'name',
		'guard',
		'level',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function users()
	{
		return $this->belongsToMany(User::class, 'user_role');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function admins()
	{
		return $this->belongsToMany(Admin::class, 'admin_role');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function permissions()
	{
		return $this->belongsToMany(Permission::class, 'role_permission');
	}


}

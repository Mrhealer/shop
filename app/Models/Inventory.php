<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Inventory
 *
 * @property-read \App\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereIn(string $string, array $params)
 * @mixin \Eloquent
 * @property int $id
 * @property string $serial
 * @property boolean $has_serial
 * @property string $warehouse
 * @property int $product_id
 * @property int $warranty_period
 * @property string|null $note
 * @property int|null $order_id
 * @property string|null $exported_at
 * @property int $is_deleted
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereExportedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereSerial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereWarehouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereWarrantyPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory where(string $col, string $value)
 * @property int $quantity
 * @property string|null $brand_warranty_period
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order[] $order
 * @property-read int|null $order_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderInventory[] $orderInventories
 * @property-read int|null $order_inventories_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereBrandWarrantyPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereHasSerial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory with(...$relations)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InventoryWarranty[] $inventoryWarranties
 * @property-read int|null $inventory_warranties_count
 */
class Inventory extends Model
{
    /** @var string $table */
    protected $table = 'inventories';

    protected $appends = ['warranty_end'];

    /** @var array $fillable */
    protected $fillable = [
        'serial',
        'has_serial',
        'quantity',
        'import_to_warehouse_id',
        'warehouse',
        'product_id',
        'warranty_period',
        'brand_warranty_period',
        'note',
        'export_to_warehouse_id',
        'order_id',
        'exported_at',
        'import_price',
        'import_admin_id',
        'supply_unit_id',
        'export_admin_id',
        'export_note',
        'export_price',
        'status',
        'is_deleted'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'import_to_warehouse_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

	public function order()
	{
		return $this->belongsTo(Order::class);
	}
    /***
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderInventories()
    {
        return $this->hasMany(OrderInventory::class);
    }

//    /***
//     * @return \Illuminate\Database\Eloquent\Relations\HasMany
//     */
    public function warranties()
    {
        return $this->hasMany(WarrantyProduct::class, 'serial', 'serial');
    }

    public function getWarrantyEndAttribute()
    {
        return  Carbon::parse($this->exported_at)->addMonths($this->warranty_period)->format('d-m-Y');
    }

    public function supply()
    {
	    return $this->belongsTo(SupplyUnit::class, 'supply_unit_id');
    }

}

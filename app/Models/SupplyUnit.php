<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SupplyUnit
 *
 */
class SupplyUnit extends Model
{
    /** @var string $table */
    protected $table = 'supply_units';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'name',
        'address',
        'description',
        'tax_code',
        'is_deleted'
    ];
}

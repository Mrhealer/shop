<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AttributeValue
 *
 * @property int $id
 * @property int $attribute_id
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Attribute $attribute
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeValue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeValue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeValue query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeValue whereAttributeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeValue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeValue whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AttributeValue whereValue($value)
 * @method static firstOrCreate(array $array)
 * @mixin \Eloquent
 */
class AttributeValue extends Model
{

    /** @var string $table */
    protected $table = 'attribute_values';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'attribute_id',
        'value',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_attribute');
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WarrantyHistory extends Model
{
    protected $table = 'warranty_histories';

    protected $fillable = ['warranty_id', 'action'];

    public function warranty()
    {
    	return $this->hasOne(Warranty::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderInventory
 *
 * @property int $id
 * @property int $inventory_id
 * @property int $order_id
 * @property string $exported_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderInventory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderInventory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderInventory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderInventory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderInventory whereExportedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderInventory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderInventory whereInventoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderInventory whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderInventory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrderInventory extends Model
{

    /** @var string $table */
    protected $table = 'order_inventory';

    /** @var array $hidden */
    protected $hidden = [];

    /** @var array $fillable */
    protected $fillable = [
        'inventory_id',
        'order_id',
        'exported_at'
    ];

}

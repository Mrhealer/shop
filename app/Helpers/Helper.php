<?php

namespace App\Helpers;

use App\Enums\OrderStatus;
use App\Enums\PaymentStatus;
use App\Enums\ShippingStatus;
use App\Models\Admin;
use App\Models\Order;
use App\Models\TransportCompany;
use App\Models\User;
use Ixudra\Curl\Facades\Curl;

class Helper
{
    public static function implodeAttributeValues($data)
    {
        if(empty($data)) {
            return '';
        }

        $total = count($data);

        if($total === 1) {
            return $data[0]->value;
        } else {
            $str = '';
            foreach ($data as $value) {
                $str .= $value->value.', ';
            }

            return rtrim($str, ", ");
        }
    }

    public static function discordNotify($chanelWebhook, $message)
    {
	    try {
		    return  Curl::to($chanelWebhook)
			    ->withData(['content' => $message])
			    ->asJson()
			    ->post();

	    } catch (\Exception $e) {
		    return $e->getMessage();
	    }
    }

    public static function getDiscordMsgTrackingOrderStatus($oldOrder, $attributes)
    {
        $orderId = $oldOrder['id'];
        $title = "**[Đơn hàng *[#$orderId](" . route('admin.orders_index', ['id' => $orderId]) . ")* có cập nhật mới]**";
        $content = "";

        if ($oldOrder['status'] !== $attributes['status']) {
            switch ($attributes['status']) {
                case OrderStatus::ERROR:
                    $content .= "\nTrạng thái đơn hàng: Đơn hủy.";
                    break;
                case OrderStatus::SUCCESS:
                    $content .= "\nTrạng thái đơn hàng: Thành công.";
                    break;
                case OrderStatus::PENDING:
                    $content .= "\nTrạng thái đơn hàng: Chờ xử lý.";
                    break;
                case OrderStatus::PROCESSING:
                    $content .= "\nTrạng thái đơn hàng: Đang xử lý.";
                    break;
                case OrderStatus::RETURN:
                    $content .= "\nTrạng thái đơn hàng: Đơn hoàn.";
                    break;
            }
        }

        if ($oldOrder['payment_status'] !== $attributes['payment_status']) {
            switch ($attributes['payment_status']) {
                case PaymentStatus::PAID:
                    $content .= "\nTrạng thái thanh toán: Đã thanh toán.";
                    break;
                case PaymentStatus::UNPAID:
                    $content .= "\nTrạng thái thanh toán: Chưa thanh toán.";
                    break;
                case PaymentStatus::REFUND:
                    $content .= "\nTrạng thái thanh toán: Hoàn tiền.";
                    break;
            }
        }

        if (!empty($attributes['note'])) {
            $content .= "\nGhi chú: " . $attributes['note'];
        }

        if (!empty($content)) {
            return $title . '```' . $content . '```';
        }

        return '';
    }

    public static function getDiscordMsgTrackingOrderShipping($oldOrder, $attributes)
    {
        $shipper = Admin::find($attributes['shipper_id']);
        $transport_company = TransportCompany::find($attributes['transport_company_id']);

        $title = sprintf(
            "**[Đơn hàng *[#%s](%s)* có cập nhật mới]\nNgười giao hàng:  *[%s](%s)* %s @everyone**",
            $oldOrder['id'],
            route('admin.orders_index', ['id' => $oldOrder['id']]),
            $shipper->name,
            route('admin.users_index', ['id' => $shipper->id]),
            empty($shipper->discord_id) ? '' : "<@$shipper->discord_id>"
        );

        $content = "";
        if ($oldOrder['shipping_status'] !== $attributes['shipping_status']) {
            switch ($attributes['shipping_status']) {
                case ShippingStatus::WAIT_SHIPPER:
                    $content .= "\nTrạng thái giao hàng: Chờ shipper nhận.";
                    break;
                case ShippingStatus::SHIPPING:
                    $content .= "\nTrạng thái giao hàng: Đang vận chuyển.";
                    break;
                case ShippingStatus::SUCCESS:
                    $content .= "\nTrạng thái giao hàng: Thành công.";
                    break;
                case ShippingStatus::FAILED:
                    $content .= "\nTrạng thái giao hàng: Thất bại.";
                    break;
            }
        }

        if ($oldOrder['transport_company_id'] != $transport_company->id) {
            $content .= "\nĐơn vị vận chuyển: " . $transport_company->name;
        }

        if ($oldOrder['shipper_id'] != $shipper->id) {
            $content .= "\nNgười giao hàng: " . $shipper->name;
        }

        if (!empty($attributes['note'])) {
            $content .= "\nGhi chú: " . $attributes['note'];
        }

        if (!empty($content)) {
            return $title . '```' . $content . '```';
        }

        return '';
    }

    public static function getDiscordMsgTrackingExportInventoryOfOrder($oldOrder, $newOrder, $note)
    {
        $orderId = $oldOrder['id'];
        $title = "**[Đơn hàng *[#$orderId](" . route('admin.orders_index', ['id' => $orderId]) . ")* có cập nhật mới]**";
        $content = "";

        if ($oldOrder['status'] !== $newOrder['status']) {
            $content .= "\nTrạng thái đơn hàng: Đang xử lý.";
        }

        $content .= "\nĐơn này đã xuất hàng.";

        if(!empty($note)) {
            $content .= "\nGhi chú: $note";
        }

        return $title . '```' . $content . '```';
    }
}

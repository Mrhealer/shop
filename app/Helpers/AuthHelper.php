<?php

use App\Enums\AdminPermissions;
use App\Models\Permission;
use App\Models\Role;

if (! function_exists('current_guard')) {
	function current_guard(){
		if (auth('admin')->check()) {
			return "admin";
		}

		if (auth('user')->check()) {
			return "user";
		}

		return 'guest';
	}
}

if (! function_exists('uploader')) {
	function uploader() {

		if (auth('admin')->check()) {
			return auth('admin')->user();
		}

		if (auth('user')->check()) {
			return auth('user')->user();
		}

		return null;
	}
}

if (! function_exists('admin')) {
	function admin() {
		if(auth('admin')->check()) {
			return auth('admin')->user()->load('roles');
		}

		return null;
	}
}

if (! function_exists('is_system_admin')) {
	function is_system_admin($admin) {
		foreach ($admin->roles as $role) {
			if($role->id === (int)config('auth.system_role_id')) {
				return true;
			}
		}
		return false;
	}
}

if (! function_exists('user')) {
	function user(array $relation = []) {

		if(auth('user')->check()) {
			if(empty($relation)) {
				$relation = 'roles';
			}
			return auth('user')->user()->load($relation);
		}

		return null;
	}
}

if (! function_exists('binding_enum')) {
	function binding_enum($enum, $langKey) {
		$data = [];

		foreach ($enum as $item) {
			$data[] = [
				'name' => __('enum.'.$langKey.'.'.strtolower($item->getKey())),
				'value' => $item->getValue(),
			];
		}

		return $data;
	}
}

if (! function_exists('binding_collection')) {
	function binding_collection($collection) {
		$data = [];

		foreach ($collection as $item) {
			$data[] = [
				'name' =>  $item->name,
				'value' => $item->id,
			];
		}

		return $data;
	}
}

if (! function_exists('binding_array')) {
	function binding_array($items) {
		$data = [];

		foreach ($items as $item) {
			$data[] = $item->id;
		}

		return $data;
	}
}

if (! function_exists('admin_avaiable_roles')) {
	function admin_avaiable_roles($admin = null, $roles = null) {
		if(empty($admin)) {
			$admin = admin();
		}
		if(empty($roles)) {
			$roles = Role::whereGuard('admin')->get();
		}

		$data = [];
		$highestLevel = null;

		foreach($admin->roles as $role) {

			if($highestLevel === null) {
				$highestLevel = $role->level;
				continue;
			}

			if($role->level < $highestLevel) {
				$highestLevel = $role->level;
			}
		}

		foreach($roles as $role) {

			if($role->level >= $highestLevel) {
				$data[] = $role->load('permissions');
			}
		}

		return $data;
	}
}

if (! function_exists('admin_avaiable_permissions')) {
	function admin_avaiable_permissions($admin = null) {
		if(empty($admin)) {
			$admin = admin()->load('roles.permissions');
		}
		$data = [];
		foreach ($admin->roles as $role) {
			foreach ($role->permissions as $permission) {
				$data[] = $permission->key;
			}
		}
		return $data;
	}
}

if(! function_exists('get_admins_via_permission')){
	function get_admins_via_permission(array $permissionKey){
		$admins = [];
		$permissions = Permission::with('roles.admins')->whereIn('key', $permissionKey)->get();
		foreach ($permissions as $permission) {
			foreach ($permission->roles as $role) {
				$admins = array_unique(Arr::prepend($admins, $role->admins));
			}
		}
		return Arr::collapse($admins);
	}
}

<?php
	return [
		'sale_chanel' => env('DISCORD_SALE_WEBHOOK'),
		'shipping_chanel' => env('DISCORD_SHIPPING_WEBHOOK'),
		'warranty_chanel' => env('DISCORD_WARRANTY_WEBHOOK'),
	];
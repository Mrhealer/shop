<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('type');
            $table->string('code', 50)->unique();
            $table->unsignedBigInteger('product_id')->nullable();
            $table->unsignedBigInteger('prod_category_id')->nullable();
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('limit');
            $table->unsignedBigInteger('number_of_uses')->default(0);
            $table->unsignedBigInteger('value');
            $table->unsignedBigInteger('min_money_required')->nullable();
            $table->unsignedBigInteger('max_money_discount')->nullable();
            $table->boolean('use_percent_discount')->default(false);
            $table->boolean('active')->default(true);
            $table->dateTime('expired_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HotfixDatabaseForPermissions extends Migration
{
	public function up()
	{
		Schema::table('inventories', function (Blueprint $table) {
			$table->renameColumn('import_user_id', 'import_admin_id');
			$table->renameColumn('export_user_id', 'export_admin_id');
		});
	}

	public function down()
	{
		Schema::table('inventories', function (Blueprint $table) {
			$table->renameColumn('import_admin_id', 'import_user_id');
			$table->renameColumn('export_admin_id', 'export_user_id');
		});
	}
}
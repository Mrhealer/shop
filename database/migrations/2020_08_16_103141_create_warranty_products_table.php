<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarrantyProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warranty_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('warranty_id');
	        $table->unsignedBigInteger('product_id');
	        $table->unsignedBigInteger('inventory_id')->nullable();
	        $table->unsignedBigInteger('tech_id')->nullable();
            $table->string('serial');
	        $table->string('new_serial')->nullable();
	        $table->boolean('is_refund_new')->default(false);
	        $table->string('descriptions')->nullable();
	        $table->string('before')->nullable();
	        $table->string('solution')->nullable();
	        $table->string('after')->nullable();
	        $table->integer('cost')->default(0);
	        $table->timestamp('return_at')->nullable();
	        $table->timestamp('expect_return_at')->nullable();
	        $table->tinyInteger('status')->default(\App\Enums\WarrantyStatus::WAIT_HANDLE);
            $table->timestamps();

            //foreign key

	        $table->foreign('warranty_id')
		        ->references('id')
		        ->on('warranties')
		        ->onDelete('cascade');

	        $table->foreign('product_id')
		        ->references('id')
		        ->on('products')
		        ->onDelete('cascade');

	        $table->foreign('inventory_id')
		        ->references('id')
		        ->on('products')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warranty_products');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            //basic info
            $table->string('logo');
            $table->string('name');
            $table->string('address');
            $table->string('phone', 15);
            $table->string('hotline', 15);
            $table->string('email', 50);
            $table->decimal('order_rate_point')->default(0);
            $table->string('main_product_title');

            // seo
            $table->string('title');
            $table->string('keyword');
            $table->string('description');
            $table->string('thumbnail');

            // banner
            $table->json('slider')->nullable();
            $table->string('banner_top')->nullable();
            $table->string('banner_top_link')->nullable();
            $table->string('banner_mid')->nullable();
            $table->string('banner_mid_link')->nullable();
            $table->string('banner_bot')->nullable();
            $table->string('banner_bot_link')->nullable();

            // 3th party
            $table->string('gg_analytic_id')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}

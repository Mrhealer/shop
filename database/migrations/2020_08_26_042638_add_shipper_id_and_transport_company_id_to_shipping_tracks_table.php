<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShipperIdAndTransportCompanyIdToShippingTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_tracks', function (Blueprint $table) {
            $table->unsignedBigInteger('shipper_id')->nullable()->after('user_id');
            $table->unsignedBigInteger('transport_company_id')->nullable()->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_tracks', function (Blueprint $table) {
            $table->dropColumn(['shipper_id', 'transport_company_id']);
        });
    }
}

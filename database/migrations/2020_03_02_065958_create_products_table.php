<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prod_category_id')->nullable();
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->string('name')->nullable();
            $table->string('slug')->unique()->nullable();
            $table->string('sku')->unique();
            $table->unsignedBigInteger('price')->nullable();
            $table->unsignedBigInteger('listed_price')->nullable();
            $table->boolean('show_percent_discount')->default(false);
            $table->boolean('is_hot')->default(false);
            $table->json('images')->nullable();
            $table->text('description')->nullable();
	        $table->text('short_description')->nullable();
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();

            //$table->foreign('prod_category_id')->references('id')->on('prod_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

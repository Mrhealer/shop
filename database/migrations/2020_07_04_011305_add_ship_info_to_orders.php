<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShipInfoToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
	        $table->tinyInteger('payment_status')->default(\App\Enums\PaymentStatus::UNPAID);
	        $table->string('shipper_name')->nullable();
	        $table->string('shipper_phone')->nullable();
	        $table->string('track_code')->nullable();
	        $table->string('transport')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('payment_status', 'shipper_name', 'shipper_phone', 'track_code', 'transport');
        });
    }
}

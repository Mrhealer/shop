<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarrantyHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warranty_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('warranty_id');
	        $table->tinyInteger('old_status')->nullable();
	        $table->string('action')->nullable();
            $table->timestamps();

	        $table->foreign('warranty_id')
		        ->references('id')
		        ->on('warranties')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warranty_histories');
    }
}

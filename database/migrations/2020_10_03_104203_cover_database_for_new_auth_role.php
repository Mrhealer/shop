<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CoverDatabaseForNewAuthRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('users', function (Blueprint $table) {
		    $table->dropColumn('role');
		    $table->string('username')->after('email')->nullable()->unique();
	    });

	    Schema::table('user_verifications', function (Blueprint $table) {
		    $table->unsignedBigInteger('admin_id')->nullable();
		    $table->unsignedBigInteger('user_id')->nullable()->change();
	    });

	    Schema::table('ticket_messages', function (Blueprint $table) {
		    $table->unsignedBigInteger('admin_id')->nullable();
		    $table->unsignedBigInteger('user_id')->nullable()->change();
	    });

	    Schema::table('shipping_tracks', function (Blueprint $table) {
		    $table->renameColumn('user_id', 'admin_id');
	    });

	    Schema::table('order_histories', function (Blueprint $table) {
		    $table->renameColumn('user_id', 'admin_id');
	    });

	    Schema::table('orders', function (Blueprint $table) {
		    $table->unsignedBigInteger('admin_id')->nullable();
	    });

	    Schema::table('warranties', function (Blueprint $table) {
		    $table->unsignedBigInteger('admin_id')->nullable();
	    });

	    Schema::table('tickets', function (Blueprint $table) {
		    $table->unsignedBigInteger('admin_id')->nullable();
	    });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('tickets', function (Blueprint $table) {
		    $table->dropColumn('admin_id');
	    });

	    Schema::table('warranties', function (Blueprint $table) {
		    $table->dropColumn('admin_id');
	    });

	    Schema::table('orders', function (Blueprint $table) {
		    $table->dropColumn('admin_id');
	    });

	    Schema::table('order_histories', function (Blueprint $table) {
		    $table->renameColumn('admin_id', 'user_id');
	    });

	    Schema::table('shipping_tracks', function (Blueprint $table) {
		    $table->renameColumn('admin_id', 'user_id');
	    });

	    Schema::table('user_verifications', function (Blueprint $table) {
		    $table->dropColumn('admin_id');
		    $table->unsignedBigInteger('user_id')->change();
	    });

	    Schema::table('ticket_messages', function (Blueprint $table) {
		    $table->dropColumn('admin_id');
		    $table->unsignedBigInteger('user_id')->change();
	    });

	    Schema::table('users', function (Blueprint $table) {
		    $table->integer('role');
		    $table->dropColumn('username');
	    });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarrantiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

	    Schema::dropIfExists('inventory_warranties');

        Schema::create('warranties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer_name');
            $table->string('customer_phone');
            $table->string('customer_email');
            $table->string('customer_address');
            $table->text('descriptions')->nullable();
            $table->text('images')->nullable();
            $table->tinyInteger('evaluation')->default(0);
            $table->date('refund_date')->nullable();
            $table->tinyInteger('status')->default(\App\Enums\WarrantyStatus::WAIT_HANDLE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warranties');
    }
}

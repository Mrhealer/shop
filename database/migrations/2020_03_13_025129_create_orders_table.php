<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedSmallInteger('user_id')->nullable();
            $table->integer('payment_method');
            $table->integer('received_method');

            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->string('province');
            $table->string('province_name');
            $table->string('district')->nullable();
            $table->string('district_name')->nullable();
            $table->string('address')->nullable();
            $table->string('note')->nullable();

            $table->string('showroom')->nullable();
            $table->datetime('received_time')->nullable();

            $table->boolean('has_bill');
            $table->string('company_name')->nullable();
            $table->string('company_address')->nullable();
            $table->string('company_tax_identification')->nullable();
            $table->string('bill_received_address')->nullable();

            $table->string('referral_code')->nullable();

            $table->string('voucher_code')->nullable();
            $table->unsignedBigInteger('voucher_discount_value')->nullable();


            $table->unsignedBigInteger('temp_money');
            $table->unsignedBigInteger('ship_money');
            $table->unsignedBigInteger('pay_money');

            $table->integer('status')->default(\App\Enums\OrderStatus::PENDING);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

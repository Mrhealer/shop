<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('has_serial')->default(true);
            $table->string('serial')->unique();
            $table->unsignedBigInteger('quantity')->default(1);
            $table->string('warehouse')->default(\App\Enums\Warehouse::HN);
            $table->unsignedBigInteger('product_id');
            $table->integer('warranty_period'); // months
            $table->date('brand_warranty_period')->nullable();;
            $table->string('note')->nullable();
            $table->unsignedBigInteger('order_id')->nullable();
            $table->dateTime('exported_at')->nullable();
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}

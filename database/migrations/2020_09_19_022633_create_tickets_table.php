<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
	        $table->integer('ticket_category_id')->unsigned();
	        $table->string('title');
	        $table->tinyInteger('star');
	        $table->tinyInteger('priority')->default(\App\Enums\TicketPriority::LOW);
	        $table->tinyInteger('status')->default(\App\Enums\TicketStatus::OPEN);
	        $table->timestamp('completed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyShippingInfoToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['shipper_name', 'shipper_phone', 'transport']);
            $table->unsignedBigInteger('shipper_id')->nullable();
            $table->unsignedBigInteger('transport_company_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['shipper_id', 'transport_company_id']);
            $table->string('shipper_name')->nullable();
            $table->string('shipper_phone')->nullable();
            $table->string('transport')->nullable();
        });
    }
}

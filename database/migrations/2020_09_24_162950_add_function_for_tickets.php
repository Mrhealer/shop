<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFunctionForTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('ticket_categories',function (Blueprint $table){
		    $table->integer('time_handle_default')->after('status');
		    $table->integer('time_receiver_default')->after('status');
		    $table->string('close_message_default')->nullable()->after('status');
	    });

	    Schema::table('tickets',function (Blueprint $table){
	    	$table->integer('time_handle')->after('star');
		    $table->dateTime('received_at')->nullable()->after('star');
		    $table->dateTime('completed_at')->nullable()->change();
		    $table->boolean('is_violated_complete')->default(false)->after('star');
		    $table->boolean('is_violated_receive')->default(false)->after('star');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('ticket_categories',function (Blueprint $table){
		    $table->dropColumn('time_handle_default');
		    $table->dropColumn('time_receiver_default');
		    $table->dropColumn('close_message_default');
	    });

	    Schema::table('tickets',function (Blueprint $table){
		    $table->dropColumn('time_handle');
		    $table->dropColumn('received_at');
		    $table->dropColumn('is_violated_complete');
		    $table->dropColumn('is_violated_receive');
	    });
    }
}

<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i < 200; $i++) {
                $product = new \App\Models\Product([
                    'prod_category_id' => rand(1, 50),
                    'brand_id'         => rand(1, 10),
                    'name'             => 'Laptop Lenovo 1810056'.$i,
                    'slug'              => 'product-'.$i,
                    'sku'              => '1810056'.$i,
                    'price'            => rand(1000000, 50000000),
                    'listed_price'     => 50000000,
                    'description'      => 'Đánh giá sản phẩm Laptop Lenovo Legion Y530-81 FV00SUVN
Dòng sản phẩm Laptop Legion đến từ Lenovo được xem như 1 thiết bị Laptop chuyên dùng cho giới game thủ đến từ tính năng đến thiết kế. Mang kiểu dáng thiết kế hiện hiện đại nổi bật lên dàn đèn Led tinh tế. Ngoài ra sức mạnh mà Legion Y530 mang lại trải nghiệm tối đa cho người dùng bằng các linh kiện sở hữu công suất cao.',
                    'images'           => json_encode([
                        '/images/sp'.rand(1,3).'.jpg',
                        '/images/sp'.rand(1,3).'.jpg',
                        '/images/sp'.rand(1,3).'.jpg',
                    ]),
                ]);
                $product->save();

        }
    }
}

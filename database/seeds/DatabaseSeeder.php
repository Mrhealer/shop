<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(SettingTableSeeder::class);
        $this->call(ProdCategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(BrandsTableSeeder::class);
        $this->call(BlogCategoriesTableSeeder::class);
        $this->call(PostTableSeeder::class);
        $this->call(AttributesTableSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new \App\Models\Setting([
            'logo'        => '/images/logo.png',
            'name'        => 'KOW Computer',
            'address'     => 'Hà Nội, Việt Nam',
            'phone'       => '0436688xxx',
            'hotline'     => '0986688xxx',
            'email'       => 'contact@kowcyber.example',
            'title'       => 'KOWCyber - The biggest cyber system in Viet Nam.',
            'keyword'     => 'kowcyber, pc, gaming, gear',
            'description' => 'Setup, training, provide gaming cyber system.',
            'thumbnail'   => '/images/banner_sli.jpg',
            'main_product_title'   => 'Sản phẩm tiêu biểu',
            'banner_top'  => '/images/banner_sli.jpg',
            'banner_top_link'  => 'https://example.com',
            'banner_mid'  => '/images/banner_sli.jpg',
            'banner_mid_link' => 'https://example.com',
            'banner_bot'  => '/images/banner_sli.jpg',
            'banner_bot_link' => 'https://example.com',
            'slider'      => json_encode([
                [
                    'name'   => 'Trùm sale - Siêu tiết kiệm',
                    'slides' => [
                       [
                           'image' => '/images/slider.jpg',
                           'link' => 'https://example.com'
                       ],
                        [
                            'image' => '/images/slider.jpg',
                            'link' => 'https://example.com'
                        ]
                    ]
                ]
            ])
        ]);
        $setting->save();
    }
}

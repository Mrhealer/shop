<?php

use App\Enums\AdminPermissions;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	DB::beginTransaction();
    	try{
		    foreach (AdminPermissions::values() as $permission) {
			    \App\Models\Permission::updateOrCreate([
				    'key' => $permission->getValue(),
			    ], [
				    'key' => $permission->getValue(),
				    'guard' => 'admin',
				    'name' => __('enum.admin_permissions.'.strtolower($permission->getKey())),
			    ]);
		    }
		    DB::commit();
	    }catch (\Exception $e){
    		DB::rollBack();
	    }



    }
}

<?php

use App\Enums\UserRole;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            [
                'name' => 'System Admin',
                'phone' => '0999999999',
                'email' => 'system@example.com',
                'image' => '/images/avatar.jpg',
                'role'  => UserRole::SYSTEM,
                'email_verified_at' => now(),
                'password' => bcrypt('password'),
                'remember_token' => Str::random(10),
            ],
            [
                'name' => 'Super Admin',
                'phone' => '0988888888',
                'email' => 'admin@example.com',
                'image' => '/images/avatar.jpg',
                'role' => UserRole::SUPER,
                'email_verified_at' => now(),
                'password' => bcrypt('password'),
                'remember_token' => Str::random(10),
            ],
            [
                'name' => 'Normal User',
                'phone' => '0977777777',
                'email' => 'user@example.com',
                'image' => '/images/avatar.jpg',
                'role' => UserRole::NORMAL,
                'email_verified_at' => now(),
                'password' => bcrypt('password'),
                'remember_token' => Str::random(10),
            ]
        ]);
    }
}

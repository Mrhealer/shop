<?php

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i < 11; $i++) {

            $post = new Post([
                'blog_category_id' => 11,
                'name' => 'Thêm 1 ca mắc Covid-19, được cách ly ngay khi nhập cảnh',
                'slug' => \App\Helpers\SlugHelper::genSlug('Thêm 1 ca mắc Covid-19, được cách ly ngay khi nhập cảnh'.$i),
                'description' => 'Ca bệnh 241 là bệnh nhân nam, quốc tịch Việt Nam, 20 tuổi, có địa chỉ tại phường Thảo Điền, Quận 2, TP. Hồ Chí Minh, là du học sinh tại Anh.

Ngày 21/3, bệnh nhân từ London (Vương quốc Anh) lên chuyến bay của Vietnam Airlines số hiệu VN0050, số ghế 5A, về tới Sân bay Cần Thơ ngày 22/3...',
                'image' => '/images/slider.jpg',
                'content' => 'Ca bệnh 241 là bệnh nhân nam, quốc tịch Việt Nam, 20 tuổi, có địa chỉ tại phường Thảo Điền, Quận 2, TP. Hồ Chí Minh, là du học sinh tại Anh.

Ngày 21/3, bệnh nhân từ London (Vương quốc Anh) lên chuyến bay của Vietnam Airlines số hiệu VN0050, số ghế 5A, về tới Sân bay Cần Thơ ngày 22/3.

Khi nhập cảnh, bệnh nhân chưa có dấu hiệu triệu chứng bệnh và được chuyển tới khu cách ly tập trung tại Trường Quân sự tỉnh Bạc Liêu.

Bệnh nhân được lấy mẫu xét nghiệm lần 1 ngày 25/3 cho kết quả âm tính với SARS-CoV-2.

Ngày 31/3, bệnh nhân có triệu chứng sốt kèm đau họng. Ngày 01/4, bệnh nhân được chuyển đến Bệnh viện Đa khoa tỉnh Bạc Liêu để cách ly, điều trị và lấy mẫu bệnh phẩm. Xét nghiệm tại Viện Pasteur TP Hồ Chí Minh cho kết quả dương tính với SARS-COV2.

Những người ở cùng phòng với bệnh nhân tại khu cách ly tập trung chưa ghi nhận triệu chứng bệnh, được cách ly riêng để theo dõi trong 14 ngày tiếp theo.'
            ]);
            $post->save();

        }
    }
}

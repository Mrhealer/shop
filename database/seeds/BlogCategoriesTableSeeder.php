<?php

use App\Helpers\SlugHelper;
use App\Models\BlogCategory;
use Illuminate\Database\Seeder;

class BlogCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [];
        for($i = 1; $i < 5; $i++) {
            $categories[] = [
                'id' => $i,
                'name' => 'Danh muc tin '.$i,
                'slug' => SlugHelper::genSlug('Danh muc tin '.$i),
                'parent_id' => null,
            ];
            for($j = 1; $j < 5; $j++) {
                $categories[] = [
                    'id' => $i.$j,
                    'name' => 'Danh muc tin '.$i.$j,
                    'slug' => SlugHelper::genSlug('Danh muc tin '.$i.$j),
                    'parent_id' => $i,
                ];
            }
        }
        foreach ($categories as $category) {
            $item = new BlogCategory($category);
            $item->save();
        }
    }
}

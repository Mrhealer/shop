<?php

use Illuminate\Database\Seeder;

class AttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 10; $i++) {
            $attribute = new \App\Models\Attribute();
            $attribute->name = 'Thuộc tính '.$i;
            $attribute->group_type = \App\Enums\AttributeGroup::GENERAL;
            $attribute->multi_values = true;
            $attribute->save();
            $attribute->categories()->sync(range(rand(1, 5), rand(6,10), 1));
        }

        for($i = 1; $i <= 50; $i++) {
            $attributeValue = new \App\Models\AttributeValue([
                'attribute_id' => rand(1, 10),
                'value' => 'Giá trị '.rand(1, 9999),
            ]);
            $attributeValue->save();
        }
    }
}

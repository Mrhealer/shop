<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 10; $i++) {
            $brand = new \App\Models\Brand([
                'name' => 'Brand '.$i,
                'url' => 'example.com',
                'description' => 'Đánh giá sản phẩm Laptop Lenovo Legion Y530-81 FV00SUVN
Dòng sản phẩm Laptop Legion đến từ Lenovo được xem như 1 thiết bị Laptop chuyên dùng cho giới game thủ đến từ tính năng đến thiết kế. Mang kiểu dáng thiết kế hiện hiện đại nổi bật lên dàn đèn Led tinh tế. Ngoài ra sức mạnh mà Legion Y530 mang lại trải nghiệm tối đa cho người dùng bằng các linh kiện sở hữu công suất cao.',
                'image' => 'https://img1.phongvu.vn/media/catalog/product/2/_/2_40_36.jpg',
            ]);
            $brand->save();
            $brand->categories()->sync(range(1, 10));
        }
    }
}

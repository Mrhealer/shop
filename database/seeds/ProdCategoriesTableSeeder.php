<?php

use App\Helpers\SlugHelper;
use App\Models\ProdCategory;
use Illuminate\Database\Seeder;

class ProdCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [];
        for($i = 1; $i < 10; $i++) {
            $categories[] = [
                'name' => 'Danh mục sản phẩm '.$i,
                'slug' => SlugHelper::genSlug('Danh mục sản phẩm '.$i),
                'parent_id' => null,
                'icon' => '/images/star.png',
                'banner' => '/images/category-banner.jpeg',
                'slider' => json_encode(['/images/category-slider.jpeg']),
            ];

        }
        for($i = 10; $i < 20; $i++) {
            $categories[] = [
                'name' => 'Danh mục sản phẩm '.$i,
                'slug' => SlugHelper::genSlug('Danh mục sản phẩm '.$i),
                'parent_id' =>  rand(1, 9),
                'icon' => '/images/star.png',
                'banner' => '/images/category-banner.jpeg',
                'slider' => json_encode(['/images/category-slider.jpeg']),
            ];
        }
        for($i = 20; $i <= 50; $i++) {
            $categories[] = [
                'name' => 'Danh mục sản phẩm '.$i,
                'slug' => SlugHelper::genSlug('Danh mục sản phẩm '.$i),
                'parent_id' => rand(10, 19),
                'icon' => '/images/star.png',
                'banner' => '/images/category-banner.jpeg',
                'slider' => json_encode(['/images/category-slider.jpeg']),
            ];
        }
        foreach ($categories as $category) {
            $item = new ProdCategory($category);
            $item->save();
        }
    }
}
